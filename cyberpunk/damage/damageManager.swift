
public class DamageManager extends IScriptable {

  public final static func ModifyHitData(hitEvent: ref<gameHitEvent>) -> Void {
    let chargeVal: Float;
    let attackData: ref<AttackData> = hitEvent.attackData;
    let statusEffectsSystem: ref<StatusEffectSystem> = GameInstance.GetStatusEffectSystem(hitEvent.target.GetGame());
    let statPoolSystem: ref<StatPoolsSystem> = GameInstance.GetStatPoolsSystem(hitEvent.target.GetGame());
    let ownerStatsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(attackData.GetInstigator().GetGame());
    let attackType: gamedataAttackType = attackData.GetAttackType();
    if !IsDefined(DamageManager.GetScriptedPuppetTarget(hitEvent)) {
      LogAI("No scripted puppet has been found /!\\");
      return;
    };
    if ArraySize(hitEvent.hitRepresentationResult.hitShapes) > 0 && DamageSystemHelper.IsProtectionLayer(DamageSystemHelper.GetHitShape(hitEvent)) {
      attackData.AddFlag(hitFlag.DamageNullified, n"ProtectionLayer");
    };
    if IsDefined(attackData.GetWeapon()) && GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(attackData.GetWeapon().GetEntityID()), gamedataStatType.CanSilentKill) > 0.00 {
      attackData.AddFlag(hitFlag.SilentKillModifier, n"CanSilentKill");
    };
    if attackData.GetInstigator().IsPlayer() {
      if !AttackData.IsPlayerInCombat(attackData) {
        attackData.AddFlag(hitFlag.StealthHit, n"Player attacked from out of combat");
      };
    };
    if AttackData.IsBullet(attackType) || AttackData.IsMelee(attackType) || AttackData.IsStrongMelee(attackType) {
      if ownerStatsSystem.GetStatValue(Cast<StatsObjectID>(attackData.GetInstigator().GetEntityID()), gamedataStatType.CanForceDismbember) > 0.00 {
        attackData.AddFlag(hitFlag.ForceDismember, n"CanForceDismbember ability");
      };
      if !hitEvent.target.IsPlayer() && ownerStatsSystem.GetStatValue(Cast<StatsObjectID>(attackData.GetInstigator().GetEntityID()), gamedataStatType.CanInstaKillNPCs) > 0.00 {
        attackData.AddFlag(hitFlag.Kill, n"CanInstaKillNPCs ability");
      };
    };
    if statusEffectsSystem.HasStatusEffect(hitEvent.target.GetEntityID(), t"BaseStatusEffect.Defeated") {
      attackData.AddFlag(hitFlag.Defeated, n"Defeated");
    };
    if statPoolSystem.HasActiveStatPool(Cast<StatsObjectID>(attackData.GetWeapon().GetEntityID()), gamedataStatPoolType.WeaponCharge) {
      chargeVal = statPoolSystem.GetStatPoolValue(Cast<StatsObjectID>(attackData.GetWeapon().GetEntityID()), gamedataStatPoolType.WeaponCharge, false);
      if Equals(RPGManager.GetWeaponEvolution(attackData.GetWeapon().GetItemID()), gamedataWeaponEvolution.Tech) && chargeVal >= 50.00 {
        attackData.AddFlag(hitFlag.WeaponFullyCharged, n"Charge Weapon");
      } else {
        if chargeVal >= 100.00 {
          attackData.AddFlag(hitFlag.WeaponFullyCharged, n"Charge Weapon");
        };
      };
    };
    if !AttackData.IsBullet(attackType) && !AttackData.IsExplosion(attackType) {
      DamageManager.ProcessDefensiveState(hitEvent);
    };
    return;
  }

  public final static func IsValidDirectionToDefendMeleeAttack(attackerForward: Vector4, defenderForward: Vector4) -> Bool {
    let finalHitDirection: Float = Vector4.GetAngleBetween(attackerForward, defenderForward);
    return finalHitDirection < 180.00;
  }

  private final static func ProcessDefensiveState(hitEvent: ref<gameHitEvent>) -> Void {
    let attackType: gamedataAttackType;
    let attackData: ref<AttackData> = hitEvent.attackData;
    let targetID: StatsObjectID = Cast<StatsObjectID>(hitEvent.target.GetEntityID());
    let targetPuppet: wref<ScriptedPuppet> = hitEvent.target as ScriptedPuppet;
    let aiComponent: ref<AIHumanComponent> = targetPuppet.GetAIControllerComponent();
    let attackSource: wref<GameObject> = attackData.GetSource();
    let hitAIEvent: ref<AIEvent> = new AIEvent();
    let statSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(hitEvent.target.GetGame());
    if AttackData.IsMelee(hitEvent.attackData.GetAttackType()) {
      if DamageManager.IsValidDirectionToDefendMeleeAttack(attackSource.GetWorldForward(), hitEvent.target.GetWorldForward()) {
        attackType = attackData.GetAttackType();
        if statSystem.GetStatValue(targetID, gamedataStatType.IsDeflecting) == 1.00 && NotEquals(attackType, gamedataAttackType.QuickMelee) && (targetPuppet.IsPlayer() || aiComponent.GetActionBlackboard().GetBool(GetAllBlackboardDefs().AIAction.attackParried)) {
          attackData.AddFlag(hitFlag.WasDeflected, n"Parry");
          attackData.AddFlag(hitFlag.DontShowDamageFloater, n"Parry");
          AnimationControllerComponent.PushEvent(attackSource, n"myAttackParried");
          hitAIEvent.name = n"MyAttackParried";
          attackSource.QueueEvent(hitAIEvent);
          if hitEvent.target.IsPlayer() {
            DamageManager.SendNameEventToPSM(n"successfulDeflect", hitEvent);
          };
        } else {
          if aiComponent.GetActionBlackboard().GetBool(GetAllBlackboardDefs().AIAction.attackBlocked) || targetPuppet.IsPlayer() && (statSystem.GetStatValue(targetID, gamedataStatType.IsBlocking) == 1.00 || statSystem.GetStatValue(targetID, gamedataStatType.IsDeflecting) == 1.00 && Equals(attackType, gamedataAttackType.QuickMelee)) {
            attackData.AddFlag(hitFlag.WasBlocked, n"Block");
            attackData.AddFlag(hitFlag.DontShowDamageFloater, n"Block");
            AnimationControllerComponent.PushEvent(attackSource, n"myAttackBlocked");
            ScriptedPuppet.SendActionSignal(attackSource as NPCPuppet, n"BlockSignal", 0.30);
            hitAIEvent.name = n"MyAttackBlocked";
            attackSource.QueueEvent(hitAIEvent);
            DamageManager.DealStaminaDamage(hitEvent, targetID, statSystem);
          } else {
            ScriptedPuppet.SendActionSignal(attackSource as NPCPuppet, n"HitSignal", 0.30);
            hitAIEvent.name = n"MyAttackHit";
            attackSource.QueueEvent(hitAIEvent);
          };
        };
      };
    } else {
      ScriptedPuppet.SendActionSignal(attackSource as NPCPuppet, n"HitSignal", 0.30);
      hitAIEvent.name = n"MyAttackHit";
      attackSource.QueueEvent(hitAIEvent);
    };
  }

  protected final static func SendNameEventToPSM(eventName: CName, hitEvent: ref<gameHitEvent>) -> Void {
    let player: ref<PlayerPuppet> = hitEvent.target as PlayerPuppet;
    let es: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(player.GetGame()).Get(n"EquipmentSystem") as EquipmentSystem;
    let playerWeapon: ref<ItemObject> = es.GetActiveWeaponObject(player, gamedataEquipmentArea.Weapon);
    let psmEvent: ref<PSMPostponedParameterBool> = new PSMPostponedParameterBool();
    psmEvent.id = eventName;
    psmEvent.value = true;
    player.QueueEvent(psmEvent);
    player.QueueEventForEntityID(playerWeapon.GetEntityID(), psmEvent);
  }

  public final static func PostProcess(hitEvent: ref<gameHitEvent>) -> Void;

  public final static func CalculateSourceModifiers(hitEvent: ref<gameHitEvent>) -> Void {
    let tempStat: Float;
    let targetPuppet: ref<ScriptedPuppet> = hitEvent.target as ScriptedPuppet;
    if ScriptedPuppet.IsMechanical(targetPuppet) || hitEvent.target.IsTurret() {
      tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.attackData.GetInstigator().GetEntityID()), gamedataStatType.BonusDamageAgainstMechanicals);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 + tempStat);
      };
    };
    if Equals(targetPuppet.GetNPCRarity(), gamedataNPCRarity.Boss) {
      tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.attackData.GetInstigator().GetEntityID()), gamedataStatType.BonusDamageAgainstBosses);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 + tempStat * 0.01);
      };
    };
    if hitEvent.attackData.GetInstigator().IsPlayer() {
      if Equals(targetPuppet.GetNPCRarity(), gamedataNPCRarity.Elite) {
        tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.attackData.GetInstigator().GetEntityID()), gamedataStatType.BonusDamageAgainstElites);
        if !FloatIsEqual(tempStat, 0.00) {
          hitEvent.attackComputed.MultAttackValue(1.00 + tempStat * 0.01);
        };
      } else {
        if Equals(targetPuppet.GetNPCRarity(), gamedataNPCRarity.Rare) {
          tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.attackData.GetInstigator().GetEntityID()), gamedataStatType.BonusDamageAgainstRares);
          if !FloatIsEqual(tempStat, 0.00) {
            hitEvent.attackComputed.MultAttackValue(1.00 + tempStat * 0.01);
          };
        };
      };
      if AttackData.IsMelee(hitEvent.attackData.GetAttackType()) && StatusEffectSystem.ObjectHasStatusEffect(hitEvent.attackData.GetInstigator(), t"BaseStatusEffect.BerserkPlayerBuff") {
        tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.attackData.GetInstigator().GetEntityID()), gamedataStatType.BerserkMeleeDamageBonus);
        if !FloatIsEqual(tempStat, 0.00) {
          hitEvent.attackComputed.MultAttackValue(1.00 + tempStat * 0.01);
        };
      };
    };
  }

  public final static func CalculateTargetModifiers(hitEvent: ref<gameHitEvent>) -> Void {
    let tempStat: Float;
    if AttackData.IsExplosion(hitEvent.attackData.GetAttackType()) {
      tempStat = tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.target.GetEntityID()), gamedataStatType.DamageReductionExplosion);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 - tempStat);
      };
    };
    if AttackData.IsDoT(hitEvent.attackData.GetAttackType()) {
      tempStat = tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.target.GetEntityID()), gamedataStatType.DamageReductionDamageOverTime);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 - tempStat);
      };
    };
    if AttackData.IsMelee(hitEvent.attackData.GetAttackType()) {
      tempStat = tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.target.GetEntityID()), gamedataStatType.DamageReductionMelee);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 - tempStat / 100.00);
      };
    };
    if AttackData.IsHack(hitEvent.attackData.GetAttackType()) {
      tempStat = tempStat = GameInstance.GetStatsSystem(hitEvent.target.GetGame()).GetStatValue(Cast<StatsObjectID>(hitEvent.target.GetEntityID()), gamedataStatType.DamageReductionQuickhacks);
      if !FloatIsEqual(tempStat, 0.00) {
        hitEvent.attackComputed.MultAttackValue(1.00 - tempStat / 100.00);
      };
    };
  }

  public final static func CalculateGlobalModifiers(hitEvent: ref<gameHitEvent>) -> Void;

  private final static func GetScriptedPuppetTarget(hitEvent: ref<gameHitEvent>) -> ref<ScriptedPuppet> {
    return hitEvent.target as ScriptedPuppet;
  }

  protected final static func DealStaminaDamage(hitEvent: ref<gameHitEvent>, targetID: StatsObjectID, statSystem: ref<StatsSystem>) -> Void {
    let weapon: ref<WeaponObject> = hitEvent.attackData.GetWeapon();
    let staminaDamageValue: Float = statSystem.GetStatValue(Cast<StatsObjectID>(weapon.GetEntityID()), gamedataStatType.StaminaDamage);
    GameInstance.GetStatPoolsSystem(hitEvent.target.GetGame()).RequestChangingStatPoolValue(targetID, gamedataStatPoolType.Stamina, -staminaDamageValue, hitEvent.attackData.GetInstigator(), false, false);
  }
}
