
public class OdaCementBagController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<OdaCementBagControllerPS> {
    return this.GetBasePS() as OdaCementBagControllerPS;
  }
}

public class OdaCementBagControllerPS extends ScriptableDeviceComponentPS {

  protected let m_cementEffectCooldown: Float;

  protected cb func OnInstantiated() -> Bool {
    super.OnInstantiated();
  }

  protected func Initialize() -> Void {
    this.Initialize();
  }

  public final func GetCementCooldown() -> Float {
    return this.m_cementEffectCooldown;
  }
}
