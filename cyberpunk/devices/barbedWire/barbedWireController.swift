
public class BarbedWireController extends ActivatedDeviceController {

  public const func GetPS() -> ref<BarbedWireControllerPS> {
    return this.GetBasePS() as BarbedWireControllerPS;
  }
}
