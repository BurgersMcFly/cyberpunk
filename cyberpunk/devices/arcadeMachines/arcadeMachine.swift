
public class ArcadeMachine extends InteractiveDevice {

  private let m_isShortGlitchActive: Bool;

  private let m_shortGlitchDelayID: DelayID;

  private let m_currentGame: ResRef;

  protected let m_currentGameAudio: CName;

  protected let m_currentGameAudioStop: CName;

  private let m_meshAppearanceOn: CName;

  private let m_meshAppearanceOff: CName;

  private let m_arcadeMinigameComponent: ref<WorkspotResourceComponent>;

  protected cb func OnRequestComponents(ri: EntityRequestComponentsInterface) -> Bool {
    EntityRequestComponentsInterface.RequestComponent(ri, n"ui", n"worlduiWidgetComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"arcadeMinigamePlayerWorkspot", n"workWorkspotResourceComponent", false);
    super.OnRequestComponents(ri);
  }

  protected cb func OnPersitentStateInitialized(evt: ref<GameAttachedEvent>) -> Bool {
    super.OnPersitentStateInitialized(evt);
    this.SetupMinigame();
  }

  public func ResavePersistentData(ps: ref<PersistentState>) -> Bool {
    return false;
  }

  protected cb func OnTakeControl(ri: EntityResolveComponentsInterface) -> Bool {
    this.m_uiComponent = EntityResolveComponentsInterface.GetComponent(ri, n"ui") as worlduiWidgetComponent;
    this.m_arcadeMinigameComponent = EntityResolveComponentsInterface.GetComponent(ri, n"arcadeMinigamePlayerWorkspot") as WorkspotResourceComponent;
    super.OnTakeControl(ri);
    this.m_controller = EntityResolveComponentsInterface.GetComponent(ri, n"controller") as ArcadeMachineController;
  }

  protected func ResolveGameplayState() -> Void {
    this.ResolveGameplayState();
    if this.IsUIdirty() && this.m_isInsideLogicArea {
      this.RefreshUI();
    };
  }

  protected func CreateBlackboard() -> Void {
    this.m_blackboard = IBlackboard.Create(GetAllBlackboardDefs().ArcadeMachineBlackBoard);
  }

  public const func GetBlackboardDef() -> ref<ArcadeMachineBlackboardDef> {
    return this.GetDevicePS().GetBlackboardDef();
  }

  protected const func GetController() -> ref<ArcadeMachineController> {
    return this.m_controller as ArcadeMachineController;
  }

  public const func GetDevicePS() -> ref<ArcadeMachineControllerPS> {
    return this.GetController().GetPS();
  }

  protected func StartGlitching(glitchState: EGlitchState, opt intensity: Float) -> Void {
    let evt: ref<AdvertGlitchEvent>;
    let glitchData: GlitchData;
    glitchData.state = glitchState;
    glitchData.intensity = intensity;
    if intensity == 0.00 {
      intensity = 1.00;
    };
    evt = new AdvertGlitchEvent();
    evt.SetShouldGlitch(intensity);
    this.QueueEvent(evt);
    this.GetBlackboard().SetVariant(this.GetBlackboardDef().GlitchData, ToVariant(glitchData), true);
    this.GetBlackboard().FireCallbacks();
    GameObjectEffectHelper.ActivateEffectAction(this, gamedataFxActionType.Start, n"hack_fx");
  }

  protected func StopGlitching() -> Void {
    let glitchData: GlitchData;
    let evt: ref<AdvertGlitchEvent> = new AdvertGlitchEvent();
    evt.SetShouldGlitch(0.00);
    this.QueueEvent(evt);
    glitchData.state = EGlitchState.NONE;
    this.GetBlackboard().SetVariant(this.GetBlackboardDef().GlitchData, ToVariant(glitchData));
    this.GetBlackboard().FireCallbacks();
    GameObjectEffectHelper.ActivateEffectAction(this, gamedataFxActionType.BreakLoop, n"hack_fx");
  }

  protected cb func OnHitEvent(hit: ref<gameHitEvent>) -> Bool {
    super.OnHitEvent(hit);
    this.StartShortGlitch();
  }

  private final func StartShortGlitch() -> Void {
    let evt: ref<StopShortGlitchEvent>;
    if this.GetDevicePS().IsGlitching() || this.GetDevicePS().IsDistracting() {
      return;
    };
    if !this.m_isShortGlitchActive {
      evt = new StopShortGlitchEvent();
      this.StartGlitching(EGlitchState.DEFAULT, 1.00);
      this.m_shortGlitchDelayID = GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, evt, 0.25);
      this.m_isShortGlitchActive = true;
    };
  }

  protected cb func OnStopShortGlitch(evt: ref<StopShortGlitchEvent>) -> Bool {
    this.m_isShortGlitchActive = false;
    if !this.GetDevicePS().IsGlitching() && !this.GetDevicePS().IsDistracting() {
      this.StopGlitching();
    };
  }

  protected func TurnOnDevice() -> Void {
    this.TurnOnDevice();
    this.TurnOnScreen();
  }

  protected func TurnOffDevice() -> Void {
    this.TurnOffDevice();
    this.TurnOffScreen();
  }

  protected func CutPower() -> Void {
    this.CutPower();
    this.TurnOffScreen();
  }

  protected func TurnOffScreen() -> Void {
    this.m_uiComponent.Toggle(false);
    GameObject.PlaySound(this, this.m_currentGameAudioStop);
    this.SetMeshAppearance(this.m_meshAppearanceOff);
  }

  protected func TurnOnScreen() -> Void {
    this.m_uiComponent.Toggle(true);
    GameObject.PlaySound(this, this.m_currentGameAudio);
    this.SetMeshAppearance(this.m_meshAppearanceOn);
  }

  protected cb func OnBeginArcadeMinigameUI(evt: ref<BeginArcadeMinigameUI>) -> Bool {
    let playerStateMachineBlackboard: ref<IBlackboard>;
    let workspotSystem: ref<WorkspotGameSystem>;
    let puppet: ref<GameObject> = evt.GetExecutor();
    if puppet.IsPlayer() {
      workspotSystem = GameInstance.GetWorkspotSystem(this.GetGame());
      if IsDefined(workspotSystem) {
        playerStateMachineBlackboard = GameInstance.GetBlackboardSystem(this.GetGame()).GetLocalInstanced(puppet.GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
        playerStateMachineBlackboard.SetBool(GetAllBlackboardDefs().PlayerStateMachine.IsInteractingWithDevice, true);
        workspotSystem.PlayInDevice(this, puppet, n"lockedCamera", n"arcadeMinigamePlayerWorkspot");
      };
    };
  }

  public const func DeterminGameplayRole() -> EGameplayRole {
    return EGameplayRole.Distract;
  }

  protected func ApplyActiveStatusEffect(target: EntityID, statusEffect: TweakDBID) -> Void {
    if this.IsActiveStatusEffectValid() && this.GetDevicePS().IsGlitching() {
      GameInstance.GetStatusEffectSystem(this.GetGame()).ApplyStatusEffect(target, statusEffect);
    };
  }

  protected func UploadActiveProgramOnNPC(targetID: EntityID) -> Void {
    let evt: ref<ExecutePuppetActionEvent>;
    if this.IsActiveProgramToUploadOnNPCValid() && this.GetDevicePS().IsGlitching() {
      evt = new ExecutePuppetActionEvent();
      evt.actionID = this.GetActiveProgramToUploadOnNPC();
      this.QueueEventForEntityID(targetID, evt);
    };
  }

  private final func SetupMinigame() -> Void {
    let randValue: Int32 = -1;
    let panzerMovie: ResRef = r"base\\movies\\misc\\arcade\\hishousai_panzer.bk2";
    let quadracerMovie: ResRef = r"base\\movies\\misc\\arcade\\quadracer.bk2";
    let retrosMovie: ResRef = r"base\\movies\\misc\\arcade\\retros.bk2";
    let roachraceMovie1: ResRef = r"base\\movies\\misc\\arcade\\roach_race.bk2";
    let roachraceMovie2: ResRef = r"base\\movies\\misc\\arcade\\roachrace.bk2";
    let minigame: ArcadeMinigame = ArcadeMinigame.INVALID;
    this.m_currentGame = this.GetDevicePS().GetGameVideoPath();
    if !ResRef.IsValid(this.m_currentGame) {
      randValue = RandRange(0, 6);
      if randValue == 0 {
        this.m_currentGame = panzerMovie;
      } else {
        if randValue == 1 {
          this.m_currentGame = quadracerMovie;
        } else {
          if randValue == 2 {
            this.m_currentGame = retrosMovie;
          } else {
            this.m_currentGame = roachraceMovie1;
          };
        };
      };
    };
    if Equals(this.m_currentGame, panzerMovie) {
      minigame = ArcadeMinigame.Panzer;
      this.m_currentGameAudio = n"mus_cp_arcade_panzer_START_menu";
      this.m_currentGameAudioStop = n"mus_cp_arcade_panzer_STOP";
      this.m_meshAppearanceOn = n"ap4";
      this.m_meshAppearanceOff = n"ap4_off";
    } else {
      if Equals(this.m_currentGame, quadracerMovie) {
        minigame = ArcadeMinigame.Quadracer;
        this.m_currentGameAudio = n"mus_cp_arcade_quadra_START_menu";
        this.m_currentGameAudioStop = n"mus_cp_arcade_quadra_STOP";
        this.m_meshAppearanceOn = n"ap1";
        this.m_meshAppearanceOff = n"ap1_off";
      } else {
        if Equals(this.m_currentGame, retrosMovie) {
          minigame = ArcadeMinigame.Retros;
          this.m_currentGameAudio = n"mus_cp_arcade_shooter_START_menu";
          this.m_currentGameAudioStop = n"mus_cp_arcade_shooter_STOP";
          this.m_meshAppearanceOn = n"ap3";
          this.m_meshAppearanceOff = n"ap3_off";
        } else {
          if Equals(this.m_currentGame, roachraceMovie1) || Equals(this.m_currentGame, roachraceMovie2) {
            minigame = ArcadeMinigame.RoachRace;
            this.m_currentGameAudio = n"mus_cp_arcade_roach_START_menu";
            this.m_currentGameAudioStop = n"mus_cp_arcade_roach_STOP";
            this.m_meshAppearanceOn = n"ap2";
            this.m_meshAppearanceOff = n"ap2_off";
          } else {
            minigame = ArcadeMinigame.INVALID;
            this.m_meshAppearanceOn = n"default";
            this.m_meshAppearanceOff = n"default";
          };
        };
      };
    };
    this.GetDevicePS().SetArcadeMinigame(minigame);
  }

  public final const func GetArcadeGame() -> ResRef {
    return this.m_currentGame;
  }

  public final const func GetArcadeGameAudio() -> CName {
    return this.m_currentGameAudio;
  }

  public final const func GetArcadeGameAudioStop() -> CName {
    return this.m_currentGameAudioStop;
  }
}
