
public class LaserDetectorController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<LaserDetectorControllerPS> {
    return this.GetBasePS() as LaserDetectorControllerPS;
  }
}

public class LaserDetectorControllerPS extends ScriptableDeviceComponentPS {

  protected func GetDeviceIconTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected func GetBackgroundTextureTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceBackground";
  }
}
