
public class DoorProximityDetectorController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<DoorProximityDetectorControllerPS> {
    return this.GetBasePS() as DoorProximityDetectorControllerPS;
  }
}

public class DoorProximityDetectorControllerPS extends ScriptableDeviceComponentPS {

  protected func OnSecuritySystemOutput(evt: ref<SecuritySystemOutput>) -> EntityNotificationType {
    this.OnSecuritySystemOutput(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public func GetQuestActionByName(actionName: CName) -> ref<DeviceAction> {
    let action: ref<DeviceAction> = this.GetQuestActionByName(actionName);
    if action == null {
      switch actionName {
        case n"QuestStartGlitch":
          action = this.ActionQuestStartGlitch();
          break;
        case n"QuestStopGlitch":
          action = this.ActionQuestStopGlitch();
      };
    };
    return action;
  }

  public func GetQuestActions(out outActions: array<ref<DeviceAction>>, context: GetActionsContext) -> Void {
    this.GetQuestActions(outActions, context);
    ArrayPush(outActions, this.ActionQuestStartGlitch());
    ArrayPush(outActions, this.ActionQuestStopGlitch());
  }

  protected func GetDeviceIconTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected func GetBackgroundTextureTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceBackground";
  }
}
