
public class ProximityDetectorController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<ProximityDetectorControllerPS> {
    return this.GetBasePS() as ProximityDetectorControllerPS;
  }
}

public class ProximityDetectorControllerPS extends ScriptableDeviceComponentPS {

  protected func PerformRestart() -> Void;

  protected func OnTargetAssessmentRequest(evt: ref<TargetAssessmentRequest>) -> EntityNotificationType {
    return EntityNotificationType.SendThisEventToEntity;
  }

  protected func GetDeviceIconTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceIcon";
  }

  protected func GetBackgroundTextureTweakDBID() -> TweakDBID {
    return t"DeviceIcons.SecuritySystemDeviceBackground";
  }
}
