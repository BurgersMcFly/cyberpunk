
public class CandleController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<CandleControllerPS> {
    return this.GetBasePS() as CandleControllerPS;
  }
}

public class CandleControllerPS extends ScriptableDeviceComponentPS {

  protected inline let m_candleSkillChecks: ref<EngDemoContainer>;

  protected cb func OnInstantiated() -> Bool {
    super.OnInstantiated();
  }

  protected func Initialize() -> Void {
    this.Initialize();
  }

  protected func GameAttached() -> Void;

  protected func LogicReady() -> Void {
    this.LogicReady();
    this.InitializeSkillChecks(this.m_candleSkillChecks, false);
  }

  public func GetActions(out actions: array<ref<DeviceAction>>, context: GetActionsContext) -> Bool {
    ArrayPush(actions, this.ActionToggleON());
    return true;
  }

  protected func GetBackgroundTextureTweakDBID() -> TweakDBID {
    return t"DeviceIcons.LightDeviceBackground";
  }

  protected func GetDeviceIconTweakDBID() -> TweakDBID {
    return t"DeviceIcons.LightDeviceIcon";
  }
}
