
public class CleaningMachineController extends BasicDistractionDeviceController {

  public const func GetPS() -> ref<CleaningMachineControllerPS> {
    return this.GetBasePS() as CleaningMachineControllerPS;
  }
}

public class CleaningMachineControllerPS extends BasicDistractionDeviceControllerPS {

  protected inline let m_cleaningMachineSkillChecks: ref<EngDemoContainer>;

  protected cb func OnInstantiated() -> Bool {
    super.OnInstantiated();
  }

  protected func Initialize() -> Void {
    this.Initialize();
  }

  protected func GameAttached() -> Void;

  protected func LogicReady() -> Void {
    this.LogicReady();
    this.InitializeSkillChecks(this.m_cleaningMachineSkillChecks, false);
  }
}
