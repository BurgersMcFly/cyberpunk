
public class HoloFeederController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<HoloFeederControllerPS> {
    return this.GetBasePS() as HoloFeederControllerPS;
  }
}

public class HoloFeederControllerPS extends ScriptableDeviceComponentPS {

  protected cb func OnInstantiated() -> Bool {
    super.OnInstantiated();
    if !IsStringValid(this.m_deviceName) {
      this.m_deviceName = "LocKey#95";
    };
  }

  protected func Initialize() -> Void {
    this.Initialize();
  }

  public func GetActions(out actions: array<ref<DeviceAction>>, context: GetActionsContext) -> Bool {
    this.GetActions(actions, context);
    if !this.IsUserAuthorized(context.processInitiatorObject.GetEntityID()) {
      return false;
    };
    if ToggleON.IsDefaultConditionMet(this, context) {
      ArrayPush(actions, this.ActionToggleON());
    };
    this.SetActionIllegality(actions, this.m_illegalActions.regularActions);
    return true;
  }
}
