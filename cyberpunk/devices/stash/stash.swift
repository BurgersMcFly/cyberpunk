
public class OpenStash extends ActionBool {

  public final func SetProperties() -> Void {
    this.actionName = n"OpenStash";
    this.prop = DeviceActionPropertyFunctions.SetUpProperty_Bool(this.actionName, true, n"LocKey#15799", n"LocKey#15799");
  }

  public final static func IsDefaultConditionMet(device: ref<ScriptableDeviceComponentPS>, context: GetActionsContext) -> Bool {
    if OpenStash.IsAvailable(device) && OpenStash.IsClearanceValid(context.clearance) {
      return true;
    };
    return false;
  }

  public final static func IsAvailable(device: ref<ScriptableDeviceComponentPS>) -> Bool {
    if device.IsUnpowered() || device.IsDisabled() {
      return false;
    };
    return true;
  }

  public final static func IsClearanceValid(clearance: ref<Clearance>) -> Bool {
    if Clearance.IsInRange(clearance, 2) {
      return true;
    };
    return false;
  }
}

public class Stash extends InteractiveDevice {

  public let m_inventory: ref<Inventory>;

  protected cb func OnRequestComponents(ri: EntityRequestComponentsInterface) -> Bool {
    EntityRequestComponentsInterface.RequestComponent(ri, n"inventory", n"Inventory", false);
    super.OnRequestComponents(ri);
  }

  protected cb func OnTakeControl(ri: EntityResolveComponentsInterface) -> Bool {
    this.m_inventory = EntityResolveComponentsInterface.GetComponent(ri, n"inventory") as Inventory;
    super.OnTakeControl(ri);
    this.m_controller = EntityResolveComponentsInterface.GetComponent(ri, n"controller") as StashController;
  }

  protected cb func OnInteractionActivated(evt: ref<InteractionActivationEvent>) -> Bool {
    let actorUpdateData: ref<HUDActorUpdateData>;
    super.OnInteractionActivated(evt);
    if Equals(evt.eventType, gameinteractionsEInteractionEventType.EIET_activate) {
      if Equals(evt.layerData.tag, n"LogicArea") {
        actorUpdateData = new HUDActorUpdateData();
        actorUpdateData.updateIsInIconForcedVisibilityRange = true;
        actorUpdateData.isInIconForcedVisibilityRangeValue = true;
        this.RequestHUDRefresh(actorUpdateData);
      };
    } else {
      if Equals(evt.layerData.tag, n"LogicArea") {
        actorUpdateData = new HUDActorUpdateData();
        actorUpdateData.updateIsInIconForcedVisibilityRange = true;
        actorUpdateData.isInIconForcedVisibilityRangeValue = false;
        this.RequestHUDRefresh(actorUpdateData);
      };
    };
  }

  public const func DeterminGameplayRoleMappinVisuaState(data: SDeviceMappinData) -> EMappinVisualState {
    if this.GetDevicePS().IsDisabled() {
      return EMappinVisualState.Inactive;
    };
    return EMappinVisualState.Default;
  }

  public const func GetDevicePS() -> ref<StashControllerPS> {
    return this.GetController().GetPS();
  }

  private const func GetController() -> ref<StashController> {
    return this.m_controller as StashController;
  }

  public const func DeterminGameplayRole() -> EGameplayRole {
    return EGameplayRole.PlayerStash;
  }

  protected cb func OnOpenStash(evt: ref<OpenStash>) -> Bool {
    let storageBB: ref<IBlackboard>;
    let storageData: ref<StorageUserData>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(this.GetGame());
    let player: ref<GameObject> = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject();
    if IsDefined(transactionSystem) && IsDefined(player) {
      Stash.ProcessStashRetroFixes(this);
      storageData = new StorageUserData();
      storageData.storageObject = this;
      storageBB = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().StorageBlackboard);
      if IsDefined(storageBB) {
        storageBB.SetVariant(GetAllBlackboardDefs().StorageBlackboard.StorageData, ToVariant(storageData), true);
      };
    };
  }

  public const func IsPlayerStash() -> Bool {
    return true;
  }

  public final static func ProcessStashRetroFixes(stashObj: ref<GameObject>) -> Void {
    let factVal: Int32;
    let game: GameInstance;
    if !IsDefined(stashObj) {
      return;
    };
    game = stashObj.GetGame();
    factVal = GetFact(game, n"ClothingModsRemovedStash");
    if factVal <= 0 && EnumInt(gameGameVersion.Current) >= 1500 {
      Stash.RemoveAllModsFromClothing(stashObj);
      SetFactValue(game, n"ClothingModsRemovedStash", 1);
    };
    factVal = GetFact(game, n"DLCPlayerStashItemsRevamp");
    if factVal <= 0 && EnumInt(gameGameVersion.Current) >= 1500 {
      Stash.InstallModsToRedesignedItems(stashObj);
      SetFactValue(game, n"DLCPlayerStashItemsRevamp", 1);
    };
    factVal = GetFact(game, n"CYBMETA1695");
    if factVal <= 0 {
      Stash.RemoveRedundantScopesFromAchillesRifles(stashObj);
      SetFactValue(game, n"CYBMETA1695", 1);
    };
    factVal = GetFact(game, n"BuckGradScopeStashFix");
    if factVal <= 0 && EnumInt(gameGameVersion.Current) >= 1600 {
      Stash.InstallModsToRedesignedItems(stashObj);
      SetFactValue(game, n"BuckGradScopeStashFix", 1);
    };
  }

  private final static func InstallModsToRedesignedItems(stashObj: ref<GameObject>) -> Void {
    let i: Int32;
    let itemData: ref<gameItemData>;
    let partItemID: ItemID;
    let storageItems: array<wref<gameItemData>>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(stashObj.GetGame());
    let itemModificationSystem: wref<ItemModificationSystem> = GameInstance.GetScriptableSystemsContainer(stashObj.GetGame()).Get(n"ItemModificationSystem") as ItemModificationSystem;
    transactionSystem.GetItemList(stashObj, storageItems);
    i = 0;
    while i < ArraySize(storageItems) {
      itemData = storageItems[i];
      if IsDefined(itemData) && itemData.HasTag(n"DLCStashItem") {
        partItemID = ItemID.FromTDBID(t"Items.DLCItemsQualityUpgradeMod");
        transactionSystem.GiveItem(stashObj, partItemID, 1);
        itemModificationSystem.QueueRequest(Stash.CreateInstallPartRequest(stashObj, itemData, partItemID));
      };
      if IsDefined(itemData) && itemData.HasTag(n"Buck_Grad") {
        partItemID = ItemID.FromTDBID(t"Items.Buck_scope");
        transactionSystem.GiveItem(stashObj, partItemID, 1);
        itemModificationSystem.QueueRequest(Stash.CreateInstallPartRequest(stashObj, itemData, partItemID));
      };
      i += 1;
    };
  }

  private final static func CreateInstallPartRequest(stashObj: ref<GameObject>, itemData: ref<gameItemData>, part: ItemID) -> ref<InstallItemPart> {
    let installPartRequest: ref<InstallItemPart>;
    let slotID: TweakDBID;
    switch itemData.GetItemType() {
      case gamedataItemType.Clo_InnerChest:
        slotID = t"AttachmentSlots.InnerChestFabricEnhancer4";
        break;
      case gamedataItemType.Clo_OuterChest:
        slotID = t"AttachmentSlots.OuterChestFabricEnhancer4";
        break;
      case gamedataItemType.Wea_Katana:
        slotID = t"AttachmentSlots.IconicMeleeWeaponMod1";
        break;
      case gamedataItemType.Wea_SniperRifle:
        slotID = t"AttachmentSlots.Scope";
        break;
      default:
    };
    installPartRequest = new InstallItemPart();
    installPartRequest.Set(stashObj, itemData.GetID(), part, slotID);
    return installPartRequest;
  }

  private final static func RemoveAllModsFromClothing(stashObj: ref<GameObject>) -> Void {
    let currentItem: ItemID;
    let i: Int32;
    let itemData: ref<gameItemData>;
    let j: Int32;
    let storageItems: array<wref<gameItemData>>;
    let usedSlots: array<TweakDBID>;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(stashObj.GetGame());
    let itemModificationSystem: wref<ItemModificationSystem> = GameInstance.GetScriptableSystemsContainer(stashObj.GetGame()).Get(n"ItemModificationSystem") as ItemModificationSystem;
    transactionSystem.GetItemList(stashObj, storageItems);
    i = 0;
    while i < ArraySize(storageItems) {
      itemData = storageItems[i];
      if IsDefined(itemData) {
        currentItem = itemData.GetID();
        if RPGManager.IsItemClothing(currentItem) {
          ArrayClear(usedSlots);
          transactionSystem.GetUsedSlotsOnItem(stashObj, currentItem, usedSlots);
          j = 0;
          while j < ArraySize(usedSlots) {
            itemModificationSystem.QueueRequest(Stash.CreateRemovePartRequest(stashObj, currentItem, usedSlots[j]));
            j += 1;
          };
        };
      };
      i += 1;
    };
  }

  private final static func RemoveRedundantScopesFromAchillesRifles(stashObj: ref<GameObject>) -> Void {
    let stashItems: array<wref<gameItemData>>;
    let ts: ref<TransactionSystem> = GameInstance.GetTransactionSystem(stashObj.GetGame());
    let ims: ref<ItemModificationSystem> = GameInstance.GetScriptableSystemsContainer(stashObj.GetGame()).Get(n"ItemModificationSystem") as ItemModificationSystem;
    if !IsDefined(ts) || !IsDefined(ims) {
      return;
    };
    ts.GetItemList(stashObj, stashItems);
    ims.RemoveRedundantScopesFromAchillesRifles(stashItems);
  }

  private final static func CreateRemovePartRequest(stashObj: ref<GameObject>, item: ItemID, slotID: TweakDBID) -> ref<RemoveItemPart> {
    let removePartRequest: ref<RemoveItemPart> = new RemoveItemPart();
    removePartRequest.Set(stashObj, item, slotID);
    return removePartRequest;
  }
}

public class StashController extends ScriptableDeviceComponent {

  public const func GetPS() -> ref<StashControllerPS> {
    return this.GetBasePS() as StashControllerPS;
  }
}

public class StashControllerPS extends ScriptableDeviceComponentPS {

  private final const func ActionOpenStash() -> ref<OpenStash> {
    let action: ref<OpenStash> = new OpenStash();
    action.clearanceLevel = 2;
    action.SetUp(this);
    action.SetProperties();
    action.AddDeviceName(this.m_deviceName);
    action.CreateInteraction();
    return action;
  }

  private final func OnOpenStash(evt: ref<OpenStash>) -> EntityNotificationType {
    this.UseNotifier(evt);
    return EntityNotificationType.SendThisEventToEntity;
  }

  public func GetActions(out outActions: array<ref<DeviceAction>>, context: GetActionsContext) -> Bool {
    if !this.GetActions(outActions, context) {
      return false;
    };
    if OpenStash.IsDefaultConditionMet(this, context) {
      ArrayPush(outActions, this.ActionOpenStash());
    };
    this.SetActionIllegality(outActions, this.m_illegalActions.regularActions);
    return true;
  }
}
