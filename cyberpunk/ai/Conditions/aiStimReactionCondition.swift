
public class CheckReaction extends AIbehaviorconditionScript {

  public edit let m_reactionToCompare: gamedataOutput;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>(Equals(AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetDesiredReactionName(), this.m_reactionToCompare));
  }
}

public class CheckReactionValueThreshold extends AIbehaviorconditionScript {

  public edit let m_reactionValue: EReactionValue;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let currentStat: Float;
    let threshold: Float;
    switch this.m_reactionValue {
      case EReactionValue.Fear:
        currentStat = AIBehaviorScriptBase.GetStatPoolValue(context, gamedataStatPoolType.Fear);
        threshold = TweakDBInterface.GetCharacterRecord(AIBehaviorScriptBase.GetPuppet(context).GetRecordID()).ReactionPreset().FearThreshold();
        break;
      default:
        return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if threshold == 0.00 {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if currentStat >= threshold - 0.01 {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class InvestigateController extends AIbehaviorconditionScript {

  protected let m_investigateData: stimInvestigateData;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    this.m_investigateData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData().stimInvestigateData;
    return Cast<AIbehaviorConditionOutcomes>(this.m_investigateData.investigateController);
  }
}

public class CheckReactionStimType extends AIbehaviorconditionScript {

  public edit let m_stimToCompare: gamedataStimType;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>(Equals(AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData().stimType, this.m_stimToCompare));
  }
}

public class CheckStimTag extends AIbehaviorconditionScript {

  public edit const let m_stimTagToCompare: array<CName>;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let i: Int32;
    let tags: array<CName>;
    let activeReactionData: ref<AIReactionData> = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData();
    if !IsDefined(activeReactionData) {
      activeReactionData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetDesiredReactionData();
    };
    tags = activeReactionData.stimRecord.Tags();
    i = 0;
    while i < ArraySize(this.m_stimTagToCompare) {
      if ArrayContains(tags, this.m_stimTagToCompare[i]) {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
      i += 1;
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class PlayInitFearAnimation extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let ownerPuppet: wref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    let activeReactionData: ref<AIReactionData> = ownerPuppet.GetStimReactionComponent().GetActiveReactionData();
    if !IsDefined(activeReactionData) {
      activeReactionData = ownerPuppet.GetStimReactionComponent().GetDesiredReactionData();
    };
    if activeReactionData.initAnimInWorkspot {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if activeReactionData.skipInitialAnimation {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if Equals(activeReactionData.stimType, gamedataStimType.GrenadeLanded) && RandRangeF(0.00, 1.00) < 0.75 {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if Equals(activeReactionData.reactionBehaviorName, gamedataOutput.DodgeToSide) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}

public class PlayStartupLocoFearAnimation extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let ownerPuppet: wref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    let reactionCmp: ref<ReactionManagerComponent> = ownerPuppet.GetStimReactionComponent();
    let activeReactionData: ref<AIReactionData> = reactionCmp.GetActiveReactionData();
    if !IsDefined(activeReactionData) {
      activeReactionData = reactionCmp.GetDesiredReactionData();
    };
    if activeReactionData.initAnimInWorkspot && reactionCmp.GetPreviousFearPhase() != 2 {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}

public class IsWorkspotReaction extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>((ScriptExecutionContext.GetOwner(context) as ScriptedPuppet).GetStimReactionComponent().GetWorkSpotReactionFlag());
  }
}

public class IsValidCombatTarget extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let activeReactionData: ref<AIReactionData>;
    if AIBehaviorScriptBase.GetPuppet(context).IsPrevention() {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    activeReactionData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData();
    if !IsDefined(activeReactionData) {
      activeReactionData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetDesiredReactionData();
    };
    if Equals(activeReactionData.stimType, gamedataStimType.Dying) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if Equals(activeReactionData.stimType, gamedataStimType.CombatHit) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}

public class IsPlayerAKiller extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let investigateData: stimInvestigateData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData().stimInvestigateData;
    let killer: ref<GameObject> = investigateData.attackInstigator as GameObject;
    if killer.IsPlayer() {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class CheckStimRevealsInstigatorPosition extends AIbehaviorconditionScript {

  public edit let m_checkStimType: Bool;

  public edit let m_stimType: gamedataStimType;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let investigateData: stimInvestigateData;
    let stimuliCache: array<ref<StimEventTaskData>> = (ScriptExecutionContext.GetOwner(context) as ScriptedPuppet).GetStimReactionComponent().GetStimuliCache();
    if ArraySize(stimuliCache) != 0 {
      investigateData = stimuliCache[ArraySize(stimuliCache) - 1].cachedEvt.stimInvestigateData;
      if investigateData.revealsInstigatorPosition {
        if !this.m_checkStimType || Equals(stimuliCache[ArraySize(stimuliCache) - 1].cachedEvt.GetStimType(), this.m_stimType) {
          return Cast<AIbehaviorConditionOutcomes>(true);
        };
      };
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class CheckLastTriggeredStimuli extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let stimSourcePos: Vector4;
    let stimuliCache: array<ref<StimEventTaskData>> = (ScriptExecutionContext.GetOwner(context) as ScriptedPuppet).GetStimReactionComponent().GetStimuliCache();
    if ArraySize(stimuliCache) != 0 {
      stimSourcePos = stimuliCache[ArraySize(stimuliCache) - 1].cachedEvt.sourcePosition;
      if !Vector4.IsZero(stimSourcePos) {
        ScriptExecutionContext.SetArgumentVector(context, n"StimSource", stimSourcePos);
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class CheckAnimSetTags extends AIbehaviorconditionScript {

  public edit const let m_animsetTagToCompare: array<CName>;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    if (ScriptExecutionContext.GetOwner(context) as ScriptedPuppet).HasRuntimeAnimsetTags(this.m_animsetTagToCompare) {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class HasPositionFarFromThreat extends AIbehaviorconditionScript {

  public edit let desiredDistance: Float;

  public edit let minDistance: Float;

  public edit let minPathLength: Float;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let currentPosition: Vector4;
    let destination: Vector4;
    let pathLength: Float;
    let pathResult: ref<NavigationPath>;
    let threatPosition: Vector4;
    let threat: ref<GameObject> = ScriptExecutionContext.GetArgumentObject(context, n"StimTarget");
    if threat == null {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    currentPosition = ScriptExecutionContext.GetOwner(context).GetWorldPosition();
    threatPosition = threat.GetWorldPosition();
    if !GameInstance.GetNavigationSystem(AIBehaviorScriptBase.GetGame(context)).FindNavmeshPointAwayFromReferencePoint(currentPosition, threatPosition, this.desiredDistance, NavGenAgentSize.Human, destination, 5.00, 90.00) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    if Vector4.LengthSquared(threatPosition - destination) < this.minDistance * this.minDistance {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    pathResult = GameInstance.GetAINavigationSystem(AIBehaviorScriptBase.GetGame(context)).CalculatePathForCharacter(currentPosition, destination, 1.00, ScriptExecutionContext.GetOwner(context));
    if IsDefined(pathResult) {
      pathLength = pathResult.CalculateLength();
    };
    if pathLength < this.minPathLength {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    ScriptExecutionContext.SetArgumentFloat(context, n"PathLength", pathLength);
    ScriptExecutionContext.SetArgumentVector(context, n"MovementDestination", destination);
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}

public class CanNPCRun extends AIbehaviorconditionScript {

  public edit let m_maxRunners: Int32;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let reactionSystem: ref<ScriptedReactionSystem> = GameInstance.GetScriptableSystemsContainer(ScriptExecutionContext.GetOwner(context).GetGame()).Get(n"ScriptedReactionSystem") as ScriptedReactionSystem;
    let runners: Int32 = reactionSystem.GetFleeingNPCsCount();
    let reactionData: ref<AIReactionData> = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData();
    if !IsDefined(reactionData) {
      reactionData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetDesiredReactionData();
    };
    if runners >= this.m_maxRunners && NotEquals(reactionData.stimType, gamedataStimType.HijackVehicle) {
      if reactionSystem.GetRegisterTimeout() > EngineTime.ToFloat(GameInstance.GetSimTime(ScriptExecutionContext.GetOwner(context).GetGame())) {
        runners = reactionSystem.GetFleeingNPCsCountInDistance(ScriptExecutionContext.GetOwner(context).GetWorldPosition(), 10.00);
        if runners >= this.m_maxRunners {
          return Cast<AIbehaviorConditionOutcomes>(false);
        };
      };
    };
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}

public class ShouldNPCContinueInAlerted extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let aiComponent: ref<AIHumanComponent>;
    let timestamp: Float;
    let puppet: ref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    let reactionComponent: ref<ReactionManagerComponent> = puppet.GetStimReactionComponent();
    let NPCIgnoreList: array<EntityID> = reactionComponent.GetIgnoreList();
    if puppet.IsConnectedToSecuritySystem() {
      if reactionComponent.IsAlertedByDeadBody() {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
      if puppet.GetSecuritySystem().IsReprimandOngoing() {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
      if ArraySize(NPCIgnoreList) != 0 {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
    };
    aiComponent = puppet.GetAIControllerComponent();
    if IsDefined(aiComponent) && Equals(aiComponent.GetAIRole().GetRoleEnum(), EAIRole.Patrol) && aiComponent.GetAIPatrolBlackboard().GetBool(GetAllBlackboardDefs().AIPatrol.forceAlerted) {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    timestamp = ScriptExecutionContext.GetArgumentFloat(context, n"SearchingStarted");
    if timestamp + 15.00 > EngineTime.ToFloat(GameInstance.GetSimTime(ScriptExecutionContext.GetOwner(context).GetGame())) {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class IsInTrafficLane extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>(AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().IsInTrafficLane());
  }
}

public class IsBlockedInTraffic extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>(AIBehaviorScriptBase.GetPuppet(context).GetCrowdMemberComponent().IsBlockedInTraffic());
  }
}

public class PreviousFearPhaseCheck extends AIbehaviorconditionScript {

  public edit let m_fearPhase: Int32;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    return Cast<AIbehaviorConditionOutcomes>(AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetPreviousFearPhase() == this.m_fearPhase);
  }
}

public class HearStimThreshold extends AIbehaviorconditionScript {

  public edit let m_thresholdNumber: Int32;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let curThresholdNumber: Int32;
    let stimTime: Float;
    let puppet: ref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    let reactionComponent: ref<ReactionManagerComponent> = puppet.GetStimReactionComponent();
    if !IsDefined(reactionComponent) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    stimTime = reactionComponent.GetCurrentStimTimeStamp();
    curThresholdNumber = reactionComponent.GetCurrentStimThresholdValue();
    if stimTime >= EngineTime.ToFloat(GameInstance.GetSimTime(ScriptExecutionContext.GetOwner(context).GetGame())) {
      if curThresholdNumber >= this.m_thresholdNumber {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class StealthStimThreshold extends AIbehaviorconditionScript {

  public edit let m_stealthThresholdNumber: Int32;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let curThresholdNumber: Int32;
    let stimTime: Float;
    let puppet: ref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    let reactionComponent: ref<ReactionManagerComponent> = puppet.GetStimReactionComponent();
    if !IsDefined(reactionComponent) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    stimTime = reactionComponent.GetCurrentStealthStimTimeStamp();
    curThresholdNumber = reactionComponent.GetCurrentStealthStimThresholdValue();
    if stimTime >= EngineTime.ToFloat(GameInstance.GetSimTime(ScriptExecutionContext.GetOwner(context).GetGame())) {
      if curThresholdNumber >= this.m_stealthThresholdNumber {
        return Cast<AIbehaviorConditionOutcomes>(true);
      };
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class CanDoReactionAction extends AIbehaviorconditionScript {

  public edit let m_reactionName: CName;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let reactionSystem: ref<ReactionSystem> = GameInstance.GetReactionSystem(ScriptExecutionContext.GetOwner(context).GetGame());
    if IsDefined(reactionSystem) && Equals(reactionSystem.RegisterReaction(this.m_reactionName), AIReactionCountOutcome.Succeded) {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class CheckTimestamp extends AIbehaviorconditionScript {

  public edit let m_validationTime: Float;

  public edit let m_timestampArgument: CName;

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let timestamp: Float = ScriptExecutionContext.GetArgumentFloat(context, this.m_timestampArgument);
    if timestamp + this.m_validationTime > EngineTime.ToFloat(GameInstance.GetSimTime(ScriptExecutionContext.GetOwner(context).GetGame())) {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class EscalateProvoke extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let reactionData: ref<AIReactionData> = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetActiveReactionData();
    if !IsDefined(reactionData) {
      reactionData = AIBehaviorScriptBase.GetPuppet(context).GetStimReactionComponent().GetDesiredReactionData();
    };
    if IsDefined(reactionData) && reactionData.escalateProvoke {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    return Cast<AIbehaviorConditionOutcomes>(false);
  }
}

public class ReactAfterDodge extends AIbehaviorconditionScript {

  protected func Check(context: ScriptExecutionContext) -> AIbehaviorConditionOutcomes {
    let mountInfo: MountingInfo;
    let target: ref<GameObject>;
    let ownerPuppet: ref<ScriptedPuppet> = ScriptExecutionContext.GetOwner(context) as ScriptedPuppet;
    if ownerPuppet.IsCrowd() {
      return Cast<AIbehaviorConditionOutcomes>(true);
    };
    target = ScriptExecutionContext.GetArgumentObject(context, n"StimTarget");
    if GameObject.IsVehicle(target) {
      mountInfo = GameInstance.GetMountingFacility(ownerPuppet.GetGame()).GetMountingInfoSingleWithObjects(target);
      target = GameInstance.FindEntityByID(ownerPuppet.GetGame(), mountInfo.childId) as GameObject;
    };
    if IsDefined(target) && Equals(GameObject.GetAttitudeBetween(ownerPuppet, target), EAIAttitude.AIA_Friendly) {
      return Cast<AIbehaviorConditionOutcomes>(false);
    };
    return Cast<AIbehaviorConditionOutcomes>(true);
  }
}
