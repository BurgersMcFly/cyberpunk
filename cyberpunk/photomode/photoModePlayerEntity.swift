
public class PhotoModePlayerEntityComponent extends ScriptableComponent {

  private let usedWeaponItemId: ItemID;

  private let currentWeaponInSlot: ItemID;

  private let availableCurrentItemTypesList: array<gamedataItemType>;

  private let availableCurrentItemsList: array<wref<gameItemData>>;

  private let swapMeleeWeaponItemId: ItemID;

  private let swapHangunWeaponItemId: ItemID;

  private let swapRifleWeaponItemId: ItemID;

  private let swapShootgunWeaponItemId: ItemID;

  private let fakePuppet: wref<gamePuppet>;

  private let mainPuppet: wref<PlayerPuppet>;

  private let currentPuppet: wref<PlayerPuppet>;

  private let TS: ref<TransactionSystem>;

  private let loadingItems: array<ItemID>;

  private let itemsLoadingTime: Float;

  private let muzzleEffectEnabled: Bool;

  private let customizable: Bool;

  private final func OnGameAttach() -> Void;

  private final func OnGameDetach() -> Void;

  private final func HasAllItemsFinishedLoading() -> Bool {
    let time: Float = EngineTime.ToFloat(this.GetEngineTime()) - this.itemsLoadingTime;
    if time > 5.00 {
      ArrayClear(this.loadingItems);
    };
    return ArraySize(this.loadingItems) == 0;
  }

  private final func PutOnFakeItem(itemToAdd: ItemID, puppet: wref<PlayerPuppet>) -> Void {
    let item: ItemID;
    let itemData: wref<gameItemData>;
    let equipAreaType: gamedataEquipmentArea = EquipmentSystem.GetEquipAreaType(itemToAdd);
    let currSlot: TweakDBID = EquipmentSystem.GetPlacementSlot(itemToAdd);
    if EquipmentSystem.GetData(puppet).IsSlotHidden(equipAreaType) {
      return;
    };
    item = ItemID.FromTDBID(ItemID.GetTDBID(itemToAdd));
    if Equals(equipAreaType, gamedataEquipmentArea.RightArm) {
      this.TS.GiveItem(this.fakePuppet, item, 1);
    } else {
      if Equals(equipAreaType, gamedataEquipmentArea.Weapon) || Equals(equipAreaType, gamedataEquipmentArea.WeaponLeft) || Equals(equipAreaType, gamedataEquipmentArea.WeaponHeavy) {
        itemData = this.TS.GetItemData(puppet, itemToAdd);
        this.TS.GiveItemByItemData(this.fakePuppet, itemData);
        item = ItemID.CreateQuery(ItemID.GetTDBID(itemToAdd));
      } else {
        if EquipmentSystem.IsClothing(item) {
          this.TS.GivePreviewItemByItemID(this.fakePuppet, item);
          item = this.TS.CreatePreviewItemID(item);
        } else {
          itemData = this.TS.GetItemData(puppet, itemToAdd);
          this.TS.GivePreviewItemByItemData(this.fakePuppet, itemData);
          item = this.TS.CreatePreviewItemID(itemToAdd);
        };
      };
    };
    if this.TS.CanPlaceItemInSlot(this.fakePuppet, currSlot, item) && this.TS.AddItemToSlot(this.fakePuppet, currSlot, item, true) {
      if this.TS.HasItemInSlot(puppet, currSlot, itemToAdd) {
        ArrayPush(this.loadingItems, item);
      };
      this.itemsLoadingTime = EngineTime.ToFloat(this.GetEngineTime());
    };
  }

  private final func PutOnFakeItemFromMainPuppet(itemToAdd: ItemID) -> Void {
    this.PutOnFakeItem(itemToAdd, this.mainPuppet);
  }

  private final func PutOnFakeItemFromCurrentPuppet(itemToAdd: ItemID) -> Void {
    this.PutOnFakeItem(itemToAdd, this.currentPuppet);
  }

  private final func RemoveAllItems(areas: array<SEquipArea>) -> Void {
    let currentPlayerItem: ItemID;
    let i: Int32 = 0;
    while i < ArraySize(areas) {
      currentPlayerItem = EquipmentSystem.GetData(this.fakePuppet).GetActiveItem(areas[i].areaType);
      if ItemID.IsValid(currentPlayerItem) {
        this.TS.RemoveItem(this.fakePuppet, currentPlayerItem, 1);
      };
      i += 1;
    };
  }

  private final func ListAllCurrentItems() -> Void {
    let i: Int32;
    let itemType: gamedataItemType;
    this.TS.GetItemList(this.currentPuppet, this.availableCurrentItemsList);
    i = 0;
    while i < ArraySize(this.availableCurrentItemsList) {
      itemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(this.availableCurrentItemsList[i].GetID())).ItemType().Type();
      if !ArrayContains(this.availableCurrentItemTypesList, itemType) {
        ArrayPush(this.availableCurrentItemTypesList, itemType);
      };
      i += 1;
    };
  }

  private final func GetAllAvailableItemTypes() -> array<gamedataItemType> {
    return this.availableCurrentItemTypesList;
  }

  private final func GetWeaponInHands() -> gamedataItemType {
    let itemType: gamedataItemType;
    if !ItemID.IsValid(this.usedWeaponItemId) {
      return gamedataItemType.Invalid;
    };
    itemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(this.usedWeaponItemId)).ItemType().Type();
    return itemType;
  }

  private final func IsItemOfThisType(item: ItemID, typesList: array<gamedataItemType>) -> Bool {
    let itemType: gamedataItemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(item)).ItemType().Type();
    let i: Int32 = 0;
    while i < ArraySize(typesList) {
      if Equals(itemType, typesList[i]) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  private final func AddAmmoForWeapon(weaponID: ItemID) -> Void {
    let ammoID: ItemID = WeaponObject.GetAmmoType(weaponID);
    if ItemID.IsValid(ammoID) {
      this.TS.GiveItem(this.fakePuppet, ammoID, 1);
    };
  }

  private final func EquipWeaponOfThisType(typesList: array<gamedataItemType>) -> Void {
    let currSlot: TweakDBID;
    let i: Int32;
    if ItemID.IsValid(this.currentWeaponInSlot) && this.IsItemOfThisType(this.currentWeaponInSlot, typesList) {
      return;
    };
    if ItemID.IsValid(this.usedWeaponItemId) && this.IsItemOfThisType(this.usedWeaponItemId, typesList) {
      this.AddAmmoForWeapon(this.usedWeaponItemId);
      this.PutOnFakeItemFromCurrentPuppet(this.usedWeaponItemId);
      this.currentWeaponInSlot = this.usedWeaponItemId;
      return;
    };
    i = 0;
    while i < ArraySize(this.availableCurrentItemsList) {
      if ItemID.IsValid(this.availableCurrentItemsList[i].GetID()) && this.IsItemOfThisType(this.availableCurrentItemsList[i].GetID(), typesList) {
        this.AddAmmoForWeapon(this.availableCurrentItemsList[i].GetID());
        this.PutOnFakeItemFromCurrentPuppet(this.availableCurrentItemsList[i].GetID());
        this.currentWeaponInSlot = this.availableCurrentItemsList[i].GetID();
        return;
      };
      i += 1;
    };
    if ItemID.IsValid(this.currentWeaponInSlot) {
      currSlot = EquipmentSystem.GetPlacementSlot(this.currentWeaponInSlot);
      this.TS.RemoveItemFromSlot(this.fakePuppet, currSlot);
      this.currentWeaponInSlot = ItemID.None();
    };
  }

  protected cb func OnItemAddedToSlot(evt: ref<ItemAddedToSlot>) -> Bool {
    let area: gamedataEquipmentArea;
    let data: ref<EquipmentSystemPlayerData>;
    let obj: ref<ItemObject>;
    let i: Int32 = 0;
    while i < ArraySize(this.loadingItems) {
      if this.loadingItems[i] == evt.GetItemID() {
        ArrayErase(this.loadingItems, i);
        break;
      };
      i += 1;
    };
    data = EquipmentSystem.GetData(this.mainPuppet);
    area = EquipmentSystem.GetEquipAreaType(evt.GetItemID());
    if data.IsSlotHidden(area) {
      this.TS.RemoveItem(this.fakePuppet, evt.GetItemID(), 1);
    } else {
      if Equals(area, gamedataEquipmentArea.OuterChest) && this.TS.MatchVisualTagByItemID(evt.GetItemID(), this.fakePuppet, n"hide_T1part") {
        obj = this.TS.GetItemInSlot(this.fakePuppet, t"AttachmentSlots.Chest");
        if IsDefined(obj) {
          this.TS.ResetItemAppearance(this.fakePuppet, obj.GetItemID());
        };
      };
    };
  }

  protected cb func OnItemVisualsAddedToSlot(evt: ref<ItemVisualsAddedToSlot>) -> Bool {
    let currentWeaponInSlotItemType: gamedataItemType;
    let evtItemType: gamedataItemType;
    if !EquipmentSystem.GetData(this.mainPuppet).IsSlotHidden(EquipmentSystem.GetEquipAreaType(evt.GetItemID())) && this.muzzleEffectEnabled && ItemID.IsValid(this.currentWeaponInSlot) {
      currentWeaponInSlotItemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(this.currentWeaponInSlot)).ItemType().Type();
      evtItemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(evt.GetItemID())).ItemType().Type();
      if Equals(currentWeaponInSlotItemType, evtItemType) {
        this.SetMuzzleEffectEnabled(true);
      };
    };
  }

  public final func StopWeaponShootEffects() -> Void {
    let weaponInHands: ref<WeaponObject> = GameObject.GetActiveWeapon(this.currentPuppet);
    if IsDefined(weaponInHands) {
      WeaponObject.StopWeaponEffects(this.currentPuppet, weaponInHands, gamedataFxAction.Shoot);
    };
  }

  public final func SetMuzzleEffectEnabled(enabled: Bool) -> Void {
    let weaponInHands: ref<WeaponObject> = GameObject.GetActiveWeapon(this.fakePuppet);
    this.muzzleEffectEnabled = enabled;
    if !IsDefined(weaponInHands) {
      return;
    };
    if enabled {
      GameObjectEffectHelper.StartEffectEvent(weaponInHands, n"muzzle_flash_photo_mode", false, null);
    } else {
      GameObjectEffectHelper.StopEffectEvent(weaponInHands, n"muzzle_flash_photo_mode");
    };
  }

  public final func IsMuzzleFireSupported() -> Bool {
    let itemType: gamedataItemType;
    if !ItemID.IsValid(this.currentWeaponInSlot) {
      return false;
    };
    itemType = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(this.currentWeaponInSlot)).ItemType().Type();
    return Equals(itemType, gamedataItemType.Wea_AssaultRifle) || Equals(itemType, gamedataItemType.Wea_Handgun) || Equals(itemType, gamedataItemType.Wea_HeavyMachineGun) || Equals(itemType, gamedataItemType.Wea_LightMachineGun) || Equals(itemType, gamedataItemType.Wea_PrecisionRifle) || Equals(itemType, gamedataItemType.Wea_Revolver) || Equals(itemType, gamedataItemType.Wea_Rifle) || Equals(itemType, gamedataItemType.Wea_Shotgun) || Equals(itemType, gamedataItemType.Wea_ShotgunDual) || Equals(itemType, gamedataItemType.Wea_SniperRifle) || Equals(itemType, gamedataItemType.Wea_SubmachineGun);
  }

  private final func ClearInventory() -> Void {
    let equipmentData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(this.fakePuppet);
    let visualAreas: array<SEquipArea> = this.GetPhotoModeVisualEquipAreas(equipmentData, true);
    let activeAreas: array<SEquipArea> = this.GetPhotoModeActiveEquipAreas(equipmentData);
    this.RemoveAllItems(visualAreas);
    this.RemoveAllItems(activeAreas);
  }

  private final func SetupUnderwear() -> Void;

  private final func SetupInventory(isCurrentPlayerObjectCustomizable: Bool) -> Void {
    let activeAreas: array<SEquipArea>;
    let currentPlayerItem: ItemID;
    let equipmentData: ref<EquipmentSystemPlayerData>;
    let head: ItemID;
    let i: Int32;
    let visualAreas: array<SEquipArea>;
    let weaponInHands: ref<WeaponObject>;
    this.muzzleEffectEnabled = false;
    this.customizable = isCurrentPlayerObjectCustomizable;
    this.fakePuppet = this.GetEntity() as gamePuppet;
    this.mainPuppet = GameInstance.GetPlayerSystem(this.fakePuppet.GetGame()).GetLocalPlayerMainGameObject() as PlayerPuppet;
    this.currentPuppet = GameInstance.GetPlayerSystem(this.fakePuppet.GetGame()).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let gender: CName = this.fakePuppet.GetResolvedGenderName();
    this.TS = GameInstance.GetTransactionSystem(this.fakePuppet.GetGame());
    this.ListAllCurrentItems();
    weaponInHands = GameObject.GetActiveWeapon(this.currentPuppet);
    if IsDefined(weaponInHands) {
      this.usedWeaponItemId = weaponInHands.GetItemID();
    };
    if !this.customizable {
      return;
    };
    equipmentData = EquipmentSystem.GetData(this.mainPuppet);
    activeAreas = this.GetPhotoModeActiveEquipAreas(equipmentData);
    i = 0;
    while i < ArraySize(activeAreas) {
      currentPlayerItem = equipmentData.GetActiveItem(activeAreas[i].areaType);
      if ItemID.IsValid(currentPlayerItem) {
        this.PutOnFakeItemFromMainPuppet(currentPlayerItem);
      };
      i += 1;
    };
    visualAreas = this.GetPhotoModeVisualEquipAreas(equipmentData, this.customizable);
    i = 0;
    while i < ArraySize(visualAreas) {
      currentPlayerItem = equipmentData.GetVisualItemInSlot(visualAreas[i].areaType);
      if ItemID.IsValid(currentPlayerItem) {
        this.PutOnFakeItemFromMainPuppet(currentPlayerItem);
      };
      i += 1;
    };
    if Equals(gender, n"Male") {
      head = ItemID.FromTDBID(t"Items.PlayerMaPhotomodeHead");
    } else {
      if Equals(gender, n"Female") {
        head = ItemID.FromTDBID(t"Items.PlayerWaPhotomodeHead");
      };
    };
    ArrayPush(this.loadingItems, head);
    this.TS.GiveItem(this.fakePuppet, head, 1);
    this.TS.AddItemToSlot(this.fakePuppet, EquipmentSystem.GetPlacementSlot(head), head, true);
    this.itemsLoadingTime = EngineTime.ToFloat(this.GetEngineTime());
  }

  public final const func GetPhotoModeEquipAreas(equipmentData: ref<EquipmentSystemPlayerData>, isVisual: Bool, withUnderwear: Bool) -> array<SEquipArea> {
    let areas: array<SEquipArea>;
    let slots: array<gamedataEquipmentArea> = isVisual ? this.GetPhotoModeVisualSlots(withUnderwear) : this.GetPhotoModeActiveSlots();
    let i: Int32 = 0;
    while i < ArraySize(slots) {
      ArrayPush(areas, this.GetEquipArea(equipmentData, slots[i]));
      i += 1;
    };
    return areas;
  }

  public final const func GetPhotoModeVisualEquipAreas(equipmentData: ref<EquipmentSystemPlayerData>, withUnderwear: Bool) -> array<SEquipArea> {
    return this.GetPhotoModeEquipAreas(equipmentData, true, withUnderwear);
  }

  public final const func GetPhotoModeActiveEquipAreas(equipmentData: ref<EquipmentSystemPlayerData>) -> array<SEquipArea> {
    return this.GetPhotoModeEquipAreas(equipmentData, false, false);
  }

  public final const func GetPhotoModeVisualSlots(withUnderwear: Bool) -> array<gamedataEquipmentArea> {
    let slots: array<gamedataEquipmentArea>;
    if withUnderwear {
      ArrayPush(slots, gamedataEquipmentArea.UnderwearTop);
      ArrayPush(slots, gamedataEquipmentArea.UnderwearBottom);
    };
    ArrayPush(slots, gamedataEquipmentArea.Face);
    ArrayPush(slots, gamedataEquipmentArea.Feet);
    ArrayPush(slots, gamedataEquipmentArea.Head);
    ArrayPush(slots, gamedataEquipmentArea.InnerChest);
    ArrayPush(slots, gamedataEquipmentArea.Legs);
    ArrayPush(slots, gamedataEquipmentArea.OuterChest);
    ArrayPush(slots, gamedataEquipmentArea.Outfit);
    ArrayPush(slots, gamedataEquipmentArea.HandsCW);
    ArrayPush(slots, gamedataEquipmentArea.LeftArm);
    ArrayPush(slots, gamedataEquipmentArea.RightArm);
    return slots;
  }

  public final const func GetPhotoModeActiveSlots() -> array<gamedataEquipmentArea> {
    let slots: array<gamedataEquipmentArea>;
    return slots;
  }

  private final const func GetEquipArea(equipmentData: ref<EquipmentSystemPlayerData>, areaType: gamedataEquipmentArea) -> SEquipArea {
    let emptyArea: SEquipArea;
    let equipment: SLoadout = equipmentData.GetEquipment();
    let i: Int32 = 0;
    while i < ArraySize(equipment.equipAreas) {
      if Equals(equipment.equipAreas[i].areaType, areaType) {
        return equipment.equipAreas[i];
      };
      i += 1;
    };
    return emptyArea;
  }
}
