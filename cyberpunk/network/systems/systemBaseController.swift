
public class BaseNetworkSystemController extends MasterController {

  public const func GetPS() -> ref<BaseNetworkSystemControllerPS> {
    return this.GetBasePS() as BaseNetworkSystemControllerPS;
  }
}
