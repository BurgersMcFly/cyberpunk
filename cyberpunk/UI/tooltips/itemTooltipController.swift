
public class ItemTooltipCommonController extends AGenericTooltipController {

  protected edit let m_backgroundContainer: inkWidgetRef;

  protected edit let m_itemEquippedContainer: inkWidgetRef;

  protected edit let m_itemHeaderContainer: inkWidgetRef;

  protected edit let m_itemIconContainer: inkWidgetRef;

  protected edit let m_itemWeaponInfoContainer: inkWidgetRef;

  protected edit let m_itemClothingInfoContainer: inkWidgetRef;

  protected edit let m_itemGrenadeInfoContainer: inkWidgetRef;

  protected edit let m_itemRequirementsContainer: inkWidgetRef;

  protected edit let m_itemDetailsContainer: inkWidgetRef;

  protected edit let m_itemRecipeDataContainer: inkWidgetRef;

  protected edit let m_itemEvolutionContainer: inkWidgetRef;

  protected edit let m_itemCraftedContainer: inkWidgetRef;

  protected edit let m_itemActionContainer: inkWidgetRef;

  protected edit let m_itemBottomContainer: inkWidgetRef;

  protected edit let m_descriptionWrapper: inkWidgetRef;

  protected edit let m_descriptionText: inkTextRef;

  protected edit let DEBUG_iconErrorWrapper: inkWidgetRef;

  protected edit let DEBUG_iconErrorText: inkTextRef;

  protected let m_spawnedModules: array<wref<ItemTooltipModuleController>>;

  protected let m_itemEquippedController: wref<ItemTooltipEquippedModule>;

  protected let m_itemHeaderController: wref<ItemTooltipHeaderController>;

  protected let m_itemIconController: wref<ItemTooltipIconModule>;

  protected let m_itemWeaponInfoController: wref<ItemTooltipWeaponInfoModule>;

  protected let m_itemClothingInfoController: wref<ItemTooltipClothingInfoModule>;

  protected let m_itemGrenadeInfoController: wref<ItemTooltipGrenadeInfoModule>;

  protected let m_itemRequirementsController: wref<ItemTooltipRequirementsModule>;

  protected let m_itemDetailsController: wref<ItemTooltipDetailsModule>;

  protected let m_itemRecipeDataController: wref<ItemTooltipRecipeDataModule>;

  protected let m_itemEvolutionController: wref<ItemTooltipEvolutionModule>;

  protected let m_itemCraftedController: wref<ItemTooltipCraftedModule>;

  protected let m_itemBottomController: wref<ItemTooltipBottomModule>;

  protected let DEBUG_showAdditionalInfo: Bool;

  protected let m_data: ref<MinimalItemTooltipData>;

  protected let m_itemData: ref<UIInventoryItem>;

  protected let m_comparisonData: ref<UIInventoryItemComparisonManager>;

  protected let m_player: wref<PlayerPuppet>;

  protected let m_requestedModules: array<CName>;

  protected let m_tooltipDisplayContext: InventoryTooltipDisplayContext;

  protected let m_itemDisplayContext: ItemDisplayContext;

  protected let m_priceOverride: Int32;

  protected cb func OnInitialize() -> Bool {
    super.OnInitialize();
    this.RegisterToGlobalInputCallback(n"OnPostOnPress", this, n"OnGlobalPress");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
  }

  protected cb func OnUninitialize() -> Bool {
    this.UnregisterFromGlobalInputCallback(n"OnPostOnPress", this, n"OnGlobalPress");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
  }

  protected cb func OnGlobalPress(evt: ref<inkPointerEvent>) -> Bool {
    this.DEBUG_showAdditionalInfo = evt.IsShiftDown();
    this.DEBUG_UpdateIconErrorInfo();
  }

  protected cb func OnGlobalRelease(evt: ref<inkPointerEvent>) -> Bool {
    if !evt.IsShiftDown() {
      this.DEBUG_showAdditionalInfo = false;
    };
    this.DEBUG_UpdateIconErrorInfo();
  }

  public final func SetData(data: ItemViewData) -> Void {
    this.SetData(InventoryTooltipData.FromItemViewData(data));
  }

  public func SetData(tooltipData: ref<ATooltipData>) -> Void {
    let tooltipWrapper: ref<UIInventoryItemTooltipWrapper>;
    if IsDefined(tooltipData as InventoryTooltipData) {
      this.UpdateData(tooltipData as InventoryTooltipData);
    } else {
      if IsDefined(tooltipData as UIInventoryItemTooltipWrapper) {
        tooltipWrapper = tooltipData as UIInventoryItemTooltipWrapper;
        this.m_itemData = tooltipWrapper.m_data;
        this.m_comparisonData = tooltipWrapper.m_comparisonData;
        this.m_player = tooltipWrapper.m_displayContext.GetPlayerAsPuppet();
        this.m_itemDisplayContext = tooltipWrapper.m_displayContext.GetDisplayContext();
        this.m_tooltipDisplayContext = tooltipWrapper.m_displayContext.GetTooltipDisplayContext();
        this.m_priceOverride = tooltipWrapper.m_overridePrice;
        this.InvalidateSpawnedModules();
        this.NEW_UpdateLayout();
      } else {
        this.m_data = tooltipData as MinimalItemTooltipData;
        this.UpdateLayout();
      };
    };
    this.DEBUG_UpdateIconErrorInfo();
  }

  public final func UpdateData(tooltipData: ref<InventoryTooltipData>) -> Void {
    this.m_data = MinimalItemTooltipData.FromInventoryTooltipData(tooltipData);
    this.UpdateLayout();
  }

  protected final func RequestModule(container: inkWidgetRef, moduleName: CName, callback: CName, opt data: ref<ItemTooltipModuleSpawnedCallbackData>) -> Bool {
    if ArrayContains(this.m_requestedModules, moduleName) {
      return false;
    };
    ArrayPush(this.m_requestedModules, moduleName);
    this.AsyncSpawnFromLocal(inkWidgetRef.Get(container), moduleName, this, callback, data);
    return true;
  }

  protected final func HandleModuleSpawned(widget: wref<inkWidget>, data: ref<ItemTooltipModuleSpawnedCallbackData>) -> Void {
    let controller: wref<ItemTooltipModuleController>;
    ArrayRemove(this.m_requestedModules, data.moduleName);
    widget.SetVAlign(inkEVerticalAlign.Top);
    controller = widget.GetController() as ItemTooltipModuleController;
    ArrayPush(this.m_spawnedModules, controller);
    controller.SetDisplayContext(this.m_itemDisplayContext, this.m_tooltipDisplayContext);
  }

  protected final func InvalidateSpawnedModules() -> Void {
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(this.m_spawnedModules);
    while i < limit {
      this.m_spawnedModules[i].SetDisplayContext(this.m_itemDisplayContext, this.m_tooltipDisplayContext);
      i += 1;
    };
  }

  protected func UpdateLayout() -> Void {
    this.UpdateEquippedModule();
    this.UpdateHeaderModule();
    this.UpdateIconModule();
    this.UpdateWeaponInfoModule();
    this.UpdateClothingInfoModule();
    this.UpdateGrenadeInfoModule();
    this.UpdateRequirementsModule();
    this.UpdateDetailsModule();
    this.UpdateRecipeDataModule();
    this.UpdateEvolutionModule();
    this.UpdateCraftedModule();
    this.UpdateTransmogModule();
    this.UpdateBottomModule();
    if IsStringValid(this.m_data.description) && this.m_data.itemTweakID != t"Items.money" {
      inkWidgetRef.SetVisible(this.m_descriptionWrapper, true);
      inkTextRef.SetText(this.m_descriptionText, GetLocalizedText(this.m_data.description));
    } else {
      inkWidgetRef.SetVisible(this.m_descriptionWrapper, false);
    };
    inkWidgetRef.SetVisible(this.m_backgroundContainer, NotEquals(this.m_data.displayContext, InventoryTooltipDisplayContext.Crafting));
  }

  protected func UpdateEquippedModule() -> Void {
    if this.m_data.isEquipped && NotEquals(this.m_data.displayContext, InventoryTooltipDisplayContext.Crafting) && NotEquals(this.m_data.displayContext, InventoryTooltipDisplayContext.Upgrading) {
      if !IsDefined(this.m_itemEquippedController) {
        this.RequestModule(this.m_itemEquippedContainer, n"itemEquipped", n"OnEquippedModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemEquippedContainer, true);
      this.m_itemEquippedController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemEquippedContainer, false);
    };
  }

  protected cb func OnEquippedModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemEquippedController = widget.GetController() as ItemTooltipEquippedModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateEquippedModule();
  }

  protected func UpdateHeaderModule() -> Void {
    if !IsDefined(this.m_itemHeaderController) {
      this.RequestModule(this.m_itemHeaderContainer, n"itemHeader", n"OnHeaderModuleSpawned");
      return;
    };
    this.m_itemHeaderController.Update(this.m_data);
  }

  protected cb func OnHeaderModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemHeaderController = widget.GetController() as ItemTooltipHeaderController;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateHeaderModule();
  }

  protected func UpdateIconModule() -> Void {
    let isCrafting: Bool = Equals(this.m_data.displayContext, InventoryTooltipDisplayContext.Crafting) || Equals(this.m_data.displayContext, InventoryTooltipDisplayContext.Upgrading);
    let isShard: Bool = Equals(this.m_data.itemType, gamedataItemType.Gen_Readable);
    if !isCrafting && !isShard {
      if !IsDefined(this.m_itemIconController) {
        this.RequestModule(this.m_itemIconContainer, n"itemIcon", n"OnIconModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemIconContainer, true);
      this.m_itemIconController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemIconContainer, false);
    };
  }

  protected cb func OnIconModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemIconController = widget.GetController() as ItemTooltipIconModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateIconModule();
  }

  protected cb func OnHideIconModuleEvent(evt: ref<HideIconModuleEvent>) -> Bool {
    inkWidgetRef.SetVisible(this.m_itemIconContainer, false);
  }

  protected func UpdateWeaponInfoModule() -> Void {
    if Equals(this.m_data.equipmentArea, gamedataEquipmentArea.Weapon) || Equals(this.m_data.equipmentArea, gamedataEquipmentArea.WeaponHeavy) || RPGManager.IsItemTypeCyberwareWeapon(this.m_data.itemType) {
      if !IsDefined(this.m_itemWeaponInfoController) {
        this.RequestModule(this.m_itemWeaponInfoContainer, n"itemWeaponInfo", n"OnWeaponInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemWeaponInfoContainer, true);
      this.m_itemWeaponInfoController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemWeaponInfoContainer, false);
    };
  }

  protected cb func OnWeaponInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemWeaponInfoController = widget.GetController() as ItemTooltipWeaponInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateWeaponInfoModule();
  }

  protected func UpdateClothingInfoModule() -> Void {
    if (Equals(this.m_data.itemCategory, gamedataItemCategory.Clothing) || Equals(this.m_data.itemCategory, gamedataItemCategory.Cyberware)) && this.m_data.armorValue > 0.00 {
      if !IsDefined(this.m_itemClothingInfoController) {
        this.RequestModule(this.m_itemClothingInfoContainer, n"itemClothingInfo", n"OnClothingInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemClothingInfoContainer, true);
      this.m_itemClothingInfoController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemClothingInfoContainer, false);
    };
  }

  protected cb func OnClothingInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemClothingInfoController = widget.GetController() as ItemTooltipClothingInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateClothingInfoModule();
  }

  protected func UpdateGrenadeInfoModule() -> Void {
    if Equals(this.m_data.itemType, gamedataItemType.Gad_Grenade) {
      if !IsDefined(this.m_itemGrenadeInfoController) {
        this.RequestModule(this.m_itemGrenadeInfoContainer, n"itemGrenadeInfo", n"OnGrenadeInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemGrenadeInfoContainer, true);
      this.m_itemGrenadeInfoController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemGrenadeInfoContainer, false);
    };
  }

  protected cb func OnGrenadeInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemGrenadeInfoController = widget.GetController() as ItemTooltipGrenadeInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateGrenadeInfoModule();
  }

  protected func UpdateRequirementsModule() -> Void {
    let anyRequirementNotMet: Bool = this.m_data.requirements.isLevelRequirementNotMet || this.m_data.requirements.isSmartlinkRequirementNotMet || this.m_data.requirements.isStrengthRequirementNotMet || this.m_data.requirements.isReflexRequirementNotMet || this.m_data.requirements.isAnyStatRequirementNotMet || this.m_data.requirements.isPerkRequirementNotMet || this.m_data.requirements.isRarityRequirementNotMet;
    if anyRequirementNotMet {
      if !IsDefined(this.m_itemRequirementsController) {
        this.RequestModule(this.m_itemRequirementsContainer, n"itemRequirements", n"OnRequirementsModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemRequirementsContainer, true);
      this.m_itemRequirementsController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemRequirementsContainer, false);
    };
  }

  protected cb func OnRequirementsModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemRequirementsController = widget.GetController() as ItemTooltipRequirementsModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateRequirementsModule();
  }

  protected func UpdateDetailsModule() -> Void {
    let hasStats: Bool = ArraySize(this.m_data.stats) > 0;
    let hasDedicatedMods: Bool = ArraySize(this.m_data.dedicatedMods) > 0;
    let hasMods: Bool = ArraySize(this.m_data.mods) > 0;
    let isWeaponOnHud: Bool = Equals(this.m_data.displayContext, InventoryTooltipDisplayContext.HUD) && Equals(this.m_data.equipmentArea, gamedataEquipmentArea.Weapon);
    let isWeaponInCrafting: Bool = Equals(this.m_data.displayContext, InventoryTooltipDisplayContext.Crafting) && Equals(this.m_data.equipmentArea, gamedataEquipmentArea.Weapon);
    let showInCrafting: Bool = isWeaponInCrafting && (hasDedicatedMods || hasMods);
    let showOutsideCraftingAndHud: Bool = !isWeaponInCrafting && !isWeaponOnHud && (hasStats || hasDedicatedMods || hasMods);
    let showInHud: Bool = isWeaponOnHud && (hasDedicatedMods || hasMods);
    if showOutsideCraftingAndHud || showInCrafting || showInHud {
      if !IsDefined(this.m_itemDetailsController) {
        this.RequestModule(this.m_itemDetailsContainer, n"itemDetails", n"OnDetailsModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemDetailsContainer, true);
      this.m_itemDetailsController.Update(this.m_data, hasStats, hasDedicatedMods, hasMods);
    } else {
      inkWidgetRef.SetVisible(this.m_itemDetailsContainer, false);
    };
  }

  protected cb func OnDetailsModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemDetailsController = widget.GetController() as ItemTooltipDetailsModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateDetailsModule();
  }

  protected func UpdateRecipeDataModule() -> Void {
    if this.m_data.recipeData != null {
      if !IsDefined(this.m_itemRecipeDataController) {
        this.RequestModule(this.m_itemRecipeDataContainer, n"itemRecipeData", n"OnRecipeDataModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemRecipeDataContainer, true);
      this.m_itemRecipeDataController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemRecipeDataContainer, false);
    };
  }

  protected cb func OnRecipeDataModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemRecipeDataController = widget.GetController() as ItemTooltipRecipeDataModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateRecipeDataModule();
  }

  protected func UpdateEvolutionModule() -> Void {
    if NotEquals(this.m_data.itemEvolution, gamedataWeaponEvolution.Invalid) {
      if !IsDefined(this.m_itemEvolutionController) {
        this.RequestModule(this.m_itemEvolutionContainer, n"itemEvolution", n"OnEvolutionModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemEvolutionContainer, true);
      this.m_itemEvolutionController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemEvolutionContainer, false);
    };
  }

  protected cb func OnEvolutionModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemEvolutionController = widget.GetController() as ItemTooltipEvolutionModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateEvolutionModule();
  }

  protected func UpdateCraftedModule() -> Void {
    if NotEquals(this.m_data.displayContext, InventoryTooltipDisplayContext.Crafting) && this.m_data.isCrafted {
      if !IsDefined(this.m_itemCraftedController) {
        this.RequestModule(this.m_itemCraftedContainer, n"itemCrafted", n"OnCraftedModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemCraftedContainer, true);
      this.m_itemCraftedController.Update(this.m_data);
    } else {
      inkWidgetRef.SetVisible(this.m_itemCraftedContainer, false);
    };
  }

  protected cb func OnCraftedModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemCraftedController = widget.GetController() as ItemTooltipCraftedModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateCraftedModule();
  }

  protected func UpdateTransmogModule() -> Void {
    inkWidgetRef.SetVisible(this.m_itemActionContainer, false);
  }

  protected func UpdateBottomModule() -> Void {
    if !IsDefined(this.m_itemBottomController) {
      this.RequestModule(this.m_itemBottomContainer, n"itemBottom", n"OnBottomModuleSpawned");
      return;
    };
    inkWidgetRef.SetVisible(this.m_itemBottomContainer, true);
    this.m_itemBottomController.Update(this.m_data);
  }

  protected cb func OnBottomModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemBottomController = widget.GetController() as ItemTooltipBottomModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.UpdateBottomModule();
  }

  private final func NEW_UpdateLayout() -> Void {
    this.NEW_UpdateEquippedModule();
    this.NEW_UpdateHeaderModule();
    this.NEW_UpdateIconModule();
    this.NEW_UpdateWeaponInfoModule();
    this.NEW_UpdateClothingInfoModule();
    this.NEW_UpdateGrenadeInfoModule();
    this.NEW_UpdateRequirementsModule();
    this.NEW_UpdateDetailsModule();
    this.NEW_UpdateRecipeDataModule();
    this.NEW_UpdateEvolutionModule();
    this.NEW_UpdateCraftedModule();
    this.NEW_UpdateTransmogModule();
    this.NEW_UpdateBottomModule();
    if !this.m_itemData.IsRecipe() && this.m_itemData.GetTweakDBID() != t"Items.money" {
      inkWidgetRef.SetVisible(this.m_descriptionWrapper, true);
      inkTextRef.SetText(this.m_descriptionText, this.m_itemData.GetDescription());
    } else {
      inkWidgetRef.SetVisible(this.m_descriptionWrapper, false);
    };
    inkWidgetRef.SetVisible(this.m_backgroundContainer, NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting));
  }

  protected func NEW_UpdateEquippedModule() -> Void {
    if this.m_itemData.IsEquipped() && NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) && NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Upgrading) {
      if !IsDefined(this.m_itemEquippedController) {
        this.RequestModule(this.m_itemEquippedContainer, n"itemEquipped", n"OnNEW_EquippedModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemEquippedContainer, true);
      this.m_itemEquippedController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemEquippedContainer, false);
    };
  }

  protected cb func OnNEW_EquippedModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemEquippedController = widget.GetController() as ItemTooltipEquippedModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateEquippedModule();
  }

  protected func NEW_UpdateHeaderModule() -> Void {
    if !IsDefined(this.m_itemHeaderController) {
      this.RequestModule(this.m_itemHeaderContainer, n"itemHeader", n"OnNEW_HeaderModuleSpawned");
      return;
    };
    this.m_itemHeaderController.NEW_Update(this.m_itemData);
  }

  protected cb func OnNEW_HeaderModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemHeaderController = widget.GetController() as ItemTooltipHeaderController;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateHeaderModule();
  }

  protected func NEW_UpdateIconModule() -> Void {
    let isCrafting: Bool = Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) || Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Upgrading);
    let isShard: Bool = Equals(this.m_itemData.GetItemType(), gamedataItemType.Gen_Readable);
    if !isCrafting && !isShard {
      if !IsDefined(this.m_itemIconController) {
        this.RequestModule(this.m_itemIconContainer, n"itemIcon", n"OnNEW_IconModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemIconContainer, true);
      this.m_itemIconController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemIconContainer, false);
    };
  }

  protected cb func OnNEW_IconModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemIconController = widget.GetController() as ItemTooltipIconModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateIconModule();
  }

  protected cb func OnNEW_HideIconModuleEvent(evt: ref<HideIconModuleEvent>) -> Bool {
    inkWidgetRef.SetVisible(this.m_itemIconContainer, false);
  }

  protected func NEW_UpdateWeaponInfoModule() -> Void {
    if this.m_itemData.IsWeapon() && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemWeaponInfoController) {
        this.RequestModule(this.m_itemWeaponInfoContainer, n"itemWeaponInfo", n"OnNEW_WeaponInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemWeaponInfoContainer, true);
      this.m_itemWeaponInfoController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemWeaponInfoContainer, false);
    };
  }

  protected cb func OnNEW_WeaponInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemWeaponInfoController = widget.GetController() as ItemTooltipWeaponInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateWeaponInfoModule();
  }

  protected func NEW_UpdateClothingInfoModule() -> Void {
    if (this.m_itemData.IsClothing() || this.m_itemData.IsCyberware()) && this.m_itemData.GetPrimaryStat().Value > 0.00 && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemClothingInfoController) {
        this.RequestModule(this.m_itemClothingInfoContainer, n"itemClothingInfo", n"OnNEW_ClothingInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemClothingInfoContainer, true);
      this.m_itemClothingInfoController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemClothingInfoContainer, false);
    };
  }

  protected cb func OnNEW_ClothingInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemClothingInfoController = widget.GetController() as ItemTooltipClothingInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateClothingInfoModule();
  }

  protected func NEW_UpdateGrenadeInfoModule() -> Void {
    if Equals(this.m_itemData.GetItemType(), gamedataItemType.Gad_Grenade) && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemGrenadeInfoController) {
        this.RequestModule(this.m_itemGrenadeInfoContainer, n"itemGrenadeInfo", n"OnNEW_GrenadeInfoModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemGrenadeInfoContainer, true);
      this.m_itemGrenadeInfoController.NEW_Update(this.m_player, this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemGrenadeInfoContainer, false);
    };
  }

  protected cb func OnNEW_GrenadeInfoModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemGrenadeInfoController = widget.GetController() as ItemTooltipGrenadeInfoModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateGrenadeInfoModule();
  }

  protected func NEW_UpdateRequirementsModule() -> Void {
    if this.m_itemData.GetRequirementsManager(this.m_player).IsAnyRequirementNotMet() && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemRequirementsController) {
        this.RequestModule(this.m_itemRequirementsContainer, n"itemRequirements", n"OnNEW_RequirementsModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemRequirementsContainer, true);
      this.m_itemRequirementsController.NEW_Update(this.m_itemData, this.m_player);
    } else {
      inkWidgetRef.SetVisible(this.m_itemRequirementsContainer, false);
    };
  }

  protected cb func OnNEW_RequirementsModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemRequirementsController = widget.GetController() as ItemTooltipRequirementsModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateRequirementsModule();
  }

  protected func NEW_UpdateDetailsModule() -> Void {
    let hasDedicatedMods: Bool;
    let hasMods: Bool;
    let hasStats: Bool;
    let isWeapon: Bool;
    let isWeaponInCrafting: Bool;
    let isWeaponOnHud: Bool;
    let modsManager: wref<UIInventoryItemModsManager>;
    let showInCrafting: Bool;
    let showInHud: Bool;
    let showOutsideCraftingAndHud: Bool;
    if !this.m_itemData.IsRecipe() {
      modsManager = this.m_itemData.GetModsManager();
      hasStats = Cast<Bool>(this.m_itemData.GetStatsManager().Size());
      hasDedicatedMods = modsManager.GetDedicatedModsSize() > 0;
      hasMods = modsManager.GetModsSize() > 0;
      isWeapon = this.m_itemData.IsWeapon();
      isWeaponOnHud = Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.HUD) && isWeapon;
      isWeaponInCrafting = Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) && isWeapon;
      showInCrafting = isWeaponInCrafting && (hasDedicatedMods || hasMods);
      showOutsideCraftingAndHud = !isWeaponInCrafting && !isWeaponOnHud && (hasStats || hasDedicatedMods || hasMods);
      showInHud = isWeaponOnHud && (hasDedicatedMods || hasMods);
      if showOutsideCraftingAndHud || showInCrafting || showInHud {
        if !IsDefined(this.m_itemDetailsController) {
          this.RequestModule(this.m_itemDetailsContainer, n"itemDetails", n"OnNEW_DetailsModuleSpawned");
          return;
        };
        inkWidgetRef.SetVisible(this.m_itemDetailsContainer, true);
        this.m_itemDetailsController.NEW_Update(this.m_itemData, this.m_comparisonData, hasStats, hasDedicatedMods, hasMods);
        return;
      };
    };
    inkWidgetRef.SetVisible(this.m_itemDetailsContainer, false);
  }

  protected cb func OnNEW_DetailsModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemDetailsController = widget.GetController() as ItemTooltipDetailsModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateDetailsModule();
  }

  protected func NEW_UpdateRecipeDataModule() -> Void {
    inkWidgetRef.SetVisible(this.m_itemRecipeDataContainer, false);
  }

  protected cb func OnNEW_RecipeDataModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemRecipeDataController = widget.GetController() as ItemTooltipRecipeDataModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateRecipeDataModule();
  }

  protected func NEW_UpdateEvolutionModule() -> Void {
    if NotEquals(this.m_itemData.GetWeaponEvolution(), gamedataWeaponEvolution.Invalid) && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemEvolutionController) {
        this.RequestModule(this.m_itemEvolutionContainer, n"itemEvolution", n"OnNEW_EvolutionModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemEvolutionContainer, true);
      this.m_itemEvolutionController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemEvolutionContainer, false);
    };
  }

  protected cb func OnNEW_EvolutionModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemEvolutionController = widget.GetController() as ItemTooltipEvolutionModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateEvolutionModule();
  }

  protected func NEW_UpdateCraftedModule() -> Void {
    if NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) && this.m_itemData.IsCrafted() && !this.m_itemData.IsRecipe() {
      if !IsDefined(this.m_itemCraftedController) {
        this.RequestModule(this.m_itemCraftedContainer, n"itemCrafted", n"OnNEW_CraftedModuleSpawned");
        return;
      };
      inkWidgetRef.SetVisible(this.m_itemCraftedContainer, true);
      this.m_itemCraftedController.NEW_Update(this.m_itemData);
    } else {
      inkWidgetRef.SetVisible(this.m_itemCraftedContainer, false);
    };
  }

  protected cb func OnNEW_CraftedModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemCraftedController = widget.GetController() as ItemTooltipCraftedModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateCraftedModule();
  }

  protected func NEW_UpdateTransmogModule() -> Void {
    inkWidgetRef.SetVisible(this.m_itemActionContainer, false);
  }

  protected cb func OnNEW_TransmogModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
  }

  protected func NEW_UpdateBottomModule() -> Void {
    if !IsDefined(this.m_itemBottomController) {
      this.RequestModule(this.m_itemBottomContainer, n"itemBottom", n"OnNEW_BottomModuleSpawned");
      return;
    };
    inkWidgetRef.SetVisible(this.m_itemBottomContainer, true);
    this.m_itemBottomController.NEW_Update(this.m_itemData, this.m_player, this.m_priceOverride);
  }

  protected cb func OnNEW_BottomModuleSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    this.m_itemBottomController = widget.GetController() as ItemTooltipBottomModule;
    this.HandleModuleSpawned(widget, userData as ItemTooltipModuleSpawnedCallbackData);
    this.NEW_UpdateBottomModule();
  }

  private final func DEBUG_UpdateIconErrorInfo() -> Void {
    let craftableItems: array<wref<Item_Record>>;
    let errorData: ref<DEBUG_IconErrorInfo>;
    let recipeRecord: ref<RecipeItem_Record>;
    let resultText: String;
    let iconsNameResolver: ref<IconsNameResolver> = IconsNameResolver.GetIconsNameResolver();
    if !iconsNameResolver.IsInDebugMode() {
      inkWidgetRef.SetVisible(this.DEBUG_iconErrorWrapper, false);
      return;
    };
    errorData = this.m_itemData.DEBUG_iconErrorInfo;
    inkWidgetRef.SetVisible(this.DEBUG_iconErrorWrapper, errorData != null || this.DEBUG_showAdditionalInfo);
    if this.DEBUG_showAdditionalInfo {
      resultText += " - itemID:\\n";
      resultText += TDBID.ToStringDEBUG(this.m_itemData.GetTweakDBID());
      if this.m_itemData.GetItemData().HasTag(n"Recipe") {
        recipeRecord = TweakDBInterface.GetRecipeItemRecord(this.m_itemData.GetTweakDBID());
        if IsDefined(recipeRecord) {
          recipeRecord.CraftableItems(craftableItems);
          if ArraySize(craftableItems) > 0 {
            resultText += "\\n - inner itemID:\\n";
            resultText += TDBID.ToStringDEBUG(craftableItems[0].GetID());
          };
        };
      };
      inkTextRef.SetText(this.DEBUG_iconErrorText, resultText);
    } else {
      if errorData != null {
        resultText += "   ErrorType: " + EnumValueToString("inkIconResult", Cast<Int64>(EnumInt(errorData.errorType))) + "\\n\\n";
        resultText += " - itemID:\\n";
        resultText += errorData.itemName;
        if IsStringValid(errorData.innerItemName) {
          resultText += "\\n - inner itemID:\\n";
          resultText += errorData.innerItemName;
        };
        if errorData.isManuallySet {
          resultText += "\\n - resolved icon name (manually set):\\n";
        } else {
          resultText += "\\n - resolved icon name (auto generated):\\n";
        };
        resultText += errorData.resolvedIconName;
        resultText += "\\n - error message:\\n";
        resultText += errorData.errorMessage;
        inkTextRef.SetText(this.DEBUG_iconErrorText, resultText);
      };
    };
  }
}

public class ItemTooltipModuleController extends inkLogicController {

  protected edit let m_lineWidget: inkWidgetRef;

  protected let m_tooltipDisplayContext: InventoryTooltipDisplayContext;

  protected let m_itemDisplayContext: ItemDisplayContext;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void;

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void;

  public final func SetDisplayContext(itemDisplayContext: ItemDisplayContext, tooltipDisplayContext: InventoryTooltipDisplayContext) -> Void {
    this.m_itemDisplayContext = itemDisplayContext;
    this.m_tooltipDisplayContext = tooltipDisplayContext;
  }

  protected final func UseCraftingLayout() -> Bool {
    return Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) || Equals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Upgrading);
  }

  protected final func UseCraftingLayout(data: ref<MinimalItemTooltipData>) -> Bool {
    return Equals(data.displayContext, InventoryTooltipDisplayContext.Crafting) || Equals(data.displayContext, InventoryTooltipDisplayContext.Upgrading);
  }

  protected final func GetArrowWrapperState(diffValue: Float) -> CName {
    if diffValue < 0.00 {
      return n"Worse";
    };
    if diffValue > 0.00 {
      return n"Better";
    };
    return n"Default";
  }
}

public class ItemTooltipHeaderController extends ItemTooltipModuleController {

  private edit let m_itemNameText: inkTextRef;

  private edit let m_itemRarityText: inkTextRef;

  private edit let m_itemTypeText: inkTextRef;

  private let m_localizedIconicText: String;

  protected cb func OnInitialize() -> Bool {
    this.m_localizedIconicText = GetLocalizedText(UIItemsHelper.QualityToLocalizationKey(gamedataQuality.Iconic));
  }

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    inkTextRef.SetText(this.m_itemTypeText, UIItemsHelper.GetItemTypeKey(data.itemData, data.equipmentArea, data.itemTweakID, data.itemType, data.itemEvolution));
    if this.UseCraftingLayout(data) {
      inkWidgetRef.SetVisible(this.m_itemNameText, false);
      inkWidgetRef.SetVisible(this.m_itemRarityText, false);
    } else {
      this.UpdateName(data);
      this.UpdateRarity(data);
    };
  }

  private final func UpdateName(data: ref<MinimalItemTooltipData>) -> Void {
    let finalItemName: String;
    inkWidgetRef.SetVisible(this.m_itemNameText, true);
    finalItemName = UIItemsHelper.GetTooltipItemName(data.itemTweakID, data.itemData, data.itemName);
    if data.quantity > 1 {
      finalItemName += " [" + IntToString(data.quantity) + "]";
    };
    inkTextRef.SetText(this.m_itemNameText, finalItemName);
  }

  private final func UpdateRarity(data: ref<MinimalItemTooltipData>) -> Void {
    let iconicLabel: String;
    let qualityName: CName;
    let rarityLabel: String;
    if !data.hasRarity {
      inkWidgetRef.SetVisible(this.m_itemRarityText, data.hasRarity);
      return;
    };
    inkWidgetRef.SetVisible(this.m_itemRarityText, true);
    qualityName = UIItemsHelper.QualityEnumToName(data.quality);
    rarityLabel = GetLocalizedText(UIItemsHelper.QualityToLocalizationKey(data.quality));
    iconicLabel = GetLocalizedText(UIItemsHelper.QualityToLocalizationKey(gamedataQuality.Iconic));
    inkWidgetRef.SetState(this.m_itemNameText, qualityName);
    inkWidgetRef.SetState(this.m_itemRarityText, qualityName);
    inkTextRef.SetText(this.m_itemRarityText, data.isIconic ? rarityLabel + " / " + iconicLabel : rarityLabel);
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    inkTextRef.SetText(this.m_itemTypeText, UIItemsHelper.GetItemTypeKey(data.GetItemData(), data.GetEquipmentArea(), data.GetTweakDBID(), data.GetItemType(), data.GetWeaponEvolution()));
    if this.UseCraftingLayout() {
      inkWidgetRef.SetVisible(this.m_itemNameText, false);
      inkWidgetRef.SetVisible(this.m_itemRarityText, false);
    } else {
      this.NEW_UpdateName(data.GetName(), data.GetQuantity());
      this.NEW_UpdateRarity(data.GetQuality(), data.IsIconic());
    };
  }

  private final func NEW_UpdateName(itemName: String, quantity: Int32) -> Void {
    inkWidgetRef.SetVisible(this.m_itemNameText, true);
    if quantity > 1 {
      itemName += " [" + IntToString(quantity) + "]";
    };
    inkTextRef.SetText(this.m_itemNameText, itemName);
  }

  private final func NEW_UpdateRarity(quality: gamedataQuality, isIconic: Bool) -> Void {
    let qualityName: CName;
    let rarityLabel: String;
    inkWidgetRef.SetVisible(this.m_itemRarityText, true);
    qualityName = UIItemsHelper.QualityEnumToName(quality);
    rarityLabel = GetLocalizedText(UIItemsHelper.QualityToLocalizationKey(quality));
    inkWidgetRef.SetState(this.m_itemNameText, qualityName);
    inkWidgetRef.SetState(this.m_itemRarityText, qualityName);
    inkTextRef.SetText(this.m_itemRarityText, isIconic ? rarityLabel + " / " + this.m_localizedIconicText : rarityLabel);
  }
}

public class ItemTooltipIconModule extends ItemTooltipModuleController {

  private edit let m_container: inkImageRef;

  private edit let m_icon: inkImageRef;

  private edit let m_iconicLines: inkImageRef;

  private edit let m_transmogged: inkImageRef;

  private let iconsNameResolver: ref<IconsNameResolver>;

  protected cb func OnInitialize() -> Bool {
    this.iconsNameResolver = IconsNameResolver.GetIconsNameResolver();
  }

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    let craftingResult: wref<CraftingResult_Record>;
    let itemRecord: wref<Item_Record>;
    let recipeRecord: wref<ItemRecipe_Record>;
    inkWidgetRef.SetVisible(this.m_iconicLines, data.isIconic);
    if IsDefined(data.itemData) && data.itemData.HasTag(n"Recipe") {
      recipeRecord = TweakDBInterface.GetItemRecipeRecord(data.itemTweakID);
      craftingResult = recipeRecord.CraftingResult();
      if IsDefined(craftingResult) {
        itemRecord = craftingResult.Item();
      };
    };
    inkWidgetRef.SetScale(this.m_icon, this.GetIconScale(data, itemRecord.EquipArea().Type()));
    inkWidgetRef.SetOpacity(this.m_icon, 0.00);
    InkImageUtils.RequestSetImage(this, this.m_icon, this.GetIconPath(data, itemRecord), n"OnIconCallback");
    if inkWidgetRef.IsValid(this.m_transmogged) {
      inkWidgetRef.SetVisible(this.m_transmogged, ItemID.IsValid(data.transmogItem));
    };
  }

  protected cb func OnIconCallback(e: ref<iconAtlasCallbackData>) -> Bool {
    if Equals(e.loadResult, inkIconResult.Success) {
      inkWidgetRef.SetOpacity(this.m_icon, 1.00);
    } else {
      this.QueueEvent(new HideIconModuleEvent());
    };
  }

  private final func GetIconPath(data: ref<MinimalItemTooltipData>, opt itemRecord: wref<Item_Record>) -> CName {
    let craftingIconName: String;
    let resolvedIcon: CName;
    if IsDefined(itemRecord) {
      craftingIconName = itemRecord.IconPath();
      if IsStringValid(craftingIconName) {
        return StringToName("UIIcon." + craftingIconName);
      };
      resolvedIcon = this.iconsNameResolver.TranslateItemToIconName(itemRecord.GetID(), data.useMaleIcon);
    } else {
      if IsDefined(data) && IsStringValid(data.iconPath) {
        return StringToName("UIIcon." + data.iconPath);
      };
      if ItemID.IsValid(data.transmogItem) {
        resolvedIcon = this.iconsNameResolver.TranslateItemToIconName(ItemID.GetTDBID(data.transmogItem), data.useMaleIcon);
      } else {
        resolvedIcon = this.iconsNameResolver.TranslateItemToIconName(data.itemTweakID, data.useMaleIcon);
      };
    };
    if IsNameValid(resolvedIcon) {
      return StringToName("UIIcon." + NameToString(resolvedIcon));
    };
    return UIItemsHelper.GetSlotShadowIcon(TDBID.None(), data.itemType, data.equipmentArea);
  }

  private final func GetIconScale(data: ref<MinimalItemTooltipData>, equipmentArea: gamedataEquipmentArea) -> Vector2 {
    let areaToCheck: gamedataEquipmentArea = Equals(equipmentArea, gamedataEquipmentArea.AbilityCW) ? data.equipmentArea : equipmentArea;
    return Equals(areaToCheck, gamedataEquipmentArea.Outfit) ? new Vector2(0.50, 0.50) : new Vector2(1.00, 1.00);
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    inkWidgetRef.SetVisible(this.m_iconicLines, data.IsIconic());
    inkWidgetRef.SetScale(this.m_icon, this.NEW_GetIconScale(data.GetEquipmentArea()));
    inkWidgetRef.SetOpacity(this.m_icon, 0.00);
    InkImageUtils.RequestSetImage(this, this.m_icon, data.GetIconPath(), n"OnNEW_IconCallback");
    inkWidgetRef.SetVisible(this.m_transmogged, false);
  }

  protected cb func OnNEW_IconCallback(e: ref<iconAtlasCallbackData>) -> Bool {
    if Equals(e.loadResult, inkIconResult.Success) {
      inkWidgetRef.SetOpacity(this.m_icon, 1.00);
    } else {
      this.QueueEvent(new HideIconModuleEvent());
    };
  }

  private final func NEW_GetIconScale(equipmentArea: gamedataEquipmentArea) -> Vector2 {
    return Equals(equipmentArea, gamedataEquipmentArea.Outfit) ? new Vector2(0.50, 0.50) : new Vector2(1.00, 1.00);
  }
}

public class ItemTooltipWeaponInfoModule extends ItemTooltipModuleController {

  private edit let m_wrapper: inkWidgetRef;

  private edit let m_arrow: inkImageRef;

  private edit let m_dpsText: inkTextRef;

  private edit let m_perHitText: inkTextRef;

  private edit let m_attacksPerSecondText: inkTextRef;

  private edit let m_nonLethal: inkTextRef;

  private edit let m_scopeIndicator: inkWidgetRef;

  private edit let m_silencerIndicator: inkWidgetRef;

  private edit let m_ammoText: inkTextRef;

  private edit let m_ammoWrapper: inkWidgetRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    let attacksPerSecond: Float;
    let damagePerHit: Float;
    let damagePerHitMax: Float;
    let damagePerHitMin: Float;
    let divideAttacksByPellets: Bool;
    let projectilesPerShot: Float;
    let dpsParams: ref<inkTextParams> = new inkTextParams();
    let attackPerSecondParams: ref<inkTextParams> = new inkTextParams();
    let damageParams: ref<inkTextParams> = new inkTextParams();
    inkWidgetRef.SetState(this.m_wrapper, this.GetArrowWrapperState(data.dpsDiff));
    inkWidgetRef.SetVisible(this.m_wrapper, data.dpsValue >= 0.00);
    inkWidgetRef.SetVisible(this.m_arrow, data.dpsDiff != 0.00);
    inkWidgetRef.SetVisible(this.m_nonLethal, Equals(data.itemEvolution, gamedataWeaponEvolution.Blunt));
    if !data.itemData.HasTag(n"MeleeWeapon") {
      inkWidgetRef.SetVisible(this.m_ammoWrapper, true);
      inkTextRef.SetText(this.m_ammoText, IntToString(data.ammoCount));
    } else {
      inkWidgetRef.SetVisible(this.m_ammoWrapper, false);
    };
    if data.hasScope {
      inkWidgetRef.SetVisible(this.m_scopeIndicator, true);
      inkWidgetRef.SetState(this.m_scopeIndicator, data.isScopeInstalled ? n"Default" : n"Empty");
    } else {
      inkWidgetRef.SetVisible(this.m_scopeIndicator, false);
    };
    if data.hasSilencer {
      inkWidgetRef.SetVisible(this.m_silencerIndicator, true);
      inkWidgetRef.SetState(this.m_silencerIndicator, data.isSilencerInstalled ? n"Default" : n"Empty");
    } else {
      inkWidgetRef.SetVisible(this.m_silencerIndicator, false);
    };
    if data.dpsDiff > 0.00 {
      inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.NoMirror);
    } else {
      if data.dpsDiff < 0.00 {
        inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.Vertical);
      };
    };
    dpsParams.AddNumber("value", FloorF(data.dpsValue));
    dpsParams.AddNumber("valueDecimalPart", RoundF((data.dpsValue - Cast<Float>(RoundF(data.dpsValue))) * 10.00) % 10);
    if Equals(data.displayContext, InventoryTooltipDisplayContext.Upgrading) {
      projectilesPerShot = data.projectilesPerShot;
      attacksPerSecond = data.attackSpeed;
    } else {
      projectilesPerShot = data.itemData.GetStatValueByType(gamedataStatType.ProjectilesPerShot);
      attacksPerSecond = data.itemData.GetStatValueByType(gamedataStatType.AttacksPerSecond);
    };
    divideAttacksByPellets = TweakDBInterface.GetBool(data.itemTweakID + t".divideAttacksByPelletsOnUI", false) && projectilesPerShot > 0.00;
    attackPerSecondParams.AddString("value", FloatToStringPrec(divideAttacksByPellets ? attacksPerSecond / projectilesPerShot : attacksPerSecond, 2));
    inkTextRef.SetLocalizedTextScript(this.m_attacksPerSecondText, "UI-Tooltips-AttacksPerSecond", attackPerSecondParams);
    inkTextRef.SetTextParameters(this.m_dpsText, dpsParams);
    if data.itemData.HasTag(n"Melee") {
      damagePerHit = data.itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHit);
      inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitMeleeTemplate");
      damageParams.AddString("value", IntToString(RoundF(damagePerHit)));
      inkTextRef.SetTextParameters(this.m_perHitText, damageParams);
    } else {
      if Equals(data.displayContext, InventoryTooltipDisplayContext.Upgrading) {
        damagePerHitMin = data.dpsValue / data.attackSpeed * 0.90;
        damagePerHitMax = data.dpsValue / data.attackSpeed * 1.10;
      } else {
        damagePerHitMin = data.itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHitMin);
        damagePerHitMax = data.itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHitMax);
      };
      damageParams.AddString("value", IntToString(RoundF(damagePerHitMin)));
      damageParams.AddString("valueMax", IntToString(RoundF(damagePerHitMax)));
      if (Equals(data.itemType, gamedataItemType.Wea_Shotgun) || Equals(data.itemType, gamedataItemType.Wea_ShotgunDual)) && projectilesPerShot > 0.00 {
        inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitWithMultiplierTemplate");
        damageParams.AddString("multiplier", IntToString(RoundF(projectilesPerShot)));
      } else {
        inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitTemplate");
      };
      inkTextRef.SetTextParameters(this.m_perHitText, damageParams);
    };
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    let projectilesPerShot: Int32;
    let dpsDiff: Float = 0.00;
    let dpsParams: ref<inkTextParams> = new inkTextParams();
    let attackPerSecondParams: ref<inkTextParams> = new inkTextParams();
    let damageParams: ref<inkTextParams> = new inkTextParams();
    let dpsValue: Float = data.GetPrimaryStat().Value;
    inkWidgetRef.SetState(this.m_wrapper, this.GetArrowWrapperState(dpsDiff));
    inkWidgetRef.SetVisible(this.m_wrapper, dpsValue >= 0.00);
    inkWidgetRef.SetVisible(this.m_arrow, dpsDiff != 0.00);
    inkWidgetRef.SetVisible(this.m_nonLethal, Equals(data.GetWeaponEvolution(), gamedataWeaponEvolution.Blunt));
    if Equals(data.GetWeaponType(), WeaponType.Ranged) {
      inkWidgetRef.SetVisible(this.m_ammoWrapper, true);
      inkTextRef.SetText(this.m_ammoText, IntToString(data.GetAmmo()));
    } else {
      inkWidgetRef.SetVisible(this.m_ammoWrapper, false);
    };
    if data.HasScopeSlot() {
      inkWidgetRef.SetVisible(this.m_scopeIndicator, true);
      inkWidgetRef.SetState(this.m_scopeIndicator, data.HasScopeInstalled() ? n"Default" : n"Empty");
    } else {
      inkWidgetRef.SetVisible(this.m_scopeIndicator, false);
    };
    if data.HasSilencerSlot() {
      inkWidgetRef.SetVisible(this.m_silencerIndicator, true);
      inkWidgetRef.SetState(this.m_silencerIndicator, data.HasSilencerInstalled() ? n"Default" : n"Empty");
    } else {
      inkWidgetRef.SetVisible(this.m_silencerIndicator, false);
    };
    if dpsDiff > 0.00 {
      inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.NoMirror);
    } else {
      if dpsDiff < 0.00 {
        inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.Vertical);
      };
    };
    dpsParams.AddNumber("value", FloorF(dpsValue));
    dpsParams.AddNumber("valueDecimalPart", RoundF((dpsValue - Cast<Float>(RoundF(dpsValue))) * 10.00) % 10);
    projectilesPerShot = data.GetNumberOfPellets();
    attackPerSecondParams.AddString("value", FloatToStringPrec(data.GetAttackSpeed(), 2));
    inkTextRef.SetLocalizedTextScript(this.m_attacksPerSecondText, "UI-Tooltips-AttacksPerSecond", attackPerSecondParams);
    inkTextRef.SetTextParameters(this.m_dpsText, dpsParams);
    if Equals(data.GetWeaponType(), WeaponType.Melee) {
      inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitMeleeTemplate");
      damageParams.AddString("value", IntToString(RoundF(data.GetDamageMin())));
      inkTextRef.SetTextParameters(this.m_perHitText, damageParams);
    } else {
      damageParams.AddString("value", IntToString(RoundF(data.GetDamageMin())));
      damageParams.AddString("valueMax", IntToString(RoundF(data.GetDamageMax())));
      if (Equals(data.GetItemType(), gamedataItemType.Wea_Shotgun) || Equals(data.GetItemType(), gamedataItemType.Wea_ShotgunDual)) && projectilesPerShot > 0 {
        inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitWithMultiplierTemplate");
        damageParams.AddString("multiplier", IntToString(projectilesPerShot));
      } else {
        inkTextRef.SetText(this.m_perHitText, "UI-Tooltips-DamagePerHitTemplate");
      };
      inkTextRef.SetTextParameters(this.m_perHitText, damageParams);
    };
  }
}

public class ItemTooltipClothingInfoModule extends ItemTooltipModuleController {

  private edit let m_value: inkTextRef;

  private edit let m_arrow: inkImageRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    let armorParams: ref<inkTextParams> = new inkTextParams();
    if data.armorDiff > 0.00 {
      inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.NoMirror);
    } else {
      if data.armorDiff < 0.00 {
        inkImageRef.SetBrushMirrorType(this.m_arrow, inkBrushMirrorType.Vertical);
      };
    };
    inkWidgetRef.SetState(this.m_arrow, this.GetArrowWrapperState(data.armorDiff));
    inkWidgetRef.SetVisible(this.m_arrow, data.armorDiff != 0.00);
    armorParams.AddNumber("value", FloorF(data.armorValue));
    armorParams.AddNumber("valueDecimalPart", RoundF((data.armorValue - Cast<Float>(RoundF(data.armorValue))) * 10.00) % 10);
    inkTextRef.SetTextParameters(this.m_value, armorParams);
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    let armorParams: ref<inkTextParams> = new inkTextParams();
    let armorValue: Float = data.GetPrimaryStat().Value;
    inkWidgetRef.SetVisible(this.m_arrow, false);
    armorParams.AddNumber("value", FloorF(armorValue));
    armorParams.AddNumber("valueDecimalPart", RoundF((armorValue - Cast<Float>(RoundF(armorValue))) * 10.00) % 10);
    inkTextRef.SetTextParameters(this.m_value, armorParams);
  }
}

public class ItemTooltipGrenadeInfoModule extends ItemTooltipModuleController {

  private edit let m_headerText: inkTextRef;

  private edit let m_totalDamageText: inkTextRef;

  private edit let m_durationText: inkTextRef;

  private edit let m_rangeText: inkTextRef;

  private edit let m_deliveryIcon: inkImageRef;

  private edit let m_deliveryText: inkTextRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    let damageParams: ref<inkTextParams>;
    let dpsParams: ref<inkTextParams>;
    let dpsValue: Float;
    let durationParams: ref<inkTextParams>;
    let hasDamage: Bool;
    let measurementUnit: EMeasurementUnit;
    let rangeParams: ref<inkTextParams>;
    let localizedSeconds: String = GetLocalizedText("UI-Quickhacks-Seconds");
    if Equals(data.grenadeData.type, GrenadeDamageType.DoT) {
      dpsParams = new inkTextParams();
      damageParams = new inkTextParams();
      durationParams = new inkTextParams();
      inkWidgetRef.SetVisible(this.m_headerText, true);
      inkWidgetRef.SetVisible(this.m_totalDamageText, true);
      inkWidgetRef.SetVisible(this.m_durationText, true);
      dpsValue = data.grenadeData.damagePerTick * 1.00 / data.grenadeData.delay;
      dpsParams.AddNumber("value", FloorF(dpsValue));
      dpsParams.AddNumber("valueDecimalPart", RoundF((dpsValue - Cast<Float>(RoundF(dpsValue))) * 10.00) % 10);
      damageParams.AddNumber("value", FloorF(data.grenadeData.totalDamage));
      durationParams.AddString("value", FloatToStringPrec(data.grenadeData.duration, 2));
      durationParams.AddString("unit", localizedSeconds);
      inkTextRef.SetText(this.m_headerText, GetLocalizedText("LocKey#77445"));
      inkTextRef.SetTextParameters(this.m_headerText, dpsParams);
      inkTextRef.SetTextParameters(this.m_durationText, durationParams);
      inkTextRef.SetTextParameters(this.m_totalDamageText, damageParams);
    } else {
      if Equals(data.grenadeData.type, GrenadeDamageType.Normal) {
        hasDamage = data.grenadeData.totalDamage > 0.00;
        inkWidgetRef.SetVisible(this.m_headerText, hasDamage);
        inkWidgetRef.SetVisible(this.m_totalDamageText, false);
        inkWidgetRef.SetVisible(this.m_durationText, false);
        if hasDamage {
          damageParams = new inkTextParams();
          damageParams.AddNumber("value", FloorF(data.grenadeData.totalDamage));
          inkTextRef.SetText(this.m_headerText, GetLocalizedText("LocKey#78473"));
          inkTextRef.SetTextParameters(this.m_headerText, damageParams);
        };
      };
    };
    rangeParams = new inkTextParams();
    measurementUnit = UILocalizationHelper.GetSystemBaseUnit();
    rangeParams.AddNumber("value", RoundTo(MeasurementUtils.ValueUnitToUnit(data.grenadeData.range, EMeasurementUnit.Meter, measurementUnit), 1));
    rangeParams.AddString("unit", GetLocalizedText(NameToString(MeasurementUtils.GetUnitLocalizationKey(measurementUnit))));
    inkTextRef.SetTextParameters(this.m_rangeText, rangeParams);
    this.UpdateGrenadeDeliveryMethod(data.grenadeData.deliveryMethod);
  }

  public final func NEW_Update(player: wref<PlayerPuppet>, data: wref<UIInventoryItem>) -> Void {
    let damageParams: ref<inkTextParams>;
    let dpsParams: ref<inkTextParams>;
    let dpsValue: Float;
    let durationParams: ref<inkTextParams>;
    let hasDamage: Bool;
    let measurementUnit: EMeasurementUnit;
    let rangeParams: ref<inkTextParams>;
    let grenadeData: wref<UIInventoryItemGrenadeData> = data.GetGrenadeData(player, true);
    let localizedSeconds: String = GetLocalizedText("UI-Quickhacks-Seconds");
    if Equals(grenadeData.Type, GrenadeDamageType.DoT) {
      dpsParams = new inkTextParams();
      damageParams = new inkTextParams();
      durationParams = new inkTextParams();
      inkWidgetRef.SetVisible(this.m_headerText, true);
      inkWidgetRef.SetVisible(this.m_totalDamageText, true);
      inkWidgetRef.SetVisible(this.m_durationText, true);
      dpsValue = grenadeData.DamagePerTick * 1.00 / grenadeData.Delay;
      dpsParams.AddNumber("value", FloorF(dpsValue));
      dpsParams.AddNumber("valueDecimalPart", RoundF((dpsValue - Cast<Float>(RoundF(dpsValue))) * 10.00) % 10);
      damageParams.AddNumber("value", FloorF(grenadeData.TotalDamage));
      durationParams.AddString("value", FloatToStringPrec(grenadeData.Duration, 2));
      durationParams.AddString("unit", localizedSeconds);
      inkTextRef.SetText(this.m_headerText, GetLocalizedText("LocKey#77445"));
      inkTextRef.SetTextParameters(this.m_headerText, dpsParams);
      inkTextRef.SetTextParameters(this.m_durationText, durationParams);
      inkTextRef.SetTextParameters(this.m_totalDamageText, damageParams);
    } else {
      if Equals(grenadeData.Type, GrenadeDamageType.Normal) {
        hasDamage = grenadeData.TotalDamage > 0.00;
        inkWidgetRef.SetVisible(this.m_headerText, hasDamage);
        inkWidgetRef.SetVisible(this.m_totalDamageText, false);
        inkWidgetRef.SetVisible(this.m_durationText, false);
        if hasDamage {
          damageParams = new inkTextParams();
          damageParams.AddNumber("value", FloorF(grenadeData.TotalDamage));
          inkTextRef.SetText(this.m_headerText, GetLocalizedText("LocKey#78473"));
          inkTextRef.SetTextParameters(this.m_headerText, damageParams);
        };
      };
    };
    rangeParams = new inkTextParams();
    measurementUnit = UILocalizationHelper.GetSystemBaseUnit();
    rangeParams.AddNumber("value", FloorF(MeasurementUtils.ValueUnitToUnit(grenadeData.Range, EMeasurementUnit.Meter, measurementUnit)));
    rangeParams.AddString("unit", GetLocalizedText(NameToString(MeasurementUtils.GetUnitLocalizationKey(measurementUnit))));
    inkTextRef.SetTextParameters(this.m_rangeText, rangeParams);
    this.UpdateGrenadeDeliveryMethod(grenadeData.DeliveryMethod);
  }

  private final func UpdateGrenadeDeliveryMethod(deliveryMethod: gamedataGrenadeDeliveryMethodType) -> Void {
    switch deliveryMethod {
      case gamedataGrenadeDeliveryMethodType.Regular:
        inkTextRef.SetText(this.m_deliveryText, GetLocalizedText("Gameplay-Items-Stats-Delivery-Regular"));
        break;
      case gamedataGrenadeDeliveryMethodType.Sticky:
        inkTextRef.SetText(this.m_deliveryText, GetLocalizedText("Gameplay-Items-Stats-Delivery-Sticky"));
        break;
      case gamedataGrenadeDeliveryMethodType.Homing:
        inkTextRef.SetText(this.m_deliveryText, GetLocalizedText("Gameplay-Items-Stats-Delivery-Homing"));
    };
  }
}

public class ItemTooltipRequirementsModule extends ItemTooltipModuleController {

  private edit let m_levelRequirementsWrapper: inkWidgetRef;

  private edit let m_strenghtOrReflexWrapper: inkWidgetRef;

  private edit let m_smartlinkGunWrapper: inkWidgetRef;

  private edit let m_anyAttributeWrapper: inkCompoundRef;

  private edit let m_levelRequirementsText: inkTextRef;

  private edit let m_strenghtOrReflexText: inkTextRef;

  private edit let m_perkText: inkTextRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    let textParams: ref<inkTextParams>;
    inkWidgetRef.SetVisible(this.m_levelRequirementsWrapper, false);
    inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, false);
    inkWidgetRef.SetVisible(this.m_smartlinkGunWrapper, false);
    inkWidgetRef.SetVisible(this.m_anyAttributeWrapper, false);
    if data.requirements.isSmartlinkRequirementNotMet {
      inkWidgetRef.SetVisible(this.m_smartlinkGunWrapper, true);
    };
    if data.requirements.isLevelRequirementNotMet {
      inkWidgetRef.SetVisible(this.m_levelRequirementsWrapper, true);
      inkTextRef.SetText(this.m_levelRequirementsText, IntToString(data.requirements.requiredLevel));
    };
    if data.requirements.isStrengthRequirementNotMet || data.requirements.isReflexRequirementNotMet {
      inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, true);
      textParams = new inkTextParams();
      textParams.AddString("statName", GetLocalizedText(data.requirements.strengthOrReflexStatName));
      textParams.AddNumber("statValue", data.requirements.strengthOrReflexValue);
      inkTextRef.SetText(this.m_strenghtOrReflexText, GetLocalizedText("LocKey#78420"));
      inkTextRef.SetTextParameters(this.m_strenghtOrReflexText, textParams);
    } else {
      if data.requirements.isRarityRequirementNotMet {
        inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, true);
        inkWidgetRef.SetVisible(this.m_strenghtOrReflexText, true);
        inkTextRef.SetLocalizedText(this.m_strenghtOrReflexText, n"UI-Tooltips-ModQualityRestriction");
      };
    };
    if data.requirements.isAnyStatRequirementNotMet {
      this.UpdateRequirements(data.requirements.anyStatRequirements);
    };
    if data.requirements.isPerkRequirementNotMet {
      textParams = new inkTextParams();
      textParams.AddLocalizedString("perkName", data.requirements.perkLocKey);
      inkWidgetRef.SetVisible(this.m_perkText, true);
      inkTextRef.SetLocalizedTextScript(this.m_perkText, "LocKey#42796", textParams);
    } else {
      inkWidgetRef.SetVisible(this.m_perkText, false);
    };
  }

  private final func UpdateRequirements(statRequirements: array<ref<MinimalItemTooltipDataStatRequirement>>) -> Void {
    let controller: ref<ItemTooltipAttributeRequirement>;
    let i: Int32;
    let requirementsSize: Int32;
    inkWidgetRef.SetVisible(this.m_anyAttributeWrapper, true);
    requirementsSize = ArraySize(statRequirements);
    while inkCompoundRef.GetNumChildren(this.m_anyAttributeWrapper) > requirementsSize {
      inkCompoundRef.RemoveChildByIndex(this.m_anyAttributeWrapper, 0);
    };
    while inkCompoundRef.GetNumChildren(this.m_anyAttributeWrapper) < requirementsSize {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_anyAttributeWrapper), n"itemAttrbuteRequirement");
    };
    i = 0;
    while i < requirementsSize {
      controller = inkCompoundRef.GetWidgetByIndex(this.m_anyAttributeWrapper, i).GetController() as ItemTooltipAttributeRequirement;
      controller.SetData(statRequirements[i]);
      i += 1;
    };
  }

  public final func NEW_Update(data: wref<UIInventoryItem>, player: wref<PlayerPuppet>) -> Void {
    let requiremenetsManager: wref<UIInventoryItemRequirementsManager>;
    let statName: String;
    let textParams: ref<inkTextParams>;
    inkWidgetRef.SetVisible(this.m_levelRequirementsWrapper, false);
    inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, false);
    inkWidgetRef.SetVisible(this.m_smartlinkGunWrapper, false);
    inkWidgetRef.SetVisible(this.m_anyAttributeWrapper, false);
    requiremenetsManager = data.GetRequirementsManager(player);
    if !requiremenetsManager.IsSmartlinkRequirementMet() {
      inkWidgetRef.SetVisible(this.m_smartlinkGunWrapper, true);
    };
    if !requiremenetsManager.IsLevelRequirementMet() {
      inkWidgetRef.SetVisible(this.m_levelRequirementsWrapper, true);
      inkTextRef.SetText(this.m_levelRequirementsText, IntToString(requiremenetsManager.GetLevelRequirementValue()));
    };
    if !requiremenetsManager.IsStrengthRequirementMet() {
      inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, true);
      textParams = new inkTextParams();
      statName = UILocalizationHelper.GetStatNameLockey(RPGManager.GetStatRecord(gamedataStatType.Strength));
      textParams.AddString("statName", GetLocalizedText(statName));
      textParams.AddNumber("statValue", requiremenetsManager.GetStrengthRequirementValue());
      inkTextRef.SetText(this.m_strenghtOrReflexText, GetLocalizedText("LocKey#78420"));
      inkTextRef.SetTextParameters(this.m_strenghtOrReflexText, textParams);
    } else {
      if !requiremenetsManager.IsReflexRequirementMet() {
        inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, true);
        textParams = new inkTextParams();
        statName = UILocalizationHelper.GetStatNameLockey(RPGManager.GetStatRecord(gamedataStatType.Reflexes));
        textParams.AddString("statName", GetLocalizedText(statName));
        textParams.AddNumber("statValue", requiremenetsManager.GetReflexRequirementValue());
        inkTextRef.SetText(this.m_strenghtOrReflexText, GetLocalizedText("LocKey#78420"));
        inkTextRef.SetTextParameters(this.m_strenghtOrReflexText, textParams);
      } else {
        if !requiremenetsManager.IsRarityRequirementMet(null) {
          inkWidgetRef.SetVisible(this.m_strenghtOrReflexWrapper, true);
          inkWidgetRef.SetVisible(this.m_strenghtOrReflexText, true);
          inkTextRef.SetLocalizedText(this.m_strenghtOrReflexText, n"UI-Tooltips-ModQualityRestriction");
        };
      };
    };
    if !requiremenetsManager.IsPerkRequirementMet() {
      textParams = new inkTextParams();
      textParams.AddLocalizedString("perkName", requiremenetsManager.GetPerkRequirementValue());
      inkWidgetRef.SetVisible(this.m_perkText, true);
      inkTextRef.SetLocalizedTextScript(this.m_perkText, "LocKey#42796", textParams);
    } else {
      inkWidgetRef.SetVisible(this.m_perkText, false);
    };
  }
}

public class ItemTooltipDetailsModule extends ItemTooltipModuleController {

  private edit let m_statsLine: inkWidgetRef;

  private edit let m_statsWrapper: inkWidgetRef;

  private edit let m_statsContainer: inkCompoundRef;

  private edit let m_dedicatedModsLine: inkWidgetRef;

  private edit let m_dedicatedModsWrapper: inkWidgetRef;

  private edit let m_dedicatedModsContainer: inkCompoundRef;

  private edit let m_modsLine: inkWidgetRef;

  private edit let m_modsWrapper: inkWidgetRef;

  private edit let m_modsContainer: inkCompoundRef;

  public final func Update(data: ref<MinimalItemTooltipData>, hasStats: Bool, hasDedicatedMods: Bool, hasMods: Bool) -> Void {
    if hasStats && (NotEquals(data.displayContext, InventoryTooltipDisplayContext.Crafting) || data.isIconic) {
      inkWidgetRef.SetVisible(this.m_statsLine, true);
      inkWidgetRef.SetVisible(this.m_statsWrapper, true);
      this.UpdateStats(data);
    } else {
      inkWidgetRef.SetVisible(this.m_statsLine, false);
      inkWidgetRef.SetVisible(this.m_statsWrapper, false);
    };
    if hasDedicatedMods {
      inkWidgetRef.SetVisible(this.m_dedicatedModsLine, true);
      inkWidgetRef.SetVisible(this.m_dedicatedModsWrapper, true);
      this.UpdateDedicatedMods(data);
    } else {
      inkWidgetRef.SetVisible(this.m_dedicatedModsLine, false);
      inkWidgetRef.SetVisible(this.m_dedicatedModsWrapper, false);
    };
    if hasMods {
      inkWidgetRef.SetVisible(this.m_modsLine, true);
      inkWidgetRef.SetVisible(this.m_modsWrapper, true);
      this.UpdateMods(data);
    } else {
      inkWidgetRef.SetVisible(this.m_modsLine, false);
      inkWidgetRef.SetVisible(this.m_modsWrapper, false);
    };
  }

  private final func UpdateStats(data: ref<MinimalItemTooltipData>) -> Void {
    let controller: ref<ItemTooltipStatController>;
    let i: Int32;
    let widget: wref<inkWidget>;
    inkCompoundRef.RemoveAllChildren(this.m_statsContainer);
    i = 0;
    while i < ArraySize(data.stats) {
      widget = this.SpawnFromLocal(inkWidgetRef.Get(this.m_statsContainer), n"itemDetailsStat");
      controller = widget.GetController() as ItemTooltipStatController;
      controller.SetData(data.stats[i]);
      i += 1;
    };
  }

  private final func UpdateMods(data: ref<MinimalItemTooltipData>) -> Void {
    let controller: ref<ItemTooltipModController>;
    let i: Int32;
    let modsSize: Int32 = ArraySize(data.mods);
    while inkCompoundRef.GetNumChildren(this.m_modsContainer) > modsSize {
      inkCompoundRef.RemoveChildByIndex(this.m_modsContainer, 0);
    };
    while inkCompoundRef.GetNumChildren(this.m_modsContainer) < modsSize {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_modsContainer), n"itemTooltipMod");
    };
    i = 0;
    while i < modsSize {
      controller = inkCompoundRef.GetWidgetByIndex(this.m_modsContainer, i).GetController() as ItemTooltipModController;
      controller.SetData(data.mods[i]);
      i += 1;
    };
  }

  private final func UpdateDedicatedMods(data: ref<MinimalItemTooltipData>) -> Void {
    let controller: ref<ItemTooltipModController>;
    let i: Int32;
    let dedicatedModsSize: Int32 = ArraySize(data.dedicatedMods);
    while inkCompoundRef.GetNumChildren(this.m_dedicatedModsContainer) > dedicatedModsSize {
      inkCompoundRef.RemoveChildByIndex(this.m_dedicatedModsContainer, 0);
    };
    while inkCompoundRef.GetNumChildren(this.m_dedicatedModsContainer) < dedicatedModsSize {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_dedicatedModsContainer), n"itemTooltipMod");
    };
    i = 0;
    while i < dedicatedModsSize {
      controller = inkCompoundRef.GetWidgetByIndex(this.m_dedicatedModsContainer, i).GetController() as ItemTooltipModController;
      controller.SetData(data.dedicatedMods[i]);
      controller.HideDotIndicator();
      i += 1;
    };
  }

  public final func NEW_Update(data: wref<UIInventoryItem>, m_comparisonData: wref<UIInventoryItemComparisonManager>, hasStats: Bool, hasDedicatedMods: Bool, hasMods: Bool) -> Void {
    let modsManager: wref<UIInventoryItemModsManager> = data.GetModsManager();
    if hasStats && (NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Crafting) || data.IsIconic()) {
      inkWidgetRef.SetVisible(this.m_statsLine, true);
      inkWidgetRef.SetVisible(this.m_statsWrapper, true);
      this.NEW_UpdateStats(data, m_comparisonData);
    } else {
      inkWidgetRef.SetVisible(this.m_statsLine, false);
      inkWidgetRef.SetVisible(this.m_statsWrapper, false);
    };
    if hasDedicatedMods {
      inkWidgetRef.SetVisible(this.m_dedicatedModsLine, true);
      inkWidgetRef.SetVisible(this.m_dedicatedModsWrapper, true);
      this.NEW_UpdateDedicatedMods(modsManager);
    } else {
      inkWidgetRef.SetVisible(this.m_dedicatedModsLine, false);
      inkWidgetRef.SetVisible(this.m_dedicatedModsWrapper, false);
    };
    if hasMods {
      inkWidgetRef.SetVisible(this.m_modsLine, true);
      inkWidgetRef.SetVisible(this.m_modsWrapper, true);
      this.NEW_UpdateMods(modsManager);
    } else {
      inkWidgetRef.SetVisible(this.m_modsLine, false);
      inkWidgetRef.SetVisible(this.m_modsWrapper, false);
    };
  }

  private final func NEW_UpdateStats(data: wref<UIInventoryItem>, m_comparisonData: wref<UIInventoryItemComparisonManager>) -> Void {
    let controller: ref<ItemTooltipStatController>;
    let i: Int32;
    let limit: Int32;
    let stat: wref<UIInventoryItemStat>;
    let statsManager: wref<UIInventoryItemStatsManager>;
    let widget: wref<inkWidget>;
    inkCompoundRef.RemoveAllChildren(this.m_statsContainer);
    statsManager = data.GetStatsManager();
    i = 0;
    limit = statsManager.Size();
    while i < limit {
      stat = statsManager.Get(i);
      widget = this.SpawnFromLocal(inkWidgetRef.Get(this.m_statsContainer), n"itemDetailsStat");
      controller = widget.GetController() as ItemTooltipStatController;
      controller.SetData(stat, m_comparisonData.GetByType(stat.Type));
      i += 1;
    };
  }

  private final func NEW_UpdateMods(modsManager: wref<UIInventoryItemModsManager>) -> Void {
    let controller: ref<ItemTooltipModController>;
    let i: Int32;
    let modsSize: Int32 = modsManager.GetModsSize();
    while inkCompoundRef.GetNumChildren(this.m_modsContainer) > modsSize {
      inkCompoundRef.RemoveChildByIndex(this.m_modsContainer, 0);
    };
    while inkCompoundRef.GetNumChildren(this.m_modsContainer) < modsSize {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_modsContainer), n"itemTooltipMod");
    };
    i = 0;
    while i < modsSize {
      controller = inkCompoundRef.GetWidgetByIndex(this.m_modsContainer, i).GetController() as ItemTooltipModController;
      controller.SetData(modsManager.GetMod(i));
      i += 1;
    };
  }

  private final func NEW_UpdateDedicatedMods(modsManager: wref<UIInventoryItemModsManager>) -> Void {
    let controller: ref<ItemTooltipModController>;
    let i: Int32;
    let dedicatedModsSize: Int32 = modsManager.GetDedicatedModsSize();
    while inkCompoundRef.GetNumChildren(this.m_dedicatedModsContainer) > dedicatedModsSize {
      inkCompoundRef.RemoveChildByIndex(this.m_dedicatedModsContainer, 0);
    };
    while inkCompoundRef.GetNumChildren(this.m_dedicatedModsContainer) < dedicatedModsSize {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_dedicatedModsContainer), n"itemTooltipMod");
    };
    i = 0;
    while i < dedicatedModsSize {
      controller = inkCompoundRef.GetWidgetByIndex(this.m_dedicatedModsContainer, i).GetController() as ItemTooltipModController;
      controller.SetData(modsManager.GetDedicatedMod(i));
      controller.HideDotIndicator();
      i += 1;
    };
  }
}

public class ItemTooltipRecipeDataModule extends ItemTooltipModuleController {

  private edit let m_randomQualityLabel: inkTextRef;

  private edit let m_randomQualityWrapper: inkWidgetRef;

  private edit let m_statsLabel: inkTextRef;

  private edit let m_statsWrapper: inkWidgetRef;

  private edit let m_statsContainer: inkCompoundRef;

  private edit let m_damageTypesLabel: inkTextRef;

  private edit let m_damageTypesWrapper: inkWidgetRef;

  private edit let m_damageTypesContainer: inkCompoundRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    this.UpdateRandomQuality(data);
    this.UpdatemRecipeDamageTypes(data);
    this.UpdatemRecipeProperties(data);
  }

  private final func UpdateRandomQuality(data: ref<MinimalItemTooltipData>) -> Void {
    let nextCraftingLevel: Int32;
    let nextQuality: gamedataQuality;
    let qualityParams: ref<inkTextParams>;
    let itemRecord: ref<Item_Record> = TweakDBInterface.GetItemRecord(data.itemTweakID);
    if Equals(itemRecord.Quality().Type(), gamedataQuality.Random) && NotEquals(data.quality, gamedataQuality.Legendary) && NotEquals(itemRecord.ItemCategory().Type(), gamedataItemCategory.Clothing) && NotEquals(itemRecord.ItemCategory().Type(), gamedataItemCategory.Weapon) {
      qualityParams = new inkTextParams();
      nextQuality = RPGManager.GetNextItemQuality(data.itemData);
      nextCraftingLevel = RPGManager.GetCraftingNextLevelBasedOnRandomQuality(nextQuality);
      qualityParams.AddString("quality", GetLocalizedText(UIItemsHelper.QualityToLocalizationKey(nextQuality)));
      qualityParams.AddNumber("level", nextCraftingLevel);
      inkTextRef.SetLocalizedText(this.m_randomQualityLabel, n"UI-Tooltips-RandomQualityDesc", qualityParams);
      inkWidgetRef.SetVisible(this.m_randomQualityWrapper, true);
    } else {
      inkWidgetRef.SetVisible(this.m_randomQualityWrapper, false);
    };
  }

  private final func UpdatemRecipeDamageTypes(data: ref<MinimalItemTooltipData>) -> Void {
    let controller: ref<ItemTooltipStatController>;
    let i: Int32;
    let stat: InventoryTooltipData_StatData;
    let damagesTypesSize: Int32 = ArraySize(data.recipeData.damageTypes);
    if damagesTypesSize > 0 {
      while inkCompoundRef.GetNumChildren(this.m_damageTypesContainer) > damagesTypesSize {
        inkCompoundRef.RemoveChildByIndex(this.m_damageTypesContainer, 0);
      };
      while inkCompoundRef.GetNumChildren(this.m_damageTypesContainer) < damagesTypesSize {
        this.SpawnFromLocal(inkWidgetRef.Get(this.m_damageTypesContainer), n"itemDetailsStat");
      };
      i = 0;
      while i < damagesTypesSize {
        stat = data.recipeData.damageTypes[i];
        controller = inkCompoundRef.GetWidgetByIndex(this.m_damageTypesContainer, i).GetController() as ItemTooltipStatController;
        controller.SetData(stat);
        i += 1;
      };
      inkWidgetRef.SetVisible(this.m_damageTypesWrapper, true);
    } else {
      inkWidgetRef.SetVisible(this.m_damageTypesWrapper, false);
    };
  }

  private final func UpdatemRecipeProperties(data: ref<MinimalItemTooltipData>) -> Void {
    let controller: ref<ItemRandomizedStatsController>;
    let statsQuantityParams: ref<inkTextParams>;
    let widget: wref<inkWidget>;
    if ArraySize(data.recipeData.recipeStats) > 0 {
      statsQuantityParams = new inkTextParams();
      statsQuantityParams.AddString("value", IntToString(data.recipeData.statsNumber));
      inkTextRef.SetLocalizedText(this.m_statsLabel, n"UI-Tooltips-RandomStatsNumber", statsQuantityParams);
      if inkCompoundRef.GetNumChildren(this.m_statsContainer) == 0 {
        widget = this.SpawnFromLocal(inkWidgetRef.Get(this.m_statsContainer), n"itemTooltipRecipeStat");
      } else {
        widget = inkCompoundRef.GetWidgetByIndex(this.m_statsContainer, 0);
      };
      controller = widget.GetController() as ItemRandomizedStatsController;
      controller.SetData(data.recipeData.recipeStats);
      inkWidgetRef.SetVisible(this.m_statsWrapper, true);
    } else {
      inkWidgetRef.SetVisible(this.m_statsWrapper, false);
    };
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void;
}

public class ItemTooltipEvolutionModule extends ItemTooltipModuleController {

  private edit let m_weaponEvolutionIcon: inkImageRef;

  private edit let m_weaponEvolutionName: inkTextRef;

  private edit let m_weaponEvolutionDescription: inkTextRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    switch data.itemEvolution {
      case gamedataWeaponEvolution.Power:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_power");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54118");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54117");
        break;
      case gamedataWeaponEvolution.Smart:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_smart");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54119");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54120");
        break;
      case gamedataWeaponEvolution.Tech:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_tech");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54121");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54122");
        break;
      case gamedataWeaponEvolution.Blunt:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_blunt");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#77968");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#77969 ");
        break;
      case gamedataWeaponEvolution.Blade:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_blades");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#77957");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#77960");
    };
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    switch data.GetWeaponEvolution() {
      case gamedataWeaponEvolution.Power:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_power");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54118");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54117");
        break;
      case gamedataWeaponEvolution.Smart:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_smart");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54119");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54120");
        break;
      case gamedataWeaponEvolution.Tech:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_tech");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#54121");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#54122");
        break;
      case gamedataWeaponEvolution.Blunt:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_blunt");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#77968");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#77969 ");
        break;
      case gamedataWeaponEvolution.Blade:
        inkImageRef.SetTexturePart(this.m_weaponEvolutionIcon, n"ico_blades");
        inkTextRef.SetText(this.m_weaponEvolutionName, "LocKey#77957");
        inkTextRef.SetText(this.m_weaponEvolutionDescription, "LocKey#77960");
    };
  }
}

public class ItemTooltipCraftedModule extends ItemTooltipModuleController {

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    this.GetRootWidget().SetState(UIItemsHelper.QualityEnumToName(data.quality));
  }

  public func NEW_Update(data: wref<UIInventoryItem>) -> Void {
    this.GetRootWidget().SetState(UIItemsHelper.QualityEnumToName(data.GetQuality()));
  }
}

public class ItemTooltipBottomModule extends ItemTooltipModuleController {

  private edit let m_weightWrapper: inkWidgetRef;

  private edit let m_priceWrapper: inkWidgetRef;

  private edit let m_weightText: inkTextRef;

  private edit let m_priceText: inkTextRef;

  public func Update(data: ref<MinimalItemTooltipData>) -> Void {
    inkTextRef.SetText(this.m_weightText, FloatToStringPrec(data.weight, 1));
    if NotEquals(data.displayContext, InventoryTooltipDisplayContext.Vendor) && (data.itemData.HasTag(n"Shard") || data.itemData.HasTag(n"Recipe") || Equals(data.itemType, gamedataItemType.Con_Ammo) || Equals(data.itemType, gamedataItemType.Wea_Fists) || Equals(data.lootItemType, LootItemType.Quest)) {
      inkTextRef.SetText(this.m_priceText, "N/A");
    } else {
      inkTextRef.SetText(this.m_priceText, IntToString(RoundF(data.price) * data.itemData.GetQuantity()));
    };
  }

  public final func NEW_Update(data: wref<UIInventoryItem>, player: wref<PlayerPuppet>, m_overridePrice: Int32) -> Void {
    let itemData: wref<gameItemData>;
    let itemType: gamedataItemType;
    inkTextRef.SetText(this.m_weightText, FloatToStringPrec(data.GetWeight(), 1));
    itemType = data.GetItemType();
    itemData = data.GetItemData();
    if NotEquals(this.m_tooltipDisplayContext, InventoryTooltipDisplayContext.Vendor) && (itemData.HasTag(n"Shard") || itemData.HasTag(n"Recipe") || Equals(itemType, gamedataItemType.Con_Ammo) || Equals(itemType, gamedataItemType.Wea_Fists)) {
      inkTextRef.SetText(this.m_priceText, "N/A");
    } else {
      if m_overridePrice >= 0 {
        inkTextRef.SetText(this.m_priceText, IntToString(m_overridePrice));
      } else {
        if Equals(this.m_itemDisplayContext, ItemDisplayContext.Vendor) {
          inkTextRef.SetText(this.m_priceText, IntToString(RoundF(data.GetBuyPrice()) * data.GetQuantity()));
        } else {
          inkTextRef.SetText(this.m_priceText, IntToString(RoundF(data.GetSellPrice()) * data.GetQuantity()));
        };
      };
    };
  }
}

public class ItemTooltipAttributeRequirement extends inkLogicController {

  private edit let m_labelRef: inkTextRef;

  public final func SetData(data: ref<MinimalItemTooltipDataStatRequirement>) -> Void {
    let textParams: ref<inkTextParams> = new inkTextParams();
    textParams.AddNumber("value", data.statValue);
    textParams.AddString("statName", data.statName);
    textParams.AddString("statColor", data.statColor);
    inkTextRef.SetLocalizedTextScript(this.m_labelRef, data.statLocKey, textParams);
  }
}
