
public abstract class AGenericTooltipController extends inkLogicController {

  protected let m_Root: wref<inkCompoundWidget>;

  protected cb func OnInitialize() -> Bool {
    this.m_Root = this.GetRootCompoundWidget();
  }

  public func SetStyle(styleResPath: ResRef) -> Void;

  public func Show() -> Void {
    this.m_Root.SetVisible(true);
    this.m_Root.SetAffectsLayoutWhenHidden(true);
  }

  public func Hide() -> Void {
    this.m_Root.SetVisible(false);
    this.m_Root.SetAffectsLayoutWhenHidden(false);
  }

  public func SetData(tooltipData: ref<ATooltipData>) -> Void;

  public func Refresh() -> Void;
}

public class IdentifiedWrappedTooltipData extends ATooltipData {

  public let m_identifier: CName;

  public let m_tooltipOwner: EntityID;

  public let m_data: ref<ATooltipData>;

  public final static func Make(identifier: CName, opt data: ref<ATooltipData>) -> ref<IdentifiedWrappedTooltipData> {
    let instance: ref<IdentifiedWrappedTooltipData> = new IdentifiedWrappedTooltipData();
    instance.m_identifier = identifier;
    instance.m_data = data;
    return instance;
  }
}

public class UIInventoryItemTooltipWrapper extends ATooltipData {

  public let m_data: wref<UIInventoryItem>;

  public let m_displayContext: ref<ItemDisplayContextData>;

  @default(UIInventoryItemTooltipWrapper, -1)
  public let m_overridePrice: Int32;

  public let m_comparisonData: ref<UIInventoryItemComparisonManager>;

  public final static func Make(data: wref<UIInventoryItem>, displayContext: ref<ItemDisplayContextData>) -> ref<UIInventoryItemTooltipWrapper> {
    let instance: ref<UIInventoryItemTooltipWrapper> = new UIInventoryItemTooltipWrapper();
    instance.m_data = data;
    instance.m_displayContext = displayContext;
    return instance;
  }
}
