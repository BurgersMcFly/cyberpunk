
public class MenuScenario_ArcadeMinigame extends MenuScenario_BaseMenu {

  protected cb func OnEnterScenario(prevScenario: CName, userData: ref<IScriptable>) -> Bool {
    this.SwitchMenu(n"roach_race");
  }

  protected cb func OnBack() -> Bool;

  protected cb func OnCloseHubMenuRequest() -> Bool;

  protected cb func OnArcadeMinigameEnd() -> Bool {
    this.GotoIdleState();
  }
}
