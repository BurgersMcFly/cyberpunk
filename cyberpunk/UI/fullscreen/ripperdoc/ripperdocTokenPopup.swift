
public class RipperdocTokenPopup extends inkGameController {

  protected edit let m_option1: inkWidgetRef;

  protected edit let m_option2: inkWidgetRef;

  protected edit let m_option3: inkWidgetRef;

  protected edit let m_buttonCancel: inkWidgetRef;

  private edit let m_buttonHintsRoot: inkWidgetRef;

  private let m_data: ref<RipperdocTokenPopupData>;

  private edit let m_libraryPath: inkWidgetLibraryReference;

  protected cb func OnInitialize() -> Bool {
    this.RegisterToGlobalInputCallback(n"OnPostOnPress", this, n"OnHandlePressInput");
    this.m_data = this.GetRootWidget().GetUserData(n"RipperdocTokenPopupData") as RipperdocTokenPopupData;
    inkWidgetRef.RegisterToCallback(this.m_buttonCancel, n"OnRelease", this, n"OnCancelClick");
    inkWidgetRef.RegisterToCallback(this.m_option1, n"OnRelease", this, n"OnOption1Click");
    inkWidgetRef.RegisterToCallback(this.m_option2, n"OnRelease", this, n"OnOption2Click");
    inkWidgetRef.RegisterToCallback(this.m_option3, n"OnRelease", this, n"OnOption3Click");
    this.SetButtonHints();
  }

  protected cb func OnUninitialize() -> Bool {
    this.UnregisterFromGlobalInputCallback(n"OnPostOnPress", this, n"OnHandlePressInput");
  }

  private final func SetButtonHints() -> Void {
    this.AddButtonHints(n"UI_Cancel", "UI-ResourceExports-Cancel");
  }

  private final func AddButtonHints(actionName: CName, label: String) -> Void {
    let buttonHint: ref<LabelInputDisplayController> = this.SpawnFromExternal(inkWidgetRef.Get(this.m_buttonHintsRoot), inkWidgetLibraryResource.GetPath(this.m_libraryPath.widgetLibrary), this.m_libraryPath.widgetItem).GetController() as LabelInputDisplayController;
    buttonHint.SetInputActionLabel(actionName, label);
  }

  protected cb func OnHandlePressInput(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"cancel") {
      this.Close(false, -1);
    };
  }

  protected cb func OnOption1Click(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.Close(true, 0);
    };
  }

  protected cb func OnOption2Click(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.Close(true, 1);
    };
  }

  protected cb func OnOption3Click(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.Close(true, 2);
    };
  }

  protected cb func OnCancelClick(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.Close(false, -1);
    };
  }

  private final func Close(success: Bool, option: Int32) -> Void {
    let closeData: ref<RipperdocTokenPopupCloseData> = new RipperdocTokenPopupCloseData();
    closeData.confirm = success;
    closeData.chosenOptionIndex = option;
    closeData.cwItemData = this.m_data.cwItemData;
    this.m_data.token.TriggerCallback(closeData);
  }
}
