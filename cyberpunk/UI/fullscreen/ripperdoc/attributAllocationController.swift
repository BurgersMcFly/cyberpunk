
public class AttributeAllocationController extends inkLogicController {

  private edit let m_pointsContainer: inkCompoundRef;

  private edit let m_attributeName: inkTextRef;

  private edit let m_attributePoints: inkTextRef;

  private edit let m_attributeIcon: inkImageRef;

  private let m_data: ref<AttributeAllocationData>;

  public final func Setup(data: ref<AttributeAllocationData>) -> Void {
    this.m_data = data;
    inkImageRef.SetTexturePart(this.m_attributeIcon, this.GetIconAtlasPart(this.m_data.AttributeType));
    inkTextRef.SetText(this.m_attributeName, TweakDBInterface.GetStatRecord(TDBID.Create("BaseStats." + EnumValueToString("gamedataStatType", Cast<Int64>(EnumInt(this.m_data.AttributeType))))).LocalizedName());
    this.UpdatePointsValues();
  }

  public final func ChangeHoverState(hover: Bool, opt value: Int32) -> Void {
    let state: CName;
    if hover {
      state = value > this.m_data.TotalPoints - this.m_data.AllocatedPoints ? n"Unavailable" : n"Available";
      inkTextRef.SetText(this.m_attributePoints, IntToString(this.m_data.AllocatedPoints + value) + "/" + IntToString(this.m_data.TotalPoints));
      this.GetRootWidget().SetState(state);
    } else {
      this.GetRootWidget().SetState(n"Default");
      inkTextRef.SetText(this.m_attributePoints, IntToString(this.m_data.AllocatedPoints) + "/" + IntToString(this.m_data.TotalPoints));
    };
  }

  public final func UpdateData(allocatedPoints: Int32, totalPoints: Int32) -> Void {
    this.m_data.TotalPoints = totalPoints;
    this.m_data.AllocatedPoints = allocatedPoints;
    this.UpdatePointsValues();
  }

  public final func GetStatType() -> gamedataStatType {
    return this.m_data.AttributeType;
  }

  private final func UpdatePointsValues() -> Void {
    let state: CName = this.m_data.AllocatedPoints > this.m_data.TotalPoints ? n"Unavailable" : n"Default";
    this.GetRootWidget().SetState(state);
    inkTextRef.SetText(this.m_attributePoints, IntToString(this.m_data.AllocatedPoints) + "/" + IntToString(this.m_data.TotalPoints));
  }

  protected final func GetIconAtlasPart(attribute: gamedataStatType) -> CName {
    switch attribute {
      case gamedataStatType.Strength:
        return n"ico_body";
      case gamedataStatType.Reflexes:
        return n"ico_ref";
      case gamedataStatType.TechnicalAbility:
        return n"ico_tech";
      case gamedataStatType.Cool:
        return n"ico_cool";
      case gamedataStatType.Intelligence:
        return n"ico_int";
    };
    return n"undiscovered";
  }
}
