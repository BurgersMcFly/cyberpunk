
public class RipperDocItemBoughtCallback extends InventoryScriptCallback {

  private let eventTarget: wref<RipperDocGameController>;

  public final func Bind(eventTargetArg: ref<RipperDocGameController>) -> Void {
    this.eventTarget = eventTargetArg;
  }

  public func OnItemAdded(itemIDArg: ItemID, itemData: wref<gameItemData>, flaggedAsSilent: Bool) -> Void {
    this.eventTarget.OnItemBought(itemIDArg, itemData);
  }
}

public class RipperDocGameController extends gameuiMenuGameController {

  private edit let m_TooltipsManagerRef: inkWidgetRef;

  private edit let m_buttonHintsManagerRef: inkWidgetRef;

  private edit let m_animationControllerContainer: inkWidgetRef;

  private edit let m_ripperdocIdContainerV1: inkWidgetRef;

  private edit let m_ripperdocIdContainerV2: inkWidgetRef;

  private edit let m_frontalCortexAnchor: inkCompoundRef;

  private edit let m_ocularCortexAnchor: inkCompoundRef;

  private edit let m_leftMiddleGridAnchor: inkCompoundRef;

  private edit let m_leftButtomGridAnchor: inkCompoundRef;

  private edit let m_rightTopGridAnchor: inkCompoundRef;

  private edit let m_rightButtomGridAnchor: inkCompoundRef;

  private edit let m_skeletonAnchor: inkCompoundRef;

  private edit let m_handsAnchor: inkCompoundRef;

  private edit let m_playerMoney: inkTextRef;

  private edit let m_playerMoneyHolder: inkWidgetRef;

  private edit let m_allocationPointContainerDefault: inkCompoundRef;

  private edit let m_cyberwareSlotsList: inkCompoundRef;

  private edit let m_cyberwareVirtualGrid: inkVirtualCompoundRef;

  private edit let m_radioGroupRef: inkWidgetRef;

  private edit let m_cyberwareInfoContainer: inkCompoundRef;

  private edit let m_itemsListScrollAreaContainer: inkWidgetRef;

  private edit let m_sortingButton: inkWidgetRef;

  private edit let m_sortingDropdown: inkWidgetRef;

  private let m_mode: RipperdocModes;

  private let m_screen: CyberwareScreenType;

  private let m_player: wref<PlayerPuppet>;

  private let m_buttonHintsController: wref<ButtonHints>;

  private let m_TooltipsManager: wref<gameuiTooltipsManager>;

  private let m_InventoryManager: ref<InventoryDataManagerV2>;

  private let m_uiScriptableSystem: wref<UIScriptableSystem>;

  private let m_menuEventDispatcher: wref<inkMenuEventDispatcher>;

  private let m_ripperdocTokenManager: ref<RipperdocTokenManager>;

  private let m_vendorUserData: ref<VendorUserData>;

  private let m_VendorDataManager: ref<VendorDataManager>;

  private let m_soldItems: ref<SoldItemsCache>;

  private let m_VendorBlackboard: wref<IBlackboard>;

  private let m_equipmentBlackboard: wref<IBlackboard>;

  private let m_equipmentBlackboardCallback: ref<CallbackHandle>;

  private let m_tokenBlackboard: wref<IBlackboard>;

  private let m_tokenBlackboardCallback: ref<CallbackHandle>;

  private let m_cyberwareInfo: wref<AGenericTooltipController>;

  private let m_cyberwareInfoType: CyberwareInfoType;

  private let m_virtualCyberwareListController: wref<inkVirtualGridController>;

  private let m_cyberwareClassifier: ref<CyberwareTemplateClassifier>;

  private let m_cyberwareDataSource: ref<ScriptableDataSource>;

  private let m_cyberwaregDataView: ref<CyberwareDataView>;

  private let m_currentFilter: RipperdocFilter;

  private let m_radioGroup: wref<FilterRadioGroup>;

  private let m_ripperId: wref<RipperdocIdPanel>;

  private let m_selectedArea: gamedataEquipmentArea;

  private let m_equipmentGrid: wref<CyberwareInventoryMiniGrid>;

  private let m_inventoryListener: ref<InventoryScriptListener>;

  private let m_cybewareGrids: array<wref<CyberwareInventoryMiniGrid>>;

  private let m_isActivePanel: Bool;

  private let m_equiped: Bool;

  private let m_activeSlotIndex: Int32;

  private let m_animationController: wref<RipperdocScreenAnimationController>;

  private let m_tokenPopup: ref<inkGameNotificationToken>;

  private let m_useAlternativeSwitch: Bool;

  protected cb func OnSetUserData(userData: ref<IScriptable>) -> Bool {
    this.m_vendorUserData = userData as VendorUserData;
  }

  protected cb func OnInitialize() -> Bool {
    let ripperdocIdRoot: inkWidgetRef;
    let vendorData: VendorData;
    let vendorPanelData: ref<VendorPanelData>;
    this.m_player = this.GetPlayerControlledObject() as PlayerPuppet;
    this.m_useAlternativeSwitch = GameInstance.GetTransactionSystem(this.m_player.GetGame()).UseAlternativeCyberware();
    if IsDefined(this.m_vendorUserData) {
      vendorPanelData = this.m_vendorUserData.vendorData;
      vendorData = vendorPanelData.data;
      this.m_screen = CyberwareScreenType.Ripperdoc;
      this.m_VendorDataManager = new VendorDataManager();
      this.m_VendorDataManager.Initialize(this.GetPlayerControlledObject(), vendorData.entityID);
      inkWidgetRef.SetVisible(this.m_ripperdocIdContainerV1, !this.m_useAlternativeSwitch);
      inkWidgetRef.SetVisible(this.m_ripperdocIdContainerV2, this.m_useAlternativeSwitch);
      ripperdocIdRoot = this.m_useAlternativeSwitch ? this.m_ripperdocIdContainerV2 : this.m_ripperdocIdContainerV1;
      this.m_ripperId = this.SpawnFromLocal(inkWidgetRef.Get(ripperdocIdRoot), n"ripperdoc_id").GetController() as RipperdocIdPanel;
      this.m_ripperId.SetName(this.m_VendorDataManager.GetVendorName());
      this.m_ripperId.SetFeatureVersion(this.m_useAlternativeSwitch);
      inkWidgetRef.SetVisible(this.m_playerMoneyHolder, true);
      this.UpdateMoney();
    } else {
      this.m_screen = CyberwareScreenType.Inventory;
      inkWidgetRef.SetVisible(this.m_playerMoneyHolder, false);
    };
    this.RegisterInventoryListener(this.GetPlayerControlledObject());
    this.Init();
    this.m_soldItems = new SoldItemsCache();
    inkCompoundRef.RemoveAllChildren(this.m_cyberwareSlotsList);
    this.m_virtualCyberwareListController = inkWidgetRef.GetControllerByType(this.m_cyberwareVirtualGrid, n"inkVirtualGridController") as inkVirtualGridController;
    this.m_cyberwareClassifier = new CyberwareTemplateClassifier();
    this.m_cyberwareDataSource = new ScriptableDataSource();
    this.m_cyberwaregDataView = new CyberwareDataView();
    this.m_cyberwaregDataView.SetSource(this.m_cyberwareDataSource);
    this.m_virtualCyberwareListController.SetClassifier(this.m_cyberwareClassifier);
    this.m_virtualCyberwareListController.SetSource(this.m_cyberwaregDataView);
    this.m_selectedArea = gamedataEquipmentArea.Invalid;
    this.SetupSorting();
    GameInstance.GetTelemetrySystem(this.GetPlayerControlledObject().GetGame()).LogVendorMenuState(this.m_VendorDataManager.GetVendorID(), true);
    super.OnInitialize();
  }

  protected cb func OnUninitialize() -> Bool {
    let vendorData: VendorData;
    vendorData.isActive = false;
    this.m_VendorBlackboard.SetVariant(GetAllBlackboardDefs().UI_Vendor.VendorData, ToVariant(vendorData), true);
    this.m_virtualCyberwareListController.SetClassifier(null);
    this.m_virtualCyberwareListController.SetSource(null);
    this.m_cyberwaregDataView.SetSource(null);
    this.m_cyberwareClassifier = null;
    this.m_cyberwareDataSource = null;
    this.m_cyberwaregDataView = null;
    this.UnregisterInventoryListener(this.GetPlayerControlledObject());
    this.m_InventoryManager.UnInitialize();
    this.UnregisterBlackboard();
    this.m_menuEventDispatcher.UnregisterFromEvent(n"OnBack", this, n"OnBack");
    GameInstance.GetTelemetrySystem(this.GetPlayerControlledObject().GetGame()).LogVendorMenuState(this.m_VendorDataManager.GetVendorID(), false);
    this.m_equipmentGrid = null;
    ArrayClear(this.m_cybewareGrids);
    super.OnUninitialize();
  }

  private final func Init() -> Void {
    this.m_TooltipsManager = inkWidgetRef.GetControllerByType(this.m_TooltipsManagerRef, n"gameuiTooltipsManager") as gameuiTooltipsManager;
    this.m_TooltipsManager.Setup(ETooltipsStyle.Menus);
    this.SetFilters();
    this.m_buttonHintsController = this.SpawnFromExternal(inkWidgetRef.Get(this.m_buttonHintsManagerRef), r"base\\gameplay\\gui\\common\\buttonhints.inkwidget", n"Root").GetController() as ButtonHints;
    this.m_InventoryManager = new InventoryDataManagerV2();
    this.m_InventoryManager.Initialize(this.m_player);
    this.m_uiScriptableSystem = UIScriptableSystem.GetInstance(this.m_player.GetGame());
    this.m_cyberwaregDataView.BindUIScriptableSystem(this.m_uiScriptableSystem);
    this.RegisterBlackboard(this.GetPlayerControlledObject());
    this.PrepareCyberwareSlots();
    this.m_animationController = inkWidgetRef.GetControllerByType(this.m_animationControllerContainer, n"RipperdocScreenAnimationController") as RipperdocScreenAnimationController;
    this.m_animationController.SetGender(Equals(this.m_player.GetResolvedGenderName(), n"Female"));
    this.m_ripperdocTokenManager = new RipperdocTokenManager();
    this.m_ripperdocTokenManager.Initialize(this.m_player);
    this.OpenDefaultMode();
    this.m_ripperId.PlayIntoAnimation();
    this.PlayLibraryAnimation(n"Paperdoll_default_tab_intro");
  }

  protected cb func OnVendorHubMenuChanged(evt: ref<VendorHubMenuChanged>) -> Bool {
    this.m_isActivePanel = Equals(evt.item, HubVendorMenuItems.Cyberware);
  }

  private final func SetFilters() -> Void {
    this.m_radioGroup = inkWidgetRef.GetControllerByType(this.m_radioGroupRef, n"FilterRadioGroup") as FilterRadioGroup;
    this.m_radioGroup.SetData(3, this.m_TooltipsManager, 0);
    this.m_radioGroup.RegisterToCallback(n"OnValueChanged", this, n"OnFilterChange");
    this.m_currentFilter = RipperdocFilter.All;
  }

  protected cb func OnFilterChange(controller: wref<inkRadioGroupController>, selectedIndex: Int32) -> Bool {
    this.PlaySound(n"Button", n"OnPress");
    this.m_cyberwaregDataView.SetFilterType(IntEnum<RipperdocFilter>(selectedIndex));
    this.PlayLibraryAnimation(n"filter_change");
    (inkWidgetRef.GetController(this.m_itemsListScrollAreaContainer) as inkScrollController).SetScrollPosition(0.00);
  }

  private final func SetupSorting() -> Void {
    let controller: ref<DropdownListController>;
    let sortingButtonController: ref<DropdownButtonController>;
    inkWidgetRef.RegisterToCallback(this.m_sortingButton, n"OnRelease", this, n"OnSortingButtonClicked");
    controller = inkWidgetRef.GetController(this.m_sortingDropdown) as DropdownListController;
    sortingButtonController = inkWidgetRef.GetController(this.m_sortingButton) as DropdownButtonController;
    controller.Setup(this, SortingDropdownData.GetDefaultDropdownOptions(), sortingButtonController);
    sortingButtonController.SetData(SortingDropdownData.GetDropdownOption(controller.GetData(), ItemSortMode.Default));
  }

  protected cb func OnDropdownItemClickedEvent(evt: ref<DropdownItemClickedEvent>) -> Bool {
    let sortingButtonController: ref<DropdownButtonController>;
    let identifier: ItemSortMode = FromVariant<ItemSortMode>(evt.identifier);
    let data: ref<DropdownItemData> = SortingDropdownData.GetDropdownOption((inkWidgetRef.GetController(this.m_sortingDropdown) as DropdownListController).GetData(), identifier);
    this.PlaySound(n"Button", n"OnPress");
    if IsDefined(data) {
      sortingButtonController = inkWidgetRef.GetController(this.m_sortingButton) as DropdownButtonController;
      sortingButtonController.SetData(data);
      this.m_cyberwaregDataView.SetSortMode(identifier);
    };
  }

  protected cb func OnSortingButtonClicked(evt: ref<inkPointerEvent>) -> Bool {
    let controller: ref<DropdownListController>;
    if evt.IsAction(n"click") {
      this.PlaySound(n"Button", n"OnPress");
      controller = inkWidgetRef.GetController(this.m_sortingDropdown) as DropdownListController;
      controller.Toggle();
    };
  }

  private final func UpdateMoney() -> Void {
    let vendorMoney: Int32 = MarketSystem.GetVendorMoney(this.m_VendorDataManager.GetVendorInstance());
    this.m_ripperId.SetMoney(vendorMoney);
    inkTextRef.SetText(this.m_playerMoney, IntToString(this.m_VendorDataManager.GetLocalPlayerCurrencyAmount()));
  }

  private final func RegisterInventoryListener(player: ref<GameObject>) -> Void {
    let itemBoughtCallback: ref<RipperDocItemBoughtCallback> = new RipperDocItemBoughtCallback();
    itemBoughtCallback.itemID = ItemID.None();
    itemBoughtCallback.Bind(this);
    this.m_inventoryListener = GameInstance.GetTransactionSystem(player.GetGame()).RegisterInventoryListener(player, itemBoughtCallback);
  }

  private final func UnregisterInventoryListener(player: ref<GameObject>) -> Void {
    if IsDefined(this.m_inventoryListener) {
      GameInstance.GetTransactionSystem(player.GetGame()).UnregisterInventoryListener(player, this.m_inventoryListener);
      this.m_inventoryListener = null;
    };
  }

  public final func OnItemBought(itemID: ItemID, itemData: wref<gameItemData>) -> Void {
    this.m_soldItems.RemoveItem(itemID, 1);
    this.m_InventoryManager.MarkToRebuild();
    this.UpdateMoney();
    this.SetInventoryCWList();
    this.EquipCyberware(itemData);
  }

  protected cb func OnUIVendorItemSoldEvent(evt: ref<UIVendorItemsSoldEvent>) -> Bool {
    let i: Int32 = 0;
    while i < ArraySize(evt.itemsID) {
      this.m_soldItems.AddItem(evt.itemsID[i], evt.quantity[i], evt.piecesPrice[i]);
      i += 1;
    };
    this.m_InventoryManager.MarkToRebuild();
    this.UpdateMoney();
    this.SetInventoryCWList();
  }

  private final func EquipCyberware(itemData: wref<gameItemData>) -> Void {
    let additionalInfo: ref<VendorRequirementsNotMetNotificationData>;
    let equipRequest: ref<EquipRequest>;
    let notification: ref<UIMenuNotificationEvent>;
    if !EquipmentSystem.GetInstance(this.m_player).GetPlayerData(this.m_player).IsEquippable(itemData) {
      notification = new UIMenuNotificationEvent();
      notification.m_notificationType = UIMenuNotificationType.VendorRequirementsNotMet;
      additionalInfo = new VendorRequirementsNotMetNotificationData();
      additionalInfo.m_data = this.GetEquipRequirements(itemData);
      notification.m_additionalInfo = ToVariant(additionalInfo);
      GameInstance.GetUISystem(this.m_player.GetGame()).QueueEvent(notification);
      return;
    };
    this.m_activeSlotIndex = this.m_equipmentGrid.GetSlotToEquipe(itemData.GetID());
    this.m_equiped = false;
    equipRequest = new EquipRequest();
    equipRequest.owner = this.m_player;
    equipRequest.itemID = itemData.GetID();
    equipRequest.slotIndex = this.m_activeSlotIndex;
    this.PlaySound(n"ItemCyberware", n"OnInstall");
    GameInstance.GetScriptableSystemsContainer(this.m_player.GetGame()).Get(n"EquipmentSystem").QueueRequest(equipRequest);
  }

  private final func GetEquipRequirements(itemData: ref<gameItemData>) -> SItemStackRequirementData {
    let data: SItemStackRequirementData;
    let i: Int32;
    let prereqs: array<wref<IPrereq_Record>>;
    let statPrereq: ref<StatPrereq_Record>;
    let itemRecord: wref<Item_Record> = RPGManager.GetItemRecord(itemData.GetID());
    itemRecord.EquipPrereqs(prereqs);
    if itemRecord.UsesVariants() {
      itemRecord.GetVariantsItem(itemData.GetVariant()).VariantPrereqs(prereqs);
    };
    i = 0;
    while i < ArraySize(prereqs) {
      statPrereq = prereqs[i] as StatPrereq_Record;
      if IsDefined(statPrereq) {
        data.statType = IntEnum<gamedataStatType>(Cast<Int32>(EnumValueFromName(n"gamedataStatType", statPrereq.StatType())));
        data.requiredValue = statPrereq.ValueToCheck();
        return data;
      };
      i += 1;
    };
    return data;
  }

  protected final func RegisterBlackboard(player: ref<GameObject>) -> Void {
    this.m_equipmentBlackboard = GameInstance.GetBlackboardSystem(player.GetGame()).Get(GetAllBlackboardDefs().UI_Equipment);
    if IsDefined(this.m_equipmentBlackboard) {
      this.m_equipmentBlackboardCallback = this.m_equipmentBlackboard.RegisterDelayedListenerVariant(GetAllBlackboardDefs().UI_Equipment.itemEquipped, this, n"OnItemEquiped");
    };
    this.m_tokenBlackboard = GameInstance.GetBlackboardSystem(player.GetGame()).Get(GetAllBlackboardDefs().TokenUpgradedCyberwareBlackboard);
    if IsDefined(this.m_tokenBlackboard) {
      this.m_tokenBlackboardCallback = this.m_tokenBlackboard.RegisterDelayedListenerVariant(GetAllBlackboardDefs().TokenUpgradedCyberwareBlackboard.CyberwareTypes, this, n"OnItemUpgrade");
    };
    this.m_VendorBlackboard = this.GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_Vendor);
  }

  protected final func UnregisterBlackboard() -> Void {
    if IsDefined(this.m_equipmentBlackboard) {
      this.m_equipmentBlackboard.UnregisterListenerVariant(GetAllBlackboardDefs().UI_Equipment.itemEquipped, this.m_equipmentBlackboardCallback);
    };
    this.m_VendorBlackboard = null;
  }

  private final func PrepareCyberwareSlots() -> Void {
    inkCompoundRef.RemoveAllChildren(this.m_rightButtomGridAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_frontalCortexAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_ocularCortexAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_leftMiddleGridAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_leftButtomGridAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_rightTopGridAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_skeletonAnchor);
    inkCompoundRef.RemoveAllChildren(this.m_handsAnchor);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.FrontalCortexCW, this.m_frontalCortexAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.EyesCW, this.m_ocularCortexAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.CardiovascularSystemCW, this.m_leftMiddleGridAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.ImmuneSystemCW, this.m_leftButtomGridAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.NervousSystemCW, this.m_leftButtomGridAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.IntegumentarySystemCW, this.m_leftButtomGridAnchor, inkEHorizontalAlign.Right);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.SystemReplacementCW, this.m_rightTopGridAnchor, inkEHorizontalAlign.Left);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.MusculoskeletalSystemCW, this.m_skeletonAnchor, inkEHorizontalAlign.Left);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.HandsCW, this.m_handsAnchor, inkEHorizontalAlign.Left);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.ArmsCW, this.m_rightButtomGridAnchor, inkEHorizontalAlign.Left);
    this.SpawnCWAreaGrid(gamedataEquipmentArea.LegsCW, this.m_rightButtomGridAnchor, inkEHorizontalAlign.Left);
  }

  private final func SpawnCWAreaGrid(equipArea: gamedataEquipmentArea, parentRef: inkCompoundRef, align: inkEHorizontalAlign) -> Void {
    let gridUserData: ref<GridUserData> = new GridUserData();
    gridUserData.equipArea = equipArea;
    gridUserData.align = align;
    let widgetName: CName = Equals(align, inkEHorizontalAlign.Right) ? n"cyberwareInventoryMiniGridLeft" : n"cyberwareInventoryMiniGridRight";
    this.AsyncSpawnFromLocal(inkWidgetRef.Get(parentRef), widgetName, this, n"OnGridSpawned", gridUserData);
  }

  protected cb func OnGridSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    let amountOfNewItems: Int32;
    let cyberwares: array<InventoryItemData>;
    let i: Int32;
    let itemData: InventoryItemData;
    let minigridController: ref<CyberwareInventoryMiniGrid>;
    let numSlots: Int32;
    let gridUserData: ref<GridUserData> = userData as GridUserData;
    widget.SetHAlign(gridUserData.align);
    minigridController = widget.GetController() as CyberwareInventoryMiniGrid;
    numSlots = this.m_InventoryManager.GetNumberOfSlots(gridUserData.equipArea);
    i = 0;
    while i < numSlots {
      itemData = this.m_InventoryManager.GetItemDataEquippedInArea(gridUserData.equipArea, i);
      InventoryItemData.SetEquipmentArea(itemData, gridUserData.equipArea);
      ArrayPush(cyberwares, itemData);
      i += 1;
    };
    amountOfNewItems = Equals(this.m_screen, CyberwareScreenType.Ripperdoc) ? this.GetAmountOfAvailableItems(gridUserData.equipArea) : this.GetAmountOfMods(itemData);
    minigridController.SetupData(gridUserData.equipArea, cyberwares, amountOfNewItems, this, Equals(this.m_mode, RipperdocModes.Item) ? n"OnEquipmentSlotClick" : n"OnPreviewCyberwareClick", this.m_screen, InventoryItemData.GetAttachmentsSize(itemData) > 0);
    ArrayPush(this.m_cybewareGrids, minigridController);
  }

  private final func UpdateCWAreaGrid(selectedArea: gamedataEquipmentArea) -> Void {
    let amountOfNewItems: Int32;
    let cyberwares: array<InventoryItemData>;
    let itemData: InventoryItemData;
    let numSlots: Int32 = this.m_InventoryManager.GetNumberOfSlots(selectedArea);
    let i: Int32 = 0;
    while i < numSlots {
      itemData = this.m_InventoryManager.GetItemDataEquippedInArea(selectedArea, i);
      InventoryItemData.SetEquipmentArea(itemData, selectedArea);
      ArrayPush(cyberwares, itemData);
      i += 1;
    };
    amountOfNewItems = Equals(this.m_screen, CyberwareScreenType.Ripperdoc) ? this.GetAmountOfAvailableItems(selectedArea) : this.GetAmountOfMods(itemData);
    i = 0;
    while i < ArraySize(this.m_cybewareGrids) {
      if Equals(this.m_cybewareGrids[i].GetEquipementArea(), selectedArea) {
        this.m_cybewareGrids[i].UpdateData(selectedArea, cyberwares, amountOfNewItems, this.m_screen);
        return;
      };
      i += 1;
    };
  }

  private final func SelectSlot(newSlotIndex: Int32) -> Void {
    this.m_equipmentGrid.SelectSlot(newSlotIndex);
    this.UpdateTooltipData(this.m_equipmentGrid.GetSelectedSlotData());
  }

  private final func UpdateTooltipData(itemData: InventoryItemData) -> Void {
    let targetCyberwareInfoType: CyberwareInfoType;
    let tooltipsData: ref<InventoryTooltipData>;
    if InventoryItemData.IsEmpty(itemData) {
      inkWidgetRef.SetVisible(this.m_cyberwareInfoContainer, false);
      return;
    };
    if InventoryItemData.GetGameItemData(itemData).HasTag(n"Cyberdeck") {
      targetCyberwareInfoType = CyberwareInfoType.Cyberdeck;
    };
    inkWidgetRef.SetVisible(this.m_cyberwareInfoContainer, true);
    if this.m_cyberwareInfo == null || NotEquals(this.m_cyberwareInfoType, targetCyberwareInfoType) {
      inkCompoundRef.RemoveAllChildren(this.m_cyberwareInfoContainer);
      this.m_cyberwareInfo = null;
      if Equals(targetCyberwareInfoType, CyberwareInfoType.Cyberdeck) {
        this.m_cyberwareInfo = this.SpawnFromExternal(inkWidgetRef.Get(this.m_cyberwareInfoContainer), r"base\\gameplay\\gui\\common\\tooltip\\cyberdecktooltip.inkwidget", n"cyberdeckTooltip").GetController() as AGenericTooltipController;
      } else {
        this.m_cyberwareInfo = this.SpawnFromExternal(inkWidgetRef.Get(this.m_cyberwareInfoContainer), r"base\\gameplay\\gui\\common\\tooltip\\itemtooltip.inkwidget", n"itemTooltip").GetController() as AGenericTooltipController;
      };
      this.m_cyberwareInfoType = targetCyberwareInfoType;
    };
    tooltipsData = this.m_InventoryManager.GetTooltipDataForInventoryItem(itemData, InventoryItemData.IsEquipped(itemData));
    this.m_equipmentGrid.UpdateTitle(tooltipsData.itemName);
    this.m_cyberwareInfo.SetData(tooltipsData);
    inkWidgetRef.SetVisible(this.m_cyberwareInfoContainer, true);
    this.PlayLibraryAnimationOnTargets(n"tooltipContainer_change", SelectWidgets(inkWidgetRef.Get(this.m_cyberwareInfoContainer)));
  }

  private final func GetAmountOfAvailableItems(equipArea: gamedataEquipmentArea) -> Int32 {
    let ripperdocInventory: array<InventoryItemData> = this.GetRipperdocItemsForEquipmentArea(equipArea);
    let playerInventory: array<InventoryItemData> = this.m_InventoryManager.GetPlayerInventoryData(equipArea);
    let i: Int32 = 0;
    while i < ArraySize(playerInventory) {
      if !InventoryItemData.IsEquipped(playerInventory[i]) {
        ArrayPush(ripperdocInventory, playerInventory[i]);
      };
      i = i + 1;
    };
    return ArraySize(ripperdocInventory);
  }

  private final func GetAmountOfMods(itemData: InventoryItemData) -> Int32 {
    let attachments: ref<InventoryItemAttachments>;
    let i: Int32;
    let mods: array<InventoryItemData>;
    let modsCount: Int32;
    let slotsCount: Int32 = InventoryItemData.GetAttachmentsSize(itemData);
    if NotEquals(InventoryItemData.GetEquipmentArea(itemData), gamedataEquipmentArea.ArmsCW) && slotsCount > 0 {
      attachments = InventoryItemData.GetAttachment(itemData, 0);
      mods = this.m_InventoryManager.GetPlayerInventoryPartsForItem(InventoryItemData.GetID(itemData), attachments.SlotID);
      return ArraySize(mods);
    };
    i = 0;
    while i < slotsCount {
      attachments = InventoryItemData.GetAttachment(itemData, i);
      mods = this.m_InventoryManager.GetPlayerInventoryPartsForItem(InventoryItemData.GetID(itemData), attachments.SlotID);
      modsCount += ArraySize(mods);
      i += 1;
    };
    return modsCount;
  }

  protected cb func OnVendorUpdated(value: Variant) -> Bool;

  protected cb func OnSetMenuEventDispatcher(menuEventDispatcher: wref<inkMenuEventDispatcher>) -> Bool {
    super.OnSetMenuEventDispatcher(menuEventDispatcher);
    this.m_menuEventDispatcher = menuEventDispatcher;
    this.m_menuEventDispatcher.RegisterToEvent(n"OnBack", this, n"OnBack");
  }

  protected cb func OnBack(userData: ref<IScriptable>) -> Bool {
    switch this.m_mode {
      case RipperdocModes.Default:
        if Equals(this.m_screen, CyberwareScreenType.Inventory) {
          super.OnBack(userData);
        } else {
          if !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.GetPlayerControlledObject(), n"LockInHubMenu") {
            this.m_menuEventDispatcher.SpawnEvent(n"OnVendorClose");
          };
        };
        break;
      case RipperdocModes.Item:
        this.OpenDefaultMode();
    };
  }

  protected cb func OnItemEquiped(value: Variant) -> Bool {
    let attributeEvt: ref<AttributeUpdateEvent>;
    if !this.m_equiped {
      attributeEvt = new AttributeUpdateEvent();
      this.QueueEvent(attributeEvt);
      this.SetInventoryCWList();
      this.SetEquipmentGrid();
      this.PlayLibraryAnimation(n"filter_change");
      this.m_equiped = true;
    };
  }

  protected cb func OnItemUpgrade(value: Variant) -> Bool {
    let attributeEvt: ref<AttributeUpdateEvent> = new AttributeUpdateEvent();
    this.QueueEvent(attributeEvt);
  }

  protected cb func OnPreviewCyberwareClick(evt: ref<inkPointerEvent>) -> Bool {
    let itemController: wref<InventoryItemDisplayController>;
    let itemData: InventoryItemData;
    let openModsScreenEvent: ref<CyberwareTabModsRequest>;
    if evt.IsAction(n"select") {
      itemController = this.GetCyberwareSlotControllerFromTarget(evt);
      this.m_selectedArea = itemController.GetEquipmentArea();
      this.m_activeSlotIndex = itemController.GetSlotIndex();
      this.PlaySound(n"Button", n"OnPress");
      switch this.m_screen {
        case CyberwareScreenType.Ripperdoc:
          this.OpenItemMode();
          break;
        case CyberwareScreenType.Inventory:
          itemData = itemController.GetItemData();
          if InventoryItemData.GetAttachmentsSize(itemData) > 0 {
            openModsScreenEvent = new CyberwareTabModsRequest();
            openModsScreenEvent.open = true;
            openModsScreenEvent.wrapper = new CyberwareDisplayWrapper();
            openModsScreenEvent.wrapper.displayData = itemController.GetItemDisplayData();
            this.QueueEvent(openModsScreenEvent);
          };
      };
    } else {
      if evt.IsAction(n"upgrade_perk") && this.CheckTokenAvailability() {
        this.OpenTokenPopup(itemData);
      };
    };
  }

  protected cb func OnEquipmentSlotClick(evt: ref<inkPointerEvent>) -> Bool {
    let itemController: wref<InventoryItemDisplayController> = this.GetCyberwareSlotControllerFromTarget(evt);
    if evt.IsAction(n"select") {
      this.PlaySound(n"Button", n"OnPress");
      this.SelectSlot(itemController.GetSlotIndex());
    } else {
      if evt.IsAction(n"upgrade_perk") && !InventoryItemData.IsEmpty(itemController.GetItemData()) && this.CheckTokenAvailability() {
        this.OpenTokenPopup(itemController.GetItemData());
      };
    };
  }

  private final func OpenDefaultMode() -> Void {
    this.m_animationController.PlayDefaultScreenOpen(this.m_selectedArea);
    this.m_mode = RipperdocModes.Default;
    this.UpdateCWAreaGrid(this.m_selectedArea);
    this.ProcessRipperdocSlotsModeTutorial();
    this.SetDefaultModeButtonHints();
  }

  private func ReadUICondition(condition: gamedataUICondition) -> Bool {
    switch condition {
      case gamedataUICondition.InSubMenu:
        return Equals(this.m_mode, RipperdocModes.Item);
      case gamedataUICondition.InHandsSubMenu:
        return Equals(this.m_selectedArea, gamedataEquipmentArea.HandsCW);
      case gamedataUICondition.InEyesSubMenu:
        return Equals(this.m_selectedArea, gamedataEquipmentArea.EyesCW);
    };
    return false;
  }

  private final func OpenItemMode() -> Void {
    this.m_equiped = true;
    this.m_mode = RipperdocModes.Item;
    this.SetInventoryCWList();
    this.SetEquipmentGrid();
    this.ProcessRipperdocItemModeTutorial();
    this.SetItemModeButtonHints();
    this.m_animationController.PlayItemSceenOpen(this.m_equipmentGrid.GetRootWidget(), this.m_selectedArea);
  }

  protected final func ProcessRipperdocSlotsModeTutorial() -> Void {
    if GameInstance.GetQuestsSystem(this.m_player.GetGame()).GetFact(n"tutorial_ripperdoc_slots") == 0 && Equals(this.m_screen, CyberwareScreenType.Ripperdoc) {
      GameInstance.GetQuestsSystem(this.m_player.GetGame()).SetFact(n"tutorial_ripperdoc_slots", 1);
    };
  }

  protected final func ProcessRipperdocItemModeTutorial() -> Void {
    if GameInstance.GetQuestsSystem(this.m_player.GetGame()).GetFact(n"tutorial_ripperdoc_buy") == 0 && Equals(this.m_screen, CyberwareScreenType.Ripperdoc) {
      GameInstance.GetQuestsSystem(this.m_player.GetGame()).SetFact(n"tutorial_ripperdoc_buy", 1);
    };
  }

  private final func SetEquipmentGrid() -> Void {
    let widget: wref<inkWidget>;
    let cyberwares: array<InventoryItemData> = this.GetEquippedCWList();
    if this.m_equipmentGrid == null {
      inkCompoundRef.RemoveAllChildren(this.m_cyberwareSlotsList);
      widget = this.SpawnFromLocal(inkWidgetRef.Get(this.m_cyberwareSlotsList), n"cyberwareInventoryMiniGrid");
      widget.SetHAlign(inkEHorizontalAlign.Fill);
      this.m_equipmentGrid = widget.GetController() as CyberwareInventoryMiniGrid;
      this.m_equipmentGrid.SetupData(this.m_selectedArea, cyberwares, 0, this, n"OnEquipmentSlotClick", this.m_screen, false);
    } else {
      this.m_equipmentGrid.UpdateData(this.m_selectedArea, cyberwares);
    };
    this.SelectSlot(this.m_activeSlotIndex);
  }

  private final func GetEquippedCWList() -> array<InventoryItemData> {
    let cyberwares: array<InventoryItemData>;
    let itemData: InventoryItemData;
    let numSlots: Int32 = this.m_InventoryManager.GetNumberOfSlots(this.m_selectedArea);
    let i: Int32 = 0;
    while i < numSlots {
      itemData = this.m_InventoryManager.GetItemDataEquippedInArea(this.m_selectedArea, i);
      InventoryItemData.SetEquipmentArea(itemData, this.m_selectedArea);
      ArrayPush(cyberwares, itemData);
      i += 1;
    };
    return cyberwares;
  }

  private final func SetInventoryCWList() -> Void {
    let cacheItem: ref<SoldItem>;
    let hasBuyBack: Bool;
    let isReqMet: Bool;
    let itemDatas: array<ref<IScriptable>>;
    let itemWrapper: ref<CyberwareDataWrapper>;
    let playerMoney: Int32;
    let ripperdocCyberwares: array<InventoryItemData> = this.GetRipperdocItemsForEquipmentArea(this.m_selectedArea);
    let playerCyberwares: array<InventoryItemData> = this.m_InventoryManager.GetPlayerInventoryData(this.m_selectedArea);
    let i: Int32 = 0;
    while i < ArraySize(playerCyberwares) {
      if !InventoryItemData.IsEquipped(playerCyberwares[i]) {
        InventoryItemData.SetEquipRequirements(playerCyberwares[i], RPGManager.GetEquipRequirements(this.m_player, InventoryItemData.GetGameItemData(playerCyberwares[i])));
        itemWrapper = new CyberwareDataWrapper();
        isReqMet = EquipmentSystem.GetInstance(this.m_player).GetPlayerData(this.m_player).IsEquippable(InventoryItemData.GetGameItemData(playerCyberwares[i]));
        InventoryItemData.SetIsEquippable(playerCyberwares[i], isReqMet);
        InventoryItemData.SetIsVendorItem(playerCyberwares[i], false);
        itemWrapper.InventoryItem = playerCyberwares[i];
        this.m_InventoryManager.GetOrCreateInventoryItemSortData(itemWrapper.InventoryItem, this.m_uiScriptableSystem);
        itemWrapper.IsVendor = false;
        itemWrapper.IsUpgraded = this.m_ripperdocTokenManager.IsItemUpgraded(InventoryItemData.GetID(itemWrapper.InventoryItem));
        ArrayPush(itemDatas, itemWrapper);
      };
      i = i + 1;
    };
    playerMoney = this.m_VendorDataManager.GetLocalPlayerCurrencyAmount();
    i = 0;
    while i < ArraySize(ripperdocCyberwares) {
      cacheItem = this.m_soldItems.GetItem(InventoryItemData.GetID(ripperdocCyberwares[i]));
      if cacheItem != null {
        hasBuyBack = true;
      };
      InventoryItemData.SetEquipRequirements(ripperdocCyberwares[i], RPGManager.GetEquipRequirements(this.m_player, InventoryItemData.GetGameItemData(ripperdocCyberwares[i])));
      itemWrapper = new CyberwareDataWrapper();
      itemWrapper.InventoryItem = ripperdocCyberwares[i];
      this.m_InventoryManager.GetOrCreateInventoryItemSortData(itemWrapper.InventoryItem, this.m_uiScriptableSystem);
      itemWrapper.IsVendor = true;
      itemWrapper.PlayerMoney = playerMoney;
      itemWrapper.IsBuyback = cacheItem != null;
      ArrayPush(itemDatas, itemWrapper);
      i = i + 1;
    };
    if hasBuyBack {
      this.m_radioGroup.AddFilter(EnumInt(RipperdocFilter.Buyback));
    } else {
      this.m_radioGroup.RemoveFilter(EnumInt(RipperdocFilter.Buyback));
    };
    this.m_cyberwareDataSource.Reset(itemDatas);
  }

  protected cb func OnSlotClick(evt: ref<ItemDisplayClickEvent>) -> Bool {
    let additionalInfo: ref<VendorRequirementsNotMetNotificationData>;
    let itemGameData: wref<gameItemData>;
    let type: VendorConfirmationPopupType;
    let vendorNotification: ref<UIMenuNotificationEvent>;
    let itemData: InventoryItemData = evt.itemData;
    if !this.m_isActivePanel || InventoryItemData.IsEmpty(itemData) {
      return false;
    };
    itemGameData = InventoryItemData.GetGameItemData(itemData);
    if evt.actionName.IsAction(n"click") {
      this.PlaySound(n"Button", n"OnPress");
      if InventoryItemData.IsVendorItem(itemData) {
        if !InventoryItemData.IsRequirementMet(itemData) {
          vendorNotification = new UIMenuNotificationEvent();
          vendorNotification.m_notificationType = UIMenuNotificationType.VendorRequirementsNotMet;
          additionalInfo = new VendorRequirementsNotMetNotificationData();
          additionalInfo.m_data = InventoryItemData.GetRequirement(itemData);
          vendorNotification.m_additionalInfo = ToVariant(additionalInfo);
          GameInstance.GetUISystem(this.m_player.GetGame()).QueueEvent(vendorNotification);
        } else {
          if evt.isBuybackStack {
            this.m_VendorDataManager.BuybackItemFromVendor(itemGameData, 1);
          } else {
            if this.m_VendorDataManager.GetBuyingPrice(itemGameData.GetID()) > this.m_VendorDataManager.GetLocalPlayerCurrencyAmount() {
              vendorNotification = new UIMenuNotificationEvent();
              vendorNotification.m_notificationType = UIMenuNotificationType.VNotEnoughMoney;
              GameInstance.GetUISystem(this.m_player.GetGame()).QueueEvent(vendorNotification);
            } else {
              type = EquipmentSystem.GetInstance(this.m_player).GetPlayerData(this.m_player).IsEquippable(itemGameData) ? VendorConfirmationPopupType.BuyAndEquipCyberware : VendorConfirmationPopupType.BuyNotEquipableCyberware;
              this.OpenConfirmationPopup(itemData, this.m_VendorDataManager.GetBuyingPrice(InventoryItemData.GetID(itemData)), type, n"OnBuyConfirmationPopupClosed");
            };
          };
        };
      } else {
        if !InventoryItemData.IsEquipped(itemData) && this.m_equiped {
          this.EquipCyberware(itemGameData);
        };
      };
    } else {
      if evt.actionName.IsAction(n"disassemble_item") && !InventoryItemData.IsEquipped(itemData) && !InventoryItemData.IsVendorItem(itemData) {
        this.OpenConfirmationPopup(itemData, this.m_VendorDataManager.GetSellingPrice(InventoryItemData.GetID(itemData)), VendorConfirmationPopupType.SellCyberware, n"OnSellConfirmationPopupClosed");
      };
    };
  }

  private final func OpenConfirmationPopup(itemData: InventoryItemData, price: Int32, type: VendorConfirmationPopupType, listener: CName) -> Void {
    let data: ref<VendorConfirmationPopupData> = new VendorConfirmationPopupData();
    data.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\vendor_confirmation.inkwidget";
    data.isBlocking = true;
    data.useCursor = true;
    data.queueName = n"modal_popup";
    data.itemData = itemData;
    data.quantity = InventoryItemData.GetQuantity(itemData);
    data.type = type;
    data.price = price;
    this.m_tokenPopup = this.ShowGameNotification(data);
    this.m_tokenPopup.RegisterListener(this, listener);
    this.m_buttonHintsController.Hide();
  }

  protected cb func OnBuyConfirmationPopupClosed(data: ref<inkGameNotificationData>) -> Bool {
    this.m_tokenPopup = null;
    let resultData: ref<VendorConfirmationPopupCloseData> = data as VendorConfirmationPopupCloseData;
    if resultData.confirm {
      this.m_VendorDataManager.BuyItemFromVendor(InventoryItemData.GetGameItemData(resultData.itemData), InventoryItemData.GetQuantity(resultData.itemData));
    };
    this.m_buttonHintsController.Show();
    this.PlaySound(n"Button", n"OnPress");
  }

  protected cb func OnSellConfirmationPopupClosed(data: ref<inkGameNotificationData>) -> Bool {
    this.m_tokenPopup = null;
    let resultData: ref<VendorConfirmationPopupCloseData> = data as VendorConfirmationPopupCloseData;
    if resultData.confirm {
      this.m_VendorDataManager.SellItemToVendor(InventoryItemData.GetGameItemData(resultData.itemData), InventoryItemData.GetQuantity(resultData.itemData));
    };
    this.m_buttonHintsController.Show();
    this.PlaySound(n"Button", n"OnPress");
  }

  private final func OpenTokenPopup(itemData: InventoryItemData) -> Void {
    let data: ref<RipperdocTokenPopupData> = new RipperdocTokenPopupData();
    data.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\ripperdoc_token_popup.inkwidget";
    data.isBlocking = true;
    data.useCursor = true;
    data.cwItemData = itemData;
    data.queueName = n"modal_popup";
    this.m_tokenPopup = this.ShowGameNotification(data);
    this.m_tokenPopup.RegisterListener(this, n"OnBuyTokenPopupClosed");
    this.m_buttonHintsController.Hide();
  }

  protected cb func OnBuyTokenPopupClosed(data: ref<inkGameNotificationData>) -> Bool {
    this.m_tokenPopup = null;
    let resultData: ref<RipperdocTokenPopupCloseData> = data as RipperdocTokenPopupCloseData;
    if resultData.confirm {
      this.m_ripperdocTokenManager.ApplyToken(InventoryItemData.GetID(resultData.cwItemData));
    };
    this.m_buttonHintsController.Show();
    this.PlaySound(n"Button", n"OnPress");
  }

  protected cb func OnCyberwareSlotHoverOver(evt: ref<ItemDisplayHoverOverEvent>) -> Bool {
    this.InventoryItemHoverOver(evt.itemData, evt.isBuybackStack);
    this.m_selectedArea = InventoryItemData.GetEquipmentArea(evt.itemData);
    this.m_animationController.PlayHoverAnimation(this.m_selectedArea, this.m_mode);
  }

  protected cb func OnCyberwareSlotHoverOut(evt: ref<ItemDisplayHoverOutEvent>) -> Bool {
    let hoverEvt: ref<HoverSatateChangedEvent>;
    this.m_TooltipsManager.HideTooltips();
    this.SetInventoryItemButtonHintsHoverOut();
    this.m_animationController.PlayHoverOverAnimation(this.m_selectedArea, this.m_mode);
    hoverEvt = new HoverSatateChangedEvent();
    hoverEvt.isHovered = false;
    this.QueueEvent(hoverEvt);
  }

  private final func InventoryItemHoverOver(itemData: InventoryItemData, isBuybackStack: Bool) -> Void {
    let equippedCyberwares: array<InventoryItemData>;
    let hoverEvt: ref<HoverSatateChangedEvent>;
    let i: Int32;
    let itemTooltipData: ref<InventoryTooltipData>;
    this.m_TooltipsManager.HideTooltips();
    this.SetInventoryItemButtonHintsHoverOver(itemData);
    if InventoryItemData.IsEmpty(itemData) {
      return;
    };
    if InventoryItemData.IsEquipped(itemData) {
      itemTooltipData = this.m_InventoryManager.GetTooltipDataForInventoryItem(itemData, true, InventoryItemData.IsVendorItem(itemData));
    } else {
      equippedCyberwares = this.GetEquippedCWList();
      i = 0;
      while i < ArraySize(equippedCyberwares) {
        if !InventoryItemData.IsEmpty(equippedCyberwares[0]) {
          itemTooltipData = this.m_InventoryManager.GetComparisonTooltipsData(equippedCyberwares[0], itemData, false);
        };
        i += 1;
      };
      if itemTooltipData == null {
        itemTooltipData = this.m_InventoryManager.GetTooltipDataForInventoryItem(itemData, false, InventoryItemData.IsVendorItem(itemData));
      };
      if isBuybackStack {
        itemTooltipData.buyPrice = Cast<Float>(RPGManager.CalculateSellPrice(this.m_VendorDataManager.GetVendorInstance().GetGame(), this.m_VendorDataManager.GetVendorInstance(), InventoryItemData.GetID(itemData)));
      };
      hoverEvt = new HoverSatateChangedEvent();
      hoverEvt.isHovered = true;
      hoverEvt.itemInventoryData = itemData;
      this.QueueEvent(hoverEvt);
    };
    this.ShowCWTooltip(itemData, itemTooltipData);
  }

  private final func ShowCWTooltip(itemData: InventoryItemData, itemTooltipData: ref<InventoryTooltipData>) -> Void {
    if InventoryItemData.GetGameItemData(itemData).HasTag(n"Cyberdeck") {
      this.m_TooltipsManager.ShowTooltip(n"cyberdeckTooltip", itemTooltipData, new inkMargin(60.00, 60.00, 0.00, 0.00));
    } else {
      if Equals(InventoryItemData.GetItemType(itemData), gamedataItemType.Prt_Program) {
        this.m_TooltipsManager.ShowTooltip(n"programTooltip", itemTooltipData, new inkMargin(60.00, 60.00, 0.00, 0.00));
      } else {
        this.m_TooltipsManager.ShowTooltip(n"itemTooltip", itemTooltipData, new inkMargin(60.00, 60.00, 0.00, 0.00));
      };
    };
  }

  private final func SetDefaultModeButtonHints() -> Void {
    this.m_buttonHintsController.RemoveButtonHint(n"back");
    if !StatusEffectSystem.ObjectHasStatusEffectWithTag(this.GetPlayerControlledObject(), n"LockInHubMenu") || Equals(this.m_screen, CyberwareScreenType.Inventory) {
      this.m_buttonHintsController.AddButtonHint(n"back", GetLocalizedText("LocKey#903"));
    };
  }

  private final func SetItemModeButtonHints() -> Void {
    this.m_buttonHintsController.AddButtonHint(n"back", "LocKey#15324");
  }

  private final func SetInventoryItemButtonHintsHoverOver(displayingData: InventoryItemData) -> Void {
    let isEmpty: Bool = InventoryItemData.IsEmpty(displayingData);
    let isEquipped: Bool = InventoryItemData.IsEquipped(displayingData);
    switch this.m_screen {
      case CyberwareScreenType.Ripperdoc:
        if Equals(this.m_mode, RipperdocModes.Default) {
          this.m_buttonHintsController.AddButtonHint(n"select", "LocKey#273");
        } else {
          if Equals(this.m_mode, RipperdocModes.Item) {
            if isEmpty || isEquipped {
              this.m_buttonHintsController.AddButtonHint(n"select", "LocKey#34928");
            } else {
              if InventoryItemData.IsVendorItem(displayingData) {
                this.m_buttonHintsController.AddButtonHint(n"select", "LocKey#17847");
              } else {
                this.m_buttonHintsController.AddButtonHint(n"disassemble_item", "LocKey#17848");
                this.m_buttonHintsController.AddButtonHint(n"select", "LocKey#246");
              };
            };
          };
        };
        if isEquipped && this.CheckTokenAvailability() {
          this.m_buttonHintsController.AddButtonHint(n"upgrade_perk", this.m_ripperdocTokenManager.IsItemUpgraded(InventoryItemData.GetID(displayingData)) ? "LocKey#79251" : "LocKey#79250");
        };
        this.SetCursorContext(n"Hover");
        break;
      case CyberwareScreenType.Inventory:
        if Equals(this.m_mode, RipperdocModes.Default) {
          this.m_buttonHintsController.AddButtonHint(n"select", "LocKey#273");
        };
        this.SetCursorContext(n"Default");
    };
  }

  private final func SetInventoryItemButtonHintsHoverOut() -> Void {
    this.m_buttonHintsController.RemoveButtonHint(n"select");
    this.m_buttonHintsController.RemoveButtonHint(n"upgrade_perk");
    this.m_buttonHintsController.RemoveButtonHint(n"disassemble_item");
  }

  private final func GetCyberwareSlotControllerFromTarget(evt: ref<inkPointerEvent>) -> ref<InventoryItemDisplayController> {
    let widget: ref<inkWidget> = evt.GetCurrentTarget();
    let controller: wref<InventoryItemDisplayController> = widget.GetController() as InventoryItemDisplayController;
    return controller;
  }

  private final func GetRipperdocItemsForEquipmentArea(equipArea: gamedataEquipmentArea) -> array<InventoryItemData> {
    let gameData: ref<gameItemData>;
    let itemData: InventoryItemData;
    let itemDataArray: array<InventoryItemData>;
    let itemRecord: wref<Item_Record>;
    let data: array<ref<VendorGameItemData>> = this.m_VendorDataManager.GetRipperDocItems();
    let owner: wref<GameObject> = this.m_VendorDataManager.GetVendorInstance();
    let i: Int32 = 0;
    while i < ArraySize(data) {
      gameData = data[i].gameItemData;
      itemRecord = TweakDBInterface.GetItemRecord(ItemID.GetTDBID(gameData.GetID()));
      if Equals(itemRecord.EquipArea().Type(), equipArea) {
        itemData = this.m_InventoryManager.GetInventoryItemData(owner, data[i].gameItemData, true);
        InventoryItemData.SetIsVendorItem(itemData, true);
        InventoryItemData.SetIsEquippable(itemData, EquipmentSystem.GetInstance(this.m_player).GetPlayerData(this.m_player).IsEquippable(data[i].gameItemData));
        InventoryItemData.SetIsRequirementMet(itemData, data[i].itemStack.isAvailable);
        InventoryItemData.SetRequirement(itemData, data[i].itemStack.requirement);
        ArrayPush(itemDataArray, itemData);
      };
      i += 1;
    };
    return itemDataArray;
  }

  private final func CheckTokenAvailability() -> Bool {
    return this.m_useAlternativeSwitch && this.m_ripperdocTokenManager.IfPlayerHasTokens();
  }
}

public class CyberwareTemplateClassifier extends inkVirtualItemTemplateClassifier {

  public func ClassifyItem(data: Variant) -> Uint32 {
    return 0u;
  }
}

public class CyberwareDataView extends ScriptableDataView {

  private let m_itemFilterType: RipperdocFilter;

  private let m_itemSortMode: ItemSortMode;

  private let m_uiScriptableSystem: wref<UIScriptableSystem>;

  public final func BindUIScriptableSystem(uiScriptableSystem: wref<UIScriptableSystem>) -> Void {
    this.m_uiScriptableSystem = uiScriptableSystem;
  }

  public final func SetFilterType(type: RipperdocFilter) -> Void {
    this.m_itemFilterType = type;
    this.Filter();
  }

  public func FilterItem(data: ref<IScriptable>) -> Bool {
    let m_wrappedData: ref<CyberwareDataWrapper> = data as CyberwareDataWrapper;
    switch this.m_itemFilterType {
      case RipperdocFilter.Player:
        return !InventoryItemData.IsVendorItem(m_wrappedData.InventoryItem);
      case RipperdocFilter.Vendor:
        return InventoryItemData.IsVendorItem(m_wrappedData.InventoryItem);
      case RipperdocFilter.Buyback:
        return m_wrappedData.IsBuyback;
      default:
        return true;
    };
    return true;
  }

  public final func SetSortMode(mode: ItemSortMode) -> Void {
    this.m_itemSortMode = mode;
    this.EnableSorting();
    this.Sort();
    this.DisableSorting();
  }

  protected func PreSortingInjection(builder: ref<ItemCompareBuilder>) -> ref<ItemCompareBuilder> {
    return builder;
  }

  public func SortItem(left: ref<IScriptable>, right: ref<IScriptable>) -> Bool {
    let leftItem: InventoryItemSortData = InventoryItemData.GetSortData(left as CyberwareDataWrapper.InventoryItem);
    let rightItem: InventoryItemSortData = InventoryItemData.GetSortData(right as CyberwareDataWrapper.InventoryItem);
    switch this.m_itemSortMode {
      case ItemSortMode.NewItems:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).NewItem(this.m_uiScriptableSystem).QualityDesc().ItemType().NameAsc().GetBool();
      case ItemSortMode.NameAsc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).NameAsc().QualityDesc().GetBool();
      case ItemSortMode.NameDesc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).NameDesc().QualityDesc().GetBool();
      case ItemSortMode.QualityAsc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).QualityDesc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.QualityDesc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).QualityAsc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.WeightAsc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).WeightAsc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.WeightDesc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).WeightDesc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.PriceAsc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).PriceAsc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.PriceDesc:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).PriceDesc().NameAsc().QualityDesc().GetBool();
      case ItemSortMode.ItemType:
        return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).ItemType().NameAsc().QualityDesc().GetBool();
    };
    return this.PreSortingInjection(ItemCompareBuilder.Make(leftItem, rightItem)).QualityDesc().ItemType().NameAsc().GetBool();
  }
}

public class CyberwareItemLogicController extends inkVirtualCompoundItemController {

  protected edit let m_slotRoot: inkCompoundRef;

  protected let m_slot: wref<InventoryItemDisplayController>;

  public final func OnDataChanged(value: Variant) -> Void {
    let itemData: ref<CyberwareDataWrapper> = FromVariant<ref<IScriptable>>(value) as CyberwareDataWrapper;
    if !IsDefined(this.m_slot) {
      this.m_slot = ItemDisplayUtils.SpawnCommonSlotController(this, inkWidgetRef.Get(this.m_slotRoot), n"itemDisplay") as InventoryItemDisplayController;
    };
    if itemData.IsVendor {
      this.m_slot.Setup(itemData.InventoryItem, ItemDisplayContext.Vendor, itemData.PlayerMoney >= Cast<Int32>(InventoryItemData.GetBuyPrice(itemData.InventoryItem)));
    } else {
      this.m_slot.Setup(itemData.InventoryItem, ItemDisplayContext.VendorPlayer, true, true);
    };
    this.m_slot.SetBuybackStack(itemData.IsBuyback);
  }
}

public class RipperdocIdPanel extends inkLogicController {

  protected edit let m_fluffContainer: inkWidgetRef;

  protected edit let m_nameLabel: inkTextRef;

  protected edit let m_moneyLabel: inkTextRef;

  public final func SetName(vendorName: String) -> Void {
    if !IsStringValid(vendorName) {
      vendorName = "MISSING VENDOR NAME";
    };
    inkTextRef.SetText(this.m_nameLabel, vendorName);
  }

  public final func SetFeatureVersion(alternativeSwitch: Bool) -> Void {
    inkWidgetRef.SetVisible(this.m_fluffContainer, !alternativeSwitch);
  }

  public final func SetMoney(money: Int32) -> Void {
    inkTextRef.SetText(this.m_moneyLabel, IntToString(money));
  }

  public final func PlayIntoAnimation() -> Void {
    this.PlayLibraryAnimation(n"ripper_id");
  }
}
