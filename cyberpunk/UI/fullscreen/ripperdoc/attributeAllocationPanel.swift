
public class AttributeAllocationPanelController extends gameuiMenuGameController {

  private edit let m_allocationPointsContainer: inkCompoundRef;

  private edit let m_armorAmountText: inkTextRef;

  private edit let m_tokenAmountText: inkTextRef;

  private let m_allocationData: array<ref<AttributeAllocationData>>;

  private let m_allocationWidgets: array<wref<AttributeAllocationController>>;

  private let m_statsSystem: wref<StatsSystem>;

  private let m_player: wref<PlayerPuppet>;

  private let m_useAlternativeSwitch: Bool;

  private let m_ripperdocTokenManager: ref<RipperdocTokenManager>;

  protected cb func OnInitialize() -> Bool {
    this.m_player = this.GetPlayerControlledObject() as PlayerPuppet;
    this.m_useAlternativeSwitch = GameInstance.GetTransactionSystem(this.m_player.GetGame()).UseAlternativeCyberware();
    if !this.m_useAlternativeSwitch {
      this.GetRootWidget().SetVisible(false);
      return false;
    };
    this.m_statsSystem = GameInstance.GetStatsSystem(this.m_player.GetGame());
    this.m_ripperdocTokenManager = new RipperdocTokenManager();
    this.m_ripperdocTokenManager.Initialize(this.m_player);
    inkTextRef.SetText(this.m_tokenAmountText, IntToString(this.m_ripperdocTokenManager.GetTokensAmount()));
    inkTextRef.SetText(this.m_armorAmountText, IntToString(this.GetStateValue(gamedataStatType.Armor)));
    this.SetAllocationPoints();
  }

  private final func SetAllocationPoints() -> Void {
    inkCompoundRef.RemoveAllChildren(this.m_allocationPointsContainer);
    this.CreateAllocationDataWidget(gamedataStatType.Strength);
    this.CreateAllocationDataWidget(gamedataStatType.Reflexes);
    this.CreateAllocationDataWidget(gamedataStatType.TechnicalAbility);
    this.CreateAllocationDataWidget(gamedataStatType.Intelligence);
    this.CreateAllocationDataWidget(gamedataStatType.Cool);
  }

  private final func CreateAllocationDataWidget(statType: gamedataStatType) -> Void {
    let data: ref<AttributeAllocationData> = new AttributeAllocationData();
    data.AttributeType = statType;
    data.TotalPoints = this.GetStateValue(this.GetStatAvalaibleType(statType));
    data.AllocatedPoints = this.GetStateValue(this.GetStatAllocatedType(statType));
    let widgetController: wref<AttributeAllocationController> = this.SpawnFromLocal(inkWidgetRef.Get(this.m_allocationPointsContainer), n"atrributeAllocation").GetController() as AttributeAllocationController;
    widgetController.Setup(data);
    ArrayPush(this.m_allocationWidgets, widgetController);
  }

  protected cb func OnUpdateStats(evt: ref<AttributeUpdateEvent>) -> Bool {
    let token: Int32;
    let type: gamedataStatType;
    let i: Int32 = 0;
    while i < ArraySize(this.m_allocationWidgets) {
      type = this.m_allocationWidgets[i].GetStatType();
      this.m_allocationWidgets[i].UpdateData(this.GetStateValue(this.GetStatAllocatedType(type)), this.GetStateValue(type));
      i += 1;
    };
    token = this.m_ripperdocTokenManager.GetTokensAmount();
    inkTextRef.SetText(this.m_tokenAmountText, IntToString(token));
    inkTextRef.SetText(this.m_armorAmountText, IntToString(this.GetStateValue(gamedataStatType.Armor)));
  }

  protected cb func OnHoverStateChanged(evt: ref<HoverSatateChangedEvent>) -> Bool {
    if evt.isHovered {
      this.HoverPanel(evt.itemInventoryData);
    } else {
      this.HoverOverPanel();
    };
  }

  private final func HoverPanel(itemData: InventoryItemData) -> Void {
    let attributeType: gamedataStatType;
    let attributeValue: Int32;
    let j: Int32;
    let statPrereqs: array<SItemStackRequirementData> = InventoryItemData.GetEquipRequirements(itemData);
    let i: Int32 = 0;
    while i < ArraySize(this.m_allocationWidgets) {
      j = 0;
      while j < ArraySize(statPrereqs) {
        attributeType = statPrereqs[j].statType;
        attributeValue = Cast<Int32>(statPrereqs[j].requiredValue);
        if Equals(this.GetStatAvalaibleType(this.m_allocationWidgets[i].GetStatType()), attributeType) && attributeValue > 0 {
          this.m_allocationWidgets[i].ChangeHoverState(true, attributeValue);
        };
        j += 1;
      };
      i += 1;
    };
  }

  private final func HoverOverPanel() -> Void {
    let i: Int32 = 0;
    while i < ArraySize(this.m_allocationWidgets) {
      this.m_allocationWidgets[i].ChangeHoverState(false);
      i += 1;
    };
  }

  private final func GetStateValue(type: gamedataStatType) -> Int32 {
    return RoundF(this.m_statsSystem.GetStatValue(Cast<StatsObjectID>(this.m_player.GetEntityID()), type));
  }

  private final func GetStatAvalaibleType(attribute: gamedataStatType) -> gamedataStatType {
    switch attribute {
      case gamedataStatType.Strength:
        return gamedataStatType.StrengthAvailable;
      case gamedataStatType.Reflexes:
        return gamedataStatType.ReflexesAvailable;
      case gamedataStatType.TechnicalAbility:
        return gamedataStatType.TechnicalAbilityAvailable;
      case gamedataStatType.Cool:
        return gamedataStatType.CoolAvailable;
      case gamedataStatType.Intelligence:
        return gamedataStatType.IntelligenceAvailable;
    };
    return gamedataStatType.Invalid;
  }

  private final func GetStatAllocatedType(attribute: gamedataStatType) -> gamedataStatType {
    switch attribute {
      case gamedataStatType.Strength:
        return gamedataStatType.StrengthAlocated;
      case gamedataStatType.Reflexes:
        return gamedataStatType.ReflexesAlocated;
      case gamedataStatType.TechnicalAbility:
        return gamedataStatType.TechnicalAbilityAlocated;
      case gamedataStatType.Cool:
        return gamedataStatType.CoolAlocated;
      case gamedataStatType.Intelligence:
        return gamedataStatType.IntelligenceAlocated;
    };
    return gamedataStatType.Invalid;
  }
}
