
public class RipperdocScreenAnimationController extends inkLogicController {

  private edit let m_defaultAnimationTab: inkWidgetRef;

  private edit let m_itemAnimationTab: inkWidgetRef;

  private edit let m_defaultTab: inkWidgetRef;

  private edit let m_itemTab: inkWidgetRef;

  private edit let m_femaleHovers: inkWidgetRef;

  private edit let m_maleHovers: inkWidgetRef;

  private edit let m_F_immuneHoverTexture: inkWidgetRef;

  private edit let m_F_systemReplacementHoverTexture: inkWidgetRef;

  private edit let m_F_integumentaryHoverTexture: inkWidgetRef;

  private edit let m_F_musculoskeletalHoverTexture: inkWidgetRef;

  private edit let m_F_nervousHoverTexture: inkWidgetRef;

  private edit let m_F_eyesHoverTexture: inkWidgetRef;

  private edit let m_F_legsHoverTexture: inkWidgetRef;

  private edit let m_F_frontalCortexHoverTexture: inkWidgetRef;

  private edit let m_F_handsHoverTexture: inkWidgetRef;

  private edit let m_F_cardiovascularHoverTexture: inkWidgetRef;

  private edit let m_F_armsHoverTexture: inkWidgetRef;

  private edit let m_M_integumentaryHoverTexture: inkWidgetRef;

  private edit let m_M_armsHoverTexture: inkWidgetRef;

  private edit let m_M_cardiovascularHoverTexture: inkWidgetRef;

  private edit let m_M_handsHoverTexture: inkWidgetRef;

  private edit let m_M_frontalCortexHoverTexture: inkWidgetRef;

  private edit let m_M_immuneHoverTexture: inkWidgetRef;

  private edit let m_M_legsHoverTexture: inkWidgetRef;

  private edit let m_M_systemReplacementHoverTexture: inkWidgetRef;

  private edit let m_M_musculoskeletalHoverTexture: inkWidgetRef;

  private edit let m_M_nervousHoverTexture: inkWidgetRef;

  private edit let m_M_eyesHoverTexture: inkWidgetRef;

  private edit let m_man_wiresTexture: inkWidgetRef;

  private edit let m_woman_wiresTexture: inkWidgetRef;

  private let m_hoverAnimation: ref<inkAnimProxy>;

  private let m_hoverOverAnimation: ref<inkAnimProxy>;

  private let m_introDefaultAnimation: ref<inkAnimProxy>;

  private let m_outroDefaultAnimation: ref<inkAnimProxy>;

  private let m_introPaperdollAnimation: ref<inkAnimProxy>;

  private let m_outroPaperdollAnimation: ref<inkAnimProxy>;

  private let m_isFemale: Bool;

  protected cb func OnInitialize() -> Bool {
    inkWidgetRef.SetOpacity(this.m_F_frontalCortexHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_eyesHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_cardiovascularHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_immuneHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_nervousHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_integumentaryHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_systemReplacementHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_musculoskeletalHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_handsHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_armsHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_F_legsHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_frontalCortexHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_eyesHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_cardiovascularHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_immuneHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_nervousHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_integumentaryHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_systemReplacementHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_musculoskeletalHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_handsHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_armsHoverTexture, 0.00);
    inkWidgetRef.SetOpacity(this.m_M_legsHoverTexture, 0.00);
  }

  public final func SetGender(female: Bool) -> Void {
    this.m_isFemale = female;
    inkWidgetRef.SetVisible(this.m_femaleHovers, this.m_isFemale);
    inkWidgetRef.SetVisible(this.m_maleHovers, !this.m_isFemale);
    inkWidgetRef.SetVisible(this.m_woman_wiresTexture, this.m_isFemale);
    inkWidgetRef.SetVisible(this.m_man_wiresTexture, !this.m_isFemale);
  }

  public final func PlayItemSceenOpen(target: ref<inkWidget>, area: gamedataEquipmentArea) -> Void {
    inkWidgetRef.SetVisible(this.m_itemTab, true);
    this.StopAllAnimations();
    this.m_outroDefaultAnimation = this.PlayLibraryAnimation(n"default_tab_outro");
    this.m_outroDefaultAnimation.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnDefaultTabOutroAnimFinished");
    this.PlayLibraryAnimation(n"item_tab_intro");
    this.PlayIntroAnimation(area);
  }

  protected cb func OnDefaultTabOutroAnimFinished(anim: ref<inkAnimProxy>) -> Bool {
    this.m_outroDefaultAnimation.UnregisterFromCallback(inkanimEventType.OnFinish, this, n"OnDefaultTabOutroAnimFinished");
    inkWidgetRef.SetVisible(this.m_defaultTab, false);
  }

  public final func PlayDefaultScreenOpen(area: gamedataEquipmentArea) -> Void {
    inkWidgetRef.SetVisible(this.m_itemTab, false);
    inkWidgetRef.SetVisible(this.m_defaultTab, true);
    this.StopAllAnimations();
    this.m_introDefaultAnimation = this.PlayLibraryAnimation(n"default_tab_intro");
    if NotEquals(area, gamedataEquipmentArea.Invalid) {
      this.PlayOutroAnimation(area);
    };
  }

  private final func StopAllAnimations() -> Void {
    if this.m_hoverAnimation.IsPlaying() {
      this.m_hoverAnimation.GotoStartAndStop();
    };
    if this.m_hoverOverAnimation.IsPlaying() {
      this.m_hoverOverAnimation.GotoStartAndStop();
    };
    if this.m_introDefaultAnimation.IsPlaying() {
      this.m_introDefaultAnimation.GotoStartAndStop();
    };
    if this.m_outroDefaultAnimation.IsPlaying() {
      this.m_outroDefaultAnimation.GotoStartAndStop();
    };
    if this.m_introPaperdollAnimation.IsPlaying() {
      this.m_introPaperdollAnimation.GotoStartAndStop();
    };
    if this.m_outroPaperdollAnimation.IsPlaying() {
      this.m_outroPaperdollAnimation.GotoStartAndStop();
    };
  }

  private final func IsTransitionAnimationPlaying() -> Bool {
    return this.m_introDefaultAnimation.IsPlaying() || this.m_outroDefaultAnimation.IsPlaying() || this.m_introPaperdollAnimation.IsPlaying() || this.m_outroPaperdollAnimation.IsPlaying();
  }

  private final func PlayIntroAnimation(area: gamedataEquipmentArea) -> Void {
    let animName: CName;
    if this.m_introPaperdollAnimation.IsPlaying() {
      this.m_introPaperdollAnimation.GotoStartAndStop();
    };
    if this.m_outroPaperdollAnimation.IsPlaying() {
      this.m_outroPaperdollAnimation.GotoStartAndStop();
    };
    switch area {
      case gamedataEquipmentArea.FrontalCortexCW:
        animName = n"frontalCortex_intro";
        break;
      case gamedataEquipmentArea.EyesCW:
        animName = n"ocular_intro";
        break;
      case gamedataEquipmentArea.CardiovascularSystemCW:
        animName = n"circlatory_intro";
        break;
      case gamedataEquipmentArea.ImmuneSystemCW:
        animName = n"immune_intro";
        break;
      case gamedataEquipmentArea.NervousSystemCW:
        animName = n"nervous_intro";
        break;
      case gamedataEquipmentArea.IntegumentarySystemCW:
        animName = n"integumentary_intro";
        break;
      case gamedataEquipmentArea.SystemReplacementCW:
        animName = n"operating_intro";
        break;
      case gamedataEquipmentArea.MusculoskeletalSystemCW:
        animName = n"skeleton_intro";
        break;
      case gamedataEquipmentArea.HandsCW:
        animName = n"hands_intro";
        break;
      case gamedataEquipmentArea.ArmsCW:
        animName = n"arms_intro";
        break;
      case gamedataEquipmentArea.LegsCW:
        animName = n"legs_intro";
    };
    animName = this.m_isFemale ? animName : n"M_" + animName;
    inkWidgetRef.SetVisible(this.m_itemAnimationTab, true);
    this.m_introPaperdollAnimation = this.PlayLibraryAnimation(animName);
    this.m_introPaperdollAnimation.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnIntroPaperdollAnimationFinished");
  }

  protected cb func OnIntroPaperdollAnimationFinished(anim: ref<inkAnimProxy>) -> Bool {
    this.m_introPaperdollAnimation.UnregisterFromCallback(inkanimEventType.OnFinish, this, n"OnIntroPaperdollAnimationFinished");
    inkWidgetRef.SetVisible(this.m_defaultAnimationTab, false);
  }

  private final func PlayOutroAnimation(area: gamedataEquipmentArea) -> Void {
    let animName: CName;
    if this.m_introPaperdollAnimation.IsPlaying() {
      this.m_introPaperdollAnimation.GotoStartAndStop();
    };
    if this.m_outroPaperdollAnimation.IsPlaying() {
      this.m_outroPaperdollAnimation.GotoStartAndStop();
    };
    switch area {
      case gamedataEquipmentArea.FrontalCortexCW:
        animName = n"frontalCortex_outro";
        break;
      case gamedataEquipmentArea.EyesCW:
        animName = n"ocular_outro";
        break;
      case gamedataEquipmentArea.CardiovascularSystemCW:
        animName = n"circlatory_outro";
        break;
      case gamedataEquipmentArea.ImmuneSystemCW:
        animName = n"immune_outro";
        break;
      case gamedataEquipmentArea.NervousSystemCW:
        animName = n"nervous_outro";
        break;
      case gamedataEquipmentArea.IntegumentarySystemCW:
        animName = n"integumentary_outro";
        break;
      case gamedataEquipmentArea.SystemReplacementCW:
        animName = n"operating_outro";
        break;
      case gamedataEquipmentArea.MusculoskeletalSystemCW:
        animName = n"skeleton_outro";
        break;
      case gamedataEquipmentArea.HandsCW:
        animName = n"hands_outro";
        break;
      case gamedataEquipmentArea.ArmsCW:
        animName = n"arms_outro";
        break;
      case gamedataEquipmentArea.LegsCW:
        animName = n"legs_outro";
    };
    animName = this.m_isFemale ? animName : n"M_" + animName;
    inkWidgetRef.SetVisible(this.m_defaultAnimationTab, true);
    this.m_outroPaperdollAnimation = this.PlayLibraryAnimation(animName);
    this.m_outroPaperdollAnimation.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnOutroPaperdollAnimationFinished");
  }

  protected cb func OnOutroPaperdollAnimationFinished(anim: ref<inkAnimProxy>) -> Bool {
    this.m_outroPaperdollAnimation.UnregisterFromCallback(inkanimEventType.OnFinish, this, n"OnOutroPaperdollAnimationFinished");
    inkWidgetRef.SetVisible(this.m_itemAnimationTab, false);
  }

  private final func GetHoverAnimationTarget(area: gamedataEquipmentArea) -> wref<inkWidget> {
    let target: wref<inkWidget>;
    switch area {
      case gamedataEquipmentArea.FrontalCortexCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_frontalCortexHoverTexture) : inkWidgetRef.Get(this.m_M_frontalCortexHoverTexture);
        break;
      case gamedataEquipmentArea.EyesCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_eyesHoverTexture) : inkWidgetRef.Get(this.m_M_eyesHoverTexture);
        break;
      case gamedataEquipmentArea.CardiovascularSystemCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_cardiovascularHoverTexture) : inkWidgetRef.Get(this.m_M_cardiovascularHoverTexture);
        break;
      case gamedataEquipmentArea.ImmuneSystemCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_immuneHoverTexture) : inkWidgetRef.Get(this.m_M_immuneHoverTexture);
        break;
      case gamedataEquipmentArea.NervousSystemCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_nervousHoverTexture) : inkWidgetRef.Get(this.m_M_nervousHoverTexture);
        break;
      case gamedataEquipmentArea.IntegumentarySystemCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_integumentaryHoverTexture) : inkWidgetRef.Get(this.m_M_integumentaryHoverTexture);
        break;
      case gamedataEquipmentArea.SystemReplacementCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_systemReplacementHoverTexture) : inkWidgetRef.Get(this.m_M_systemReplacementHoverTexture);
        break;
      case gamedataEquipmentArea.MusculoskeletalSystemCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_musculoskeletalHoverTexture) : inkWidgetRef.Get(this.m_M_musculoskeletalHoverTexture);
        break;
      case gamedataEquipmentArea.HandsCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_handsHoverTexture) : inkWidgetRef.Get(this.m_M_handsHoverTexture);
        break;
      case gamedataEquipmentArea.ArmsCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_armsHoverTexture) : inkWidgetRef.Get(this.m_M_armsHoverTexture);
        break;
      case gamedataEquipmentArea.LegsCW:
        target = this.m_isFemale ? inkWidgetRef.Get(this.m_F_legsHoverTexture) : inkWidgetRef.Get(this.m_M_legsHoverTexture);
    };
    return target;
  }

  public final func PlayHoverAnimation(area: gamedataEquipmentArea, mode: RipperdocModes) -> Void {
    let target: wref<inkWidget>;
    if this.m_hoverAnimation.IsPlaying() {
      this.m_hoverAnimation.GotoStartAndStop();
    };
    if this.IsTransitionAnimationPlaying() || NotEquals(mode, RipperdocModes.Default) {
      return;
    };
    target = this.GetHoverAnimationTarget(area);
    this.m_hoverAnimation = this.PlayLibraryAnimationOnTargets(n"hover_area", SelectWidgets(target));
  }

  public final func PlayHoverOverAnimation(area: gamedataEquipmentArea, mode: RipperdocModes) -> Void {
    let target: wref<inkWidget>;
    if this.m_hoverOverAnimation.IsPlaying() {
      this.m_hoverOverAnimation.GotoEndAndStop();
    };
    if this.IsTransitionAnimationPlaying() || NotEquals(mode, RipperdocModes.Default) {
      return;
    };
    target = this.GetHoverAnimationTarget(area);
    this.m_hoverOverAnimation = this.PlayLibraryAnimationOnTargets(n"hoverover_area", SelectWidgets(target));
  }
}
