
public native class gameuiSaveHandlingController extends gameuiMenuGameController {

  public final native func DeleteSavedGame(saveId: Int32) -> Void;

  public final native func IsTransferSavedExportSupported() -> Bool;

  public final native func IsTransferSavedImportSupported() -> Bool;

  public final native func TransferSavedGame(scriptableData: ref<IScriptable>) -> Void;

  public final native func RequestSaveFailedNotification() -> Void;

  public final native func RequestGameSavedNotification() -> Void;

  public final native func IsSaveFailedNotificationActive() -> Bool;

  public final native func IsGameSavedNotificationActive() -> Bool;

  public final native func LoadSaveInGame(saveId: Int32) -> Void;

  public final native func LoadModdedSave(saveId: Int32) -> Void;

  public final native func OverrideSavedGame(saveId: Int32) -> Void;

  public final native func SetNextInitialLoadingScreen(tweakID: Uint64) -> Void;

  public final native func PreSpawnInitialLoadingScreen(tweakID: Uint64) -> Void;

  public final func ShowSavingLockedNotification(const locks: script_ref<array<gameSaveLock>>) -> Void {
    GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame()).QueueEvent(new UIInGameNotificationRemoveEvent());
    GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame()).QueueEvent(UIInGameNotificationEvent.CreateSavingLockedEvent(locks));
  }
}

public class SingleplayerMenuGameController extends MainMenuGameController {

  private edit let m_buttonHintsManagerRef: inkWidgetRef;

  private edit let m_gogButtonWidgetRef: inkWidgetRef;

  private edit let m_continuetooltipContainer: inkCompoundRef;

  private let m_onlineSystem: wref<IOnlineSystem>;

  private let m_requestHandler: wref<inkISystemRequestsHandler>;

  private let m_buttonHintsController: wref<ButtonHints>;

  private let m_continueGameTooltipController: wref<ContinueGameTooltip>;

  private let m_dataSyncStatus: CloudSavesQueryStatus;

  private let m_savesCount: Int32;

  private let m_savesReady: Bool;

  private let m_isOffline: Bool;

  private let m_isModded: Bool;

  protected cb func OnInitialize() -> Bool {
    this.m_requestHandler = this.GetSystemRequestsHandler();
    this.m_savesCount = 0;
    this.m_savesCount = this.m_requestHandler.RequestSavesCountSync();
    this.m_requestHandler.RegisterToCallback(n"OnSavesForLoadReady", this, n"OnSavesForLoadReady");
    this.m_requestHandler.RegisterToCallback(n"OnBoughtFullGame", this, n"OnRedrawRequested");
    this.m_requestHandler.RegisterToCallback(n"OnSaveMetadataReady", this, n"OnSaveMetadataReady");
    this.m_requestHandler.RegisterToCallback(n"OnCloudSavesQueryStatusChanged", this, n"OnCloudSavesQueryStatusChanged");
    this.m_requestHandler.RequestSavesForLoad();
    this.m_onlineSystem = GameInstance.GetOnlineSystem(this.GetPlayerControlledObject().GetGame());
    super.OnInitialize();
    this.m_menuListController.GetRootWidget().RegisterToCallback(n"OnRelease", this, n"OnListRelease");
    this.m_menuListController.GetRootWidget().RegisterToCallback(n"OnRepeat", this, n"OnListRelease");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
    this.SetNextInitialLoadingScreen(this.m_requestHandler.GetLatestSaveMetadata().initialLoadingScreenID);
    this.m_buttonHintsController = this.SpawnFromExternal(inkWidgetRef.Get(this.m_buttonHintsManagerRef), r"base\\gameplay\\gui\\common\\buttonhints.inkwidget", n"Root").GetController() as ButtonHints;
    this.m_buttonHintsController.AddButtonHint(n"select", GetLocalizedText("UI-UserActions-Select"));
    if IsDefined(GameInstance.GetOnlineSystem(this.GetPlayerControlledObject().GetGame())) {
      inkWidgetRef.RegisterToCallback(this.m_gogButtonWidgetRef, n"OnRelease", this, n"OnGogPressed");
      inkWidgetRef.SetVisible(this.m_gogButtonWidgetRef, true);
      inkWidgetRef.SetInteractive(this.m_gogButtonWidgetRef, true);
    } else {
      inkWidgetRef.SetVisible(this.m_gogButtonWidgetRef, false);
      inkWidgetRef.SetInteractive(this.m_gogButtonWidgetRef, false);
    };
    this.AsyncSpawnFromLocal(inkWidgetRef.Get(this.m_continuetooltipContainer), n"ContinueTooltip", this, n"OnTooltipContainerSpawned");
  }

  protected cb func OnUninitialize() -> Bool {
    this.m_requestHandler.CancelSavesRequest();
    this.m_requestHandler.CancelSavedGameScreenshotRequests();
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
    this.m_menuListController.GetRootWidget().UnregisterFromCallback(n"OnRelease", this, n"OnListRelease");
    this.m_menuListController.GetRootWidget().UnregisterFromCallback(n"OnRepeat", this, n"OnListRelease");
    if IsDefined(GameInstance.GetOnlineSystem(this.GetPlayerControlledObject().GetGame())) {
      inkWidgetRef.UnregisterFromCallback(this.m_gogButtonWidgetRef, n"OnRelease", this, n"OnGogPressed");
    };
    super.OnUninitialize();
    this.m_requestHandler.UnregisterFromCallback(n"OnBoughtFullGame", this, n"OnRedrawRequested");
    this.m_menuEventDispatcher.UnregisterFromEvent(n"OnCheckPatchNotes", this, n"OnCheckPatchNotes");
    this.m_menuEventDispatcher.UnregisterFromEvent(n"OnClosePatchNotes", this, n"OnClosePatchNotes");
  }

  protected cb func OnSetMenuEventDispatcher(menuEventDispatcher: wref<inkMenuEventDispatcher>) -> Bool {
    super.OnSetMenuEventDispatcher(menuEventDispatcher);
    menuEventDispatcher.RegisterToEvent(n"OnCheckPatchNotes", this, n"OnCheckPatchNotes");
    menuEventDispatcher.RegisterToEvent(n"OnClosePatchNotes", this, n"OnClosePatchNotes");
  }

  protected cb func OnCheckPatchNotes(userData: ref<IScriptable>) -> Bool {
    let uiSystem: ref<UISystem> = GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame());
    let introPackage: gameuiPatchIntroPackage = uiSystem.GetNeededPatchIntroPackage();
    if ArraySize(introPackage.patchIntrosNeeded) > 0 {
      this.m_menuEventDispatcher.SpawnEvent(n"OnRequetPatchNotes");
      inkWidgetRef.SetOpacity(this.m_gogButtonWidgetRef, 0.00);
      inkWidgetRef.SetVisible(this.m_menuList, false);
      inkWidgetRef.SetVisible(this.m_buttonHintsManagerRef, false);
    };
  }

  protected cb func OnClosePatchNotes(userData: ref<IScriptable>) -> Bool {
    inkWidgetRef.SetOpacity(this.m_gogButtonWidgetRef, 1.00);
    inkWidgetRef.SetVisible(this.m_menuList, true);
    inkWidgetRef.SetVisible(this.m_buttonHintsManagerRef, true);
  }

  protected cb func OnRedrawRequested() -> Bool {
    this.ShowActionsList();
  }

  protected cb func OnTooltipContainerSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Bool {
    let previewImageWidget: wref<inkImage>;
    widget.SetVisible(false);
    this.m_continueGameTooltipController = widget.GetController() as ContinueGameTooltip;
    if this.m_savesReady || Equals(this.m_dataSyncStatus, CloudSavesQueryStatus.FetchedSuccessfully) {
      previewImageWidget = this.m_continueGameTooltipController.GetPreviewImageWidget();
      if IsDefined(previewImageWidget) {
        this.GetSystemRequestsHandler().RequestSavedGameScreenshot(0, previewImageWidget);
      };
      this.m_continueGameTooltipController.UpdateNetworkStatus(this.m_dataSyncStatus);
      this.m_continueGameTooltipController.SetOfflineStatus(this.m_isOffline);
    };
  }

  protected cb func OnContinueButtonEnter(evt: ref<inkPointerEvent>) -> Bool {
    if this.m_savesCount > 0 || !this.m_savesReady {
      this.m_continueGameTooltipController.GetRootWidget().SetVisible(true);
    };
  }

  protected cb func OnContinueButtonLeave(evt: ref<inkPointerEvent>) -> Bool {
    this.m_continueGameTooltipController.GetRootWidget().SetVisible(false);
  }

  protected cb func OnSavesForLoadReady(saves: array<String>) -> Bool {
    let previewImageWidget: wref<inkImage>;
    let prevSavesCount: Int32 = this.m_savesCount;
    this.m_savesCount = ArraySize(saves);
    this.m_savesReady = true;
    if this.m_savesCount > 0 && prevSavesCount == 0 {
      this.ShowActionsList();
    };
    if IsDefined(this.m_continueGameTooltipController) {
      if this.m_savesCount > 0 {
        previewImageWidget = this.m_continueGameTooltipController.GetPreviewImageWidget();
        if IsDefined(previewImageWidget) {
          this.GetSystemRequestsHandler().RequestSavedGameScreenshot(0, this.m_continueGameTooltipController.GetPreviewImageWidget());
        };
      } else {
        this.m_continueGameTooltipController.GetRootWidget().SetVisible(false);
      };
    };
  }

  protected func ShowActionsList() -> Void {
    let continueButton: wref<inkWidget>;
    this.ShowActionsList();
    continueButton = inkCompoundRef.GetWidgetByIndex(this.m_menuList, 0);
    continueButton.RegisterToCallback(n"OnEnter", this, n"OnContinueButtonEnter");
    continueButton.RegisterToCallback(n"OnLeave", this, n"OnContinueButtonLeave");
  }

  protected cb func OnSaveMetadataReady(info: ref<SaveMetadataInfo>) -> Bool {
    let characterCustomizationSystem: ref<gameuiICharacterCustomizationSystem>;
    if info.saveIndex == 0 {
      if info.isValid {
        this.m_isModded = info.isModded;
        characterCustomizationSystem = GameInstance.GetCharacterCustomizationSystem(this.GetPlayerControlledObject().GetGame());
        this.m_continueGameTooltipController.SetMetadata(info);
        this.m_continueGameTooltipController.CheckThumbnailCensorship(!characterCustomizationSystem.IsNudityAllowed());
        this.LoadBackgroundWidget(info.initialLoadingScreenID);
      } else {
        this.m_continueGameTooltipController.SetInvalid(info);
      };
    };
  }

  private func PopulateMenuItemList() -> Void {
    if this.m_savesCount > 0 {
      this.AddMenuItem(GetLocalizedText("UI-ScriptExports-Continue0"), PauseMenuAction.QuickLoad);
    };
    this.AddMenuItem(GetLocalizedText("UI-ScriptExports-NewGame0"), n"OnNewGame");
    this.AddMenuItem(GetLocalizedText("UI-ScriptExports-LoadGame0"), n"OnLoadGame");
    this.AddMenuItem(GetLocalizedText("UI-Labels-Settings"), n"OnSwitchToSettings");
    this.AddMenuItem(GetLocalizedText("UI-DLC-MenuTitle"), n"OnSwitchToDlc");
    this.AddMenuItem(GetLocalizedText("UI-Labels-Credits"), n"OnSwitchToCredits");
    if TrialHelper.IsInPS5TrialMode() {
      this.AddMenuItem(GetLocalizedText("UI-Notifications-Ps5TrialBuyMenuItem"), n"OnBuyGame");
    };
    if !IsFinal() || UseProfiler() {
      this.AddMenuItem("DEBUG NEW GAME", n"OnDebug");
    };
    this.m_menuListController.Refresh();
    this.SetCursorOverWidget(inkCompoundRef.GetWidgetByIndex(this.m_menuList, 0));
  }

  protected cb func OnListRelease(e: ref<inkPointerEvent>) -> Bool {
    if e.IsHandled() {
      return false;
    };
    this.m_menuListController.HandleInput(e, this);
  }

  protected cb func OnGlobalRelease(e: ref<inkPointerEvent>) -> Bool {
    let delayEvent: ref<RetrySaveDataRequestDelay>;
    if e.IsHandled() {
      return false;
    };
    if e.IsAction(n"back") {
      this.PlaySound(n"Button", n"OnPress");
      this.m_menuEventDispatcher.SpawnEvent(n"OnBack");
      e.Handle();
    } else {
      if e.IsAction(n"next_menu") {
        this.PlaySound(n"Button", n"OnPress");
        this.m_menuEventDispatcher.SpawnEvent(n"OnGOGProfile");
        e.Handle();
      } else {
        if e.IsAction(n"navigate_down") || e.IsAction(n"navigate_up") || e.IsAction(n"navigate_left") || e.IsAction(n"navigate_right") {
          this.SetCursorOverWidget(inkCompoundRef.GetWidgetByIndex(this.m_menuList, 0));
        } else {
          if e.IsAction(n"reload") {
            this.m_continueGameTooltipController.DisplayDataSyncIndicator(true);
            delayEvent = new RetrySaveDataRequestDelay();
            GameInstance.GetDelaySystem(this.GetPlayerControlledObject().GetGame()).DelayEvent(this.GetPlayerControlledObject(), delayEvent, 1.00);
          };
        };
      };
    };
  }

  protected cb func OnRetrySaveDataRequestDelay(evt: ref<RetrySaveDataRequestDelay>) -> Bool {
    this.m_requestHandler.RequestSavesForLoad();
    this.m_onlineSystem.RequestInitialStatus();
  }

  protected cb func OnGogPressed(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.PlaySound(n"Button", n"OnPress");
      evt.Handle();
      this.m_menuEventDispatcher.SpawnEvent(n"OnGOGProfile");
    };
  }

  protected cb func OnCloudSavesQueryStatusChanged(status: CloudSavesQueryStatus) -> Bool {
    let previewImageWidget: wref<inkImage>;
    this.m_dataSyncStatus = status;
    if IsDefined(this.m_continueGameTooltipController) {
      this.m_continueGameTooltipController.UpdateNetworkStatus(this.m_dataSyncStatus);
      if Equals(this.m_dataSyncStatus, CloudSavesQueryStatus.FetchedSuccessfully) {
        previewImageWidget = this.m_continueGameTooltipController.GetPreviewImageWidget();
        if IsDefined(previewImageWidget) {
          this.GetSystemRequestsHandler().RequestSavedGameScreenshot(0, this.m_continueGameTooltipController.GetPreviewImageWidget());
        };
      };
    };
  }

  protected cb func OnOnlineStatusChanged(value: GOGRewardsSystemStatus) -> Bool {
    let error: GOGRewardsSystemErrors = this.m_onlineSystem.GetError();
    this.m_isOffline = NotEquals(error, GOGRewardsSystemErrors.None);
    if IsDefined(this.m_continueGameTooltipController) {
      this.m_continueGameTooltipController.SetOfflineStatus(this.m_isOffline);
    };
  }

  protected func HandleMenuItemActivate(data: ref<PauseMenuListItemData>) -> Bool {
    if this.HandleMenuItemActivate(data) {
      return false;
    };
    switch data.action {
      case PauseMenuAction.QuickLoad:
        if this.m_savesCount > 0 {
          GameInstance.GetTelemetrySystem(this.GetPlayerControlledObject().GetGame()).LogLastCheckpointLoaded();
          if this.m_isModded {
            this.LoadModdedSave(0);
          } else {
            this.GetSystemRequestsHandler().LoadLastCheckpoint(false);
          };
          return true;
        };
    };
    return false;
  }
}
