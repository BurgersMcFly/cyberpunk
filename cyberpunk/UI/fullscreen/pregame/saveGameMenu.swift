
public class SaveGameMenuGameController extends gameuiSaveHandlingController {

  private edit let m_list: inkCompoundRef;

  private edit let m_buttonHintsManagerRef: inkWidgetRef;

  private edit let m_gogButtonWidgetRef: inkWidgetRef;

  private edit let m_gogContainer: inkWidgetRef;

  private edit let m_scrollbar: inkWidgetRef;

  private let m_eventDispatcher: wref<inkMenuEventDispatcher>;

  private let m_handler: wref<inkISystemRequestsHandler>;

  private let m_buttonHintsController: wref<ButtonHints>;

  private let m_saveInfo: ref<SaveMetadataInfo>;

  private let m_saves: array<String>;

  private let m_pendingRegistration: Bool;

  private let m_hasEmptySlot: Bool;

  private let m_saveInProgress: Bool;

  private let m_loadComplete: Bool;

  private let m_saveFilesReady: Bool;

  private let m_cloudSynced: Bool;

  protected cb func OnInitialize() -> Bool {
    this.m_handler = this.GetSystemRequestsHandler();
    this.m_handler.RegisterToCallback(n"OnSavesForSaveReady", this, n"OnSavesForSaveReady");
    this.m_handler.RegisterToCallback(n"OnSaveMetadataReady", this, n"OnSaveMetadataReady");
    this.m_handler.RegisterToCallback(n"OnSaveDeleted", this, n"OnSaveDeleted");
    this.m_handler.RegisterToCallback(n"OnGogLoginStatusChanged", this, n"OnGogLoginStatusChanged");
    this.m_handler.RegisterToCallback(n"OnSavingComplete", this, n"OnSavingComplete");
    this.m_handler.RegisterToCallback(n"OnCloudSavesQueryStatusChanged", this, n"OnCloudSavesQueryStatusChanged");
    this.m_handler.RegisterToCallback(n"OnCloudSaveUploadFinish", this, n"OnCloudSaveUploadFinish");
    this.m_handler.RequestSavesForSave();
    inkCompoundRef.RemoveAllChildren(this.m_list);
    this.m_hasEmptySlot = false;
    this.PlayLibraryAnimation(n"intro");
    this.m_buttonHintsController = this.SpawnFromExternal(inkWidgetRef.Get(this.m_buttonHintsManagerRef), r"base\\gameplay\\gui\\common\\buttonhints.inkwidget", n"Root").GetController() as ButtonHints;
    this.m_buttonHintsController.AddButtonHint(n"back", GetLocalizedText("Common-Access-Close"));
    this.m_buttonHintsController.AddButtonHint(n"delete_save", GetLocalizedText("UI-Menus-DeleteSave"));
    this.m_buttonHintsController.AddButtonHint(n"select", GetLocalizedText("UI-UserActions-Select"));
    if this.IsTransferSavedExportSupported() {
      this.m_buttonHintsController.AddButtonHint(n"transfer_save", GetLocalizedText("UI-Menus-ExportSave"));
    };
    this.InitCrossProgression();
    this.PlayLoadingAnimation();
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnButtonRelease");
  }

  protected cb func OnUninitialize() -> Bool {
    this.m_handler.CancelSavesRequest();
    this.m_handler.CancelSavedGameScreenshotRequests();
    if IsDefined(GameInstance.GetOnlineSystem(this.GetPlayerControlledObject().GetGame())) {
      inkWidgetRef.UnregisterFromCallback(this.m_gogButtonWidgetRef, n"OnRelease", this, n"OnGogPressed");
    };
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnButtonRelease");
  }

  protected cb func OnButtonRelease(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"next_menu") {
      this.PlaySound(n"Button", n"OnPress");
      this.GogLogin();
      evt.Handle();
    };
  }

  protected cb func OnRefreshGOGState(evt: ref<RefreshGOGState>) -> Bool {
    if Equals(evt.status, GOGRewardsSystemStatus.RegistrationPending) {
      this.m_pendingRegistration = true;
    } else {
      if this.m_pendingRegistration && Equals(evt.status, GOGRewardsSystemStatus.Registered) {
        this.m_pendingRegistration = false;
        this.m_saveFilesReady = false;
        this.m_cloudSynced = false;
        this.PlayLoadingAnimation();
        this.m_handler.RequestSavesForLoad();
      };
    };
  }

  private final func InitCrossProgression() -> Void {
    let gameInst: GameInstance = this.GetPlayerControlledObject().GetGame();
    let hudGroup: ref<ConfigGroup> = GameInstance.GetSettingsSystem(gameInst).GetGroup(n"/gameplay/misc");
    let settingsVar: ref<ConfigVarBool> = hudGroup.GetVar(n"EnableCloudSaves") as ConfigVarBool;
    if IsDefined(GameInstance.GetOnlineSystem(gameInst)) && settingsVar.GetValue() && settingsVar.IsVisible() {
      inkWidgetRef.RegisterToCallback(this.m_gogButtonWidgetRef, n"OnRelease", this, n"OnGogPressed");
      inkWidgetRef.SetVisible(this.m_gogButtonWidgetRef, true);
      inkWidgetRef.SetInteractive(this.m_gogButtonWidgetRef, true);
    } else {
      inkWidgetRef.SetVisible(this.m_gogButtonWidgetRef, false);
      inkWidgetRef.SetInteractive(this.m_gogButtonWidgetRef, false);
    };
  }

  private final func TryToCreateEmptySlot() -> Void {
    let currButton: wref<inkCompoundWidget>;
    let currLogic: wref<LoadListItem>;
    if this.m_hasEmptySlot {
      return;
    };
    if !this.m_handler.HasFreeSaveSlot("ManualSave-") {
      return;
    };
    currButton = this.SpawnFromLocal(inkWidgetRef.Get(this.m_list), n"LoadListItem") as inkCompoundWidget;
    currButton.RegisterToCallback(n"OnRelease", this, n"OnSaveFile");
    inkCompoundRef.ReorderChild(this.m_list, currButton, 0);
    currLogic = currButton.GetController() as LoadListItem;
    currLogic.SetData(-1, true);
    this.m_hasEmptySlot = true;
  }

  private final func SetupLoadItems(saves: array<String>) -> Void {
    let i: Int32 = 0;
    while i < ArraySize(saves) {
      this.CreateLoadItem(i);
      i += 1;
    };
  }

  private final func CreateLoadItem(index: Int32) -> Void {
    let currLogic: wref<LoadListItem>;
    let currButton: wref<inkCompoundWidget> = this.SpawnFromLocal(inkWidgetRef.Get(this.m_list), n"LoadListItem") as inkCompoundWidget;
    currButton.RegisterToCallback(n"OnRelease", this, n"OnSaveFile");
    currLogic = currButton.GetController() as LoadListItem;
    currLogic.SetData(index);
    this.GetSystemRequestsHandler().RequestSavedGameScreenshot(index, currLogic.GetPreviewImageWidget());
  }

  protected cb func OnSaveFile(e: ref<inkPointerEvent>) -> Bool {
    let button: wref<inkWidget>;
    let controller: wref<LoadListItem>;
    let transferSaveData: ref<TransferSaveData>;
    if !this.m_loadComplete || this.m_saveInProgress || this.IsSaveFailedNotificationActive() || this.IsGameSavedNotificationActive() {
      this.PlaySound(n"Button", n"OnPress");
      return false;
    };
    button = e.GetCurrentTarget();
    controller = button.GetController() as LoadListItem;
    if e.IsAction(n"click") {
      this.PlaySound(n"Button", n"OnPress");
      if controller.EmptySlot() {
        this.m_saveInProgress = true;
        this.GetSystemRequestsHandler().ManualSave("ManualSave-");
      } else {
        this.OverrideSavedGame(controller.Index());
      };
      return true;
    };
    if e.IsAction(n"transfer_save") && this.IsTransferSavedExportSupported() {
      if !controller.EmptySlot() {
        transferSaveData = new TransferSaveData();
        transferSaveData.saveIndex = controller.Index();
        transferSaveData.action = TransferSaveAction.Export;
        this.TransferSavedGame(transferSaveData);
      };
    };
    if e.IsAction(n"delete_save") {
      if !controller.EmptySlot() {
        this.PlaySound(n"SaveDeleteButton", n"OnPress");
        if controller.IsCloud() {
          this.m_handler.RequestSystemNotificationGeneric(n"UI-CrossProgression-Title", n"UI-CrossProgression-DeleteSaveRestriction");
        } else {
          this.DeleteSavedGame(controller.Index());
        };
      } else {
        this.PlaySound(n"SaveDeleteButton", n"OnPress");
      };
      return true;
    };
  }

  private final func GogLogin() -> Void {
    let gogPopupController: wref<GOGProfileLogicController>;
    let gogPopupWidget: ref<inkWidget>;
    let container: wref<inkCompoundWidget> = inkWidgetRef.Get(this.m_gogContainer) as inkCompoundWidget;
    if Cast<Bool>(container.GetNumChildren()) {
      container.RemoveAllChildren();
    } else {
      gogPopupWidget = this.SpawnFromExternal(container, r"base\\gameplay\\gui\\fullscreen\\main_menu\\gog_popup.inkwidget", n"Root");
      if gogPopupWidget != null {
        gogPopupController = gogPopupWidget.GetController() as GOGProfileLogicController;
        if gogPopupController != null {
          gogPopupController.SetMenuState(EGOGMenuState.LoadGame);
        };
      };
    };
  }

  protected cb func OnSaveDeleted(result: Bool, idx: Int32) -> Bool {
    let button: wref<inkWidget>;
    let controller: wref<LoadListItem>;
    let i: Int32;
    if result {
      i = 0;
      while i < inkCompoundRef.GetNumChildren(this.m_list) {
        button = inkCompoundRef.GetWidgetByIndex(this.m_list, i);
        controller = button.GetController() as LoadListItem;
        if controller.Index() == idx {
          inkCompoundRef.RemoveChild(this.m_list, button);
          break;
        };
        i += 1;
      };
      this.TryToCreateEmptySlot();
    };
  }

  protected cb func OnGogLoginStatusChanged(bIsSignedIn: Bool) -> Bool {
    let handler: wref<inkISystemRequestsHandler> = this.GetSystemRequestsHandler();
    if bIsSignedIn {
      handler.CancelSavesRequest();
      handler.RequestSavesForLoad();
    };
  }

  protected cb func OnOverrideSaveAccepted() -> Bool {
    this.m_saveInProgress = true;
  }

  protected cb func OnSavingComplete(success: Bool, locks: array<gameSaveLock>) -> Bool {
    if success {
      this.m_handler.CancelSavedGameScreenshotRequests();
      this.m_handler.RequestSavesForSave();
      this.RequestGameSavedNotification();
    } else {
      this.ShowSavingLockedNotification(locks);
      this.RequestSaveFailedNotification();
    };
    this.m_saveInProgress = false;
  }

  protected cb func OnSavesForSaveReady(saves: array<String>) -> Bool {
    this.m_saves = saves;
    this.m_saveFilesReady = true;
    this.UpdateSavesList();
  }

  protected cb func OnCloudSavesQueryStatusChanged(status: CloudSavesQueryStatus) -> Bool {
    this.m_cloudSynced = true;
    this.UpdateSavesList();
  }

  protected cb func OnCloudSaveUploadFinish(success: Bool) -> Bool {
    this.m_handler.CancelSavedGameScreenshotRequests();
    this.m_handler.RequestSavesForSave();
  }

  private final func UpdateSavesList() -> Void {
    if this.m_saveFilesReady && this.m_cloudSynced {
      this.m_saveFilesReady = false;
      this.m_cloudSynced = false;
      inkCompoundRef.RemoveAllChildren(this.m_list);
      this.m_hasEmptySlot = false;
      this.TryToCreateEmptySlot();
      this.SetupLoadItems(this.m_saves);
      this.m_loadComplete = true;
      this.StopLoadingAnimation();
    };
  }

  protected cb func OnSaveMetadataReady(info: ref<SaveMetadataInfo>) -> Bool {
    let button: wref<inkWidget>;
    let controller: wref<LoadListItem>;
    let i: Int32 = 0;
    while i < inkCompoundRef.GetNumChildren(this.m_list) {
      button = inkCompoundRef.GetWidgetByIndex(this.m_list, i);
      controller = button.GetController() as LoadListItem;
      if controller.Index() == info.saveIndex {
        if info.isValid {
          controller.SetMetadata(info);
        } else {
          controller.SetInvalid(info.internalName);
        };
        break;
      };
      i += 1;
    };
  }

  protected cb func OnGogPressed(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      this.PlaySound(n"Button", n"OnPress");
      this.GogLogin();
      evt.Handle();
    };
  }

  protected cb func OnSetMenuEventDispatcher(menuEventDispatcher: wref<inkMenuEventDispatcher>) -> Bool {
    this.m_eventDispatcher = menuEventDispatcher;
  }

  private final func PlayLoadingAnimation() -> Void {
    let i: Int32;
    inkCompoundRef.RemoveAllChildren(this.m_list);
    i = 0;
    while i < 7 {
      this.SpawnFromLocal(inkWidgetRef.Get(this.m_list), n"LoadListItemPlaceholder");
      i += 1;
    };
    inkWidgetRef.SetVisible(this.m_scrollbar, false);
  }

  private final func StopLoadingAnimation() -> Void {
    inkWidgetRef.SetVisible(this.m_scrollbar, true);
  }
}
