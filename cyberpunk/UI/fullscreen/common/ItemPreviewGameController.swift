
public native class inkItemPreviewGameController extends inkPreviewGameController {

  protected final native func PreviewItem(itemID: ItemID, forceCreate: Bool) -> Void;

  protected final func PreviewItem(itemID: ItemID) -> Void {
    this.PreviewItem(itemID, true);
  }

  protected final native func ClearPreview() -> Void;
}

public class ItemPreviewGameController extends inkItemPreviewGameController {

  private edit let m_colliderWidgetRef: inkWidgetRef;

  private let m_colliderWidget: wref<inkWidget>;

  private edit let m_itemNameText: inkTextRef;

  private edit let m_itemLevelText: inkTextRef;

  private edit let m_itemRarityWidget: inkWidgetRef;

  private let m_data: ref<InventoryItemPreviewData>;

  private let m_isMouseDown: Bool;

  @default(ItemPreviewGameController, 2.0f)
  private const let c_ITEM_ROTATION_SPEED: Float;

  protected cb func OnInitialize() -> Bool {
    super.OnInitialize();
    if inkWidgetRef.IsValid(this.m_colliderWidgetRef) {
      this.m_colliderWidget = inkWidgetRef.Get(this.m_colliderWidgetRef);
    } else {
      this.m_colliderWidget = this.GetRootWidget();
    };
    this.m_data = this.GetRootWidget().GetUserData(n"InventoryItemPreviewData") as InventoryItemPreviewData;
    inkTextRef.SetText(this.m_itemNameText, this.m_data.itemName);
    inkTextRef.SetText(this.m_itemLevelText, "Required level: " + IntToString(this.m_data.requiredLevel));
    inkWidgetRef.SetState(this.m_itemRarityWidget, this.m_data.itemQualityState);
    this.PreviewItem(this.m_data.itemID);
    this.m_colliderWidget.RegisterToCallback(n"OnPress", this, n"OnPress");
    this.RegisterToGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
  }

  protected cb func OnUninitialize() -> Bool {
    super.OnUninitialize();
    this.ClearPreview();
    this.m_colliderWidget.UnregisterFromCallback(n"OnPress", this, n"OnPress");
    this.UnregisterFromGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
  }

  protected cb func OnPress(e: ref<inkPointerEvent>) -> Bool {
    let evt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if e.IsAction(n"mouse_left") {
      this.m_isMouseDown = true;
      evt = new inkGameNotificationLayer_SetCursorVisibility();
      evt.Init(false);
      this.QueueEvent(evt);
    };
  }

  protected cb func OnGlobalRelease(e: ref<inkPointerEvent>) -> Bool {
    let evt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if this.m_isMouseDown {
      if e.IsAction(n"mouse_left") {
        e.Consume();
        this.m_isMouseDown = false;
        evt = new inkGameNotificationLayer_SetCursorVisibility();
        evt.Init(true, new Vector2(0.50, 0.50));
        this.QueueEvent(evt);
      };
    } else {
      if e.IsAction(n"cancel") {
        this.m_data.token.TriggerCallback(null);
      };
    };
  }

  protected func HandleAxisInput(e: ref<inkPointerEvent>) -> Void {
    let amount: Float = e.GetAxisData();
    let ration: Float = 2.00;
    if e.IsAction(n"left_trigger") || e.IsAction(n"character_preview_rotate") {
      this.Rotate(amount * -this.c_ITEM_ROTATION_SPEED);
    } else {
      if e.IsAction(n"right_trigger") || e.IsAction(n"character_preview_rotate") {
        this.Rotate(amount * this.c_ITEM_ROTATION_SPEED);
      } else {
        if e.IsAction(n"right_stick_x") {
          this.RotateVector(new Vector3(0.00, 0.00, amount * ration));
        } else {
          if e.IsAction(n"right_stick_y") {
            this.RotateVector(new Vector3(0.00, amount * ration, 0.00));
          };
        };
      };
    };
  }

  protected cb func OnRelativeInput(e: ref<inkPointerEvent>) -> Bool {
    let amount: Float = e.GetAxisData();
    let ration: Float = 0.25;
    if this.m_isMouseDown {
      if e.IsAction(n"mouse_x") {
        this.RotateVector(new Vector3(0.00, 0.00, amount * ration));
      };
      if e.IsAction(n"mouse_y") {
        this.RotateVector(new Vector3(0.00, amount * ration, 0.00));
      };
    };
  }
}

public class ItemCraftingPreviewGameController extends inkItemPreviewGameController {

  protected cb func OnCrafrtingPreview(evt: ref<CraftingItemPreviewEvent>) -> Bool {
    if !evt.isGarment {
      this.PreviewItem(evt.itemID, true);
    } else {
      this.ClearPreview();
    };
  }
}

public native class BaseGarmentItemPreviewGameController extends inkInventoryPuppetPreviewGameController {

  protected let m_placementSlot: TweakDBID;

  protected let m_givenItem: ItemID;

  protected let m_initialItem: ItemID;

  protected cb func OnUninitialize() -> Bool {
    this.ClearViewData();
    super.OnUninitialize();
  }

  protected final func SetViewData(itemID: ItemID) -> Void {
    let legs: ItemID;
    let puppet: ref<gamePuppet>;
    let transactionSystem: ref<TransactionSystem>;
    let underwear: ItemID;
    if !ItemID.IsValid(this.m_givenItem) {
      puppet = this.GetGamePuppet();
      if IsDefined(puppet) {
        transactionSystem = GameInstance.GetTransactionSystem(puppet.GetGame());
        this.m_placementSlot = EquipmentSystem.GetPlacementSlot(itemID);
        this.m_initialItem = transactionSystem.GetItemInSlot(puppet, this.m_placementSlot).GetItemID();
        transactionSystem.RemoveItemFromSlot(puppet, this.m_placementSlot, true);
        this.m_givenItem = ItemID.FromTDBID(ItemID.GetTDBID(itemID));
        transactionSystem.GiveItem(puppet, this.m_givenItem, 1);
        transactionSystem.AddItemToSlot(puppet, this.m_placementSlot, this.m_givenItem);
        if ItemID.IsValid(this.m_initialItem) {
          if transactionSystem.MatchVisualTagByItemID(this.m_initialItem, puppet, n"hide_L1") {
            legs = transactionSystem.GetItemInSlot(puppet, t"AttachmentSlots.Legs").GetItemID();
            if ItemID.IsValid(legs) {
              transactionSystem.ResetItemAppearance(puppet, legs);
            } else {
              if this.IsBuildCensored() {
                underwear = transactionSystem.GetItemInSlot(puppet, t"AttachmentSlots.UnderwearBottom").GetItemID();
                if ItemID.IsValid(underwear) {
                  transactionSystem.ResetItemAppearance(puppet, underwear);
                };
              };
            };
          };
        };
      };
    };
  }

  protected final func ClearViewData() -> Void {
    let legs: ItemID;
    let puppet: ref<gamePuppet>;
    let transactionSystem: ref<TransactionSystem>;
    let underwear: ItemID;
    if ItemID.IsValid(this.m_givenItem) {
      puppet = this.GetGamePuppet();
      if IsDefined(puppet) {
        transactionSystem = GameInstance.GetTransactionSystem(puppet.GetGame());
        transactionSystem.RemoveItemFromSlot(puppet, this.m_placementSlot, true);
        transactionSystem.RemoveItem(puppet, this.m_givenItem, 1);
        transactionSystem.AddItemToSlot(puppet, this.m_placementSlot, this.m_initialItem);
        if ItemID.IsValid(this.m_initialItem) {
          if transactionSystem.MatchVisualTagByItemID(this.m_initialItem, puppet, n"hide_L1") {
            legs = transactionSystem.GetItemInSlot(puppet, t"AttachmentSlots.Legs").GetItemID();
            if ItemID.IsValid(legs) {
              transactionSystem.ChangeItemAppearanceByName(puppet, legs, n"empty_appearance_default");
            } else {
              if this.IsBuildCensored() {
                underwear = transactionSystem.GetItemInSlot(puppet, t"AttachmentSlots.UnderwearBottom").GetItemID();
                if ItemID.IsValid(underwear) {
                  transactionSystem.ChangeItemAppearanceByName(puppet, underwear, n"empty_appearance_default");
                };
              };
            };
          };
        };
      };
      this.m_givenItem = ItemID.None();
    };
  }

  protected final func IsBuildCensored() -> Bool {
    let charCustomization: ref<gameuiICharacterCustomizationSystem> = GameInstance.GetCharacterCustomizationSystem(this.GetGamePuppet().GetGame());
    if charCustomization != null {
      return !charCustomization.IsNudityAllowed();
    };
    return false;
  }
}

public class GarmentItemPreviewGameController extends BaseGarmentItemPreviewGameController {

  private let m_data: ref<InventoryItemPreviewData>;

  private let m_isMouseDown: Bool;

  @default(GarmentItemPreviewGameController, 100.0f)
  private const let c_GARMENT_ROTATION_SPEED: Float;

  protected cb func OnInitialize() -> Bool {
    super.OnInitialize();
    this.m_data = this.GetRootWidget().GetUserData(n"InventoryItemPreviewData") as InventoryItemPreviewData;
    this.RegisterToGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.RegisterToGlobalInputCallback(n"OnPostOnPress", this, n"OnGlobalPress");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
  }

  protected cb func OnUninitialize() -> Bool {
    this.UnregisterFromGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnPress", this, n"OnGlobalPress");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
    super.OnUninitialize();
  }

  protected cb func OnGlobalPress(e: ref<inkPointerEvent>) -> Bool {
    let evt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if e.IsAction(n"mouse_left") {
      this.m_isMouseDown = true;
      evt = new inkGameNotificationLayer_SetCursorVisibility();
      evt.Init(false);
      this.QueueEvent(evt);
    };
  }

  protected cb func OnGlobalRelease(e: ref<inkPointerEvent>) -> Bool {
    let evt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if this.m_isMouseDown {
      if e.IsAction(n"mouse_left") {
        e.Consume();
        this.m_isMouseDown = false;
        evt = new inkGameNotificationLayer_SetCursorVisibility();
        evt.Init(true, new Vector2(0.50, 0.50));
        this.QueueEvent(evt);
      };
    } else {
      if e.IsAction(n"cancel") || e.IsAction(n"click") {
        this.m_data.token.TriggerCallback(null);
      };
    };
  }

  protected cb func OnPreviewInitialized() -> Bool {
    super.OnPreviewInitialized();
    this.SetViewData(this.m_data.itemID);
  }

  protected func HandleAxisInput(e: ref<inkPointerEvent>) -> Void {
    let amount: Float = e.GetAxisData();
    if e.IsAction(n"left_trigger") || e.IsAction(n"character_preview_rotate") {
      this.Rotate(amount * -this.c_GARMENT_ROTATION_SPEED);
    } else {
      if e.IsAction(n"right_trigger") || e.IsAction(n"character_preview_rotate") {
        this.Rotate(amount * this.c_GARMENT_ROTATION_SPEED);
      };
    };
  }

  protected cb func OnRelativeInput(e: ref<inkPointerEvent>) -> Bool {
    let ratio: Float;
    let velocity: Float;
    let offset: Float = e.GetAxisData();
    if offset > 0.00 {
      ratio = ClampF(offset / this.m_maxMousePointerOffset, 0.50, 1.00);
    } else {
      ratio = ClampF(offset / this.m_maxMousePointerOffset, -1.00, -0.50);
    };
    velocity = ratio * this.m_mouseRotationSpeed;
    if this.m_isMouseDown {
      if e.IsAction(n"mouse_x") {
        this.Rotate(velocity);
      };
    };
  }
}

public class CraftingGarmentItemPreviewGameController extends WardrobeSetPreviewGameController {

  private let m_initialItems: array<ItemID>;

  private let m_previewedItem: ItemID;

  protected cb func OnPreviewInitialized() -> Bool {
    super.OnPreviewInitialized();
    this.m_previewedItem = ItemID.None();
    this.m_initialItems = this.GetVisualItems();
    this.SetUpPuppet(this.m_initialItems);
    this.PreviewUnequipFromSlot(t"AttachmentSlots.WeaponLeft");
    this.PreviewUnequipFromSlot(t"AttachmentSlots.WeaponRight");
  }

  protected cb func OnCrafrtingPreview(evt: ref<CraftingItemPreviewEvent>) -> Bool {
    let i: Int32;
    if ItemID.IsValid(this.m_previewedItem) {
      this.PreviewUnequipItem(this.m_previewedItem);
      this.m_previewedItem = ItemID.None();
      i = 0;
      while i < ArraySize(this.m_initialItems) {
        this.PreviewEquipItem(this.m_initialItems[i]);
        i += 1;
      };
    };
    if evt.isGarment {
      this.m_previewedItem = evt.itemID;
      this.PreviewEquipAndForceShowItem(evt.itemID);
    };
  }

  protected cb func OnUninitialize() -> Bool {
    this.CleanUpPuppet();
    super.OnUninitialize();
  }
}

public native class WardrobeSetPreviewGameController extends BaseGarmentItemPreviewGameController {

  private edit let m_colliderWidgetRef: inkWidgetRef;

  private let m_colliderWidget: wref<inkWidget>;

  private let m_data: ref<InventoryItemPreviewData>;

  private let m_isMouseDown: Bool;

  private let m_isNotification: Bool;

  @default(WardrobeSetPreviewGameController, 100.0f)
  private const let c_GARMENT_ROTATION_SPEED: Float;

  public final native func PreviewEquipItem(itemID: ItemID) -> Void;

  public final native func PreviewEquipAndForceShowItem(itemID: ItemID) -> Void;

  public final native func PreviewUnequipItem(itemID: ItemID) -> Void;

  public final native func PreviewUnequipFromSlot(slotID: TweakDBID) -> Void;

  public final native func PreviewUnequipFromEquipmentArea(equipmentArea: gamedataEquipmentArea) -> Void;

  public final native func ClearPuppet() -> Void;

  public final native func RestorePuppetWeapons() -> Void;

  public final native func HandleUnderwearVisualTags() -> Void;

  public final native func SetUpPuppet(visualItems: array<ItemID>) -> Void;

  protected cb func OnInitialize() -> Bool {
    let evt: ref<RegisterPreviewControllerEvent>;
    super.OnInitialize();
    if inkWidgetRef.IsValid(this.m_colliderWidgetRef) {
      this.m_colliderWidget = inkWidgetRef.Get(this.m_colliderWidgetRef);
    } else {
      this.m_colliderWidget = this.GetRootWidget();
    };
    this.m_colliderWidget.RegisterToCallback(n"OnPress", this, n"OnPress");
    this.RegisterToGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.RegisterToGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
    this.m_data = this.GetRootWidget().GetUserData(n"InventoryItemPreviewData") as InventoryItemPreviewData;
    this.m_isNotification = this.m_data != null;
    if !this.m_isNotification {
      evt = new RegisterPreviewControllerEvent();
      evt.controller = this;
      this.QueueEvent(evt);
    };
  }

  protected cb func OnUninitialize() -> Bool {
    this.m_colliderWidget.UnregisterFromCallback(n"OnPress", this, n"OnPress");
    this.UnregisterFromGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelative", this, n"OnRelativeInput");
    super.OnUninitialize();
  }

  protected cb func OnPreviewInitialized() -> Bool {
    super.OnPreviewInitialized();
    if this.m_isNotification {
      this.SetUpPuppet(this.GetVisualItems());
      this.PreviewUnequipFromSlot(t"AttachmentSlots.WeaponLeft");
      this.PreviewUnequipFromSlot(t"AttachmentSlots.WeaponRight");
      this.PreviewEquipAndForceShowItem(this.m_data.itemID);
    };
  }

  public final func GetVisualItems() -> array<ItemID> {
    let i: Int32;
    let slots: array<gamedataEquipmentArea>;
    let visualItem: ItemID;
    let visualItems: array<ItemID>;
    let gi: GameInstance = this.GetGamePuppet().GetGame();
    let player: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(gi).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
    let wardrobeSystem: ref<WardrobeSystem> = GameInstance.GetWardrobeSystem(gi);
    let clothingSet: ref<ClothingSet> = wardrobeSystem.GetActiveClothingSet();
    if clothingSet != null {
      i = 0;
      while i < ArraySize(clothingSet.clothingList) {
        visualItem = clothingSet.clothingList[i].visualItem;
        if ItemID.IsValid(visualItem) {
          ArrayPush(visualItems, visualItem);
        };
        i += 1;
      };
    } else {
      slots = equipmentSystem.GetPaperDollSlots(player);
      i = 0;
      while i < ArraySize(slots) {
        visualItem = equipmentSystem.GetActiveVisualItem(player, slots[i]);
        if ItemID.IsValid(visualItem) {
          ArrayPush(visualItems, visualItem);
        };
        i += 1;
      };
    };
    return visualItems;
  }

  public final func RestorePuppetEquipment() -> Void {
    let visualItem: ItemID;
    let puppet: ref<gamePuppet> = this.GetGamePuppet();
    let gi: GameInstance = puppet.GetGame();
    let player: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(gi).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let equipmentSystem: ref<EquipmentSystem> = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(player);
    let slots: array<gamedataEquipmentArea> = equipmentSystem.GetPaperDollSlots(player);
    let i: Int32 = 0;
    while i < ArraySize(slots) {
      visualItem = equipmentSystem.GetActiveVisualItem(player, slots[i]);
      if ItemID.IsValid(visualItem) && !playerData.IsSlotHidden(slots[i]) {
        this.PreviewEquipItem(visualItem);
      };
      i += 1;
    };
    this.HandleUnderwearVisualTags();
  }

  protected final func TryRestoreActiveWardrobeSet() -> Bool {
    let equipmentSystem: ref<EquipmentSystem>;
    let gi: GameInstance;
    let player: ref<PlayerPuppet>;
    let req: ref<EquipWardrobeSetRequest>;
    let puppet: ref<gamePuppet> = this.GetGamePuppet();
    let activeSetIndex: gameWardrobeClothingSetIndex = GameInstance.GetWardrobeSystem(puppet.GetGame()).GetActiveClothingSetIndex();
    if NotEquals(activeSetIndex, gameWardrobeClothingSetIndex.INVALID) {
      gi = puppet.GetGame();
      player = GameInstance.GetPlayerSystem(gi).GetLocalPlayerControlledGameObject() as PlayerPuppet;
      equipmentSystem = GameInstance.GetScriptableSystemsContainer(gi).Get(n"EquipmentSystem") as EquipmentSystem;
      req = new EquipWardrobeSetRequest();
      req.setID = activeSetIndex;
      req.owner = player;
      equipmentSystem.QueueRequest(req);
      return true;
    };
    return false;
  }

  public final func DelayedResetItemAppearanceInSlot(slotID: TweakDBID) -> Void {
    let playerControlledObject: ref<GameObject> = this.GetPlayerControlledObject();
    let evt: ref<ResetItemAppearanceInSlotDelayEvent> = new ResetItemAppearanceInSlotDelayEvent();
    evt.slotID = slotID;
    GameInstance.GetDelaySystem(playerControlledObject.GetGame()).DelayEventNextFrame(playerControlledObject, evt);
  }

  protected final func CleanUpPuppet() -> Void {
    let underwearID: ItemID;
    let puppet: ref<gamePuppet> = this.GetGamePuppet();
    let gi: GameInstance = puppet.GetGame();
    let player: ref<PlayerPuppet> = GameInstance.GetPlayerSystem(gi).GetLocalPlayerControlledGameObject() as PlayerPuppet;
    let transactionSystem: ref<TransactionSystem> = GameInstance.GetTransactionSystem(gi);
    let playerData: ref<EquipmentSystemPlayerData> = EquipmentSystem.GetData(player);
    this.ClearPuppet();
    if this.TryRestoreActiveWardrobeSet() {
      if playerData.IsSlotHidden(gamedataEquipmentArea.UnderwearTop) {
        transactionSystem.RemoveItemFromSlot(puppet, t"AttachmentSlots.UnderwearTop");
      } else {
        underwearID = transactionSystem.GetItemDataByTDBID(puppet, t"Items.Underwear_Basic_01_Top").GetID();
        transactionSystem.AddItemToSlot(puppet, t"AttachmentSlots.UnderwearTop", underwearID, true);
      };
      if playerData.IsSlotHidden(gamedataEquipmentArea.UnderwearBottom) {
        transactionSystem.RemoveItemFromSlot(puppet, t"AttachmentSlots.UnderwearBottom");
      } else {
        underwearID = transactionSystem.GetItemDataByTDBID(puppet, t"Items.Underwear_Basic_01_Bottom").GetID();
        transactionSystem.AddItemToSlot(puppet, t"AttachmentSlots.UnderwearBottom", underwearID, true);
      };
    } else {
      this.RestorePuppetEquipment();
    };
    this.RestorePuppetWeapons();
    this.DelayedResetItemAppearanceInSlot(t"AttachmentSlots.Chest");
  }

  protected cb func OnPress(e: ref<inkPointerEvent>) -> Bool {
    let menuEvt: ref<inkMenuLayer_SetCursorVisibility>;
    let notificationEvt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if e.IsAction(n"mouse_left") {
      this.m_isMouseDown = true;
      if this.m_isNotification {
        notificationEvt = new inkGameNotificationLayer_SetCursorVisibility();
        notificationEvt.Init(false);
        this.QueueEvent(notificationEvt);
      } else {
        menuEvt = new inkMenuLayer_SetCursorVisibility();
        menuEvt.Init(false);
        this.QueueEvent(menuEvt);
      };
    };
  }

  protected cb func OnGlobalRelease(e: ref<inkPointerEvent>) -> Bool {
    let menuEvt: ref<inkMenuLayer_SetCursorVisibility>;
    let notificationEvt: ref<inkGameNotificationLayer_SetCursorVisibility>;
    if this.m_isMouseDown {
      if e.IsAction(n"mouse_left") {
        e.Consume();
        this.m_isMouseDown = false;
        if this.m_isNotification {
          notificationEvt = new inkGameNotificationLayer_SetCursorVisibility();
          notificationEvt.Init(true, new Vector2(0.50, 0.50));
          this.QueueEvent(notificationEvt);
        } else {
          menuEvt = new inkMenuLayer_SetCursorVisibility();
          menuEvt.Init(true, new Vector2(0.25, 0.50));
          this.QueueEvent(menuEvt);
        };
      };
    } else {
      if e.IsAction(n"cancel") {
        if this.m_isNotification {
          this.CleanUpPuppet();
          this.m_data.token.TriggerCallback(null);
        };
      };
    };
  }

  protected func HandleAxisInput(e: ref<inkPointerEvent>) -> Void {
    let amount: Float = e.GetAxisData();
    if e.IsAction(n"left_trigger") || e.IsAction(n"character_preview_rotate") {
      this.Rotate(amount * -this.c_GARMENT_ROTATION_SPEED);
    } else {
      if e.IsAction(n"right_trigger") || e.IsAction(n"character_preview_rotate") {
        this.Rotate(amount * this.c_GARMENT_ROTATION_SPEED);
      };
    };
  }

  protected cb func OnRelativeInput(e: ref<inkPointerEvent>) -> Bool {
    let offset: Float;
    let ratio: Float;
    let velocity: Float;
    if this.m_isMouseDown {
      offset = e.GetAxisData();
      if offset > 0.00 {
        ratio = ClampF(offset / this.m_maxMousePointerOffset, 0.50, 1.00);
      } else {
        ratio = ClampF(offset / this.m_maxMousePointerOffset, -1.00, -0.50);
      };
      velocity = ratio * this.m_mouseRotationSpeed;
      if e.IsAction(n"mouse_x") {
        this.Rotate(velocity);
      };
    };
  }
}

public abstract class ItemPreviewHelper extends IScriptable {

  public final static func ShowPreviewItem(controller: ref<inkGameController>, itemData: InventoryItemData, isGarment: Bool, callbackName: CName) -> ref<inkGameNotificationToken> {
    let token: ref<inkGameNotificationToken>;
    let previewData: ref<InventoryItemPreviewData> = ItemPreviewHelper.GetPreviewData(controller, itemData, isGarment);
    if IsDefined(previewData) {
      token = controller.ShowGameNotification(previewData);
      token.RegisterListener(controller, callbackName);
    };
    return token;
  }

  public final static func ShowPreviewItem(controller: ref<inkGameController>, itemData: wref<UIInventoryItem>, isGarment: Bool, callbackName: CName) -> ref<inkGameNotificationToken> {
    let token: ref<inkGameNotificationToken>;
    let previewData: ref<InventoryItemPreviewData> = ItemPreviewHelper.GetPreviewData(controller, itemData, isGarment);
    if IsDefined(previewData) {
      token = controller.ShowGameNotification(previewData);
      token.RegisterListener(controller, callbackName);
    };
    return token;
  }

  private final static func GetPreviewData(controller: ref<IScriptable>, itemData: InventoryItemData, isGarment: Bool) -> ref<InventoryItemPreviewData> {
    let itemID: ItemID = InventoryItemData.GetGameItemData(itemData).GetID();
    let previewData: ref<InventoryItemPreviewData> = new InventoryItemPreviewData();
    previewData.itemID = itemID;
    previewData.itemName = InventoryItemData.GetName(itemData);
    previewData.itemQualityState = InventoryItemData.GetQuality(itemData);
    previewData.requiredLevel = InventoryItemData.GetRequiredLevel(itemData);
    previewData.queueName = n"modal_popup_fullscreen";
    previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\item_preview.inkwidget";
    previewData.isBlocking = true;
    previewData.useCursor = true;
    if isGarment {
      previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\garment_item_preview.inkwidget";
      return previewData;
    };
    previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\item_preview.inkwidget";
    return previewData;
  }

  private final static func GetPreviewData(controller: ref<IScriptable>, itemData: wref<UIInventoryItem>, isGarment: Bool) -> ref<InventoryItemPreviewData> {
    let previewData: ref<InventoryItemPreviewData> = new InventoryItemPreviewData();
    previewData.itemID = itemData.ID;
    previewData.itemName = itemData.GetName();
    previewData.itemQualityState = itemData.GetQualityName();
    previewData.requiredLevel = itemData.GetRequiredLevel();
    previewData.queueName = n"modal_popup_fullscreen";
    previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\item_preview.inkwidget";
    previewData.isBlocking = true;
    previewData.useCursor = true;
    if isGarment {
      previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\garment_item_preview.inkwidget";
      return previewData;
    };
    previewData.notificationName = n"base\\gameplay\\gui\\widgets\\notifications\\item_preview.inkwidget";
    return previewData;
  }
}
