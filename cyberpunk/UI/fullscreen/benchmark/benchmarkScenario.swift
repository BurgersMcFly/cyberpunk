
public class MenuScenario_BenchmarkResults extends MenuScenario_BaseMenu {

  private let m_callbackData: ref<inkCallbackConnectorData>;

  protected cb func OnEnterScenario(prevScenario: CName, userData: ref<IScriptable>) -> Bool {
    this.GetMenusState().OpenMenu(n"benchmark_results_menu", userData);
    this.m_callbackData = userData as inkCallbackConnectorData;
  }

  protected cb func OnBenchmarkResultsClose() -> Bool {
    this.SwitchToScenario(n"MenuScenario_Idle");
    this.m_callbackData.TriggerCallback();
  }
}
