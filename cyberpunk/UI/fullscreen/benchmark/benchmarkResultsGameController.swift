
public class BenchmarkLabelController extends inkLogicController {

  private edit let m_labelWidget: inkTextRef;

  private edit let m_valueWidget: inkTextRef;

  public final func SetLabel(label: String) -> Void {
    inkTextRef.SetText(this.m_labelWidget, label);
  }

  public final func SetValue(value: String) -> Void {
    inkTextRef.SetText(this.m_valueWidget, value);
  }
}

public class BenchmarkResultsGameController extends inkGameController {

  private edit let m_button: inkWidgetRef;

  private edit let m_entriesListContainer: inkCompoundRef;

  private edit let m_lineEntryName: CName;

  private edit let m_sectionEntryName: CName;

  private let m_benchmarkSummary: ref<worldBenchmarkSummary>;

  private let m_exitRequestToken: ref<inkGameNotificationToken>;

  protected cb func OnInitialize() -> Bool {
    inkCompoundRef.RemoveAllChildren(this.m_entriesListContainer);
    inkWidgetRef.RegisterToCallback(this.m_button, n"OnRelease", this, n"OnShowExitPrompt");
    this.DisplayBenchmarkSummary();
  }

  protected cb func OnShowExitPrompt(e: ref<inkPointerEvent>) -> Bool {
    if e.IsAction(n"click") {
      this.m_exitRequestToken = GenericMessageNotification.Show(this, GetLocalizedTextByKey(n"UI-Benchmark-ExitBenchmark"), GetLocalizedTextByKey(n"UI-Benchmark-ExitConfirmation"), GenericMessageNotificationType.ConfirmCancel);
      this.m_exitRequestToken.RegisterListener(this, n"OnCloseBenchmarkResults");
    };
  }

  protected cb func OnCloseBenchmarkResults(data: ref<inkGameNotificationData>) -> Bool {
    let menuEvent: ref<inkMenuInstance_SpawnEvent>;
    let resultData: ref<GenericMessageNotificationCloseData> = data as GenericMessageNotificationCloseData;
    if Equals(resultData.result, GenericMessageNotificationResult.Confirm) {
      menuEvent = new inkMenuInstance_SpawnEvent();
      menuEvent.Init(n"OnBenchmarkResultsClose");
      this.QueueEvent(menuEvent);
    };
    this.m_exitRequestToken = null;
  }

  protected cb func OnSetUserData(data: ref<IScriptable>) -> Bool {
    let callbackConnector: ref<inkCallbackConnectorData> = data as inkCallbackConnectorData;
    this.m_benchmarkSummary = callbackConnector.userData as worldBenchmarkSummary;
  }

  private final func SpawnSummaryLine(entryName: CName, label: String, value: String) -> Void {
    let lineData: ref<BenchmarkLineData> = new BenchmarkLineData();
    lineData.label = label;
    lineData.value = value;
    let spawnData: ref<inkAsyncSpawnData> = new inkAsyncSpawnData();
    spawnData.libraryID = entryName;
    spawnData.parentWidget = inkWidgetRef.Get(this.m_entriesListContainer) as inkCompoundWidget;
    spawnData.userData = lineData;
    this.AsyncSpawnFromLocal(spawnData, this, n"OnLineSpawned");
  }

  private final func OnLineSpawned(widget: ref<inkWidget>, userData: ref<IScriptable>) -> Void {
    let labelController: ref<BenchmarkLabelController> = widget.GetController() as BenchmarkLabelController;
    let lineData: ref<BenchmarkLineData> = userData as BenchmarkLineData;
    labelController.SetLabel(lineData.label);
    labelController.SetValue(lineData.value);
  }

  private final func DisplayBenchmarkSummary() -> Void {
    let dlssValues: array<CName>;
    let locOn: String = GetLocalizedTextByKey(n"UI-UserActions-Yes");
    let locOff: String = GetLocalizedTextByKey(n"UI-UserActions-No");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Off");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Auto");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Quality");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Balanced");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Performance");
    ArrayPush(dlssValues, n"UI-Settings-Video-QualitySetting-Ultra_Performance");
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-AverageFPS"), FloatToStringPrec(this.m_benchmarkSummary.averageFps, 2));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-MinFPS"), FloatToStringPrec(this.m_benchmarkSummary.minFps, 2));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-MaxFPS"), FloatToStringPrec(this.m_benchmarkSummary.maxFps, 2));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-Time"), FloatToStringPrec(this.m_benchmarkSummary.time, 2));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-FrameNumber"), ToString(this.m_benchmarkSummary.frameNumber));
    this.SpawnSummaryLine(this.m_sectionEntryName, GetLocalizedTextByKey(n"UI-Labels-Settings"), "");
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-PresetName"), GetLocalizedTextByKey(this.m_benchmarkSummary.presetLocalizedName));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-RenderWidth"), ToString(this.m_benchmarkSummary.renderWidth));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-RenderHeight"), ToString(this.m_benchmarkSummary.renderHeight));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-Windowed"), this.m_benchmarkSummary.windowed ? locOn : locOff);
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-VerticalSync"), this.m_benchmarkSummary.verticalSync ? locOn : locOff);
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-DLSSEnabled"), this.m_benchmarkSummary.DLSSEnabled ? locOn : locOff);
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-DLSSQuality"), GetLocalizedTextByKey(dlssValues[this.m_benchmarkSummary.DLSSQuality]));
    this.SpawnSummaryLine(this.m_lineEntryName, GetLocalizedTextByKey(n"UI-Benchmark-DRSEnabled"), this.m_benchmarkSummary.DRSEnabled ? locOn : locOff);
  }
}
