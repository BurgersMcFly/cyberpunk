
public class PatchNotesGameController extends gameuiMenuGameController {

  private edit let m_buttonHintsRef: inkWidgetRef;

  @default(PatchNotesGameController, intro)
  private edit let m_animationName: CName;

  private edit let m_confirmBtn: inkWidgetRef;

  private edit let m_selectorRef: inkWidgetRef;

  private edit let m_patchImageRef: inkImageRef;

  private edit const let m_platformSpecificNotes: array<inkWidgetRef>;

  private let m_uiSystem: ref<UISystem>;

  private let m_menuEventDispatcher: wref<inkMenuEventDispatcher>;

  private let m_animProxy: ref<inkAnimProxy>;

  @default(PatchNotesGameController, true)
  private let m_isInputBlocked: Bool;

  private let m_contentList: array<inkCompoundRef>;

  private let m_atlasParts: array<CName>;

  private let m_labelsList: array<String>;

  private let m_selectedIndex: Int32;

  private edit let m_contentPatchRef_1500: inkCompoundRef;

  private edit let m_contentPatchRef_1600: inkCompoundRef;

  private edit let m_contentPatchAtlasPart_1500: CName;

  private edit let m_contentPatchAtlasPart_1600: CName;

  private edit let m_tabsRef: inkWidgetRef;

  private edit let m_tabsController: wref<TabRadioGroup>;

  private edit let m_cloudSaveSettingsBlockRef: inkCompoundRef;

  private let m_cloudSaveSettingsBlock: wref<PatchSettingsController>;

  protected cb func OnInitialize() -> Bool {
    super.OnInitialize();
    this.PlaySound(n"GameMenu", n"OnOpen");
    this.RegisterToGlobalInputCallback(n"OnPreOnRelease", this, n"OnGlobalRelease");
    this.PlayAnimation(this.m_animationName);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnIntroAnimationFinished");
    inkWidgetRef.RegisterToCallback(this.m_confirmBtn, n"OnPress", this, n"OnPressConfirm");
    this.m_uiSystem = GameInstance.GetUISystem(this.GetPlayerControlledObject().GetGame());
    this.SetupPlatformSpecificNotes();
    this.SetPatchContent(gameuiPatchIntro.Patch1500_NextGen, this.m_contentPatchRef_1500, "UI-DLC-PatchNotes1500-Name", this.m_contentPatchAtlasPart_1500);
    this.SetPatchContent(gameuiPatchIntro.Patch1600, this.m_contentPatchRef_1600, "UI-DLC-PatchNotes1600-Name", this.m_contentPatchAtlasPart_1600);
    this.InitTabs();
    this.m_selectedIndex = ArraySize(this.m_contentList) - 1;
    inkWidgetRef.SetVisible(this.m_contentPatchRef_1600, true);
    inkImageRef.SetTexturePart(this.m_patchImageRef, this.m_contentPatchAtlasPart_1600);
    this.m_cloudSaveSettingsBlock = inkWidgetRef.GetControllerByType(this.m_cloudSaveSettingsBlockRef, n"PatchSettingsController") as PatchSettingsController;
    this.m_cloudSaveSettingsBlock.SetupControl(this.GetPlayerControlledObject().GetGame());
  }

  private final func SetupPlatformSpecificNotes() -> Void {
    let controller: ref<PlatformSpecificPatchNote>;
    let platformNameToShow: String;
    let platformNameToSkip: String;
    let shouldBeVisible: Bool;
    let platformName: String = GetPlatformShortName();
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(this.m_platformSpecificNotes);
    while i < limit {
      controller = inkWidgetRef.GetControllerByType(this.m_platformSpecificNotes[i], n"PlatformSpecificPatchNote") as PlatformSpecificPatchNote;
      platformNameToSkip = controller.GetPlatformNameToSkip();
      platformNameToShow = controller.GetPlatformName();
      shouldBeVisible = true;
      if NotEquals(platformNameToSkip, "") {
        if Equals(platformNameToSkip, platformName) {
          shouldBeVisible = false;
        };
      } else {
        if NotEquals(platformNameToShow, "") {
          shouldBeVisible = Equals(platformName, platformNameToShow);
        };
      };
      inkWidgetRef.SetVisible(this.m_platformSpecificNotes[i], shouldBeVisible);
      i += 1;
    };
  }

  private final func SetPatchContent(version: gameuiPatchIntro, contentRef: inkCompoundRef, label: String, imageAtlasPart: CName) -> Void {
    if this.m_uiSystem.IsPatchIntroNeeded(version) {
      ArrayPush(this.m_contentList, contentRef);
      ArrayPush(this.m_labelsList, label);
      ArrayPush(this.m_atlasParts, imageAtlasPart);
      this.m_uiSystem.MarkPatchIntroAsSeen(version);
    };
    inkWidgetRef.SetVisible(contentRef, false);
  }

  private final func InitTabs() -> Void {
    this.m_tabsController = inkWidgetRef.GetController(this.m_tabsRef) as TabRadioGroup;
    this.m_tabsController.SetData(ArraySize(this.m_contentList), this.m_labelsList);
    this.m_tabsController.RegisterToCallback(n"OnValueChanged", this, n"OnTabChanged");
    this.m_tabsController.Toggle(ArraySize(this.m_contentList) - 1);
  }

  protected cb func OnTabChanged(controller: wref<inkRadioGroupController>, selectedIndex: Int32) -> Bool {
    if this.m_selectedIndex != selectedIndex {
      inkWidgetRef.SetVisible(this.m_contentList[this.m_selectedIndex], false);
      this.m_selectedIndex = selectedIndex;
      inkWidgetRef.SetVisible(this.m_contentList[this.m_selectedIndex], true);
      inkImageRef.SetTexturePart(this.m_patchImageRef, this.m_atlasParts[this.m_selectedIndex]);
    };
  }

  protected cb func OnUninitialize() -> Bool {
    super.OnUninitialize();
    this.UnregisterFromGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
  }

  protected cb func OnSetMenuEventDispatcher(menuEventDispatcher: wref<inkMenuEventDispatcher>) -> Bool {
    super.OnSetMenuEventDispatcher(menuEventDispatcher);
    this.m_menuEventDispatcher = menuEventDispatcher;
  }

  protected cb func OnPatchSelectEvent(evt: ref<PatchSelectEvent>) -> Bool {
    if this.m_selectedIndex != evt.m_index {
      inkWidgetRef.SetVisible(this.m_contentList[this.m_selectedIndex], false);
      this.m_selectedIndex = evt.m_index;
      inkWidgetRef.SetVisible(this.m_contentList[this.m_selectedIndex], true);
      inkImageRef.SetTexturePart(this.m_patchImageRef, this.m_atlasParts[this.m_selectedIndex]);
    };
  }

  protected cb func OnPressConfirm(evt: ref<inkPointerEvent>) -> Bool {
    if evt.IsAction(n"click") {
      evt.Handle();
      this.Close();
    };
  }

  protected cb func OnGlobalRelease(evt: ref<inkPointerEvent>) -> Bool {
    if !this.m_isInputBlocked {
      if evt.IsAction(n"back") || evt.IsAction(n"one_click_confirm") {
        this.Close();
      };
    };
    evt.Handle();
  }

  private final func Close() -> Void {
    let playbackOptions: inkAnimOptions;
    this.PlaySound(n"Button", n"OnPress");
    this.PlaySound(n"GameMenu", n"OnClose");
    playbackOptions.playReversed = true;
    this.PlayAnimation(this.m_animationName, playbackOptions);
    this.m_animProxy.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnOutroAnimationFinished");
    this.m_isInputBlocked = true;
  }

  protected cb func OnIntroAnimationFinished(proxy: ref<inkAnimProxy>) -> Bool {
    this.m_isInputBlocked = false;
  }

  protected cb func OnOutroAnimationFinished(proxy: ref<inkAnimProxy>) -> Bool {
    this.m_menuEventDispatcher.SpawnEvent(n"OnClosePatchNotes");
  }

  private final func PlayAnimation(animationName: CName, opt playbackOptions: inkAnimOptions) -> Void {
    if IsDefined(this.m_animProxy) && this.m_animProxy.IsPlaying() {
      this.m_animProxy.Stop(true);
    };
    this.m_animProxy = this.PlayLibraryAnimation(animationName, playbackOptions);
  }
}

public class PatchSettingsController extends inkLogicController {

  private edit let m_settingsGroupName: CName;

  private edit let m_settingsVarName: CName;

  private edit let m_settingsVarControlWidget: inkWidgetRef;

  private let m_settingsVarController: wref<SettingsSelectorController>;

  private let m_settingsListener: ref<PatchSettingsControllerListener>;

  protected cb func OnInitialize() -> Bool {
    this.m_settingsVarController = inkWidgetRef.GetController(this.m_settingsVarControlWidget) as SettingsSelectorController;
    this.m_settingsListener = new PatchSettingsControllerListener();
    this.m_settingsListener.RegisterController(this);
  }

  public final func SetupControl(gameInst: GameInstance) -> Void {
    let settingsGroup: ref<ConfigGroup> = GameInstance.GetSettingsSystem(gameInst).GetGroup(this.m_settingsGroupName);
    let settingsVar: ref<ConfigVar> = settingsGroup.GetVar(this.m_settingsVarName);
    this.m_settingsListener.Register(settingsGroup.GetPath());
    if IsDefined(settingsVar) {
      this.m_settingsVarController.Setup(settingsVar, true);
      this.GetRootWidget().SetVisible(true);
    } else {
      this.GetRootWidget().SetVisible(false);
    };
  }

  public final func OnVarModified(groupPath: CName, varName: CName, varType: ConfigVarType, reason: ConfigChangeReason) -> Void {
    if Equals(this.m_settingsGroupName, groupPath) && Equals(this.m_settingsVarName, varName) {
      this.m_settingsVarController.Refresh();
    };
  }
}

public class PatchSettingsControllerListener extends ConfigVarListener {

  private let m_ctrl: wref<PatchSettingsController>;

  public final func RegisterController(ctrl: ref<PatchSettingsController>) -> Void {
    this.m_ctrl = ctrl;
  }

  public func OnVarModified(groupPath: CName, varName: CName, varType: ConfigVarType, reason: ConfigChangeReason) -> Void {
    this.m_ctrl.OnVarModified(groupPath, varName, varType, reason);
  }
}

public class SelectorControllerListInt extends SettingsSelectorControllerList {

  private let m_count: Int32;

  private let m_index: Int32;

  public func Setup(entry: ref<ConfigVar>, isPreGame: Bool) -> Void;

  public final func SpawnDots(count: Int32, index: Int32) -> Void {
    this.m_count = count;
    this.PopulateDots(this.m_count);
    this.SelectDot(0);
    this.m_index = index;
  }

  private func ChangeValue(forward: Bool) -> Void {
    let evt: ref<PatchSelectEvent>;
    if forward {
      this.m_index = this.m_index >= this.m_count - 1 ? 0 : this.m_index + 1;
    } else {
      this.m_index = this.m_index <= 0 ? this.m_count - 1 : this.m_index - 1;
    };
    this.Refresh();
    evt = new PatchSelectEvent();
    evt.m_index = this.m_index;
    this.QueueEvent(evt);
  }

  public func Refresh() -> Void {
    this.SelectDot(this.m_index);
  }
}

public class PlatformSpecificPatchNote extends inkLogicController {

  public edit let m_platformName: String;

  public edit let m_platformNameToSkip: String;

  public final func GetPlatformName() -> String {
    return this.m_platformName;
  }

  public final func GetPlatformNameToSkip() -> String {
    return this.m_platformNameToSkip;
  }
}

public static exec func DbgMarkPatchIntroAsSeen(gameInstance: GameInstance) -> Void {
  GameInstance.GetUISystem(gameInstance).MarkPatchIntroAsSeen(gameuiPatchIntro.Patch1500_NextGen);
  GameInstance.GetUISystem(gameInstance).MarkPatchIntroAsSeen(gameuiPatchIntro.Patch1600);
}

public static exec func DbgResetPatchIntro(gameInstance: GameInstance) -> Void {
  GameInstance.GetUISystem(gameInstance).ResetPatchIntro(gameuiPatchIntro.Patch1500_NextGen);
  GameInstance.GetUISystem(gameInstance).ResetPatchIntro(gameuiPatchIntro.Patch1600);
}
