
public class TarotPreviewGameController extends inkGameController {

  private edit let m_background: inkWidgetRef;

  private edit let m_previewImage: inkImageRef;

  private edit let m_previewTitle: inkTextRef;

  private edit let m_previewDescription: inkTextRef;

  private let m_data: ref<TarotCardPreviewData>;

  private let m_isClosing: Bool;

  protected cb func OnInitialize() -> Bool {
    this.m_data = this.GetRootWidget().GetUserData(n"TarotCardPreviewData") as TarotCardPreviewData;
    this.RegisterToGlobalInputCallback(n"OnPostOnRelease", this, n"OnGlobalRelease");
    this.Show(this.m_data.cardData);
  }

  protected final func Show(data: TarotCardData) -> Void {
    let localizedText: String;
    this.m_isClosing = false;
    InkImageUtils.RequestSetImage(this, this.m_previewImage, "UIIcon." + NameToString(data.imagePath) + "_BIG");
    inkTextRef.SetText(this.m_previewTitle, data.label);
    localizedText = GetLocalizedText(data.desc);
    inkTextRef.SetText(this.m_previewDescription, localizedText);
    this.PlayLibraryAnimation(n"panel_intro");
  }

  protected cb func OnGlobalRelease(evt: ref<inkPointerEvent>) -> Bool {
    let proxy: ref<inkAnimProxy>;
    if evt.IsAction(n"cancel") || evt.IsAction(n"click") {
      if !this.m_isClosing {
        this.m_isClosing = true;
        proxy = this.PlayLibraryAnimation(n"panel_outro");
        proxy.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnOutroCompleted");
      };
    };
  }

  protected cb func OnOutroCompleted(anim: ref<inkAnimProxy>) -> Bool {
    this.m_data.token.TriggerCallback(null);
  }
}
