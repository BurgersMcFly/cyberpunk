
public class UIInventoryItemProgramData extends IScriptable {

  public let MemoryCost: Int32;

  public let BaseCost: Int32;

  public let UploadTime: Float;

  public let Duration: Float;

  public let Cooldown: Float;

  public let AttackEffects: array<ref<DamageEffectUIEntry>>;

  private final static func GetDurationAndAttackEffects(instance: wref<UIInventoryItemProgramData>, actionRecord: wref<ObjectAction_Record>, player: wref<PlayerPuppet>) -> Void {
    let dummyEntityID: EntityID;
    let duration: wref<StatModifierGroup_Record>;
    let durationMods: array<wref<ObjectActionEffect_Record>>;
    let effectToCast: wref<StatusEffect_Record>;
    let effects: array<ref<DamageEffectUIEntry>>;
    let emptyObject: ref<GameObject>;
    let i: Int32;
    let ignoredDurationStats: array<wref<StatusEffect_Record>>;
    let j: Int32;
    let lastMatchingEffect: wref<StatusEffect_Record>;
    let limit: Int32;
    let limitJ: Int32;
    let statModifiers: array<wref<StatModifier_Record>>;
    ArrayPush(ignoredDurationStats, TweakDBInterface.GetStatusEffectRecord(t"BaseStatusEffect.WasQuickHacked"));
    ArrayPush(ignoredDurationStats, TweakDBInterface.GetStatusEffectRecord(t"BaseStatusEffect.QuickHackUploaded"));
    actionRecord.CompletionEffects(durationMods);
    i = 0;
    limit = ArraySize(durationMods);
    while i < limit {
      if !InventoryDataManagerV2.ProcessQuickhackEffects(player, durationMods[i].StatusEffect(), effects) {
      } else {
        j = 0;
        limitJ = ArraySize(effects);
        while j < limitJ {
          ArrayPush(instance.AttackEffects, effects[j]);
          j += 1;
        };
      };
      i += 1;
    };
    if ArraySize(durationMods) > 0 {
      i = 0;
      limit = ArraySize(durationMods);
      while i < limit {
        effectToCast = durationMods[i].StatusEffect();
        if IsDefined(effectToCast) {
          if !ArrayContains(ignoredDurationStats, effectToCast) {
            lastMatchingEffect = effectToCast;
          };
        };
        i += 1;
      };
      effectToCast = lastMatchingEffect;
      duration = effectToCast.Duration();
      duration.StatModifiers(statModifiers);
      instance.Duration = RPGManager.CalculateStatModifiers(statModifiers, player.GetGame(), emptyObject, Cast<StatsObjectID>(dummyEntityID), Cast<StatsObjectID>(player.GetEntityID()));
    };
  }

  private final static func GetUploadTime(instance: wref<UIInventoryItemProgramData>, actionRecord: wref<ObjectAction_Record>, player: wref<PlayerPuppet>) -> Void {
    let baseActionRecord: wref<ObjectAction_Record>;
    let baseStatModifiers: array<wref<StatModifier_Record>>;
    let dummyEntityID: EntityID;
    let statModifiers: array<wref<StatModifier_Record>>;
    actionRecord.ActivationTime(statModifiers);
    baseActionRecord.ActivationTime(baseStatModifiers);
    statModifiers = UIInventoryItemProgramData.StatModifiersExcept(statModifiers, baseStatModifiers);
    instance.UploadTime = RPGManager.CalculateStatModifiers(statModifiers, player.GetGame(), player, Cast<StatsObjectID>(dummyEntityID), Cast<StatsObjectID>(player.GetEntityID()));
  }

  private final static func StatModifiersExcept(statModifiers: array<wref<StatModifier_Record>>, except: array<wref<StatModifier_Record>>) -> array<wref<StatModifier_Record>> {
    let result: array<wref<StatModifier_Record>>;
    let i: Int32 = 0;
    while i < ArraySize(statModifiers) {
      if !ArrayContains(except, statModifiers[i]) {
        ArrayPush(result, statModifiers[i]);
      };
      i += 1;
    };
    return result;
  }

  private final static func GetCooldown(instance: wref<UIInventoryItemProgramData>, actionRecord: wref<ObjectAction_Record>, player: wref<PlayerPuppet>) -> Void {
    let actionStartEffects: array<wref<ObjectActionEffect_Record>>;
    let baseStatModifiers: array<wref<StatModifier_Record>>;
    let dummyEntityID: EntityID;
    let i: Int32;
    let limit: Int32;
    let statModifiers: array<wref<StatModifier_Record>>;
    let baseCooldownRecord: wref<StatModifierGroup_Record> = TweakDBInterface.GetStatModifierGroupRecord(t"BaseStatusEffect.QuickHackCooldownDuration");
    baseCooldownRecord.StatModifiers(baseStatModifiers);
    actionRecord.StartEffects(actionStartEffects);
    i = 0;
    limit = ArraySize(actionStartEffects);
    while i < limit {
      if Equals(actionStartEffects[i].StatusEffect().StatusEffectType().Type(), gamedataStatusEffectType.PlayerCooldown) {
        ArrayClear(statModifiers);
        actionStartEffects[i].StatusEffect().Duration().StatModifiers(statModifiers);
        statModifiers = UIInventoryItemProgramData.StatModifiersExcept(statModifiers, baseStatModifiers);
        instance.Cooldown = RPGManager.CalculateStatModifiers(statModifiers, player.GetGame(), player, Cast<StatsObjectID>(dummyEntityID), Cast<StatsObjectID>(player.GetEntityID()));
      };
      if instance.Cooldown != 0.00 {
        break;
      };
      i += 1;
    };
  }

  public final static func Make(itemRecord: wref<Item_Record>, player: wref<PlayerPuppet>) -> ref<UIInventoryItemProgramData> {
    let instance: ref<UIInventoryItemProgramData> = new UIInventoryItemProgramData();
    let actionRecord: wref<ObjectAction_Record> = itemRecord.GetObjectActionsItem(0);
    let shouldHideCooldown: Bool = TweakDBInterface.GetBool(actionRecord.GetID() + t".hideCooldownUI", false);
    let shouldHideDuration: Bool = TweakDBInterface.GetBool(actionRecord.GetID() + t".hideDurationUI", false);
    instance.BaseCost = BaseScriptableAction.GetBaseCostStatic(player, actionRecord);
    instance.MemoryCost = instance.BaseCost;
    if !shouldHideDuration {
      UIInventoryItemProgramData.GetDurationAndAttackEffects(instance, actionRecord, player);
    };
    UIInventoryItemProgramData.GetUploadTime(instance, actionRecord, player);
    if !shouldHideCooldown {
      UIInventoryItemProgramData.GetCooldown(instance, actionRecord, player);
    };
    return instance;
  }
}

public class UIInventoryItemComparisonManager extends IScriptable {

  public let ComparedStats: array<ref<UIInventoryItemStatComparison>>;

  public final static func Make(localItem: wref<UIInventoryItem>, comparisionItem: wref<UIInventoryItem>) -> ref<UIInventoryItemComparisonManager> {
    let comparableStat: wref<UIInventoryItemStat>;
    let comparedStat: ref<UIInventoryItemStatComparison>;
    let localStat: wref<UIInventoryItemStat>;
    let instance: ref<UIInventoryItemComparisonManager> = new UIInventoryItemComparisonManager();
    let localStatsManager: wref<UIInventoryItemStatsManager> = localItem.GetStatsManager();
    let i: Int32 = 0;
    let limit: Int32 = localStatsManager.Size();
    while i < limit {
      localStat = localStatsManager.Get(i);
      comparableStat = comparisionItem.GetStatsManager().GetByType(localStat.Type);
      comparedStat = new UIInventoryItemStatComparison();
      comparedStat.Type = localStat.Type;
      if IsDefined(comparableStat) {
        comparedStat.Value = localStat.Value - comparableStat.Value;
      } else {
        comparedStat.Value = localStat.Value;
      };
      ArrayPush(instance.ComparedStats, comparedStat);
      i += 1;
    };
    return instance;
  }

  public final func GetByType(type: gamedataStatType) -> wref<UIInventoryItemStatComparison> {
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(this.ComparedStats);
    while i < limit {
      if Equals(this.ComparedStats[i].Type, type) {
        return this.ComparedStats[i];
      };
      i += 1;
    };
    return null;
  }
}

public class UIInventoryItem extends IScriptable {

  public let ID: ItemID;

  public let Hash: Uint64;

  public let DEBUG_iconErrorInfo: ref<DEBUG_IconErrorInfo>;

  private let m_manager: wref<UIInventoryItemsManager>;

  private let m_owner: wref<GameObject>;

  private let m_itemData: wref<gameItemData>;

  private let m_itemRecord: wref<Item_Record>;

  private let m_realItemRecord: wref<Item_Record>;

  private let m_itemTweakID: TweakDBID;

  private let m_realItemTweakID: TweakDBID;

  private let m_data: ref<UIInventoryItemInternalData>;

  private let m_weaponData: ref<UIInventoryWeaponInternalData>;

  private let m_programData: ref<UIInventoryItemProgramData>;

  private let m_grenadeData: ref<UIInventoryItemGrenadeData>;

  private let m_parentItem: wref<gameItemData>;

  private let m_slotID: TweakDBID;

  private let m_fetchedFlags: Int32;

  private let m_isQuantityDirty: Bool;

  private let m_craftingResult: wref<CraftingResult_Record>;

  private let TEMP_isEquippedPrefetched: Bool;

  private let TEMP_isEquipped: Bool;

  public final static func Make(owner: wref<GameObject>, itemData: ref<gameItemData>, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItem> {
    let itemTweakID: TweakDBID = ItemID.GetTDBID(itemData.GetID());
    return UIInventoryItem.Make(owner, itemData, itemTweakID, TweakDBInterface.GetItemRecord(itemTweakID), manager);
  }

  public final static func Make(owner: wref<GameObject>, itemData: ref<gameItemData>, itemTweakID: TweakDBID, itemRecord: wref<Item_Record>, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItem> {
    let instance: ref<UIInventoryItem> = new UIInventoryItem();
    instance.ID = itemData.GetID();
    instance.Hash = ItemID.GetCombinedHash(instance.ID);
    instance.m_owner = owner;
    instance.m_itemData = itemData;
    instance.m_manager = manager;
    instance.m_realItemRecord = itemRecord;
    instance.m_realItemTweakID = itemTweakID;
    instance.m_craftingResult = TweakDBInterface.GetItemRecipeRecord(instance.m_realItemTweakID).CraftingResult();
    if IsDefined(instance.m_craftingResult) {
      instance.m_itemRecord = instance.m_craftingResult.Item();
      instance.m_itemTweakID = instance.m_itemRecord.GetID();
    } else {
      instance.m_itemRecord = instance.m_realItemRecord;
      instance.m_itemTweakID = instance.m_realItemTweakID;
    };
    instance.m_data = new UIInventoryItemInternalData();
    instance.FetchImmediate();
    return instance;
  }

  public final static func FromMinimalItemTooltipData(owner: wref<GameObject>, data: ref<MinimalItemTooltipData>, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItem> {
    let iconPathName: CName;
    let weaponType: WeaponType;
    let instance: ref<UIInventoryItem> = new UIInventoryItem();
    instance.ID = data.itemID;
    instance.Hash = ItemID.GetCombinedHash(instance.ID);
    instance.m_owner = owner;
    instance.m_itemData = data.itemData;
    instance.m_manager = manager;
    instance.m_realItemRecord = TweakDBInterface.GetItemRecord(instance.m_itemTweakID);
    instance.m_realItemTweakID = ItemID.GetTDBID(instance.ID);
    instance.m_craftingResult = TweakDBInterface.GetItemRecipeRecord(instance.m_realItemTweakID).CraftingResult();
    if IsDefined(instance.m_craftingResult) {
      instance.m_itemRecord = instance.m_craftingResult.Item();
      instance.m_itemTweakID = instance.m_itemRecord.GetID();
    } else {
      instance.m_itemRecord = instance.m_realItemRecord;
      instance.m_itemTweakID = instance.m_realItemTweakID;
    };
    instance.m_data = new UIInventoryItemInternalData();
    instance.m_data.Name = data.itemName;
    instance.m_data.Quality = data.quality;
    instance.m_data.ItemType = data.itemType;
    instance.m_data.Quantity = data.quantity;
    instance.m_data.Description = data.description;
    instance.m_data.IconPath = data.iconPath;
    if !IsStringValid(instance.m_data.IconPath) {
      iconPathName = IconsNameResolver.GetIconsNameResolver().TranslateItemToIconName(instance.m_itemTweakID, data.useMaleIcon);
      instance.m_data.IconPath = "UIIcon." + NameToString(iconPathName);
    };
    instance.m_data.IsQuestItem = instance.m_itemData.HasTag(n"Quest") || instance.m_itemData.HasTag(n"UnequipBlocked");
    instance.m_data.IsIconicItem = RPGManager.IsItemIconic(instance.m_itemData);
    instance.m_data.EquipmentArea = data.equipmentArea;
    instance.m_data.FilterCategory = ItemCategoryFliter.GetItemCategoryType(instance.m_itemData);
    instance.m_fetchedFlags |= 1919;
    weaponType = UIInventoryItemsManager.GetItemTypeWeapon(instance.m_data.ItemType);
    if NotEquals(weaponType, WeaponType.Invalid) {
      instance.FetchWeaponImmediate(weaponType);
    };
    if UIInventoryItemsManager.IsItemTypeCloting(instance.m_data.ItemType) {
      instance.m_data.UIItemCategory = UIItemCategory.Clothing;
    };
    instance.m_data.ItemTypeOrder = ItemCompareBuilder.GetItemTypeOrder(instance.m_itemData, instance.m_data.EquipmentArea, instance.m_data.ItemType);
    instance.m_data.Weight = instance.m_itemData.GetStatValueByType(gamedataStatType.Weight);
    instance.m_data.StatsManager = UIInventoryItemStatsManager.FromMinimalItemTooltipData(data, manager);
    instance.TEMP_isEquipped = data.isEquipped;
    instance.TEMP_isEquippedPrefetched = true;
    return instance;
  }

  public final static func FromInventoryItemData(owner: wref<GameObject>, itemData: InventoryItemData, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItem> {
    let weaponType: WeaponType;
    let instance: ref<UIInventoryItem> = new UIInventoryItem();
    instance.ID = InventoryItemData.GetID(itemData);
    instance.Hash = ItemID.GetCombinedHash(instance.ID);
    instance.m_owner = owner;
    instance.m_itemData = InventoryItemData.GetGameItemData(itemData);
    instance.m_manager = manager;
    instance.m_itemTweakID = ItemID.GetTDBID(instance.ID);
    instance.m_itemRecord = TweakDBInterface.GetItemRecord(instance.m_itemTweakID);
    instance.m_data = new UIInventoryItemInternalData();
    instance.m_data.Name = InventoryItemData.GetName(itemData);
    instance.m_data.Quality = UIItemsHelper.QualityNameToEnum(InventoryItemData.GetQuality(itemData));
    instance.m_data.ItemType = InventoryItemData.GetItemType(itemData);
    instance.m_data.Quantity = InventoryItemData.GetQuantity(itemData);
    instance.m_data.Description = InventoryItemData.GetDescription(itemData);
    instance.m_data.IconPath = InventoryItemData.GetIconPath(itemData);
    instance.m_data.IsQuestItem = instance.m_itemData.HasTag(n"Quest") || instance.m_itemData.HasTag(n"UnequipBlocked");
    instance.m_data.IsIconicItem = RPGManager.IsItemIconic(instance.m_itemData);
    instance.m_data.EquipmentArea = InventoryItemData.GetEquipmentArea(itemData);
    instance.m_data.FilterCategory = ItemCategoryFliter.GetItemCategoryType(instance.m_itemData);
    instance.m_fetchedFlags |= 1023;
    weaponType = UIInventoryItemsManager.GetItemTypeWeapon(instance.m_data.ItemType);
    if NotEquals(weaponType, WeaponType.Invalid) {
      instance.FetchWeaponImmediate(weaponType);
    };
    if UIInventoryItemsManager.IsItemTypeCloting(instance.m_data.ItemType) {
      instance.m_data.UIItemCategory = UIItemCategory.Clothing;
    };
    instance.m_data.ItemTypeOrder = ItemCompareBuilder.GetItemTypeOrder(instance.m_itemData, instance.m_data.EquipmentArea, instance.m_data.ItemType);
    instance.m_data.Weight = instance.m_itemData.GetStatValueByType(gamedataStatType.Weight);
    return instance;
  }

  private final func FetchImmediate() -> Void {
    this.m_data.Name = UIItemsHelper.GetItemName(this.m_itemRecord, this.m_itemData);
    this.m_data.Quantity = this.m_itemData.GetQuantity();
    this.m_data.ItemType = this.m_itemRecord.ItemType().Type();
    this.m_data.Quality = RPGManager.GetItemDataQuality(this.m_itemData);
    let weaponType: WeaponType = UIInventoryItemsManager.GetItemTypeWeapon(this.m_data.ItemType);
    if NotEquals(weaponType, WeaponType.Invalid) {
      this.FetchWeaponImmediate(weaponType);
    };
    if UIInventoryItemsManager.IsItemTypeCloting(this.m_data.ItemType) {
      this.m_data.UIItemCategory = UIItemCategory.Clothing;
    };
    this.GetName();
  }

  private final func FetchWeaponImmediate(weaponType: WeaponType) -> Void {
    this.m_data.UIItemCategory = UIItemCategory.Weapon;
    this.m_weaponData = new UIInventoryWeaponInternalData();
    this.m_weaponData.WeaponType = weaponType;
    let modsManager: wref<UIInventoryItemModsManager> = this.GetModsManager();
    if Equals(weaponType, WeaponType.Ranged) {
      this.m_weaponData.HasSilencerInstalled = modsManager.UsedSlotsContains(t"AttachmentSlots.PowerModule");
      this.m_weaponData.HasScopeInstalled = modsManager.UsedSlotsContains(t"AttachmentSlots.Scope");
      if this.m_weaponData.HasSilencerInstalled {
        this.m_weaponData.HasSilencerSlot = true;
      } else {
        this.m_weaponData.HasSilencerSlot = modsManager.EmptySlotsContains(t"AttachmentSlots.PowerModule");
      };
      if this.m_weaponData.HasScopeInstalled {
        this.m_weaponData.HasScopeSlot = true;
      } else {
        this.m_weaponData.HasScopeSlot = modsManager.EmptySlotsContains(t"AttachmentSlots.Scope");
      };
    };
  }

  public final func GetOwner() -> wref<GameObject> {
    return this.m_owner;
  }

  public final func GetID() -> ItemID {
    return this.ID;
  }

  public final func GetTweakDBID() -> TweakDBID {
    return this.m_itemTweakID;
  }

  public final func GetItemData() -> wref<gameItemData> {
    return this.m_itemData;
  }

  public final func GetItemRecord() -> wref<Item_Record> {
    return this.m_itemRecord;
  }

  public final func Internal_GetParentItem() -> wref<gameItemData> {
    return this.m_parentItem;
  }

  public final func Internal_GetSlotID() -> TweakDBID {
    return this.m_slotID;
  }

  public final func Internal_MarkStatsDirty() -> Void {
    this.m_data.StatsManager = null;
    this.m_fetchedFlags &= ~1024;
  }

  public final func Internal_MarkModsDirty() -> Void {
    this.m_data.ModsManager = null;
    this.m_fetchedFlags &= ~2048;
  }

  public final func Internal_FlushRequirements() -> Void {
    this.m_data.RequirementsManager = null;
    this.m_fetchedFlags &= ~4096;
  }

  public final func SetQuantity(quantity: Int32) -> Void {
    this.m_data.Quantity = quantity;
    this.m_isQuantityDirty = false;
  }

  public final func MarkQuantityDirty() -> Void {
    this.m_isQuantityDirty = true;
  }

  public final func GetQuantity(opt update: Bool) -> Int32 {
    if update || this.m_isQuantityDirty {
      this.SetQuantity(this.m_itemData.GetQuantity());
    };
    return this.m_data.Quantity;
  }

  public final func GetQuality() -> gamedataQuality {
    return this.m_data.Quality;
  }

  public final func GetQualityInt() -> Int32 {
    return UIInventoryItemsManager.QualityToInt(this.GetQuality());
  }

  public final func GetQualityName() -> CName {
    return UIInventoryItemsManager.QualityToName(this.GetQuality());
  }

  public final func GetItemType() -> gamedataItemType {
    return this.m_data.ItemType;
  }

  public final func GetUIItemCategory() -> UIItemCategory {
    return this.m_data.UIItemCategory;
  }

  public final func IsRecipe() -> Bool {
    if Cast<Bool>(this.m_fetchedFlags & 8192) {
      return this.m_data.IsRecipeItem;
    };
    this.m_data.IsRecipeItem = this.m_itemData.HasTag(n"Recipe");
    this.m_fetchedFlags |= 8192;
    return this.m_data.IsRecipeItem;
  }

  public final func IsWeapon() -> Bool {
    return Equals(this.m_data.UIItemCategory, UIItemCategory.Weapon);
  }

  public final func IsClothing() -> Bool {
    return Equals(this.m_data.UIItemCategory, UIItemCategory.Clothing);
  }

  public final func IsCyberware() -> Bool {
    return Equals(this.m_data.UIItemCategory, UIItemCategory.Cyberware);
  }

  public final func IsPart() -> Bool {
    return this.m_itemRecord.IsPart();
  }

  public final func GetName() -> String {
    if Cast<Bool>(this.m_fetchedFlags & 1) {
      return this.m_data.Name;
    };
    if IsDefined(this.m_craftingResult) {
      this.m_data.Name = GetLocalizedItemNameByCName(this.m_craftingResult.Item().DisplayName());
      this.m_data.Name = GetLocalizedText("Gameplay-Crafting-GenericRecipe") + " " + this.m_data.Name;
    } else {
      this.m_data.Name = GetLocalizedItemNameByCName(this.m_itemRecord.DisplayName());
    };
    this.m_fetchedFlags |= 1;
    return this.m_data.Name;
  }

  public final func GetIconPath() -> String {
    if Cast<Bool>(this.m_fetchedFlags & 4) {
      return this.m_data.IconPath;
    };
    this.m_data.IconPath = UIInventoryItemsManager.ResolveItemIconName(this.m_itemTweakID, this.m_itemRecord, this.m_manager);
    this.m_fetchedFlags |= 4;
    return this.m_data.IconPath;
  }

  public final func IsQuestItem() -> Bool {
    if Cast<Bool>(this.m_fetchedFlags & 8) {
      return this.m_data.IsQuestItem;
    };
    this.m_data.IsQuestItem = this.m_itemData.HasTag(n"Quest") || this.m_itemData.HasTag(n"UnequipBlocked");
    this.m_fetchedFlags |= 8;
    return this.m_data.IsQuestItem;
  }

  public final func IsIconic() -> Bool {
    if Cast<Bool>(this.m_fetchedFlags & 32) {
      return this.m_data.IsIconicItem;
    };
    this.m_data.IsIconicItem = RPGManager.IsItemIconic(this.m_itemData);
    this.m_fetchedFlags |= 32;
    return this.m_data.IsIconicItem;
  }

  public final func IsEquippable() -> Bool {
    return this.IsWeapon() || this.IsClothing();
  }

  public final func GetEquipmentArea() -> gamedataEquipmentArea {
    if Cast<Bool>(this.m_fetchedFlags & 16) {
      return this.m_data.EquipmentArea;
    };
    if this.IsEquippable() {
      this.m_data.EquipmentArea = this.m_itemRecord.EquipArea().Type();
    } else {
      this.m_data.EquipmentArea = gamedataEquipmentArea.Invalid;
    };
    this.m_fetchedFlags |= 16;
    return this.m_data.EquipmentArea;
  }

  public final func GetFilterCategory() -> ItemFilterCategory {
    if Cast<Bool>(this.m_fetchedFlags & 64) {
      return this.m_data.FilterCategory;
    };
    this.m_data.FilterCategory = ItemCategoryFliter.GetItemCategoryType(this.m_itemData);
    this.m_fetchedFlags |= 64;
    return this.m_data.FilterCategory;
  }

  public final func GetPrimaryStat() -> wref<UIInventoryItemStat> {
    if Cast<Bool>(this.m_fetchedFlags & 128) {
      return this.m_data.PrimaryStat;
    };
    if this.IsWeapon() {
      this.m_data.PrimaryStat = new UIInventoryItemStat();
      this.m_data.PrimaryStat.Type = gamedataStatType.EffectiveDPS;
      this.m_data.PrimaryStat.Value = this.m_itemData.GetStatValueByType(gamedataStatType.EffectiveDPS);
    } else {
      if this.IsClothing() {
        this.m_data.PrimaryStat = new UIInventoryItemStat();
        this.m_data.PrimaryStat.Type = gamedataStatType.Armor;
        this.m_data.PrimaryStat.Value = this.m_itemData.GetStatValueByType(gamedataStatType.Armor);
      };
    };
    this.m_fetchedFlags |= 128;
    return this.m_data.PrimaryStat;
  }

  public final func GetWeight() -> Float {
    if Cast<Bool>(this.m_fetchedFlags & 512) {
      return this.m_data.Weight;
    };
    this.m_data.Weight = this.m_itemData.GetStatValueByType(gamedataStatType.Weight);
    this.m_fetchedFlags |= 512;
    return this.m_data.Weight;
  }

  public final func GetItemTypeOrder() -> Int32 {
    if Cast<Bool>(this.m_fetchedFlags & 256) {
      return this.m_data.ItemTypeOrder;
    };
    this.m_data.ItemTypeOrder = ItemCompareBuilder.GetItemTypeOrder(this.m_itemData, this.GetEquipmentArea(), this.GetItemType());
    this.m_fetchedFlags |= 256;
    return this.m_data.ItemTypeOrder;
  }

  public final func GetStatsManager() -> wref<UIInventoryItemStatsManager> {
    let record: ref<UIStatsMap_Record>;
    if Cast<Bool>(this.m_fetchedFlags & 1024) {
      return this.m_data.StatsManager;
    };
    record = UIInventoryItemsManager.GetUIStatsMap(this.m_data.ItemType, this.m_manager);
    this.m_data.StatsManager = UIInventoryItemStatsManager.Make(this.m_itemData, record, this.m_manager);
    this.m_fetchedFlags |= 1024;
    return this.m_data.StatsManager;
  }

  public final func GetModsManager() -> wref<UIInventoryItemModsManager> {
    let transactionSystem: ref<TransactionSystem>;
    if Cast<Bool>(this.m_fetchedFlags & 2048) {
      return this.m_data.ModsManager;
    };
    transactionSystem = IsDefined(this.m_manager) ? this.m_manager.GetTransactionSystem() : GameInstance.GetTransactionSystem(this.m_owner.GetGame());
    this.m_data.ModsManager = UIInventoryItemModsManager.Make(this, transactionSystem);
    this.m_fetchedFlags |= 2048;
    return this.m_data.ModsManager;
  }

  public final func GetRequirementsManager(player: wref<GameObject>) -> wref<UIInventoryItemRequirementsManager> {
    if Cast<Bool>(this.m_fetchedFlags & 4096) {
      return this.m_data.RequirementsManager;
    };
    this.m_data.RequirementsManager = UIInventoryItemRequirementsManager.Make(this, player);
    this.m_fetchedFlags |= 4096;
    return this.m_data.RequirementsManager;
  }

  public final func GetDescription() -> String {
    if Cast<Bool>(this.m_fetchedFlags & 2) {
      return this.m_data.Description;
    };
    this.m_data.Description = LocKeyToString(this.m_itemRecord.LocalizedDescription());
    this.m_fetchedFlags |= 2;
    return this.m_data.Description;
  }

  public final func GetSellPrice() -> Float {
    return Cast<Float>(RPGManager.CalculateSellPrice(this.m_owner.GetGame(), this.m_owner, this.ID));
  }

  public final func GetBuyPrice() -> Float {
    return Cast<Float>(MarketSystem.GetBuyPrice(this.m_owner, this.ID));
  }

  public final func IsCrafted() -> Bool {
    return RPGManager.IsItemCrafted(this.m_itemData);
  }

  public final func IsEquipped() -> Bool {
    if this.TEMP_isEquippedPrefetched {
      return this.TEMP_isEquipped;
    };
    if IsDefined(this.m_manager) {
      return this.m_manager.IsItemEquipped(this.ID);
    };
    return false;
  }

  public final func IsTransmogItem() -> Bool {
    if IsDefined(this.m_manager) {
      return this.m_manager.IsItemTransmog(this.ID);
    };
    return false;
  }

  public final func GetRequiredLevel() -> Int32 {
    return 0;
  }

  public final func IsNew() -> Bool {
    if IsDefined(this.m_manager) {
      return this.m_manager.IsItemNew(this.ID);
    };
    return false;
  }

  public final func HasAnyTag(tagsToCheck: array<CName>) -> Bool {
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(tagsToCheck);
    while i < limit {
      if this.m_itemData.HasTag(tagsToCheck[i]) {
        return true;
      };
      i += 1;
    };
    return false;
  }

  public final func GetWeaponType() -> WeaponType {
    if this.m_weaponData == null {
      return WeaponType.Invalid;
    };
    return this.m_weaponData.WeaponType;
  }

  public final func HasSilencerSlot() -> Bool {
    if this.m_weaponData == null {
      return false;
    };
    return this.m_weaponData.HasSilencerSlot;
  }

  public final func HasScopeSlot() -> Bool {
    if this.m_weaponData == null {
      return false;
    };
    return this.m_weaponData.HasScopeSlot;
  }

  public final func HasSilencerInstalled() -> Bool {
    if this.m_weaponData == null {
      return false;
    };
    return this.m_weaponData.HasSilencerInstalled;
  }

  public final func HasScopeInstalled() -> Bool {
    if this.m_weaponData == null {
      return false;
    };
    return this.m_weaponData.HasScopeInstalled;
  }

  private final func FetchDamageData() -> Void {
    let divideByPellets: Bool;
    if Equals(this.GetWeaponType(), WeaponType.Ranged) {
      this.m_weaponData.DamageMin = this.m_itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHitMin);
      this.m_weaponData.DamageMax = this.m_itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHitMax);
      this.m_weaponData.NumberOfPellets = RoundF(this.m_itemData.GetStatValueByType(gamedataStatType.ProjectilesPerShot));
    } else {
      this.m_weaponData.DamageMin = this.m_itemData.GetStatValueByType(gamedataStatType.EffectiveDamagePerHit);
      this.m_weaponData.DamageMax = this.m_weaponData.DamageMin;
    };
    divideByPellets = TweakDBInterface.GetBool(this.m_itemTweakID + t".divideAttacksByPelletsOnUI", false);
    if divideByPellets && this.m_weaponData.NumberOfPellets > 0 {
      this.m_weaponData.AttackSpeed = this.m_itemData.GetStatValueByType(gamedataStatType.AttacksPerSecond) / Cast<Float>(this.m_weaponData.NumberOfPellets);
    } else {
      this.m_weaponData.AttackSpeed = this.m_itemData.GetStatValueByType(gamedataStatType.AttacksPerSecond);
    };
  }

  public final func GetDamageMin() -> Float {
    if this.m_weaponData == null {
      return 0.00;
    };
    if !this.m_weaponData.m_damageDataFetched {
      this.FetchDamageData();
    };
    return this.m_weaponData.DamageMin;
  }

  public final func GetDamageMax() -> Float {
    if this.m_weaponData == null {
      return 0.00;
    };
    if !this.m_weaponData.m_damageDataFetched {
      this.FetchDamageData();
    };
    return this.m_weaponData.DamageMax;
  }

  public final func GetAttackSpeed() -> Float {
    if this.m_weaponData == null {
      return 0.00;
    };
    if !this.m_weaponData.m_damageDataFetched {
      this.FetchDamageData();
    };
    return this.m_weaponData.AttackSpeed;
  }

  public final func GetNumberOfPellets() -> Int32 {
    if this.m_weaponData == null {
      return 0;
    };
    if !this.m_weaponData.m_damageDataFetched {
      this.FetchDamageData();
    };
    return this.m_weaponData.NumberOfPellets;
  }

  public final func GetWeaponEvolution() -> gamedataWeaponEvolution {
    let weaponRecord: wref<WeaponItem_Record>;
    if this.m_weaponData == null {
      return gamedataWeaponEvolution.Invalid;
    };
    if this.m_weaponData.m_weaponEvolutionFetched {
      return this.m_weaponData.Evolution;
    };
    weaponRecord = this.m_itemRecord as WeaponItem_Record;
    if IsDefined(weaponRecord) {
      this.m_weaponData.Evolution = weaponRecord.Evolution().Type();
    };
    this.m_weaponData.m_weaponEvolutionFetched = true;
    return this.m_weaponData.Evolution;
  }

  public final func GetAmmo(opt update: Bool) -> Int32 {
    if this.m_weaponData == null {
      return -1;
    };
    if this.m_weaponData.m_ammoFetched && !update {
      return this.m_weaponData.Ammo;
    };
    this.m_weaponData.Ammo = UIInventoryItemsManager.GetAmmo(this.m_itemRecord, update, this.m_manager);
    this.m_weaponData.m_ammoFetched = true;
    return this.m_weaponData.Ammo;
  }

  public final func GetProgramData(player: wref<PlayerPuppet>, force: Bool) -> wref<UIInventoryItemProgramData> {
    if IsDefined(this.m_programData) && !force {
      return this.m_programData;
    };
    if Equals(this.GetItemType(), gamedataItemType.Prt_Program) {
      this.m_programData = UIInventoryItemProgramData.Make(this.m_itemRecord, player);
    };
    return this.m_programData;
  }

  public final func GetGrenadeData(player: wref<PlayerPuppet>, force: Bool) -> wref<UIInventoryItemGrenadeData> {
    if IsDefined(this.m_grenadeData) && !force {
      return this.m_grenadeData;
    };
    if Equals(this.GetItemType(), gamedataItemType.Gad_Grenade) {
      this.m_grenadeData = UIInventoryItemGrenadeData.Make(this, player);
    };
    return this.m_grenadeData;
  }
}
