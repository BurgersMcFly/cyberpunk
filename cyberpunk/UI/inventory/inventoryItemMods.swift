
public class UIInventoryItemModsManager extends IScriptable {

  private let m_emptySlots: array<TweakDBID>;

  private let m_usedSlots: array<TweakDBID>;

  private let m_mods: array<ref<UIInventoryItemMod>>;

  private let m_dedicatedMods: array<ref<UIInventoryItemModAttachment>>;

  private let m_transactionSystem: ref<TransactionSystem>;

  public final static func Make(inventoryItem: wref<UIInventoryItem>, transactionSystem: ref<TransactionSystem>) -> ref<UIInventoryItemModsManager> {
    let instance: ref<UIInventoryItemModsManager> = new UIInventoryItemModsManager();
    let owner: wref<GameObject> = inventoryItem.GetOwner();
    let id: ItemID = inventoryItem.GetID();
    transactionSystem.GetEmptySlotsOnItem(owner, id, instance.m_emptySlots);
    transactionSystem.GetUsedSlotsOnItem(owner, id, instance.m_usedSlots);
    instance.FetchModsDataPackages(inventoryItem);
    if NotEquals(inventoryItem.GetItemType(), gamedataItemType.Prt_Program) {
      instance.GetAttachements(inventoryItem.GetOwner(), inventoryItem);
    };
    return instance;
  }

  public final func GetEmptySlotsSize() -> Int32 {
    return ArraySize(this.m_emptySlots);
  }

  public final func GetUsedSlotsSize() -> Int32 {
    return ArraySize(this.m_usedSlots);
  }

  public final func GetModsSize() -> Int32 {
    return ArraySize(this.m_mods);
  }

  public final func GetDedicatedModsSize() -> Int32 {
    return ArraySize(this.m_dedicatedMods);
  }

  public final func GetEmptySlot(index: Int32) -> TweakDBID {
    return this.m_emptySlots[index];
  }

  public final func GetUsedSlot(index: Int32) -> TweakDBID {
    return this.m_usedSlots[index];
  }

  public final func GetMod(index: Int32) -> ref<UIInventoryItemMod> {
    return this.m_mods[index];
  }

  public final func GetDedicatedMod(index: Int32) -> ref<UIInventoryItemModAttachment> {
    return this.m_dedicatedMods[index];
  }

  public final func EmptySlotsContains(slotName: TweakDBID) -> Bool {
    return ArrayContains(this.m_emptySlots, slotName);
  }

  public final func UsedSlotsContains(slotName: TweakDBID) -> Bool {
    return ArrayContains(this.m_usedSlots, slotName);
  }

  private final func FetchModsDataPackages(inventoryItem: wref<UIInventoryItem>) -> Void {
    let dataPackages: array<wref<GameplayLogicPackage_Record>>;
    let dataPackagesToDisplay: array<wref<GameplayLogicPackageUIData_Record>>;
    let i: Int32;
    let innerItemData: InnerItemData;
    let limit: Int32;
    let recordData: ref<UIInventoryItemModDataPackage>;
    let uiDataPackage: wref<GameplayLogicPackageUIData_Record>;
    let itemRecord: wref<Item_Record> = inventoryItem.GetItemRecord();
    itemRecord.OnEquip(dataPackages);
    i = 0;
    limit = ArraySize(dataPackages);
    while i < limit {
      uiDataPackage = dataPackages[i].UIData();
      if IsStringValid(uiDataPackage.LocalizedDescription()) {
        ArrayPush(dataPackagesToDisplay, uiDataPackage);
      };
      i += 1;
    };
    ArrayClear(dataPackages);
    itemRecord.OnAttach(dataPackages);
    i = 0;
    limit = ArraySize(dataPackages);
    while i < limit {
      uiDataPackage = dataPackages[i].UIData();
      if IsStringValid(uiDataPackage.LocalizedDescription()) {
        ArrayPush(dataPackagesToDisplay, uiDataPackage);
      };
      i += 1;
    };
    i = 0;
    limit = ArraySize(dataPackagesToDisplay);
    while i < limit {
      recordData = new UIInventoryItemModDataPackage();
      recordData.Description = dataPackagesToDisplay[i].LocalizedDescription();
      if IsDefined(inventoryItem.Internal_GetParentItem()) {
        innerItemData = new InnerItemData();
        inventoryItem.Internal_GetParentItem().GetItemPart(innerItemData, inventoryItem.Internal_GetSlotID());
        recordData.DataPackage = UILocalizationDataPackage.FromLogicUIDataPackage(dataPackagesToDisplay[i], innerItemData);
      } else {
        recordData.DataPackage = UILocalizationDataPackage.FromLogicUIDataPackage(dataPackagesToDisplay[i], inventoryItem.GetItemData());
      };
      ArrayPush(this.m_mods, recordData);
      i += 1;
    };
  }

  private final func GetAttachements(owner: wref<GameObject>, inventoryItem: wref<UIInventoryItem>) -> Void {
    let attachmentData: ref<UIInventoryItemModAttachment>;
    let attachmentSlotRecord: wref<AttachmentSlot_Record>;
    let attachmentType: InventoryItemAttachmentType;
    let i: Int32;
    let inventorySlots: array<TweakDBID>;
    let itemId: ItemID;
    let limit: Int32;
    let partData: InnerItemData;
    let slotsData: array<AttachmentSlotCacheData>;
    let staticData: wref<Item_Record>;
    let itemData: wref<gameItemData> = inventoryItem.GetItemData();
    let usedSlots: array<TweakDBID> = this.m_usedSlots;
    let emptySlots: array<TweakDBID> = this.m_emptySlots;
    let emptySlotsSize: Int32 = ArraySize(emptySlots);
    let usedSlotsSize: Int32 = ArraySize(usedSlots);
    if emptySlotsSize < 1 && usedSlotsSize < 1 {
      return;
    };
    itemId = inventoryItem.GetID();
    inventorySlots = UIInventoryItemModsStaticData.GetAttachmentSlots(inventoryItem.GetItemType());
    i = 0;
    limit = ArraySize(inventorySlots);
    while i < limit {
      if emptySlotsSize > 0 && ArrayContains(emptySlots, inventorySlots[i]) {
        attachmentSlotRecord = TweakDBInterface.GetAttachmentSlotRecord(inventorySlots[i]);
        ArrayPush(slotsData, new AttachmentSlotCacheData(true, attachmentSlotRecord, RPGManager.ShouldSlotBeAvailable(owner, itemId, attachmentSlotRecord), inventorySlots[i]));
        emptySlotsSize -= 1;
        ArrayRemove(emptySlots, inventorySlots[i]);
      };
      if usedSlotsSize > 0 && ArrayContains(usedSlots, inventorySlots[i]) {
        attachmentSlotRecord = TweakDBInterface.GetAttachmentSlotRecord(inventorySlots[i]);
        ArrayPush(slotsData, new AttachmentSlotCacheData(false, attachmentSlotRecord, RPGManager.ShouldSlotBeAvailable(owner, itemId, attachmentSlotRecord), inventorySlots[i]));
        usedSlotsSize -= 1;
        ArrayRemove(usedSlots, inventorySlots[i]);
      };
      i += 1;
    };
    i = 0;
    limit = ArraySize(slotsData);
    while i < limit {
      staticData = null;
      if IsDefined(slotsData[i].attachmentSlotRecord) && slotsData[i].shouldBeAvailable {
        if !slotsData[i].empty {
          itemData.GetItemPart(partData, slotsData[i].slotId);
          staticData = InnerItemData.GetStaticData(partData);
          if staticData.TagsContains(n"DummyPart") {
          } else {
            attachmentType = UIInventoryItemModsStaticData.IsAttachmentDedicated(slotsData[i].slotId) ? InventoryItemAttachmentType.Dedicated : InventoryItemAttachmentType.Generic;
            if Equals(attachmentType, InventoryItemAttachmentType.Dedicated) && staticData == null {
            } else {
              attachmentData = new UIInventoryItemModAttachment();
              attachmentData.IsEmpty = slotsData[i].empty;
              if staticData != null {
                if InnerItemData.HasStatData(partData, gamedataStatType.Quality) {
                  attachmentData.Quality = RPGManager.GetInnerItemDataQuality(partData);
                };
                this.FillSpecialAbilities(staticData, attachmentData.Abilities, itemData, partData);
                attachmentData.AbilitiesSize = ArraySize(attachmentData.Abilities);
                attachmentData.SlotName = LocKeyToString(staticData.DisplayName());
              } else {
                attachmentData.SlotName = GetLocalizedText(UIItemsHelper.GetEmptySlotName(slotsData[i].slotId));
              };
              if Equals(attachmentType, InventoryItemAttachmentType.Dedicated) {
                ArrayPush(this.m_dedicatedMods, attachmentData);
              } else {
                ArrayPush(this.m_mods, attachmentData);
              };
            };
          };
        };
        attachmentType = UIInventoryItemModsStaticData.IsAttachmentDedicated(slotsData[i].slotId) ? InventoryItemAttachmentType.Dedicated : InventoryItemAttachmentType.Generic;
        if Equals(attachmentType, InventoryItemAttachmentType.Dedicated) && staticData == null {
        } else {
          attachmentData = new UIInventoryItemModAttachment();
          attachmentData.IsEmpty = slotsData[i].empty;
          if staticData != null {
            if InnerItemData.HasStatData(partData, gamedataStatType.Quality) {
              attachmentData.Quality = RPGManager.GetInnerItemDataQuality(partData);
            };
            this.FillSpecialAbilities(staticData, attachmentData.Abilities, itemData, partData);
            attachmentData.AbilitiesSize = ArraySize(attachmentData.Abilities);
            attachmentData.SlotName = LocKeyToString(staticData.DisplayName());
          } else {
            attachmentData.SlotName = GetLocalizedText(UIItemsHelper.GetEmptySlotName(slotsData[i].slotId));
          };
          if Equals(attachmentType, InventoryItemAttachmentType.Dedicated) {
            ArrayPush(this.m_dedicatedMods, attachmentData);
          } else {
            ArrayPush(this.m_mods, attachmentData);
          };
        };
      };
      i += 1;
    };
  }

  private final const func FillSpecialAbilities(itemRecord: ref<Item_Record>, abilities: script_ref<array<InventoryItemAbility>>, opt itemData: wref<gameItemData>, opt partItemData: InnerItemData) -> Void {
    let GLPAbilities: array<wref<GameplayLogicPackage_Record>>;
    let ability: InventoryItemAbility;
    let i: Int32;
    let limit: Int32;
    let uiData: wref<GameplayLogicPackageUIData_Record>;
    itemRecord.OnAttach(GLPAbilities);
    i = 0;
    limit = ArraySize(GLPAbilities);
    while i < limit {
      if IsDefined(GLPAbilities[i]) {
        uiData = GLPAbilities[i].UIData();
        if IsDefined(uiData) {
          ability = new InventoryItemAbility(uiData.IconPath(), uiData.LocalizedName(), uiData.LocalizedDescription(), UILocalizationDataPackage.FromLogicUIDataPackage(uiData, partItemData));
          ArrayPush(Deref(abilities), ability);
        };
      };
      i += 1;
    };
    ArrayClear(GLPAbilities);
    itemRecord.OnEquip(GLPAbilities);
    i = 0;
    limit = ArraySize(GLPAbilities);
    while i < limit {
      if IsDefined(GLPAbilities[i]) {
        uiData = GLPAbilities[i].UIData();
        if IsDefined(uiData) {
          ability = new InventoryItemAbility(uiData.IconPath(), uiData.LocalizedName(), uiData.LocalizedDescription(), UILocalizationDataPackage.FromLogicUIDataPackage(uiData));
          ArrayPush(Deref(abilities), ability);
        };
      };
      i += 1;
    };
  }
}

public abstract final class UIInventoryItemModsStaticData extends IScriptable {

  public final static func GetAttachmentSlots(itemType: gamedataItemType) -> array<TweakDBID> {
    let slots: array<TweakDBID>;
    switch itemType {
      case gamedataItemType.Clo_Head:
        ArrayPush(slots, t"AttachmentSlots.HeadFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.HeadFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.HeadFabricEnhancer3");
        break;
      case gamedataItemType.Clo_Face:
        ArrayPush(slots, t"AttachmentSlots.FaceFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.FaceFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.FaceFabricEnhancer3");
        break;
      case gamedataItemType.Clo_OuterChest:
        ArrayPush(slots, t"AttachmentSlots.OuterChestFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.OuterChestFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.OuterChestFabricEnhancer3");
        ArrayPush(slots, t"AttachmentSlots.OuterChestFabricEnhancer4");
        break;
      case gamedataItemType.Clo_InnerChest:
        ArrayPush(slots, t"AttachmentSlots.InnerChestFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.InnerChestFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.InnerChestFabricEnhancer3");
        ArrayPush(slots, t"AttachmentSlots.InnerChestFabricEnhancer4");
        break;
      case gamedataItemType.Clo_Legs:
        ArrayPush(slots, t"AttachmentSlots.LegsFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.LegsFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.LegsFabricEnhancer3");
        break;
      case gamedataItemType.Clo_Feet:
        ArrayPush(slots, t"AttachmentSlots.FootFabricEnhancer1");
        ArrayPush(slots, t"AttachmentSlots.FootFabricEnhancer2");
        ArrayPush(slots, t"AttachmentSlots.FootFabricEnhancer3");
        break;
      case gamedataItemType.Wea_AssaultRifle:
      case gamedataItemType.Wea_Handgun:
      case gamedataItemType.Wea_SubmachineGun:
      case gamedataItemType.Wea_SniperRifle:
      case gamedataItemType.Wea_ShotgunDual:
      case gamedataItemType.Wea_Shotgun:
      case gamedataItemType.Wea_Rifle:
      case gamedataItemType.Wea_Revolver:
      case gamedataItemType.Wea_PrecisionRifle:
      case gamedataItemType.Wea_LightMachineGun:
      case gamedataItemType.Wea_HeavyMachineGun:
        ArrayPush(slots, t"AttachmentSlots.Scope");
        ArrayPush(slots, t"AttachmentSlots.PowerModule");
        ArrayPush(slots, t"AttachmentSlots.GenericWeaponMod1");
        ArrayPush(slots, t"AttachmentSlots.GenericWeaponMod2");
        ArrayPush(slots, t"AttachmentSlots.GenericWeaponMod3");
        ArrayPush(slots, t"AttachmentSlots.GenericWeaponMod4");
        ArrayPush(slots, t"AttachmentSlots.PowerWeaponModRare");
        ArrayPush(slots, t"AttachmentSlots.TechWeaponModRare");
        ArrayPush(slots, t"AttachmentSlots.SmartWeaponModRare");
        ArrayPush(slots, t"AttachmentSlots.PowerWeaponModEpic");
        ArrayPush(slots, t"AttachmentSlots.TechWeaponModEpic");
        ArrayPush(slots, t"AttachmentSlots.SmartWeaponModEpic");
        ArrayPush(slots, t"AttachmentSlots.PowerWeaponModLegendary");
        ArrayPush(slots, t"AttachmentSlots.TechWeaponModLegendary");
        ArrayPush(slots, t"AttachmentSlots.SmartWeaponModLegendary");
        ArrayPush(slots, t"AttachmentSlots.IconicWeaponModLegendary");
        break;
      case gamedataItemType.Wea_Machete:
      case gamedataItemType.Wea_Chainsword:
      case gamedataItemType.Wea_Axe:
      case gamedataItemType.Wea_Knife:
      case gamedataItemType.Wea_Katana:
      case gamedataItemType.Wea_Melee:
      case gamedataItemType.Wea_LongBlade:
      case gamedataItemType.Wea_OneHandedClub:
      case gamedataItemType.Wea_ShortBlade:
      case gamedataItemType.Wea_Hammer:
      case gamedataItemType.Wea_TwoHandedClub:
      case gamedataItemType.Wea_Fists:
        ArrayPush(slots, t"AttachmentSlots.MeleeWeaponMod1");
        ArrayPush(slots, t"AttachmentSlots.MeleeWeaponMod2");
        ArrayPush(slots, t"AttachmentSlots.MeleeWeaponMod3");
        ArrayPush(slots, t"AttachmentSlots.IconicMeleeWeaponMod1");
        break;
      default:
        ArrayPush(slots, t"AttachmentSlots.KiroshiOpticsSlot1");
        ArrayPush(slots, t"AttachmentSlots.KiroshiOpticsSlot2");
        ArrayPush(slots, t"AttachmentSlots.KiroshiOpticsSlot3");
        ArrayPush(slots, t"AttachmentSlots.SandevistanSlot1");
        ArrayPush(slots, t"AttachmentSlots.SandevistanSlot2");
        ArrayPush(slots, t"AttachmentSlots.SandevistanSlot3");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram1");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram2");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram3");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram4");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram5");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram6");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram7");
        ArrayPush(slots, t"AttachmentSlots.CyberdeckProgram8");
        ArrayPush(slots, t"AttachmentSlots.BerserkSlot1");
        ArrayPush(slots, t"AttachmentSlots.BerserkSlot2");
        ArrayPush(slots, t"AttachmentSlots.BerserkSlot3");
        ArrayPush(slots, t"AttachmentSlots.StrongArmsKnuckles");
        ArrayPush(slots, t"AttachmentSlots.StrongArmsBattery");
        ArrayPush(slots, t"AttachmentSlots.MantisBladesEdge");
        ArrayPush(slots, t"AttachmentSlots.MantisBladesRotor");
        ArrayPush(slots, t"AttachmentSlots.NanoWiresCable");
        ArrayPush(slots, t"AttachmentSlots.NanoWiresBattery");
        ArrayPush(slots, t"AttachmentSlots.ProjectileLauncherRound");
        ArrayPush(slots, t"AttachmentSlots.ProjectileLauncherWiring");
        ArrayPush(slots, t"AttachmentSlots.ArmsCyberwareGeneralSlot");
    };
    return slots;
  }

  public final static func IsAttachmentDedicated(slotID: TweakDBID) -> Bool {
    return slotID == t"AttachmentSlots.SmartWeaponModRare" || slotID == t"AttachmentSlots.TechWeaponModRare" || slotID == t"AttachmentSlots.PowerWeaponModRare" || slotID == t"AttachmentSlots.SmartWeaponModEpic" || slotID == t"AttachmentSlots.TechWeaponModEpic" || slotID == t"AttachmentSlots.PowerWeaponModEpic" || slotID == t"AttachmentSlots.SmartWeaponModLegendary" || slotID == t"AttachmentSlots.TechWeaponModLegendary" || slotID == t"AttachmentSlots.PowerWeaponModLegendary" || slotID == t"AttachmentSlots.IconicMeleeWeaponMod1" || slotID == t"AttachmentSlots.IconicWeaponModLegendary";
  }
}
