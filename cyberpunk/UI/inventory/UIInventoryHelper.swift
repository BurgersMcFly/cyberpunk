
public abstract final class UIInventoryHelper extends IScriptable {

  public final static func GetCommonCraftingMaterials() -> array<TweakDBID> {
    let result: array<TweakDBID>;
    ArrayPush(result, t"Items.CommonMaterial1");
    ArrayPush(result, t"Items.UncommonMaterial1");
    ArrayPush(result, t"Items.RareMaterial1");
    ArrayPush(result, t"Items.RareMaterial2");
    ArrayPush(result, t"Items.EpicMaterial1");
    ArrayPush(result, t"Items.EpicMaterial2");
    ArrayPush(result, t"Items.LegendaryMaterial1");
    ArrayPush(result, t"Items.LegendaryMaterial2");
    return result;
  }

  public final static func GetHackingCraftingMaterials() -> array<TweakDBID> {
    let result: array<TweakDBID>;
    ArrayPush(result, t"Items.QuickHackUncommonMaterial1");
    ArrayPush(result, t"Items.QuickHackRareMaterial1");
    ArrayPush(result, t"Items.QuickHackEpicMaterial1");
    ArrayPush(result, t"Items.QuickHackLegendaryMaterial1");
    return result;
  }
}
