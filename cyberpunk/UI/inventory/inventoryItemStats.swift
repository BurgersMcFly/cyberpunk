
public class UIInventoryItemStatsManager extends IScriptable {

  public let Stats: array<ref<UIInventoryItemStat>>;

  private let m_manager: wref<UIInventoryItemsManager>;

  public final static func Make(itemData: ref<gameItemData>, statMap: wref<UIStatsMap_Record>, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItemStatsManager> {
    let absValue: Float;
    let i: Int32;
    let itemStat: ref<UIInventoryItemStat>;
    let limit: Int32;
    let roundValue: Bool;
    let secondaryStats: array<wref<Stat_Record>>;
    let type: gamedataStatType;
    let value: Float;
    let isClothing: Bool = UIInventoryItemsManager.IsItemTypeCloting(itemData.GetItemType());
    let instance: ref<UIInventoryItemStatsManager> = new UIInventoryItemStatsManager();
    instance.m_manager = manager;
    statMap.SecondaryStats(secondaryStats);
    i = 0;
    limit = ArraySize(secondaryStats);
    while i < limit {
      type = secondaryStats[i].StatType();
      if isClothing && Equals(type, gamedataStatType.Armor) {
      } else {
        value = itemData.GetStatValueByType(type);
        roundValue = TweakDBInterface.GetBool(secondaryStats[i].GetID() + t".isPercentage", false);
        absValue = AbsF(value);
        if roundValue ? RoundF(absValue) <= 0 : absValue <= 0.01 {
        } else {
          itemStat = new UIInventoryItemStat();
          itemStat.Type = type;
          itemStat.Value = value;
          if IsDefined(instance.m_manager) {
            itemStat.Properties = instance.m_manager.GetCachedUIStatProperties(itemStat.Type);
          } else {
            itemStat.Properties = UIInventoryItemsManager.GetUIStatProperties(itemStat.Type);
          };
          ArrayPush(instance.Stats, itemStat);
        };
      };
      i += 1;
    };
    return instance;
  }

  public final static func FromMinimalItemTooltipData(data: ref<MinimalItemTooltipData>, opt manager: wref<UIInventoryItemsManager>) -> ref<UIInventoryItemStatsManager> {
    let itemStat: ref<UIInventoryItemStat>;
    let instance: ref<UIInventoryItemStatsManager> = new UIInventoryItemStatsManager();
    instance.m_manager = manager;
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(data.stats);
    while i < limit {
      itemStat = new UIInventoryItemStat();
      itemStat.Type = data.stats[i].type;
      itemStat.Value = data.stats[i].value;
      itemStat.Properties = UIItemStatProperties.Make(data.stats[i].statName, data.stats[i].roundValue, data.stats[i].isPercentage, data.stats[i].displayPlus, data.stats[i].inMeters, data.stats[i].inSeconds, data.stats[i].inSpeed);
      ArrayPush(instance.Stats, itemStat);
      i += 1;
    };
    return instance;
  }

  public final func Size() -> Int32 {
    return ArraySize(this.Stats);
  }

  public final func Get(index: Int32) -> wref<UIInventoryItemStat> {
    return this.Stats[index];
  }

  public final func GetByType(type: gamedataStatType) -> wref<UIInventoryItemStat> {
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(this.Stats);
    while i < limit {
      if Equals(this.Stats[i].Type, type) {
        return this.Stats[i];
      };
      i += 1;
    };
    return null;
  }
}

public class UIItemStatProperties extends IScriptable {

  private let localizedName: String;

  private let roundValue: Bool;

  private let isPercentage: Bool;

  private let displayPlus: Bool;

  private let inMeters: Bool;

  private let inSeconds: Bool;

  private let inSpeed: Bool;

  public final static func Make(localizedName: String, roundValue: Bool, isPercentage: Bool, displayPlus: Bool, inMeters: Bool, inSeconds: Bool, inSpeed: Bool) -> ref<UIItemStatProperties> {
    let instance: ref<UIItemStatProperties> = new UIItemStatProperties();
    instance.localizedName = localizedName;
    instance.roundValue = roundValue;
    instance.isPercentage = isPercentage;
    instance.displayPlus = displayPlus;
    instance.inMeters = inMeters;
    instance.inSeconds = inSeconds;
    instance.inSpeed = inSpeed;
    return instance;
  }

  public final func GetName() -> String {
    return this.localizedName;
  }

  public final func RoundValue() -> Bool {
    return this.roundValue;
  }

  public final func IsPercentage() -> Bool {
    return this.isPercentage;
  }

  public final func DisplayPlus() -> Bool {
    return this.displayPlus;
  }

  public final func InMeters() -> Bool {
    return this.inMeters;
  }

  public final func InSeconds() -> Bool {
    return this.inSeconds;
  }

  public final func InSpeed() -> Bool {
    return this.inSpeed;
  }
}
