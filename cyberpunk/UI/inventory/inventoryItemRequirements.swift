
public class UIInventoryItemRequirementsManager extends IScriptable {

  private let m_itemRequiredLevel: Int32;

  private let m_requiredStrength: Int32;

  private let m_requiredReflex: Int32;

  private let m_perkRequirementName: String;

  private let m_isSmartlinkRequirementMet: Bool;

  private let m_isLevelRequirementMet: Bool;

  private let m_isStrengthRequirementMet: Bool;

  private let m_isReflexRequirementMet: Bool;

  private let m_isPerkRequirementMet: Bool;

  private let m_attachedItem: wref<UIInventoryItem>;

  public final static func Make(inventoryItem: wref<UIInventoryItem>, player: wref<GameObject>) -> ref<UIInventoryItemRequirementsManager> {
    let instance: ref<UIInventoryItemRequirementsManager> = new UIInventoryItemRequirementsManager();
    instance.m_attachedItem = inventoryItem;
    let itemData: ref<gameItemData> = inventoryItem.GetItemData();
    let statsSystem: ref<StatsSystem> = GameInstance.GetStatsSystem(player.GetGame());
    instance.m_itemRequiredLevel = Cast<Int32>(itemData.GetStatValueByType(gamedataStatType.Level));
    instance.m_requiredStrength = Cast<Int32>(itemData.GetStatValueByType(gamedataStatType.Strength));
    instance.m_requiredReflex = Cast<Int32>(itemData.GetStatValueByType(gamedataStatType.Reflexes));
    instance.Update(player, statsSystem);
    return instance;
  }

  public final func Update(player: wref<GameObject>, opt statsSystem: ref<StatsSystem>) -> Void {
    let perkRequiredName: String;
    let itemData: ref<gameItemData> = this.m_attachedItem.GetItemData();
    if statsSystem == null {
      statsSystem = GameInstance.GetStatsSystem(player.GetGame());
    };
    if RPGManager.HasSmartLinkRequirement(itemData) {
      this.m_isSmartlinkRequirementMet = Cast<Bool>(statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.HasSmartLink));
    } else {
      this.m_isSmartlinkRequirementMet = true;
    };
    if this.m_itemRequiredLevel > 0 {
      this.m_isLevelRequirementMet = this.m_itemRequiredLevel <= Cast<Int32>(statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Level));
    } else {
      this.m_isLevelRequirementMet = true;
    };
    if this.m_requiredStrength > 0 {
      this.m_isStrengthRequirementMet = this.m_requiredStrength <= Cast<Int32>(statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Strength));
    } else {
      this.m_isStrengthRequirementMet = true;
    };
    if this.m_requiredReflex > 0 {
      this.m_isReflexRequirementMet = this.m_requiredReflex <= Cast<Int32>(statsSystem.GetStatValue(Cast<StatsObjectID>(player.GetEntityID()), gamedataStatType.Reflexes));
    } else {
      this.m_isReflexRequirementMet = true;
    };
    if RPGManager.CheckCraftedItemPerkPrereq(itemData, player, perkRequiredName) {
      this.m_isPerkRequirementMet = false;
      this.m_perkRequirementName = perkRequiredName;
    } else {
      this.m_isPerkRequirementMet = true;
    };
  }

  public final func IsSmartlinkRequirementMet() -> Bool {
    return this.m_isSmartlinkRequirementMet;
  }

  public final func IsLevelRequirementMet() -> Bool {
    return this.m_isLevelRequirementMet;
  }

  public final func IsStrengthRequirementMet() -> Bool {
    return this.m_isStrengthRequirementMet;
  }

  public final func IsReflexRequirementMet() -> Bool {
    return this.m_isReflexRequirementMet;
  }

  public final func IsPerkRequirementMet() -> Bool {
    return this.m_isPerkRequirementMet;
  }

  public final func IsRarityRequirementMet(parentItem: wref<UIInventoryItem>) -> Bool {
    if this.m_attachedItem.IsPart() && parentItem.IsClothing() {
      return parentItem.GetQualityInt() <= this.m_attachedItem.GetQualityInt();
    };
    return true;
  }

  public final func IsAnyRequirementNotMet() -> Bool {
    return !this.IsSmartlinkRequirementMet() || this.IsAnyItemDisplayRequirementNotMet();
  }

  public final func IsAnyItemDisplayRequirementNotMet() -> Bool {
    return !this.IsLevelRequirementMet() || !this.IsStrengthRequirementMet() || !this.IsReflexRequirementMet() || !this.IsPerkRequirementMet();
  }

  public final func GetLevelRequirementValue() -> Int32 {
    return this.m_itemRequiredLevel;
  }

  public final func GetStrengthRequirementValue() -> Int32 {
    return this.m_requiredStrength;
  }

  public final func GetReflexRequirementValue() -> Int32 {
    return this.m_requiredReflex;
  }

  public final func GetPerkRequirementValue() -> String {
    return this.m_perkRequirementName;
  }
}
