
public class UIInventoryScriptableSystem extends ScriptableSystem {

  private let m_attachedPlayer: wref<PlayerPuppet>;

  private let m_inventoryListenerCallback: ref<UIInventoryScriptableInventoryListenerCallback>;

  private let m_inventoryListener: ref<InventoryScriptListener>;

  private let m_equipmentListener: ref<UIInventoryScriptableEquipmentListener>;

  private let m_playerItems: ref<inkHashMap>;

  private let m_transactionSystem: ref<TransactionSystem>;

  private let m_inventoryItemsManager: ref<UIInventoryItemsManager>;

  private let m_blacklistedTags: array<CName>;

  private let m_cachedNonInventoryItems: ref<inkHashMap>;

  private let m_InventoryBlackboard: wref<IBlackboard>;

  private func OnDetach() -> Void {
    GameInstance.GetTransactionSystem(this.m_attachedPlayer.GetGame()).UnregisterInventoryListener(this.m_attachedPlayer, this.m_inventoryListener);
    this.m_inventoryListener = null;
    this.m_equipmentListener.UnregisterBlackboard();
  }

  private final func SetupInstance() -> Void {
    let i: Int32;
    let limit: Int32;
    let playerItems: array<wref<gameItemData>>;
    this.m_inventoryListenerCallback = new UIInventoryScriptableInventoryListenerCallback();
    this.m_transactionSystem = GameInstance.GetTransactionSystem(this.GetGameInstance());
    let uiScriptableSystem: wref<UIScriptableSystem> = UIScriptableSystem.GetInstance(this.GetGameInstance());
    this.m_inventoryItemsManager = UIInventoryItemsManager.Make(this.m_attachedPlayer, this.m_transactionSystem, uiScriptableSystem);
    this.m_playerItems = new inkHashMap();
    this.m_cachedNonInventoryItems = new inkHashMap();
    this.m_blacklistedTags = UIInventoryItemsManager.GetBlacklistedTags();
    this.m_transactionSystem.GetItemListExcludingTags(this.m_attachedPlayer, this.m_blacklistedTags, playerItems);
    i = 0;
    limit = ArraySize(playerItems);
    while i < limit {
      if ItemID.HasFlag(playerItems[i].GetID(), gameEItemIDFlag.Preview) {
      } else {
        this.m_playerItems.Insert(ItemID.GetCombinedHash(playerItems[i].GetID()), UIInventoryItem.Make(this.m_attachedPlayer, playerItems[i], this.m_inventoryItemsManager));
      };
      i += 1;
    };
  }

  private final func OnPlayerAttach(request: ref<PlayerAttachRequest>) -> Void {
    if !request.owner.IsPlayer() || request.owner.IsReplacer() {
      return;
    };
    if this.m_attachedPlayer != null {
      GameInstance.GetTransactionSystem(this.m_attachedPlayer.GetGame()).UnregisterInventoryListener(this.m_attachedPlayer, this.m_inventoryListener);
    };
    this.m_attachedPlayer = request.owner as PlayerPuppet;
    this.SetupInstance();
    if IsDefined(this.m_equipmentListener) {
      this.m_equipmentListener.UnregisterBlackboard();
    };
    this.m_InventoryBlackboard = null;
    this.m_equipmentListener = new UIInventoryScriptableEquipmentListener();
    this.m_equipmentListener.AttachScriptableSystem(this.m_attachedPlayer.GetGame());
    this.m_equipmentListener.RegisterBlackboard(this.m_attachedPlayer.GetGame());
    this.m_InventoryBlackboard = GameInstance.GetBlackboardSystem(this.m_attachedPlayer.GetGame()).Get(GetAllBlackboardDefs().UI_Inventory);
    this.m_inventoryItemsManager.AttachPlayer(this.m_attachedPlayer);
    this.m_inventoryListener = GameInstance.GetTransactionSystem(this.m_attachedPlayer.GetGame()).RegisterInventoryListener(this.m_attachedPlayer, this.m_inventoryListenerCallback);
    this.m_inventoryListenerCallback.AttachScriptableSystem(this.m_attachedPlayer.GetGame());
  }

  public final static func GetInstance(gameInstance: GameInstance) -> ref<UIInventoryScriptableSystem> {
    let system: ref<UIInventoryScriptableSystem> = GameInstance.GetScriptableSystemsContainer(gameInstance).Get(n"UIInventoryScriptableSystem") as UIInventoryScriptableSystem;
    return system;
  }

  public final const func GetInventoryItemsManager() -> wref<UIInventoryItemsManager> {
    return this.m_inventoryItemsManager;
  }

  public final const func GetPlayerItemsMap() -> ref<inkHashMap> {
    return this.m_playerItems;
  }

  public final const func GetPlayerItem(itemID: ItemID) -> wref<UIInventoryItem> {
    return this.m_playerItems.Get(ItemID.GetCombinedHash(itemID)) as UIInventoryItem;
  }

  public final const func GetPlayerItem(hash: Uint64) -> wref<UIInventoryItem> {
    return this.m_playerItems.Get(hash) as UIInventoryItem;
  }

  public final const func QueryPlayerItem(tweakID: TweakDBID) -> wref<UIInventoryItem> {
    return this.m_playerItems.Get(ItemID.GetCombinedHash(ItemID.CreateQuery(tweakID))) as UIInventoryItem;
  }

  public final const func GetNonInventoryItem(itemID: ItemID) -> wref<UIInventoryItem> {
    let itemData: ref<gameItemData>;
    let hash: Uint64 = ItemID.GetCombinedHash(itemID);
    if !this.m_cachedNonInventoryItems.KeyExist(hash) {
      itemData = RPGManager.GetItemData(this.m_attachedPlayer.GetGame(), this.m_attachedPlayer, itemID);
      if !this.Internal_FetchNonInventoryItem(hash, itemData) {
        return null;
      };
    };
    return this.m_cachedNonInventoryItems.Get(hash) as UIInventoryItem;
  }

  private final const func Internal_FetchNonInventoryItem(hash: Uint64, itemData: wref<gameItemData>) -> Bool {
    let inventoryItem: ref<UIInventoryItem>;
    if itemData == null {
      return false;
    };
    inventoryItem = UIInventoryItem.Make(this.m_attachedPlayer, itemData, this.m_inventoryItemsManager);
    this.m_cachedNonInventoryItems.Insert(hash, inventoryItem);
    return true;
  }

  public final const func GetPlayerItemFromAnySource(itemData: wref<gameItemData>) -> wref<UIInventoryItem> {
    let hash: Uint64 = ItemID.GetCombinedHash(itemData.GetID());
    let result: wref<UIInventoryItem> = this.GetPlayerItem(hash);
    if IsDefined(result) {
      return result;
    };
    if !this.m_cachedNonInventoryItems.KeyExist(hash) {
      if !this.Internal_FetchNonInventoryItem(hash, itemData) {
        return null;
      };
    };
    return this.m_cachedNonInventoryItems.Get(hash) as UIInventoryItem;
  }

  public final const func QueryNonInventoryItem(tweakID: TweakDBID) -> wref<UIInventoryItem> {
    return this.GetNonInventoryItem(ItemID.CreateQuery(tweakID));
  }

  public final const func GetPlayerAreaItems(equipmentArea: gamedataEquipmentArea) -> array<wref<UIInventoryItem>> {
    let result: array<wref<UIInventoryItem>>;
    let ids: array<ItemID> = this.m_inventoryItemsManager.GetRawEquippedItems(equipmentArea);
    let i: Int32 = 0;
    let limit: Int32 = ArraySize(ids);
    while i < limit {
      ArrayPush(result, this.m_playerItems.Get(ItemID.GetCombinedHash(ids[i])) as UIInventoryItem);
      i += 1;
    };
    return result;
  }

  public final const func GetPlayerAreaItem(equipmentArea: gamedataEquipmentArea, opt slotIndex: Int32) -> wref<UIInventoryItem> {
    let ids: array<ItemID> = this.m_inventoryItemsManager.GetRawEquippedItems(equipmentArea);
    return this.m_playerItems.Get(ItemID.GetCombinedHash(ids[slotIndex])) as UIInventoryItem;
  }

  public final const func FlushFullscreenCache() -> Void {
    this.FlushTempData();
  }

  public final const func FlushTempData() -> Void {
    let i: Int32;
    let limit: Int32;
    let values: array<wref<IScriptable>>;
    this.m_inventoryItemsManager.FlushAmmoCache();
    this.m_inventoryItemsManager.FlushEquippedItems();
    this.m_inventoryItemsManager.FlushTransmogItems();
    this.m_playerItems.GetValues(values);
    i = 0;
    limit = ArraySize(values);
    while i < limit {
      values[i] as UIInventoryItem.Internal_FlushRequirements();
      i += 1;
    };
  }

  private final func OnInventoryItemAdded(request: ref<UIInventoryScriptableSystemInventoryAddItem>) -> Void {
    let data: ref<UIInventoryItem>;
    let i: Int32;
    let isBackpackItem: Bool;
    let itemRecord: wref<Item_Record>;
    let itemTweakID: TweakDBID;
    let limit: Int32;
    let tags: array<CName>;
    if ItemID.HasFlag(request.itemID, gameEItemIDFlag.Preview) {
      return;
    };
    itemTweakID = ItemID.GetTDBID(request.itemID);
    itemRecord = TweakDBInterface.GetItemRecord(itemTweakID);
    tags = itemRecord.Tags();
    isBackpackItem = true;
    i = 0;
    limit = ArraySize(tags);
    while i < limit {
      if ArrayContains(this.m_blacklistedTags, tags[i]) {
        isBackpackItem = false;
      };
      i += 1;
    };
    data = UIInventoryItem.Make(this.m_attachedPlayer, request.itemData, itemTweakID, itemRecord, this.m_inventoryItemsManager);
    isBackpackItem ? this.m_playerItems : this.m_cachedNonInventoryItems.Insert(ItemID.GetCombinedHash(request.itemID), data);
    this.m_InventoryBlackboard.SetVariant(GetAllBlackboardDefs().UI_Inventory.itemAdded, ToVariant(request.itemID));
  }

  private final func OnInventoryItemRemoved(request: ref<UIInventoryScriptableSystemInventoryRemoveItem>) -> Void {
    let hash: Uint64;
    if ItemID.HasFlag(request.itemID, gameEItemIDFlag.Preview) {
      return;
    };
    hash = ItemID.GetCombinedHash(request.itemID);
    this.m_playerItems.Remove(hash);
    this.m_cachedNonInventoryItems.Remove(hash);
    this.m_InventoryBlackboard.SetVariant(GetAllBlackboardDefs().UI_Inventory.itemRemoved, ToVariant(request.itemID));
  }

  private final func OnInventoryItemQuantityChanged(request: ref<UIInventoryScriptableSystemInventoryQuantityChanged>) -> Void {
    let hash: Uint64 = ItemID.GetCombinedHash(request.itemID);
    let uiInventoryItem: wref<UIInventoryItem> = this.m_playerItems.Get(hash) as UIInventoryItem;
    if uiInventoryItem == null {
      uiInventoryItem = this.m_cachedNonInventoryItems.Get(hash) as UIInventoryItem;
    };
    uiInventoryItem.MarkQuantityDirty();
  }

  private final func OnPartInstallRequest(request: ref<PartInstallRequest>) -> Void {
    let item: wref<UIInventoryItem> = this.m_playerItems.Get(ItemID.GetCombinedHash(request.itemID)) as UIInventoryItem;
    if IsDefined(item) {
      item.Internal_MarkStatsDirty();
      item.Internal_MarkModsDirty();
    };
  }

  private final func OnPartUninstallRequest(request: ref<PartUninstallRequest>) -> Void {
    let item: wref<UIInventoryItem> = this.m_playerItems.Get(ItemID.GetCombinedHash(request.itemID)) as UIInventoryItem;
    if IsDefined(item) {
      item.Internal_MarkStatsDirty();
      item.Internal_MarkModsDirty();
    };
  }

  public final static func NumberOfWeaponSlots() -> Int32 {
    return 3;
  }
}

public class UIInventoryScriptableInventoryListenerCallback extends InventoryScriptCallback {

  private let m_uiInventoryScriptableSystem: wref<UIInventoryScriptableSystem>;

  public final func AttachScriptableSystem(gameInstance: GameInstance) -> Void {
    this.m_uiInventoryScriptableSystem = UIInventoryScriptableSystem.GetInstance(gameInstance);
  }

  public func OnItemAdded(_itemID: ItemID, itemData: wref<gameItemData>, flaggedAsSilent: Bool) -> Void {
    let request: ref<UIInventoryScriptableSystemInventoryAddItem> = new UIInventoryScriptableSystemInventoryAddItem();
    request.itemID = _itemID;
    request.itemData = itemData;
    this.m_uiInventoryScriptableSystem.QueueRequest(request);
  }

  public func OnItemRemoved(_itemID: ItemID, difference: Int32, currentQuantity: Int32) -> Void {
    let request: ref<UIInventoryScriptableSystemInventoryRemoveItem> = new UIInventoryScriptableSystemInventoryRemoveItem();
    request.itemID = _itemID;
    this.m_uiInventoryScriptableSystem.QueueRequest(request);
  }

  public func OnItemQuantityChanged(_itemID: ItemID, diff: Int32, total: Uint32, flaggedAsSilent: Bool) -> Void {
    let request: ref<UIInventoryScriptableSystemInventoryQuantityChanged> = new UIInventoryScriptableSystemInventoryQuantityChanged();
    request.itemID = _itemID;
    this.m_uiInventoryScriptableSystem.QueueRequest(request);
  }

  public func OnItemExtracted(_itemID: ItemID) -> Void {
    let request: ref<UIInventoryScriptableSystemInventoryRemoveItem> = new UIInventoryScriptableSystemInventoryRemoveItem();
    request.itemID = _itemID;
    this.m_uiInventoryScriptableSystem.QueueRequest(request);
  }
}

public class UIInventoryScriptableEquipmentListener extends IScriptable {

  private let m_uiInventoryScriptableSystem: wref<UIInventoryScriptableSystem>;

  private let m_EquipmentBlackboard: wref<IBlackboard>;

  private let m_itemEquippedListener: ref<CallbackHandle>;

  public final func AttachScriptableSystem(gameInstance: GameInstance) -> Void {
    this.m_uiInventoryScriptableSystem = UIInventoryScriptableSystem.GetInstance(gameInstance);
  }

  public final func RegisterBlackboard(gameInstance: GameInstance) -> Void {
    this.m_EquipmentBlackboard = GameInstance.GetBlackboardSystem(gameInstance).Get(GetAllBlackboardDefs().UI_Equipment);
    if IsDefined(this.m_EquipmentBlackboard) {
      this.m_itemEquippedListener = this.m_EquipmentBlackboard.RegisterListenerInt(GetAllBlackboardDefs().UI_Equipment.areaChanged, this, n"OnAreaEquippedChanged");
    };
  }

  public final func UnregisterBlackboard() -> Void {
    if IsDefined(this.m_EquipmentBlackboard) {
      this.m_EquipmentBlackboard.UnregisterListenerInt(GetAllBlackboardDefs().UI_Equipment.areaChanged, this.m_itemEquippedListener);
      this.m_itemEquippedListener = null;
    };
    this.m_EquipmentBlackboard = null;
  }

  protected cb func OnAreaEquippedChanged(equipmentAreaIndex: Int32) -> Bool;
}
