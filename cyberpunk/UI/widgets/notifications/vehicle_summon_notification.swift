
public class VehicleSummonWidgetGameController extends inkHUDGameController {

  private edit let m_vehicleNameLabel: inkTextRef;

  private edit let m_vehicleTypeIcon: inkImageRef;

  private edit let m_vehicleManufactorHolder: inkWidgetRef;

  private edit let m_vehicleManufactorIcon: inkImageRef;

  private edit let m_distanceLabel: inkTextRef;

  private edit let m_unit: EMeasurementUnit;

  private edit let m_subText: inkTextRef;

  private edit let m_radioStationName: inkTextRef;

  private let m_rootWidget: wref<inkWidget>;

  private let m_animationProxy: ref<inkAnimProxy>;

  private let m_animationCounterProxy: ref<inkAnimProxy>;

  private let m_optionIntro: inkAnimOptions;

  private let m_optionCounter: inkAnimOptions;

  private let m_vehicleSummonDataDef: ref<VehicleSummonDataDef>;

  private let m_vehicleSummonDataBB: wref<IBlackboard>;

  private let m_vehicleSummonStateCallback: ref<CallbackHandle>;

  private let m_vehicleSummonState: Uint32;

  private let m_vehiclePurchaseDataDef: ref<VehiclePurchaseDataDef>;

  private let m_vehiclePurchaseDataBB: wref<IBlackboard>;

  private let m_vehiclePurchaseStateCallback: ref<CallbackHandle>;

  private let m_purchasedVehicleID: TweakDBID;

  private let m_stateChangesCallback: ref<CallbackHandle>;

  private let m_songNameChangeCallback: ref<CallbackHandle>;

  private let m_activeVehicleBlackboard: wref<IBlackboard>;

  private let m_mountCallback: ref<CallbackHandle>;

  private let m_vehiclePos: Vector4;

  private let m_playerPos: Vector4;

  private let m_distanceVector: Vector4;

  private let m_gameInstance: GameInstance;

  private let m_distance: Int32;

  private let m_vehicleID: EntityID;

  private let m_vehicleEntity: wref<Entity>;

  private let m_vehicle: wref<VehicleObject>;

  private let m_vehicleRecord: ref<Vehicle_Record>;

  private let m_textParams: ref<inkTextParams>;

  private let m_iconRecord: ref<UIIcon_Record>;

  private let m_playerVehicle: wref<VehicleObject>;

  private let m_player: wref<PlayerPuppet>;

  protected cb func OnInitialize() -> Bool {
    this.m_rootWidget = this.GetRootWidget();
    this.m_rootWidget.SetVisible(false);
    inkWidgetRef.SetVisible(this.m_subText, false);
    inkWidgetRef.SetVisible(this.m_radioStationName, false);
    this.m_vehicleSummonDataDef = GetAllBlackboardDefs().VehicleSummonData;
    this.m_vehicleSummonDataBB = this.GetBlackboardSystem().Get(this.m_vehicleSummonDataDef);
    this.m_vehicleSummonStateCallback = this.m_vehicleSummonDataBB.RegisterListenerUint(this.m_vehicleSummonDataDef.SummonState, this, n"OnVehicleSummonStateChanged");
    this.m_vehiclePurchaseDataDef = GetAllBlackboardDefs().VehiclePurchaseData;
    this.m_vehiclePurchaseDataBB = this.GetBlackboardSystem().Get(this.m_vehiclePurchaseDataDef);
    this.m_vehiclePurchaseStateCallback = this.m_vehiclePurchaseDataBB.RegisterListenerVariant(this.m_vehiclePurchaseDataDef.PurchasedVehicleRecordID, this, n"OnVehiclePurchased");
    this.m_activeVehicleBlackboard = this.GetBlackboardSystem().Get(GetAllBlackboardDefs().UI_ActiveVehicleData);
    this.m_mountCallback = this.m_activeVehicleBlackboard.RegisterListenerBool(GetAllBlackboardDefs().UI_ActiveVehicleData.IsPlayerMounted, this, n"OnVehicleMount");
  }

  protected cb func OnUninitialize() -> Bool {
    this.m_rootWidget.SetVisible(false);
    inkWidgetRef.SetVisible(this.m_subText, false);
    inkWidgetRef.SetVisible(this.m_radioStationName, false);
    this.m_vehicleSummonDataBB.UnregisterListenerUint(this.m_vehicleSummonDataDef.SummonState, this.m_vehicleSummonStateCallback);
    this.m_vehiclePurchaseDataBB.UnregisterListenerVariant(this.m_vehiclePurchaseDataDef.PurchasedVehicleRecordID, this.m_vehiclePurchaseStateCallback);
    this.m_activeVehicleBlackboard.UnregisterListenerBool(GetAllBlackboardDefs().UI_ActiveVehicleData.IsPlayerMounted, this.m_mountCallback);
  }

  protected cb func OnPlayerAttach(playerPuppet: ref<GameObject>) -> Bool {
    this.m_playerPos = this.GetPlayerControlledObject().GetWorldPosition();
    this.m_gameInstance = playerPuppet.GetGame();
    this.m_player = playerPuppet as PlayerPuppet;
  }

  protected cb func OnVehicleSummonStateChanged(value: Uint32) -> Bool {
    this.m_vehicleSummonState = value;
    if this.m_vehicleSummonState == EnumInt(vehicleSummonState.EnRoute) {
      this.ShowSummonNotification();
    } else {
      if this.m_vehicleSummonState == EnumInt(vehicleSummonState.Arrived) {
        this.HideNotification();
      } else {
        if this.m_vehicleSummonState == EnumInt(vehicleSummonState.AlreadySummoned) {
          inkWidgetRef.SetVisible(this.m_subText, true);
          this.PlayLibraryAnimation(n"waiting");
          inkTextRef.SetText(this.m_subText, GetLocalizedText("LocKey#78044"));
        } else {
          this.m_rootWidget.SetVisible(false);
        };
      };
    };
  }

  protected cb func OnVehiclePurchased(value: Variant) -> Bool {
    let vehicleRecordID: TweakDBID;
    this.m_rootWidget.SetVisible(true);
    inkWidgetRef.SetVisible(this.m_subText, true);
    this.PlayAnim(n"OnVehiclePurchase", n"OnTimeOut");
    vehicleRecordID = FromVariant<TweakDBID>(value);
    this.m_vehicleRecord = TweakDBInterface.GetVehicleRecord(vehicleRecordID);
    inkTextRef.SetLocalizedTextScript(this.m_vehicleNameLabel, this.m_vehicleRecord.DisplayName());
    inkTextRef.SetText(this.m_subText, "LocKey#43690");
    this.SetVehicleIcon(this.m_vehicleRecord.Type().Type());
    this.SetVehicleIconManufactorIcon(this.m_vehicleRecord.Manufacturer().EnumName());
  }

  protected cb func OnVehicleRadioEvent(evt: ref<UIVehicleRadioEvent>) -> Bool {
    this.PlayAnim(n"OnSongChanged", n"OnTimeOut");
    if IsDefined(this.m_playerVehicle) {
      this.m_rootWidget.SetVisible(true);
      inkWidgetRef.SetVisible(this.m_subText, true);
      inkWidgetRef.SetVisible(this.m_radioStationName, true);
      inkTextRef.SetText(this.m_radioStationName, GetLocalizedTextByKey(this.m_playerVehicle.GetRadioReceiverStationName()));
      inkTextRef.SetText(this.m_subText, GetLocalizedTextByKey(this.m_playerVehicle.GetRadioReceiverTrackName()));
    };
  }

  protected cb func OnVehicleMount(value: Bool) -> Bool {
    let radioOn: Bool;
    this.m_rootWidget.SetVisible(value);
    if value {
      VehicleComponent.GetVehicle(this.m_gameInstance, this.m_player, this.m_playerVehicle);
      this.m_vehicleRecord = TweakDBInterface.GetVehicleRecord(this.m_playerVehicle.GetRecordID());
      if IsDefined(this.m_playerVehicle) {
        inkTextRef.SetLocalizedTextScript(this.m_vehicleNameLabel, this.m_vehicleRecord.DisplayName());
        this.SetVehicleIcon(this.m_vehicleRecord.Type().Type());
        this.SetVehicleIconManufactorIcon(this.m_vehicleRecord.Manufacturer().EnumName());
        radioOn = this.m_playerVehicle.IsRadioReceiverActive();
        if radioOn {
          this.PlayAnim(n"OnVehicleEnter", n"OnVehicleEnterDone");
          inkTextRef.SetText(this.m_radioStationName, GetLocalizedTextByKey(this.m_playerVehicle.GetRadioReceiverStationName()));
          inkTextRef.SetText(this.m_subText, GetLocalizedTextByKey(this.m_playerVehicle.GetRadioReceiverTrackName()));
        } else {
          this.PlayAnim(n"OnVehicleEnter", n"OnTimeOut");
        };
      };
    };
  }

  protected cb func OnVehicleEnterDone(anim: ref<inkAnimProxy>) -> Bool {
    inkWidgetRef.SetVisible(this.m_subText, true);
    inkWidgetRef.SetVisible(this.m_radioStationName, true);
    this.PlayAnim(n"OnSongChanged", n"OnTimeOut");
  }

  public final func ShowSummonNotification() -> Void {
    this.m_vehicleID = this.m_vehicleSummonDataBB.GetEntityID(this.m_vehicleSummonDataDef.SummonedVehicleEntityID);
    if EntityID.IsDefined(this.m_vehicleID) {
      this.m_rootWidget.SetVisible(true);
      this.m_vehicleEntity = GameInstance.FindEntityByID(this.m_gameInstance, this.m_vehicleID);
      this.m_vehicle = this.m_vehicleEntity as VehicleObject;
      this.m_vehicleRecord = TweakDBInterface.GetVehicleRecord(this.m_vehicle.GetRecordID());
      this.m_distance = RoundF(Vector4.Distance(this.m_vehicleEntity.GetWorldPosition(), this.GetPlayerControlledObject().GetWorldPosition()));
      this.m_textParams = new inkTextParams();
      this.m_textParams.AddMeasurement("distance", Cast<Float>(this.m_distance), EMeasurementUnit.Meter);
      this.m_textParams.AddString("unit", GetLocalizedText(NameToString(MeasurementUtils.GetUnitLocalizationKey(UILocalizationHelper.GetSystemBaseUnit()))));
      inkTextRef.SetText(this.m_distanceLabel, "{distance}{unit}", this.m_textParams);
      this.PlayAnim(n"intro", n"OnIntroFinished");
      this.m_optionCounter.loopType = inkanimLoopType.Cycle;
      this.m_optionCounter.loopCounter = 35u;
      this.m_animationCounterProxy = this.PlayLibraryAnimation(n"counter", this.m_optionCounter);
      this.m_animationCounterProxy.RegisterToCallback(inkanimEventType.OnEndLoop, this, n"OnEndLoop");
      GameInstance.GetAudioSystem(this.m_gameInstance).Play(n"ui_jingle_car_call");
      inkTextRef.SetLocalizedTextScript(this.m_vehicleNameLabel, this.m_vehicleRecord.DisplayName());
      this.SetVehicleIconManufactorIcon(this.m_vehicleRecord.Manufacturer().EnumName());
      this.SetVehicleIcon(this.m_vehicleRecord.Type().Type());
    };
  }

  public final func SetVehicleIcon(vehicleType: gamedataVehicleType) -> Void {
    if Equals(vehicleType, gamedataVehicleType.Bike) {
      inkImageRef.SetTexturePart(this.m_vehicleTypeIcon, n"motorcycle");
    } else {
      inkImageRef.SetTexturePart(this.m_vehicleTypeIcon, n"car");
    };
  }

  public final func SetVehicleIconManufactorIcon(manufacturerName: String) -> Void {
    this.m_iconRecord = TweakDBInterface.GetUIIconRecord(TDBID.Create("UIIcon." + manufacturerName));
    inkImageRef.SetTexturePart(this.m_vehicleManufactorIcon, this.m_iconRecord.AtlasPartName());
  }

  public final func HideNotification() -> Void {
    if IsDefined(this.m_animationProxy) && this.m_animationProxy.IsPlaying() {
      this.m_animationProxy.Stop();
    };
    if IsDefined(this.m_animationCounterProxy) && this.m_animationCounterProxy.IsPlaying() {
      this.m_animationCounterProxy.Stop();
    };
    this.m_optionIntro.loopCounter = 0u;
    this.m_optionIntro.loopType = inkanimLoopType.None;
    this.m_optionIntro.loopInfinite = false;
    this.m_animationProxy = this.PlayLibraryAnimation(n"arrived", this.m_optionIntro);
    this.m_rootWidget.SetVisible(false);
    inkWidgetRef.SetVisible(this.m_subText, false);
    inkWidgetRef.SetVisible(this.m_radioStationName, false);
  }

  protected cb func OnIntroFinished(anim: ref<inkAnimProxy>) -> Bool {
    if IsDefined(anim) && anim.IsPlaying() {
      anim.Stop();
    };
    this.m_optionIntro.loopType = inkanimLoopType.PingPong;
    this.m_optionIntro.loopCounter = 35u;
    anim = this.PlayLibraryAnimation(n"loop", this.m_optionIntro);
    anim.RegisterToCallback(inkanimEventType.OnFinish, this, n"OnTimeOut");
  }

  protected cb func OnEndLoop(anim: ref<inkAnimProxy>) -> Bool {
    this.m_vehicleID = this.m_vehicleSummonDataBB.GetEntityID(this.m_vehicleSummonDataDef.SummonedVehicleEntityID);
    if EntityID.IsDefined(this.m_vehicleID) {
      this.m_vehicleEntity = GameInstance.FindEntityByID(this.m_gameInstance, this.m_vehicleID);
      this.m_vehicle = this.m_vehicleEntity as VehicleObject;
      this.m_distance = RoundF(Vector4.Distance(this.m_vehicleEntity.GetWorldPosition(), this.GetPlayerControlledObject().GetWorldPosition()));
      this.m_textParams = new inkTextParams();
      this.m_textParams.AddMeasurement("distance", Cast<Float>(this.m_distance), EMeasurementUnit.Meter);
      this.m_textParams.AddString("unit", GetLocalizedText(NameToString(MeasurementUtils.GetUnitLocalizationKey(UILocalizationHelper.GetSystemBaseUnit()))));
      inkTextRef.SetText(this.m_distanceLabel, "{distance}{unit}", this.m_textParams);
    };
  }

  protected cb func OnTimeOut(anim: ref<inkAnimProxy>) -> Bool {
    this.HideNotification();
  }

  public final func PlayAnim(animName: CName, opt callBack: CName) -> Void {
    if IsDefined(this.m_animationProxy) && this.m_animationProxy.IsPlaying() {
      this.m_animationProxy.GotoStartAndStop();
    };
    this.m_animationProxy = this.PlayLibraryAnimation(animName);
    if NotEquals(callBack, n"None") {
      this.m_animationProxy.RegisterToCallback(inkanimEventType.OnFinish, this, callBack);
    };
  }
}
