
public static func OperatorAdd(s: String, att: EAIAttitude) -> String {
  return s + EnumValueToString("EAIAttitude", EnumInt(att));
}

public static func OperatorAdd(att: EAIAttitude, s: String) -> String {
  return EnumValueToString("EAIAttitude", EnumInt(att)) + s;
}
