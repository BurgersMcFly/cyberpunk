
public native class ScriptStatsListener extends IStatsListener {

  public func OnStatChanged(ownerID: StatsObjectID, statType: gamedataStatType, diff: Float, total: Float) -> Void;

  public func OnGodModeChanged(ownerID: EntityID, newType: gameGodModeType) -> Void;

  public final native func SetStatType(statType: gamedataStatType) -> Void;
}

public class StatsSystemHelper extends IScriptable {

  public final static func GetDetailedStatInfo(obj: ref<GameObject>, statType: gamedataStatType, out statData: gameStatDetailedData) -> Bool {
    let detailsArray: array<gameStatDetailedData> = GameInstance.GetStatsSystem(obj.GetGame()).GetStatDetails(Cast<StatsObjectID>(obj.GetEntityID()));
    let i: Int32 = 0;
    while i < ArraySize(detailsArray) {
      if Equals(detailsArray[i].statType, statType) {
        statData = detailsArray[i];
        return true;
      };
      i += 1;
    };
    return false;
  }
}
