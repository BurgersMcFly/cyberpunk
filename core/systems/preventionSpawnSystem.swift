
public native class PreventionSpawnSystem extends IPreventionSpawnSystem {

  public final native func RequestSpawn(recordID: TweakDBID, preventionLevel: Uint32, spawnTransform: WorldTransform) -> EntityID;

  public final native func RequestVehicleSpawn(recordID: TweakDBID, districtRecordID: TweakDBID, preventionLevel: Uint32) -> Void;

  public final native func ReinitVehicle(vehicle: wref<VehicleObject>) -> Void;

  public final native func RequestDespawn(entityID: EntityID) -> Void;

  public final native func RequestDespawnPreventionLevel(preventionLevel: Uint32) -> Void;

  public final native func GetNumberOfSpawnedPreventionUnits() -> Int32;

  public final native func IsPreventionVehiclePrototypeEnabled() -> Bool;

  public final native const func FindPursuitPointsRange(playerPos: Vector4, pos: Vector4, dir: Vector4, radiusMin: Float, radiusMax: Float, count: Int32, navVisCheck: Bool, agentSize: NavGenAgentSize, out results: array<Vector4>, out fallbackResults: array<Vector4>) -> Bool;

  public final native func RegisterEntityDeathCallback(scriptable: wref<IScriptable>, functionName: String, entityID: EntityID) -> Void;

  public final native func UnregisterEntityDeathCallback(scriptable: wref<IScriptable>, functionName: String, entityID: EntityID) -> Void;

  public final native func JoinTraffic(veh: wref<VehicleObject>) -> Void;

  protected final func SpawnCallback(spawnedObject: ref<GameObject>) -> Void {
    if spawnedObject.IsPuppet() {
      PreventionSystem.RegisterPoliceUnit(spawnedObject.GetGame(), spawnedObject as ScriptedPuppet);
    } else {
      if spawnedObject.IsVehicle() {
        PreventionSystem.RegisterPoliceVehicle(spawnedObject.GetGame(), spawnedObject as VehicleObject);
      };
    };
  }
}
