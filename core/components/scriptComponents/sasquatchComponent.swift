
public class SasquatchComponent extends ScriptableComponent {

  private let m_owner: wref<NPCPuppet>;

  private let m_owner_id: EntityID;

  public final func OnGameAttach() -> Void {
    this.m_owner = this.GetOwner() as NPCPuppet;
    this.m_owner_id = this.m_owner.GetEntityID();
    if !this.m_owner.IsDead() {
      StatusEffectHelper.ApplyStatusEffect(this.m_owner, t"Sasquatch.Phase1", this.m_owner.GetEntityID());
      StatusEffectHelper.ApplyStatusEffect(this.m_owner, t"BaseStatusEffect.PainInhibitors", this.m_owner.GetEntityID());
      StatusEffectHelper.ApplyStatusEffect(this.m_owner, t"Sasquatch.Healing", this.m_owner.GetEntityID());
    };
  }

  protected cb func OnStatusEffectApplied(evt: ref<ApplyStatusEffectEvent>) -> Bool {
    let tags: array<CName> = evt.staticData.GameplayTags();
    let weapon: wref<WeaponObject> = ScriptedPuppet.GetWeaponRight(this.m_owner);
    let weaponType: gamedataItemType = WeaponObject.GetWeaponType(weapon.GetItemID());
    if ArrayContains(tags, n"Overload") {
      StatusEffectHelper.ApplyStatusEffect(this.m_owner, t"BaseStatusEffect.BossNoTakeDown", this.m_owner.GetEntityID());
      if Equals(weaponType, gamedataItemType.Wea_Hammer) {
        this.m_owner.DropWeapons();
      };
    };
  }

  protected cb func OnDefeatedSasquatch(evt: ref<DefeatedEvent>) -> Bool {
    let player: wref<PlayerPuppet> = this.GetPlayerSystem().GetLocalPlayerControlledGameObject() as PlayerPuppet;
    StatusEffectHelper.RemoveStatusEffect(player, t"BaseStatusEffect.NetwatcherGeneral");
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"Sasquatch.Phase1");
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"BaseStatusEffect.PainInhibitors");
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"Sasquatch.Healing");
  }

  protected cb func OnDeath(evt: ref<gameDeathEvent>) -> Bool {
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"Sasquatch.Phase1");
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"BaseStatusEffect.PainInhibitors");
    StatusEffectHelper.RemoveStatusEffect(this.m_owner, t"Sasquatch.Healing");
  }
}
