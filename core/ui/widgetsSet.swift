
public static func SelectWidgets(widget: ref<inkWidget>, opt selectionRule: inkSelectionRule, opt param: String) -> ref<inkWidgetsSet> {
  let widgetsSet: ref<inkWidgetsSet> = new inkWidgetsSet();
  widgetsSet.Select(widget, selectionRule, param);
  return widgetsSet;
}
