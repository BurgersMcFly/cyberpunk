
public native class VehicleObject extends GameObject {

  private let m_vehicleComponent: wref<VehicleComponent>;

  private let m_uiComponent: wref<worlduiWidgetComponent>;

  protected let m_crowdMemberComponent: ref<CrowdMemberBaseComponent>;

  private let m_hitTimestamp: Float;

  private let m_drivingTrafficPattern: CName;

  private let m_onPavement: Bool;

  private let m_inTrafficLane: Bool;

  private let m_timesSentReactionEvent: Int32;

  private let m_timesToResendHandleReactionEvent: Int32;

  private let m_hasReactedToStimuli: Bool;

  private let m_isPrevention: Bool;

  private let m_gotStuckIncrement: Int32;

  private let m_waitForPassengersToSpawnEventDelayID: DelayID;

  private let m_triggerPanicDrivingEventDelayID: DelayID;

  private let m_reactionTriggerEvent: ref<HandleReactionEvent>;

  private let m_fearInside: Bool;

  private let m_vehicleUpsideDown: Bool;

  private let m_bumpedRecently: Int32;

  private let m_bumpTimestamp: Float;

  private let m_minUnconsciousImpact: Float;

  private let m_driverUnconscious: Bool;

  private let m_abandoned: Bool;

  public const func IsVehicle() -> Bool {
    return true;
  }

  public const func IsPrevention() -> Bool {
    return this.m_isPrevention;
  }

  public final native func GetBlackboard() -> ref<IBlackboard>;

  public final native const func GetRecord() -> wref<Vehicle_Record>;

  public final native const func IsPlayerMounted() -> Bool;

  public final native const func IsPlayerDriver() -> Bool;

  public final native func PreHijackPrepareDriverSlot() -> Void;

  public final native func CanUnmount(isPlayer: Bool, mountedObject: wref<GameObject>, opt checkSpecificDirection: vehicleExitDirection) -> vehicleUnmountPosition;

  public final native func ToggleRadioReceiver(toggle: Bool) -> Void;

  public final native func SetRadioReceiverStation(stationIndex: Uint32) -> Void;

  public final native func NextRadioReceiverStation() -> Void;

  public final native func SetRadioTier(radioTier: Uint32, overrideTier: Bool) -> Void;

  public final native func ToggleHorn(toggle: Bool, opt isPolice: Bool) -> Void;

  public final native func ToggleSiren(toggle: Bool) -> Void;

  public final native func ToggleHornForDuration(duration: Float) -> Void;

  public final native func DetachPart(partName: CName) -> Void;

  public final native func DetachAllParts() -> Void;

  public final native func SetHasExploded() -> Void;

  public final native func HasOccupantSlot(slotName: CName) -> Bool;

  public final native func GetRecordID() -> TweakDBID;

  public final native func GetAccessoryController() -> ref<vehicleController>;

  public final native func GetCameraManager() -> ref<VehicleCameraManager>;

  public final native const func IsPlayerVehicle() -> Bool;

  public final native const func IsPlayerActiveVehicle() -> Bool;

  public final native const func IsCrowdVehicle() -> Bool;

  public final native const func IsVehicleParked() -> Bool;

  public final native func IsRadioReceiverActive() -> Bool;

  public final native func GetRadioReceiverStationName() -> CName;

  public final native func GetRadioReceiverTrackName() -> CName;

  public final native func GetAnimsetOverrideForPassenger(slotName: CName) -> CName;

  public final native func GetAnimsetOverrideForPassengerFromSlotName(slotName: CName) -> CName;

  public final native func GetAnimsetOverrideForPassengerFromBoneName(boneName: CName) -> CName;

  public final native func GetBoneNameFromSlot(slotName: CName) -> CName;

  public final native func GetSlotIdForMountedObject(mountedObject: wref<GameObject>) -> CName;

  public final native func TurnVehicleOn(on: Bool) -> Void;

  public final native func TurnEngineOn(on: Bool) -> Void;

  public final native const func IsVehicleTurnedOn() -> Bool;

  public final native const func IsEngineTurnedOn() -> Bool;

  public final native func ForceBrakesFor(seconds: Float) -> Void;

  public final native func ForceBrakesUntilStoppedOrFor(secondsToTimeout: Float) -> Void;

  public final native func PhysicsWakeUp() -> Void;

  public final native func IsInTrafficPhysicsState() -> Bool;

  public final native const func IsExecutingAnyCommand() -> Bool;

  public final native const func IsPerformingPanicDriving() -> Bool;

  public final native const func IsPerformingSceneAnimation() -> Bool;

  public final native const func CanStartPanicDriving() -> Bool;

  public final native func EnableHighPriorityPanicDriving() -> Void;

  public final native func ApplyPermanentStun() -> Void;

  public final native func AreFrontWheelsCentered() -> Bool;

  public final native func GetCollisionForce() -> Vector4;

  public final native func GetLinearVelocity() -> Vector4;

  public final native func GetTotalMass() -> Float;

  public final func GetCurrentSpeed() -> Float {
    return this.GetBlackboard().GetFloat(GetAllBlackboardDefs().Vehicle.SpeedValue);
  }

  public final const func IsAbandoned() -> Bool {
    return this.m_abandoned;
  }

  public final native func SetDestructionGridPointValues(layer: Uint32, values: array<Float; 15>, accumulate: Bool) -> Void;

  public final native func DestructionResetGrid() -> Void;

  public final native func DestructionResetGlass() -> Void;

  private final native func GetUIComponents() -> array<ref<worlduiWidgetComponent>>;

  public final native func SendDelayedFinishedMountingEventToPS(isMounting: Bool, slotID: CName, character: ref<GameObject>, delay: Float) -> Void;

  public final const func IsDestroyed() -> Bool {
    return this.GetVehiclePS().GetIsDestroyed();
  }

  public final const func IsStolen() -> Bool {
    return this.GetVehiclePS().GetIsStolen();
  }

  public final const func RecordHasTag(tag: CName) -> Bool {
    let vehicleRecord: ref<Vehicle_Record>;
    if !VehicleComponent.GetVehicleRecord(this, vehicleRecord) {
      return false;
    };
    return this.RecordHasTag(vehicleRecord, tag);
  }

  public final const func RecordHasTag(vehicleRecord: ref<Vehicle_Record>, tag: CName) -> Bool {
    let vehicleTags: array<CName> = vehicleRecord.Tags();
    if ArrayContains(vehicleTags, tag) {
      return true;
    };
    return false;
  }

  protected cb func OnRequestComponents(ri: EntityRequestComponentsInterface) -> Bool {
    EntityRequestComponentsInterface.RequestComponent(ri, n"controller", n"VehicleComponent", false);
    EntityRequestComponentsInterface.RequestComponent(ri, n"CrowdMember", n"CrowdMemberBaseComponent", false);
    super.OnRequestComponents(ri);
  }

  protected cb func OnTakeControl(ri: EntityResolveComponentsInterface) -> Bool {
    this.m_vehicleComponent = EntityResolveComponentsInterface.GetComponent(ri, n"controller") as VehicleComponent;
    this.m_crowdMemberComponent = EntityResolveComponentsInterface.GetComponent(ri, n"CrowdMember") as CrowdMemberBaseComponent;
    super.OnTakeControl(ri);
  }

  protected cb func OnGameAttached() -> Bool {
    super.OnGameAttached();
    this.SetInteriorUIEnabled(false);
    this.m_isPrevention = Equals(this.GetRecord().Affiliation().Type(), gamedataAffiliation.NCPD);
  }

  protected cb func OnDetach() -> Bool {
    this.m_isPrevention = false;
    super.OnDetach();
  }

  protected cb func OnDeviceLinkRequest(evt: ref<DeviceLinkRequest>) -> Bool {
    let link: ref<VehicleDeviceLinkPS>;
    if this.IsCrowdVehicle() {
      return false;
    };
    link = VehicleDeviceLinkPS.CreateAndAcquirVehicleDeviceLinkPS(this.GetGame(), this.GetEntityID());
    if IsDefined(link) {
      GameInstance.GetPersistencySystem(this.GetGame()).QueuePSEvent(link.GetID(), link.GetClassName(), evt);
    };
  }

  protected cb func OnEventReceived(stimEvent: ref<StimuliEvent>) -> Bool {
    let delayReactionEvt: ref<DelayReactionToMissingPassengersEvent>;
    let mountInfos: array<MountingInfo> = GameInstance.GetMountingFacility(this.GetGame()).GetMountingInfoMultipleWithIds(this.GetEntityID());
    if !this.m_abandoned && ArraySize(mountInfos) == 0 && NotEquals(stimEvent.GetStimType(), gamedataStimType.Invalid) {
      delayReactionEvt = new DelayReactionToMissingPassengersEvent();
      delayReactionEvt.stimEvent = stimEvent;
      GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, delayReactionEvt, 2.00);
    };
  }

  protected cb func OnDelayReactionToMissingPassengersEvent(evt: ref<DelayReactionToMissingPassengersEvent>) -> Bool {
    let mountInfos: array<MountingInfo> = GameInstance.GetMountingFacility(this.GetGame()).GetMountingInfoMultipleWithIds(this.GetEntityID());
    if ArraySize(mountInfos) == 0 {
      if !evt.delayedAlready && !this.m_abandoned {
        evt.delayedAlready = true;
        GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, evt, 2.00);
      };
    } else {
      VehicleComponent.QueueEventToAllPassengers(this.GetGame(), this.GetEntityID(), evt.stimEvent);
    };
  }

  public const func GetDeviceLink() -> ref<VehicleDeviceLinkPS> {
    let link: ref<VehicleDeviceLinkPS> = VehicleDeviceLinkPS.AcquireVehicleDeviceLink(this.GetGame(), this.GetEntityID());
    if IsDefined(link) {
      return link;
    };
    return null;
  }

  protected func SendEventToDefaultPS(evt: ref<Event>) -> Void {
    let persistentState: ref<VehicleComponentPS> = this.GetVehiclePS();
    if persistentState == null {
      LogError("[SendEventToDefaultPS] Unable to send event, there is no presistent state on that entity " + ToString(this.GetEntityID()));
      return;
    };
    GameInstance.GetPersistencySystem(this.GetGame()).QueuePSEvent(persistentState.GetID(), persistentState.GetClassName(), evt);
  }

  protected cb func OnMountingEvent(evt: ref<MountingEvent>) -> Bool {
    let mountChild: ref<GameObject> = GameInstance.FindEntityByID(this.GetGame(), evt.request.lowLevelMountingInfo.childId) as GameObject;
    if mountChild == null {
      return false;
    };
    if mountChild.IsPlayer() {
      this.SetInteriorUIEnabled(true);
      if this.ReevaluateStealing(mountChild, evt.request.lowLevelMountingInfo.slotId.id, evt.request.mountData.mountEventOptions.occupiedByNeutral) {
        this.StealVehicle(mountChild);
      };
    };
  }

  protected cb func OnUnmountingEvent(evt: ref<UnmountingEvent>) -> Bool {
    let mountChild: ref<GameObject> = GameInstance.FindEntityByID(this.GetGame(), evt.request.lowLevelMountingInfo.childId) as GameObject;
    let isSilentUnmount: Bool = IsDefined(evt.request.mountData) && evt.request.mountData.mountEventOptions.silentUnmount;
    if IsDefined(mountChild) && mountChild.IsPlayer() {
      if !isSilentUnmount {
        this.SetInteriorUIEnabled(false);
      };
    };
  }

  protected cb func OnVehicleFinishedMounting(evt: ref<VehicleFinishedMountingEvent>) -> Bool {
    if evt.isMounting && IsDefined(evt.character) && evt.character.IsPlayer() {
      this.m_abandoned = false;
    };
  }

  private final func SetInteriorUIEnabled(enabled: Bool) -> Void {
    let component: ref<worlduiWidgetComponent>;
    let i: Int32;
    let uiComponents: array<ref<worlduiWidgetComponent>> = this.GetUIComponents();
    let total: Int32 = ArraySize(uiComponents);
    if total > 0 {
      i = 0;
      while i < total {
        component = uiComponents[i];
        if IsDefined(component) {
          component.Toggle(enabled);
        };
        i += 1;
      };
      this.GetBlackboard().SetBool(GetAllBlackboardDefs().Vehicle.IsUIActive, enabled);
      this.GetBlackboard().FireCallbacks();
    };
  }

  private final func ReevaluateStealing(character: wref<GameObject>, slotID: CName, stealingAction: Bool) -> Bool {
    let vehicleRecord: ref<Vehicle_Record>;
    if !IsDefined(character) || !character.IsPlayer() {
      return false;
    };
    if stealingAction {
      return true;
    };
    if this.IsStolen() || NotEquals(slotID, n"seat_front_left") || this.IsPlayerVehicle() {
      return false;
    };
    if !VehicleComponent.GetVehicleRecord(this, vehicleRecord) {
      return false;
    };
    if Equals(vehicleRecord.Affiliation().Type(), gamedataAffiliation.NCPD) || this.RecordHasTag(vehicleRecord, n"TriggerPrevention") {
      return true;
    };
    return false;
  }

  private final func StealVehicle(thief: wref<GameObject>) -> Void {
    StimBroadcasterComponent.BroadcastStim(thief, gamedataStimType.CrowdIllegalAction);
    StimBroadcasterComponent.BroadcastActiveStim(thief, gamedataStimType.CrimeWitness, 4.40);
    this.GetVehiclePS().SetIsStolen(true);
  }

  protected cb func OnWorkspotFinished(componentName: CName) -> Bool {
    if Equals(componentName, n"trunkBodyDisposalPlayer") {
      this.GetVehicleComponent().MountNpcBodyToTrunk();
    } else {
      if Equals(componentName, n"trunkBodyPickupPlayer") {
        this.GetVehicleComponent().FinishTrunkBodyPickup();
      };
    };
  }

  public const func GetVehiclePS() -> ref<VehicleComponentPS> {
    let ps: ref<PersistentState> = this.GetControllerPersistentState();
    return ps as VehicleComponentPS;
  }

  public const func GetPSClassName() -> CName {
    return this.GetVehiclePS().GetClassName();
  }

  protected final const func GetControllerPersistentState() -> ref<PersistentState> {
    let psID: PersistentID = this.GetVehicleComponent().GetPersistentID();
    if PersistentID.IsDefined(psID) {
      return GameInstance.GetPersistencySystem(this.GetGame()).GetConstAccessToPSObject(psID, this.GetVehicleComponent().GetPSName());
    };
    return null;
  }

  public const func GetVehicleComponent() -> ref<VehicleComponent> {
    return this.m_vehicleComponent;
  }

  public final const func GetCrowdMemberComponent() -> ref<CrowdMemberBaseComponent> {
    return this.m_crowdMemberComponent;
  }

  public const func ShouldShowScanner() -> Bool {
    if this.GetHudManager().IsBraindanceActive() && !this.m_scanningComponent.IsBraindanceClue() {
      return false;
    };
    return true;
  }

  protected cb func OnSetExposeQuickHacks(evt: ref<SetExposeQuickHacks>) -> Bool {
    this.RequestHUDRefresh();
  }

  public const func GetDefaultHighlight() -> ref<FocusForcedHighlightData> {
    let highlight: ref<FocusForcedHighlightData>;
    if this.IsDestroyed() || this.IsPlayerMounted() {
      return null;
    };
    if this.m_scanningComponent.IsBraindanceBlocked() || this.m_scanningComponent.IsPhotoModeBlocked() {
      return null;
    };
    highlight = new FocusForcedHighlightData();
    highlight.outlineType = this.GetCurrentOutline();
    if Equals(highlight.outlineType, EFocusOutlineType.INVALID) {
      return null;
    };
    highlight.sourceID = this.GetEntityID();
    highlight.sourceName = this.GetClassName();
    if Equals(highlight.outlineType, EFocusOutlineType.QUEST) {
      highlight.highlightType = EFocusForcedHighlightType.QUEST;
    } else {
      if Equals(highlight.outlineType, EFocusOutlineType.HACKABLE) {
        highlight.highlightType = EFocusForcedHighlightType.HACKABLE;
      };
    };
    if highlight != null {
      if this.IsNetrunner() {
        highlight.patternType = VisionModePatternType.Netrunner;
      } else {
        highlight.patternType = VisionModePatternType.Default;
      };
    };
    return highlight;
  }

  public const func GetCurrentOutline() -> EFocusOutlineType {
    let outlineType: EFocusOutlineType;
    if this.IsDestroyed() {
      return EFocusOutlineType.INVALID;
    };
    if this.IsQuest() {
      outlineType = EFocusOutlineType.QUEST;
    } else {
      if this.IsNetrunner() {
        outlineType = EFocusOutlineType.HACKABLE;
      } else {
        return EFocusOutlineType.INVALID;
      };
    };
    return outlineType;
  }

  public const func IsNetrunner() -> Bool {
    return this.GetVehiclePS().HasPlaystyle(EPlaystyle.NETRUNNER);
  }

  public const func CompileScannerChunks() -> Bool {
    let VehicleManufacturerChunk: ref<ScannerVehicleManufacturer>;
    let driveLayoutChunk: ref<ScannerVehicleDriveLayout>;
    let horsepowerChunk: ref<ScannerVehicleHorsepower>;
    let infoChunk: ref<ScannerVehicleInfo>;
    let massChunk: ref<ScannerVehicleMass>;
    let productionYearsChunk: ref<ScannerVehicleProdYears>;
    let record: ref<Vehicle_Record>;
    let stateChunk: ref<ScannerVehicleState>;
    let uiData: ref<VehicleUIData_Record>;
    let vehicleNameChunk: ref<ScannerVehicleName>;
    let scannerBlackboard: wref<IBlackboard> = GameInstance.GetBlackboardSystem(this.GetGame()).Get(GetAllBlackboardDefs().UI_ScannerModules);
    scannerBlackboard.SetInt(GetAllBlackboardDefs().UI_ScannerModules.ObjectType, EnumInt(ScannerObjectType.VEHICLE), true);
    record = this.GetRecord();
    uiData = record.VehicleUIData();
    vehicleNameChunk = new ScannerVehicleName();
    vehicleNameChunk.Set(LocKeyToString(record.DisplayName()));
    scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleName, ToVariant(vehicleNameChunk));
    VehicleManufacturerChunk = new ScannerVehicleManufacturer();
    VehicleManufacturerChunk.Set(record.Manufacturer().EnumName());
    scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleManufacturer, ToVariant(VehicleManufacturerChunk));
    productionYearsChunk = new ScannerVehicleProdYears();
    productionYearsChunk.Set(uiData.ProductionYear());
    scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleProductionYears, ToVariant(productionYearsChunk));
    massChunk = new ScannerVehicleMass();
    massChunk.Set(RoundMath(MeasurementUtils.ValueToImperial(uiData.Mass(), EMeasurementUnit.Kilogram)));
    scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleMass, ToVariant(massChunk));
    infoChunk = new ScannerVehicleInfo();
    infoChunk.Set(uiData.Info());
    scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleInfo, ToVariant(infoChunk));
    if this == (this as CarObject) || this == (this as BikeObject) {
      horsepowerChunk = new ScannerVehicleHorsepower();
      horsepowerChunk.Set(RoundMath(uiData.Horsepower()));
      scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleHorsepower, ToVariant(horsepowerChunk));
      stateChunk = new ScannerVehicleState();
      stateChunk.Set(this.m_vehicleComponent.GetVehicleStateForScanner());
      scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleState, ToVariant(stateChunk));
      driveLayoutChunk = new ScannerVehicleDriveLayout();
      driveLayoutChunk.Set(uiData.DriveLayout());
      scannerBlackboard.SetVariant(GetAllBlackboardDefs().UI_ScannerModules.ScannerVehicleDriveLayout, ToVariant(driveLayoutChunk));
    };
    return true;
  }

  protected cb func OnLookedAtEvent(evt: ref<LookedAtEvent>) -> Bool {
    super.OnLookedAtEvent(evt);
    VehicleComponent.QueueEventToAllPassengers(this.GetGame(), this, evt);
  }

  protected cb func OnCrowdSettingsEvent(evt: ref<CrowdSettingsEvent>) -> Bool {
    if !this.m_driverUnconscious {
      this.m_drivingTrafficPattern = evt.movementType;
      this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
    };
  }

  protected cb func OnStuckEvent(evt: ref<VehicleStuckEvent>) -> Bool {
    this.m_gotStuckIncrement += 1;
    this.m_drivingTrafficPattern = n"stop";
    this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
    GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, this.m_reactionTriggerEvent, 1.00);
  }

  protected cb func OnTrafficBumpEvent(evt: ref<VehicleTrafficBumpEvent>) -> Bool {
    let impactNormal: Float;
    let impact: Float = evt.impactVelocityChange;
    if impact > 20.00 {
      return IsDefined(null);
    };
    if this.IsExecutingAnyCommand() {
      return IsDefined(null);
    };
    if this.m_minUnconsciousImpact == 0.00 {
      this.m_minUnconsciousImpact = TweakDBInterface.GetFloat(t"AIGeneralSettings.minUnconsciousImpact", 6.50);
    };
    impactNormal = (impact - 20.00) * 0.11;
    if impact > this.m_minUnconsciousImpact && RandRangeF(0.00, 1.00) < (100.00 - this.m_minUnconsciousImpact + impactNormal * impactNormal * (20.00 - this.m_minUnconsciousImpact)) / 100.00 {
      this.TriggerUnconsciousBehaviorForPassengers();
    } else {
      this.EscalateBumpVehicleReaction();
    };
  }

  private final func EscalateBumpVehicleReaction() -> Void {
    let broadcaster: ref<StimBroadcasterComponent>;
    let driver: ref<GameObject>;
    if !GameObject.IsCooldownActive(this, n"bumpCooldown") {
      GameObject.StartCooldown(this, n"bumpCooldown", 1.00);
      driver = VehicleComponent.GetDriver(this.GetGame(), this.GetEntityID());
      if VehicleComponent.IsMountedToVehicle(this.GetGame(), driver) && IsDefined(driver as NPCPuppet) && ScriptedPuppet.IsActive(driver) {
        GameObject.PlayVoiceOver(driver, n"vehicle_bump", n"Scripts:EscalateBumpVehicleReaction", true);
      };
      if this.m_bumpTimestamp >= EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame())) {
        this.m_bumpedRecently += 1;
        if this.m_bumpedRecently > 2 {
          broadcaster = GameInstance.GetPlayerSystem(this.GetGame()).GetLocalPlayerMainGameObject().GetStimBroadcasterComponent();
          if IsDefined(broadcaster) && IsDefined(driver) {
            broadcaster.SendDrirectStimuliToTarget(this, gamedataStimType.Bump, driver);
          };
        };
      } else {
        this.m_bumpedRecently = 1;
        this.m_bumpTimestamp = EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame())) + 60.00;
      };
    };
  }

  private final func TriggerUnconsciousBehaviorForPassengers() -> Void {
    let delayBehaviorEvent: ref<WaitForPassengersToSpawnEvent>;
    let game: GameInstance;
    let i: Int32;
    let mountInfos: array<MountingInfo>;
    let passenger: ref<GameObject>;
    if !this.m_driverUnconscious {
      this.m_drivingTrafficPattern = n"stop";
      this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
      this.m_driverUnconscious = true;
      this.ApplyPermanentStun();
    };
    game = this.GetGame();
    mountInfos = GameInstance.GetMountingFacility(game).GetMountingInfoMultipleWithIds(this.GetEntityID());
    if ArraySize(mountInfos) == 0 {
      GameInstance.GetDelaySystem(game).CancelDelay(this.m_waitForPassengersToSpawnEventDelayID);
      delayBehaviorEvent = new WaitForPassengersToSpawnEvent();
      this.m_waitForPassengersToSpawnEventDelayID = GameInstance.GetDelaySystem(game).DelayEvent(this, delayBehaviorEvent, 1.50);
    };
    i = 0;
    while i < ArraySize(mountInfos) {
      if Equals(mountInfos[i].slotId.id, n"trunk_body") {
      } else {
        passenger = GameInstance.FindEntityByID(game, mountInfos[i].childId) as GameObject;
        if IsDefined(passenger) {
          StatusEffectHelper.ApplyStatusEffect(passenger, t"BaseStatusEffect.Defeated");
          if VehicleComponent.IsDriver(this.GetGame(), passenger) {
            this.ToggleHornForDuration(7.50);
          };
        };
      };
      i += 1;
    };
  }

  protected cb func OnUnableToStartPanicDriving(evt: ref<VehicleUnableToStartPanicDriving>) -> Bool {
    this.TriggerFearInsideVehicleBehavior();
    this.ResendHandleReactionEvent();
  }

  protected cb func OnWaitForPassengersToSpawnEvent(evt: ref<WaitForPassengersToSpawnEvent>) -> Bool {
    this.TriggerUnconsciousBehaviorForPassengers();
  }

  protected cb func OnHandleReactionEvent(evt: ref<HandleReactionEvent>) -> Bool {
    let randomDraw: Float;
    if this.IsPerformingPanicDriving() || this.IsExecutingAnyCommand() {
      return IsDefined(null);
    };
    if EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame())) <= this.m_hitTimestamp + 2.00 && evt.stimEvent.sourceObject.IsPlayer() {
      this.EnableHighPriorityPanicDriving();
    };
    if !GameObject.IsCooldownActive(this, n"vehicleReactionCooldown") && !this.m_driverUnconscious {
      this.m_reactionTriggerEvent = evt;
      GameObject.StartCooldown(this, n"vehicleReactionCooldown", 1.00);
      randomDraw = RandRangeF(0.00, 1.00);
      if this.m_gotStuckIncrement < 2 && !this.m_abandoned && this.CanStartPanicDriving() {
        this.TriggerDrivingPanicBehavior(evt.stimEvent.sourcePosition);
        this.m_fearInside = false;
      } else {
        if (randomDraw <= 0.30 || this.m_gotStuckIncrement > 2) && evt.stimEvent.sourceObject.IsPlayer() && this.CanNPCsLeaveVehicle() {
          this.TriggerFearFleeBehavior();
          this.m_fearInside = false;
        } else {
          if !this.m_fearInside {
            this.TriggerFearInsideVehicleBehavior();
            this.m_fearInside = true;
          };
          this.ResendHandleReactionEvent();
        };
      };
    };
  }

  protected cb func OnTriggerPanicDrivingEvent(evt: ref<TriggerPanicDrivingEvent>) -> Bool {
    this.PanicDrivingBehavior();
  }

  private final func PanicDrivingBehavior() -> Void {
    if !this.m_abandoned && !this.IsPlayerMounted() {
      if Equals(this.m_drivingTrafficPattern, n"stop") {
        this.ResetReactionSequenceOfAllPassengers();
      };
      GameObject.PlayVoiceOver(VehicleComponent.GetDriver(this.GetGame(), this.GetEntityID()), n"fear_run", n"Scripts:PanicDrivingBehavior", true);
      this.m_drivingTrafficPattern = n"panic";
      this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
      this.ResetTimesSentReactionEvent();
    };
  }

  private final func TriggerDrivingPanicBehavior(threatPosition: Vector4) -> Void {
    let panicDrivingEvent: ref<TriggerPanicDrivingEvent>;
    GameInstance.GetDelaySystem(this.GetGame()).CancelDelay(this.m_triggerPanicDrivingEventDelayID);
    panicDrivingEvent = new TriggerPanicDrivingEvent();
    if EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame())) <= this.m_hitTimestamp + 2.00 {
      this.QueueEvent(panicDrivingEvent);
    } else {
      if Vector4.DistanceSquared(this.GetWorldPosition(), threatPosition) < 225.00 {
        this.m_triggerPanicDrivingEventDelayID = GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, panicDrivingEvent, RandRangeF(0.40, 0.70));
      } else {
        this.m_triggerPanicDrivingEventDelayID = GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, panicDrivingEvent, RandRangeF(0.80, 1.50));
      };
    };
  }

  private final func TriggerFearInsideVehicleBehavior() -> Void {
    let npcReactionEvent: ref<DelayedCrowdReactionEvent>;
    this.m_drivingTrafficPattern = n"stop";
    this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
    npcReactionEvent = new DelayedCrowdReactionEvent();
    npcReactionEvent.stimEvent = this.m_reactionTriggerEvent.stimEvent;
    npcReactionEvent.vehicleFearPhase = 2;
    VehicleComponent.QueueEventToAllPassengers(this.GetGame(), this.GetEntityID(), npcReactionEvent, true);
  }

  private final func TriggerFearFleeBehavior() -> Void {
    let exitEvent: ref<AIEvent>;
    let npcReactionEvent: ref<DelayedCrowdReactionEvent>;
    let passengersCanLeaveCar: array<wref<GameObject>>;
    let passengersCantLeaveCar: array<wref<GameObject>>;
    VehicleComponent.CheckIfPassengersCanLeaveCar(this.GetGame(), this.GetEntityID(), passengersCanLeaveCar, passengersCantLeaveCar);
    this.m_drivingTrafficPattern = n"stop";
    this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
    npcReactionEvent = new DelayedCrowdReactionEvent();
    npcReactionEvent.stimEvent = this.m_reactionTriggerEvent.stimEvent;
    if ArraySize(passengersCanLeaveCar) > 0 {
      exitEvent = new AIEvent();
      exitEvent.name = n"ExitVehicleInPanic";
      VehicleComponent.QueueEventToPassengers(this.GetGame(), this.GetEntityID(), exitEvent, passengersCanLeaveCar, true);
      npcReactionEvent.vehicleFearPhase = 3;
      VehicleComponent.QueueEventToPassengers(this.GetGame(), this.GetEntityID(), npcReactionEvent, passengersCanLeaveCar, true);
      this.ResetTimesSentReactionEvent();
    };
    if ArraySize(passengersCantLeaveCar) > 0 {
      npcReactionEvent.vehicleFearPhase = 2;
      VehicleComponent.QueueEventToPassengers(this.GetGame(), this.GetEntityID(), npcReactionEvent, passengersCantLeaveCar, true);
      this.ResendHandleReactionEvent();
    };
    this.m_abandoned = true;
  }

  private final func ResendHandleReactionEvent() -> Void {
    let delayTime: Float;
    if !this.IsTargetClose(this.m_reactionTriggerEvent.stimEvent.sourceObject.GetWorldPosition(), 20.00) {
      if this.m_timesToResendHandleReactionEvent == 0 {
        this.m_timesToResendHandleReactionEvent = TweakDBInterface.GetInt(t"AIGeneralSettings.timesToResendHandleReactionEvent", 3);
      };
      if this.m_timesSentReactionEvent >= this.m_timesToResendHandleReactionEvent {
        return;
      };
      this.m_timesSentReactionEvent += 1;
    } else {
      this.m_timesSentReactionEvent = 0;
    };
    delayTime = RandRangeF(2.00, 3.00);
    GameInstance.GetDelaySystem(this.GetGame()).DelayEvent(this, this.m_reactionTriggerEvent, delayTime);
  }

  private final func ResetTimesSentReactionEvent() -> Void {
    this.m_timesSentReactionEvent = 0;
  }

  private final func ResetReactionSequenceOfAllPassengers() -> Void {
    let mountingInfos: array<MountingInfo> = GameInstance.GetMountingFacility(this.GetGame()).GetMountingInfoMultipleWithObjects(this);
    let count: Int32 = ArraySize(mountingInfos);
    let workspotSystem: ref<WorkspotGameSystem> = GameInstance.GetWorkspotSystem(this.GetGame());
    let i: Int32 = 0;
    while i < count {
      workspotSystem.HardResetPlaybackToStart(GameInstance.FindEntityByID(this.GetGame(), mountingInfos[i].childId) as GameObject);
      i += 1;
    };
  }

  private final func CanNPCsLeaveVehicle() -> Bool {
    let angleToTarget: Float;
    let direction: Vector4;
    if this.IsTargetClose(this.m_reactionTriggerEvent.stimEvent.sourceObject.GetWorldPosition(), 25.00) {
      direction = this.m_reactionTriggerEvent.stimEvent.sourceObject.GetWorldPosition() - this.GetWorldPosition();
      angleToTarget = Vector4.GetAngleDegAroundAxis(direction, this.GetWorldForward(), this.GetWorldUp());
      if AbsF(angleToTarget) < 85.00 {
        return true;
      };
      return false;
    };
    return true;
  }

  private final func IsTargetClose(targetPosition: Vector4, distance: Float) -> Bool {
    let distanceSquared: Float = Vector4.DistanceSquared(targetPosition, this.GetWorldPosition());
    if distanceSquared < distance * distance {
      return true;
    };
    return false;
  }

  protected cb func OnHit(evt: ref<gameHitEvent>) -> Bool {
    super.OnHit(evt);
  }

  protected func DamagePipelineFinalized(evt: ref<gameHitEvent>) -> Void {
    this.DamagePipelineFinalized(evt);
    if !GameObject.IsCooldownActive(this, n"vehicleHitCooldown") {
      GameObject.StartCooldown(this, n"vehicleHitCooldown", 1.00);
      this.m_hitTimestamp = EngineTime.ToFloat(GameInstance.GetSimTime(this.GetGame()));
    };
  }

  public final func IsOnPavement() -> Bool {
    return this.m_onPavement;
  }

  protected cb func OnPavement(evt: ref<OnPavement>) -> Bool {
    this.m_onPavement = true;
  }

  protected cb func OnOffPavement(evt: ref<OffPavement>) -> Bool {
    this.m_onPavement = false;
  }

  protected cb func OnInCrowd(evt: ref<InCrowd>) -> Bool {
    let hls: gamedataNPCHighLevelState;
    let vehicleDriver: wref<GameObject>;
    this.m_inTrafficLane = true;
    if !this.m_driverUnconscious {
      vehicleDriver = VehicleComponent.GetDriver(this.GetGame(), this.GetEntityID());
      if IsDefined(vehicleDriver) {
        hls = (vehicleDriver as ScriptedPuppet).GetHighLevelStateFromBlackboard();
        if NotEquals(hls, gamedataNPCHighLevelState.Fear) && Equals(this.m_drivingTrafficPattern, n"stop") {
          this.m_drivingTrafficPattern = n"normal";
          this.m_crowdMemberComponent.ChangeMoveType(this.m_drivingTrafficPattern);
        };
      };
      this.m_fearInside = false;
    };
  }

  protected cb func OnOutOfCrowd(evt: ref<OutOfCrowd>) -> Bool {
    this.m_inTrafficLane = false;
  }

  public final func IsInTrafficLane() -> Bool {
    return this.m_inTrafficLane;
  }

  public final func IsVehicleUpsideDown() -> Bool {
    return this.m_vehicleUpsideDown;
  }

  protected cb func OnVehicleFlippedOverEvent(evt: ref<VehicleFlippedOverEvent>) -> Bool {
    this.m_vehicleUpsideDown = evt.isFlippedOver;
  }

  protected cb func OnStealVehicleEvent(evt: ref<StealVehicleEvent>) -> Bool {
    this.m_abandoned = true;
  }

  public const func IsQuest() -> Bool {
    return this.GetVehiclePS().IsMarkedAsQuest();
  }

  protected func MarkAsQuest(isQuest: Bool) -> Void {
    this.GetVehiclePS().SetIsMarkedAsQuest(isQuest);
  }
}

public static func GetMountedVehicle(object: ref<GameObject>) -> wref<VehicleObject> {
  let game: GameInstance = object.GetGame();
  let mountingFacility: ref<IMountingFacility> = GameInstance.GetMountingFacility(game);
  let mountingInfo: MountingInfo = mountingFacility.GetMountingInfoSingleWithObjects(object);
  let vehicle: wref<VehicleObject> = GameInstance.FindEntityByID(game, mountingInfo.parentId) as VehicleObject;
  return vehicle;
}
