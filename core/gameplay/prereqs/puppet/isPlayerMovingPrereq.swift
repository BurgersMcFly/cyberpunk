
public class IsPlayerMovingPrereqState extends PlayerStateMachinePrereqState {

  public let m_bbValue: Bool;

  public let m_listenerVertical: ref<CallbackHandle>;

  protected cb func OnStateUpdateBool(value: Bool) -> Bool {
    let checkPassed: Bool;
    let prereq: ref<IsPlayerMovingPrereq> = this.GetPrereq() as IsPlayerMovingPrereq;
    if NotEquals(this.m_bbValue, value) {
      checkPassed = prereq.Evaluate(this.m_owner, value);
      this.OnChanged(checkPassed);
    };
    this.m_bbValue = value;
  }
}

public class IsPlayerMovingPrereq extends PlayerStateMachinePrereq {

  protected const func OnRegister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Bool {
    let bb: ref<IBlackboard> = GameInstance.GetBlackboardSystem(game).GetLocalInstanced((context as ScriptedPuppet).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    let castedState: ref<IsPlayerMovingPrereqState> = state as IsPlayerMovingPrereqState;
    castedState.m_owner = context as GameObject;
    castedState.m_listenerInt = bb.RegisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingHorizontally, castedState, n"OnStateUpdateBool");
    castedState.m_listenerVertical = bb.RegisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingVertically, castedState, n"OnStateUpdateBool");
    return false;
  }

  protected const func OnUnregister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let castedState: ref<IsPlayerMovingPrereqState> = state as IsPlayerMovingPrereqState;
    let bb: ref<IBlackboard> = GameInstance.GetBlackboardSystem(game).GetLocalInstanced((context as ScriptedPuppet).GetEntityID(), GetAllBlackboardDefs().PlayerStateMachine);
    bb.UnregisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingHorizontally, castedState.m_listenerInt);
    bb.UnregisterListenerBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingVertically, castedState.m_listenerVertical);
  }

  protected const func GetStateMachineEnum() -> String {
    return "";
  }

  protected const func GetCurrentPSMStateIndex(bb: ref<IBlackboard>) -> Int32 {
    if bb.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingHorizontally) || bb.GetBool(GetAllBlackboardDefs().PlayerStateMachine.IsMovingVertically) {
      return 1;
    };
    return 0;
  }

  protected func Initialize(recordID: TweakDBID) -> Void {
    this.Initialize(recordID);
    this.m_valueToListen = TweakDBInterface.GetBool(recordID + t".isMoving", true) ? 1 : 0;
  }

  public final const func Evaluate(owner: ref<GameObject>, value: Bool) -> Bool {
    if this.m_valueToListen == 0 {
      return Equals(value, false);
    };
    if this.m_valueToListen == 1 {
      return Equals(value, true);
    };
    LogError("m_valueToListen has invalid value of " + IntToString(this.m_valueToListen));
    return false;
  }
}
