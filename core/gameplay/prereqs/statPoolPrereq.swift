
public class StatPoolPrereqInventoryScriptCallback extends InventoryScriptCallback {

  protected let m_itemTDBID: TweakDBID;

  protected let m_state: wref<StatPoolPrereqState>;

  public final func Init(itemTDBID: TweakDBID, state: ref<StatPoolPrereqState>) -> Void {
    this.m_itemTDBID = itemTDBID;
    this.m_state = state;
  }

  public func OnItemAdded(item: ItemID, itemData: wref<gameItemData>, flaggedAsSilent: Bool) -> Void {
    let itemTDBID: TweakDBID = ItemID.GetTDBID(item);
    if itemTDBID == this.m_itemTDBID {
      this.m_state.UpdateItemID(Cast<StatsObjectID>(item));
    };
  }
}

public class BaseStatPoolPrereqListener extends CustomValueStatPoolsListener {

  public func RegisterState(state: ref<PrereqState>) -> Void;
}

public class StatPoolPrereqListener extends BaseStatPoolPrereqListener {

  protected let m_state: wref<StatPoolPrereqState>;

  protected cb func OnStatPoolValueReached(oldValue: Float, newValue: Float, percToPoints: Float) -> Bool {
    this.m_state.StatPoolUpdate(oldValue, newValue);
  }

  public func RegisterState(state: ref<PrereqState>) -> Void {
    this.m_state = state as StatPoolPrereqState;
  }

  public func OnStatPoolValueChanged(oldValue: Float, newValue: Float, percToPoints: Float) -> Void {
    if this.m_state.m_statpoolWasMissing {
      this.m_state.m_statpoolWasMissing = false;
      this.m_state.StatPoolUpdate(oldValue, newValue);
    };
  }
}

public class StatPoolPrereqState extends PrereqState {

  public let m_statPoolListener: ref<BaseStatPoolPrereqListener>;

  public let m_inventoryCallback: ref<StatPoolPrereqInventoryScriptCallback>;

  public let m_inventoryListener: ref<InventoryScriptListener>;

  public let m_statpoolWasMissing: Bool;

  public let m_statsObjID: StatsObjectID;

  public func StatPoolUpdate(oldValue: Float, newValue: Float) -> Void {
    let prereq: ref<StatPoolPrereq> = this.GetPrereq() as StatPoolPrereq;
    let checkPassed: Bool = ProcessCompare(prereq.m_comparisonType, newValue, prereq.m_valueToCheck);
    this.OnChanged(checkPassed);
  }

  public func UpdateItemID(statsObjID: StatsObjectID) -> Void {
    let checkPassed: Bool;
    let currentValue: Float;
    let owner: ref<GameObject> = this.GetContext() as GameObject;
    let prereq: ref<StatPoolPrereq> = this.GetPrereq() as StatPoolPrereq;
    if !StatsObjectID.IsDefined(this.m_statsObjID) {
      this.m_statsObjID = statsObjID;
      if GameInstance.GetStatPoolsSystem(owner.GetGame()).IsStatPoolAdded(this.m_statsObjID, prereq.m_statPoolType) {
        currentValue = GameInstance.GetStatPoolsSystem(owner.GetGame()).GetStatPoolValue(this.m_statsObjID, prereq.m_statPoolType, prereq.m_comparePercentage);
        checkPassed = ProcessCompare(prereq.m_comparisonType, currentValue, prereq.m_valueToCheck);
        this.OnChanged(checkPassed);
      };
      this.RegisterStatPoolListener(owner.GetGame(), prereq.m_statPoolType, prereq.m_valueToCheck);
    };
  }

  public func RegisterInventoryListener(game: GameInstance, owner: ref<GameObject>, itemTDBID: TweakDBID) -> Void {
    this.m_inventoryCallback = new StatPoolPrereqInventoryScriptCallback();
    this.m_inventoryCallback.Init(itemTDBID, this);
    this.m_inventoryListener = GameInstance.GetTransactionSystem(game).RegisterInventoryListener(owner, this.m_inventoryCallback);
  }

  public func UnregisterInventoryListener(game: GameInstance, owner: ref<GameObject>) -> Void {
    if IsDefined(this.m_inventoryListener) {
      GameInstance.GetTransactionSystem(game).UnregisterInventoryListener(owner, this.m_inventoryListener);
    };
    this.m_inventoryCallback = null;
    this.m_statPoolListener = null;
  }

  public func RegisterStatPoolListener(game: GameInstance, statPoolType: gamedataStatPoolType, valueToCheck: Float) -> Void {
    if StatsObjectID.IsDefined(this.m_statsObjID) {
      this.m_statPoolListener = new StatPoolPrereqListener();
      this.m_statPoolListener.RegisterState(this);
      this.m_statPoolListener.SetValue(valueToCheck);
      GameInstance.GetStatPoolsSystem(game).RequestRegisteringListener(this.m_statsObjID, statPoolType, this.m_statPoolListener);
    };
  }

  public func UnregisterStatPoolListener(game: GameInstance, statPoolType: gamedataStatPoolType) -> Void {
    if IsDefined(this.m_statPoolListener) {
      GameInstance.GetStatPoolsSystem(game).RequestUnregisteringListener(this.m_statsObjID, statPoolType, this.m_statPoolListener);
    };
    this.m_statPoolListener = null;
  }
}

public class StatPoolPrereq extends IScriptablePrereq {

  public let m_statPoolType: gamedataStatPoolType;

  public let m_valueToCheck: Float;

  public let m_comparisonType: EComparisonType;

  public let m_skipOnApply: Bool;

  public let m_comparePercentage: Bool;

  public let m_itemTDBID: TweakDBID;

  public let m_checkItem: Bool;

  protected final const func GetStatsObjectID(owner: ref<GameObject>) -> StatsObjectID {
    let itemData: wref<gameItemData>;
    if this.m_checkItem {
      if TDBID.IsValid(this.m_itemTDBID) {
        itemData = GameInstance.GetTransactionSystem(owner.GetGame()).GetItemDataByTDBID(owner, this.m_itemTDBID);
        return Cast<StatsObjectID>(itemData.GetID());
      };
      return new StatsObjectID();
    };
    return Cast<StatsObjectID>(owner.GetEntityID());
  }

  protected const func OnRegister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Bool {
    let owner: ref<GameObject> = context as GameObject;
    let castedState: ref<StatPoolPrereqState> = state as StatPoolPrereqState;
    castedState.m_statsObjID = this.GetStatsObjectID(owner);
    if this.m_checkItem && !StatsObjectID.IsDefined(castedState.m_statsObjID) {
      castedState.RegisterInventoryListener(game, owner, this.m_itemTDBID);
    } else {
      castedState.RegisterStatPoolListener(game, this.m_statPoolType, this.m_valueToCheck);
    };
    return false;
  }

  protected const func OnUnregister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let castedState: ref<StatPoolPrereqState> = state as StatPoolPrereqState;
    castedState.UnregisterStatPoolListener(game, this.m_statPoolType);
    castedState.UnregisterInventoryListener(game, context as GameObject);
  }

  protected func Initialize(recordID: TweakDBID) -> Void {
    let record: ref<StatPoolPrereq_Record> = TweakDBInterface.GetStatPoolPrereqRecord(recordID);
    this.m_statPoolType = IntEnum<gamedataStatPoolType>(Cast<Int32>(EnumValueFromName(n"gamedataStatPoolType", record.StatPoolType())));
    this.m_valueToCheck = record.ValueToCheck();
    this.m_comparisonType = IntEnum<EComparisonType>(Cast<Int32>(EnumValueFromName(n"EComparisonType", record.ComparisonType())));
    this.m_skipOnApply = TweakDBInterface.GetBool(recordID + t".skipOnApply", false);
    this.m_comparePercentage = TweakDBInterface.GetBool(recordID + t".comparePercentage", true);
    let itemObjectToCheckRec: wref<ItemObjectToCheck_Record> = record.ObjectToCheck() as ItemObjectToCheck_Record;
    if IsDefined(itemObjectToCheckRec) {
      this.m_checkItem = true;
      this.m_itemTDBID = itemObjectToCheckRec.Item().GetID();
    };
  }

  public const func IsFulfilled(game: GameInstance, context: ref<IScriptable>) -> Bool {
    return this.CompareValues(this.GetStatsObjectID(context as GameObject), context);
  }

  protected const func OnApplied(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let castedState: ref<StatPoolPrereqState>;
    let result: Bool;
    if this.m_skipOnApply {
      return;
    };
    castedState = state as StatPoolPrereqState;
    if GameInstance.GetStatPoolsSystem(game).IsStatPoolAdded(castedState.m_statsObjID, this.m_statPoolType) {
      result = this.CompareValues(castedState.m_statsObjID, context);
      castedState.OnChanged(result);
    } else {
      castedState.m_statpoolWasMissing = true;
    };
  }

  private final const func CompareValues(statsObjID: StatsObjectID, context: ref<IScriptable>) -> Bool {
    let currentValue: Float;
    let owner: wref<GameObject> = context as GameObject;
    if IsDefined(owner) {
      currentValue = GameInstance.GetStatPoolsSystem(owner.GetGame()).GetStatPoolValue(statsObjID, this.m_statPoolType, this.m_comparePercentage);
      return ProcessCompare(this.m_comparisonType, currentValue, this.m_valueToCheck);
    };
    return false;
  }
}
