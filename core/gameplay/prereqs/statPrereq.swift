
public class StatPrereqListener extends ScriptStatsListener {

  protected let m_state: wref<StatPrereqState>;

  public func OnStatChanged(ownerID: StatsObjectID, statType: gamedataStatType, diff: Float, total: Float) -> Void {
    this.m_state.StatUpdate(diff, total);
  }

  public final func RegisterState(state: ref<PrereqState>) -> Void {
    this.m_state = state as StatPrereqState;
  }
}

public class StatPrereqState extends PrereqState {

  public let m_listener: ref<StatPrereqListener>;

  public let m_modifiersValueToCheck: Float;

  public func StatUpdate(diff: Float, total: Float) -> Void {
    let checkPassed: Bool;
    let prereq: ref<StatPrereq> = this.GetPrereq() as StatPrereq;
    if prereq.m_statModifiersUsed {
      checkPassed = ProcessCompare(prereq.m_comparisonType, total, this.m_modifiersValueToCheck);
    } else {
      checkPassed = ProcessCompare(prereq.m_comparisonType, total, prereq.m_valueToCheck);
    };
    if prereq.m_fireAndForget {
      this.OnChangedRepeated(false);
    } else {
      this.OnChanged(checkPassed);
    };
  }

  public final func UpdateModifiersValueToCheck(value: Float) -> Void {
    this.m_modifiersValueToCheck = value;
  }
}

public class StatPrereq extends IScriptablePrereq {

  public let m_fireAndForget: Bool;

  public edit let m_statType: gamedataStatType;

  public edit let m_valueToCheck: Float;

  public edit let m_comparisonType: EComparisonType;

  public let m_statModifiersUsed: Bool;

  private let m_statPrereqRecordID: TweakDBID;

  protected const func OnRegister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Bool {
    let owner: ref<GameObject> = context as GameObject;
    let castedState: ref<StatPrereqState> = state as StatPrereqState;
    castedState.m_listener = new StatPrereqListener();
    castedState.m_listener.RegisterState(castedState);
    castedState.m_listener.SetStatType(this.m_statType);
    GameInstance.GetStatsSystem(game).RegisterListener(Cast<StatsObjectID>(owner.GetEntityID()), castedState.m_listener);
    return false;
  }

  protected const func OnUnregister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let owner: ref<GameObject> = context as GameObject;
    let castedState: ref<StatPrereqState> = state as StatPrereqState;
    GameInstance.GetStatsSystem(game).UnregisterListener(Cast<StatsObjectID>(owner.GetEntityID()), castedState.m_listener);
    castedState.m_listener = null;
  }

  protected func Initialize(recordID: TweakDBID) -> Void {
    let record: ref<StatPrereq_Record> = TweakDBInterface.GetStatPrereqRecord(recordID);
    this.m_statType = IntEnum<gamedataStatType>(Cast<Int32>(EnumValueFromName(n"gamedataStatType", record.StatType())));
    this.m_valueToCheck = record.ValueToCheck();
    this.m_comparisonType = IntEnum<EComparisonType>(Cast<Int32>(EnumValueFromName(n"EComparisonType", record.ComparisonType())));
    this.m_fireAndForget = TweakDBInterface.GetBool(recordID + t".fireAndForget", false);
    this.m_statPrereqRecordID = recordID;
    this.m_statModifiersUsed = record.GetStatModifiersCount() > 0;
  }

  public const func IsFulfilled(game: GameInstance, context: ref<IScriptable>) -> Bool {
    let modifiersValueToCheck: Float;
    let owner: ref<GameObject> = context as GameObject;
    let currentValue: Float = GameInstance.GetStatsSystem(game).GetStatValue(Cast<StatsObjectID>(owner.GetEntityID()), this.m_statType);
    if this.m_statModifiersUsed {
      modifiersValueToCheck = GameInstance.GetStatsSystem(game).GetStatPrereqModifiersValue(Cast<StatsObjectID>(owner.GetEntityID()), this.m_statPrereqRecordID);
      return ProcessCompare(this.m_comparisonType, currentValue, modifiersValueToCheck);
    };
    return ProcessCompare(this.m_comparisonType, currentValue, this.m_valueToCheck);
  }

  public final const func IsFulfilled(game: GameInstance, context: ref<IScriptable>, itemStatsID: StatsObjectID) -> Bool {
    let modifiersValueToCheck: Float;
    let owner: ref<GameObject> = context as GameObject;
    let currentValue: Float = GameInstance.GetStatsSystem(game).GetStatValue(Cast<StatsObjectID>(owner.GetEntityID()), this.m_statType);
    if this.m_statModifiersUsed {
      modifiersValueToCheck = GameInstance.GetStatsSystem(game).GetStatPrereqModifiersValue(itemStatsID, this.m_statPrereqRecordID);
      return ProcessCompare(this.m_comparisonType, currentValue, modifiersValueToCheck);
    };
    return ProcessCompare(this.m_comparisonType, currentValue, this.m_valueToCheck);
  }

  protected const func OnApplied(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let modifiersValueToCheck: Float;
    let statValue: Float;
    let owner: wref<GameObject> = context as GameObject;
    let castedState: ref<StatPrereqState> = state as StatPrereqState;
    if this.m_statModifiersUsed {
      modifiersValueToCheck = GameInstance.GetStatsSystem(game).GetStatPrereqModifiersValue(Cast<StatsObjectID>(owner.GetEntityID()), this.m_statPrereqRecordID);
      castedState.UpdateModifiersValueToCheck(modifiersValueToCheck);
    };
    statValue = GameInstance.GetStatsSystem(owner.GetGame()).GetStatValue(Cast<StatsObjectID>((context as Entity).GetEntityID()), this.m_statType);
    castedState.StatUpdate(0.00, statValue);
  }
}
