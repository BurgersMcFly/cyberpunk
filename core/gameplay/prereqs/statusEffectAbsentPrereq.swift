
public class StatusEffectAbsentPrereqState extends StatusEffectPrereqState {

  public func StatusEffectUpdate(statusEffect: wref<StatusEffect_Record>, isApplied: Bool) -> Void {
    this.OnChanged(!isApplied);
  }
}

public class StatusEffectAbsentPrereq extends StatusEffectPrereq {

  protected const func OnRegister(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Bool {
    this.OnRegister(state, game, context);
    return false;
  }

  protected func Initialize(recordID: TweakDBID) -> Void {
    let record: ref<StatusEffectPrereq_Record> = TweakDBInterface.GetStatusEffectPrereqRecord(recordID);
    this.m_statusEffectRecordID = record.StatusEffect().GetID();
    this.m_checkType = record.CheckType().Type();
    this.m_invert = record.Invert();
    this.m_tag = TweakDBInterface.GetCName(recordID + t".tagToCheck", n"None");
    this.m_fireAndForget = TweakDBInterface.GetBool(recordID + t".fireAndForget", false);
  }

  public const func IsFulfilled(game: GameInstance, context: ref<IScriptable>) -> Bool {
    let result: Bool;
    let statusEffectSystem: ref<StatusEffectSystem> = GameInstance.GetStatusEffectSystem(game);
    let owner: ref<GameObject> = context as GameObject;
    switch this.m_checkType {
      case gamedataCheckType.Tag:
        result = statusEffectSystem.HasStatusEffectWithTag(owner.GetEntityID(), this.m_tag);
        break;
      case gamedataCheckType.Type:
        result = StatusEffectSystem.ObjectHasStatusEffectOfType(owner, TweakDBInterface.GetStatusEffectRecord(this.m_statusEffectRecordID).StatusEffectType().Type());
        break;
      case gamedataCheckType.Record:
        result = statusEffectSystem.HasStatusEffect(owner.GetEntityID(), this.m_statusEffectRecordID);
        break;
      default:
        result = statusEffectSystem.HasStatusEffect(owner.GetEntityID(), this.m_statusEffectRecordID);
    };
    if this.m_invert {
      return result;
    };
    return !result;
  }

  protected const func OnApplied(state: ref<PrereqState>, game: GameInstance, context: ref<IScriptable>) -> Void {
    let result: Bool = this.IsFulfilled(game, context);
    state.OnChanged(result);
  }
}
